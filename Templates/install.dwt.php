<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<!-- TemplateBeginEditable name="doctitle" -->
<title>KAN Content Management System</title>
<!-- TemplateEndEditable -->
<style type="text/css">
<!--
body {
	background-color: #E5E5E5;
	margin-left: 0px;
	margin-top: 0px;
	margin-right: 0px;
	margin-bottom: 0px;
}
-->
</style>
<link rel="stylesheet" type="text/css" href="../install/css/install-ui.css"/>
<script language="javascript" src="../cms/scripts/form_control.js"></script>
<style>
<!--
.cms_style { background-color: #600; }
-->
</style>

<!-- TemplateBeginEditable name="head" --><!-- TemplateEndEditable -->
</head>

<body>

<div id="page">
	<div id="header">
    	<?php include('../install/header.php'); ?>
    </div>
    
    <div id="content">
    	<div id="nav">
        	<?php include('../install/setup_steps.php'); ?>
        </div>
        
        <div id="main">
        	<!-- TemplateBeginEditable name="content" -->Content Goes Here<!-- TemplateEndEditable -->
        </div>
    </div>
    
    <div id="footer">
    	<?php include('../install/footer.php'); ?>
    </div>
</div>


</body>
</html>