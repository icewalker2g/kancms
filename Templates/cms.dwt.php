<?php include("../cms/accesscheck.php"); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<!-- TemplateBeginEditable name="doctitle" -->
<title><?php echo getSetting('CMSTitle','KAN Content Management System'); ?></title>
<!-- TemplateEndEditable -->
<link rel="shortcut icon" type="image/png" href="../favicon.png" />
<link rel="stylesheet" type="text/css" href="../cms/css/widestyles.css"/>
<link rel="stylesheet" type="text/css" href="../cms/css/cms.css"/>
<link rel="stylesheet" type="text/css" href="../cms/css/cms-ui.css"/>
<link rel="stylesheet" type="text/css" href="../cms/css/util.css"/>

<script type="text/javascript" src="../scripts/common/pageManager.js"></script>
<script type="text/javascript" src="../cms/scripts/system.js"></script>
<script type="text/javascript" src="../cms/scripts/ui.js"></script>

<!-- TemplateBeginEditable name="head" --><!-- TemplateEndEditable -->
</head>

<body>

<div id="page">
	<div id="header">
    	<?php include('../cms/logo_header.php'); ?>
    </div>
    
    <?php if( isset($_SESSION['CMS_Username']) ) { ?>
    <div id="site-info-bar">
		<?php include('../cms/system_menu.php'); ?>
    </div>
    <?php } ?>
    
    
    <div id="content-wrapper">
    	<div id="content-header">
        	<span>
			<!-- TemplateBeginEditable name="section title" -->CONTENT MANAGEMENT SECTION<!-- TemplateEndEditable -->
            </span>
        </div>
    	<div id="content">
			<!-- TemplateBeginEditable name="content" -->
            <div id="nav">
                <?php include('../cms/nav_section.php'); ?>
            </div>
            
            <div id="main">
                &nbsp;
            </div>
            <!-- TemplateEndEditable -->
        </div>
    </div>
    
    <div id="footer">
    	<?php include('../cms/footer.php'); ?>
    </div>
</div>

</body>
</html>