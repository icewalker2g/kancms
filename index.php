<?php

include_once 'core/kan.inc';
$installFolder = "install/";

// if the database configuration file does not exist, and the installation folder
// is present, then we need to run the install process in order to setup KAN

if( !isset($config) && file_exists($installFolder) ) {
    header("Location: " . $installFolder);

// else if the database configuration exists, then load the default KAN site
} else if( isset($config) ) {
	
	include_once("core/SiteLoader.php");
	
	// redirect to the home of the default kan site.
	// since we are not in a KAN folder, the site home page returned should
	// just be the path from the root of the site
    header("Location: " . $site->getHomePageURL());

// else if the database configuration file is missing and the installation folder is
// also not present, inform the user that there is something wrong
} else if( !isset($config) && !file_exists($installFolder) ) {

    echo "<h3>KAN Configuration Problem</h3>" .
         "The following problems exist with your current KAN Configuration" .
         "<ul>" .
         "<li>The KAN Database Configuration File is missing</li>" .
         "<li>The KAN Installation folder is also missing" .
         "</ul>";

    echo "Please find / <a href='http://www.kancms.org/' target='_blank'>download</a> a " .
         "KAN installation package and load the KAN installation " .
         "folder to repair your current KAN installation";
}
?>
