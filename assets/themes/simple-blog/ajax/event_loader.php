<?php
include_once('ajax_config.php');


$colname_rsevents = "-1";
if (isset($_POST['id'])) {
  $colname_rsevents = $_POST['id'];
}
mysql_select_db($database_config, $config);
$query_rsevents = sprintf("SELECT *, date_format(`Date`, '%%D %%b, %%Y') AS EventDate, date_format(`StartDate`, '%%D %%b, %%Y') AS StartDate, date_format(`EndDate`, '%%D %%b, %%Y') AS EndDate FROM events WHERE id = %s", GetSQLValueString($colname_rsevents, "int"));
$rsevents = mysql_query($query_rsevents, $config) or die(mysql_error());
$row_rsevents = mysql_fetch_assoc($rsevents);
$totalRows_rsevents = mysql_num_rows($rsevents);


// the following is used to display the list of events when viewed using the tabs
if( isset($_POST['cat']) ) { 
	
	$date1 = date("Y-m-d");
	
	$maxRows_rsRecentEvents = 4;
	$pageNum_rsRecentEvents = 0;
	if (isset($_GET['pageNum_rsRecentEvents'])) {
	  $pageNum_rsRecentEvents = $_GET['pageNum_rsRecentEvents'];
	}
	$startRow_rsRecentEvents = $pageNum_rsRecentEvents * $maxRows_rsRecentEvents;
	
	$colname_rsRecentEvents = "-1";
	if (isset($_POST['cat'])) {
	  $colname_rsRecentEvents = $_POST['cat'];
	}
	mysql_select_db($database_config, $config);
	$query_rsRecentEvents = sprintf("SELECT events.id, events.EventCategory, events.EventName, events.EventDesc, events.AssocImage, events.`Time`, events.EventDuration, date_format(events.`Date`, '%%b %%d') AS Date, date_format(events.`StartDate`, '%%b %%d') AS StartDate, date_format(events.`EndDate`, '%%b %%d') AS EndDate, events.Venue, event_categories.id AS cid, event_categories.Category FROM events, event_categories WHERE (`Date` >= current_date OR events.StartDate >= current_date) AND EventCategory = %s  AND event_categories.id = EventCategory AND event_categories.SiteID = $mainSiteID ORDER BY events.`Date` ASC, events.StartDate ASC", GetSQLValueString($colname_rsRecentEvents, "text"));
	$query_limit_rsRecentEvents = sprintf("%s LIMIT %d, %d", $query_rsRecentEvents, $startRow_rsRecentEvents, $maxRows_rsRecentEvents);
	$rsRecentEvents = mysql_query($query_limit_rsRecentEvents, $config) or die(mysql_error());
	$row_rsRecentEvents = mysql_fetch_assoc($rsRecentEvents);
	
	if (isset($_GET['totalRows_rsRecentEvents'])) {
	  $totalRows_rsRecentEvents = $_GET['totalRows_rsRecentEvents'];
	} else {
	  $all_rsRecentEvents = mysql_query($query_rsRecentEvents);
	  $totalRows_rsRecentEvents = mysql_num_rows($all_rsRecentEvents);
	}
	$totalPages_rsRecentEvents = ceil($totalRows_rsRecentEvents/$maxRows_rsRecentEvents)-1;

	if( $totalRows_rsRecentEvents > 0 ) { ?>

<div class="event_tbl">
    <?php $catName = $row_rsRecentEvents['Category']; ?>
    <?php do { ?>
        <ul>
            <li> <a href="../pages/events.php?siteid=<?php echo $_GET['siteid']; ?>&amp;id=<?php echo $row_rsRecentEvents['id']; ?>"> 
                <!--<img src="../assets/images/arrow.png" alt="arrow" width="9" height="9" border="0" /> -->
                
                <?php if( $row_rsRecentEvents['EventDuration'] == "single") { ?>
                <span style="font-size:11px; color:#555; font-family:Tahoma, Geneva, sans-serif;"><?php echo $row_rsRecentEvents['Date']; ?> - </span>
                <?php } else { ?>
                <span style="font-size:11px; color:#555; font-family:Tahoma, Geneva, sans-serif;"><?php echo $row_rsRecentEvents['StartDate'] . " - " . $row_rsRecentEvents['EndDate']; ?> : </span>
                <?php } ?>
                <?php echo $row_rsRecentEvents['EventName']; ?> </a> 
            </li>
        </ul>
        <!--<div class="details"></div>-->
        
        <?php } while ($row_rsRecentEvents = mysql_fetch_assoc($rsRecentEvents)); ?>
    <div align="right">
        <a href="../pages/events.php?cat=<?php echo $_POST['cat']; ?>">View All <?php echo $catName; ?> Events</a>
    </div>
</div>
<?php } else {
		echo '<div class="event_tbl">No Events Have Currently Been Posted</div>';
	}
	mysql_free_result($rsRecentEvents);
	
	// return the loaded information
	exit;
}
?>
<div class="news_title">
    <?php echo $row_rsevents['EventName']; ?>
</div>
<div class="news_title_footer">
    <strong>Venue:</strong> <?php echo $row_rsevents['Venue']; ?> &nbsp;<strong>&nbsp;Date:</strong>
    <?php 
      if( $row_rsevents['EventDuration'] == "single" ) { 
         echo $row_rsevents['EventDate']; 
      } else {
         echo $row_rsevents['StartDate'] . " - " . $row_rsevents['EndDate'];
      }
   ?>
    <strong>&nbsp;&nbsp;Time:</strong> <?php echo $row_rsevents['Time']; ?>
</div>
<div class="news_content">
    <?php echo $row_rsevents['EventDesc']; ?>
</div>
<?php
mysql_free_result($rsevents);
?>
