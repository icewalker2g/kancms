<div class="pages-sidebar">
    <h3>Pages</h3>
    <?php echo $site->getPageManager()->getPagesAsHTMLList('page-list', 1, false); ?>


	<h3>Recent Posts</h3>
    <div><?php
		$articles = $site->getArticlesManager()->getArticles(0, 5);
		
		echo "<ul>";
		for($i = 0; $i < count($articles); $i++) {
			$article = $articles[$i];
			echo "<li>{$article->getLinkedTitle()}</li>";
		}
		echo "</ul>";
	?>
    </div>
    
    <div><?php
		$categories = $site->getLinksManager()->getLinkCategories();
		
		for($i = 0; $i < count($categories); $i++) {
			$cat = $categories[$i];
			$links = $cat->getLinks();
			
			echo "<h3>{$cat->getCategoryName()}</h3>";
			echo "<ul>";
			
			for($i = 0; $i < count($links); $i++) {
				$link = $links[$i];
				echo "<li>" . $link->getLinkAsHTMLAnchor() . "</li>";
			}
			
			echo "</ul>";
		}
	?>
    </div>
</div>