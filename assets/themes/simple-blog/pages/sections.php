<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title><?php echo $site->getSiteName(); ?> - <?php echo $page->getName();?></title>
        <?php 
			echo $site->getHTMLHeadData(); 
			echo $site->script('common/pageManager.js');
		?>

        <script type="text/javascript">
            function loadRPContent(id) {
                var query = "id=" + id;
                pageManager.get('<?php echo $site->theme->url('ajax/rpc.php'); ?>', query, 'page_data');
            }
        </script>
    </head>

    <body>
        <div id="page">
            <?php include('header.php'); ?>

            <div id="content">
                <div class="main">
                	<h2><?php echo $page->getName(); ?></h2>
					<?php echo $page->getContent(); ?>
                </div>


                <div class="sidebar"> 
                    <?php include('sidebar.php'); ?>
                </div>

            </div>

            <?php include('footer.php'); ?>
        </div>
    </body>
</html>