<?php 
// get the 10 most recent articles
$articles = $site->getArticles(0, 10);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title><?php echo $site->getSiteName(); ?> - Articles</title>
        <?php echo $site->getHTMLHeadData(); ?>
        
        <!--<link rel="alternate" type="application/rss+xml" title="<?php echo $site->getSiteName() . " &raquo; All Articles"; ?>" href="<?php echo $site->getArticlesManager()->getArticlesFeedURL(); ?>" />-->
        <?php if( isset($category) ) { ?>
        <link rel="alternate" type="application/rss+xml" title="<?php echo $site->getSiteName() . " &raquo; Articles In " . $category->getCategoryName(); ?>" href="<?php echo $category->getFeedURL(); ?>" />
        <?php } ?>
        
		<?php echo $site->script('jquery/js/jquery.js'); ?>
        <?php echo $site->theme->script('articles.js'); ?>
        <script type="text/javascript">
            $(document).ready(function(){
                articles.init({
                    article_id : <?php echo isset($article) ? $article->getId() : -1; ?>,
					url: '<?php echo $site->theme->url('ajax/articles.php'); ?>'
                });
            });
        </script>
        <!--<link rel="alternate" type="application/rss+xml" title="ICED IN CODE &raquo; Create A Blog Site With KAN CMS 1.0 Beta&nbsp;RC5+ Comments Feed" href="http://icewalker2g.wordpress.com/?p=317/feed/" />-->
    </head>

    <body>
        <div id="page">
            <?php include('header.php'); ?>

            <div id="content">
                <div class="main"><?php
					if( isset($article) ) { ?>
                        <div class="article_title"><?php echo $article->getTitle(); ?></div>
                        <div class="article_content"><?php echo $article->getContent(); ?></div>
    
    					<h3><a name="comments">Comments</a></h3>
                        <ul id="comment-list"><?php
                        $comments = $article->getApprovedComments();
                        
                        for($i = 0; $i < count($comments); $i++) { ?>
                            <li>
                                <!--<div class="comment_title"><?php echo $comments[$i]->getSubject(); ?></div>-->
                                <div class="comment-info">
                                    <div class="author"><?php echo $comments[$i]->getLinkedReaderName(); ?> says: </div>
                                    <div class="date"><?php echo $comments[$i]->getPostDate('M d, Y'); ?> at <?php echo $comments[$i]->getPostDate('H:m'); ?></div>
                                </div>
                                <div class="comment-message"><?php echo $comments[$i]->getMessage(); ?></div>
                            </li><?php
                        }
                        ?>
                        </ul>
                        
                        <?php 
						if( $article->commentsAllowed() ) { ?>
                            <div id="comments">
                                <?php include('comments_form.php'); ?>
                            </div><?php
						}
						
						
					} else if( isset($category) ) {
						include('article_summary.php');
						renderArticleSummary($category->getCategoryName(), $category);	
					} else {
						include('article_summary.php');
						renderArticleSummary("Recent Articles");	
					}?>
                </div>

                <div id="sidebar">
                    <?php include('sidebar.php'); ?>
                </div>

            </div>

            <?php include('footer.php'); ?>
        </div>
    </body>
</html>