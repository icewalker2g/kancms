
<div id="header">
	<span class="blog_title">
        <a href="<?php echo $site->getHomePageURL(); ?>">
            <?php echo $site->getSiteName(); ?>
        </a>
    </span>

    <span class="blog_slogan">
        <?php echo $site->getSiteSlogan(); ?></span>
    </span>
</div>

<div id="navigation">
	<?php $site->getPageManager()->getPagesAsHTMLList('nav', 1); ?>
    
    <div class="feeds">
        <a href="<?php echo $site->getArticlesManager()->getArticlesFeedURL(); ?>">Posts</a>
        
        <?php 
        // if we are on the articles page with the category variable available, the provide the
        // feed for that
        if( isset($category) && $category instanceof ArticleCategory ) { ?>
        <a href="<?php echo $category->getFeedURL(); ?>">Posts In <?php echo $category->getCategoryName(); ?></a><?php 
        } ?>
    </div>
</div>

<div id="sub-navigation">
	<?php $site->getArticlesManager()->getCategoriesAsHTMLList('sub', 1); ?>
</div>
