<?php 
	include('ajax_config.php'); 
    
	$rpc_id = -1;
	
    if( isset($_GET['id']) ) {
        $rpc_id = intval($_GET['id']);
		
		$pm = new PageManager();
		$page = $pm->getPage($rpc_id); ?>
		
		<div class="news_title"><?php echo $page->getName(); ?></div>
		<div class="news_content"><?php echo $page->getContent(); ?></div><?php
		
    } else {
		echo "Page Not Found";	
	}

	 
?>
