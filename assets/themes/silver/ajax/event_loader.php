<?php
include_once('ajax_config.php');

$em = new EventsManager();
$event = NULL;

if( isset($_POST['id']) ) {
	$event = $em->getEvent($_POST['id']); ?>
    
	<div class="news_title">
		<?php echo $event->getName(); ?>
	</div>
	<div class="news_title_footer">
		<strong>Venue:</strong> <?php echo $event->getVenue(); ?> &nbsp;<strong>&nbsp;Date:</strong>
		<?php 
		  if( $event->getData('EventDuration') == "single" ) { 
			 echo $event->getStartDate();
		  } else {
			 echo $event->getStartDate() . " - " . $event->getEndDate();
		  }
	   ?>
		<strong>&nbsp;&nbsp;Time:</strong> <?php echo $event->getTime(); ?>
	</div>
	<div class="news_content">
		<?php echo $event->getDescription(); ?>
	</div><?php
	
} ?>
