<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title><?php echo $site->getSiteName(); ?> - <?php echo $page->getName();?></title>
        <?php echo $site->getHTMLHeadData(); ?>
        <?php echo $site->script('common/pageManager'); ?>

        <script type="text/javascript">
            function loadRPContent(id) {
                var query = "id=" + id;
                pageManager.get('<?php echo $site->theme->url('ajax/rpc.php'); ?>', query, 'page_data');
            }
        </script>
    </head>

    <body>
        <div id="page">
            <?php 
				include('header.php');
				include('navigation.php');
			?>

            <div id="content">
                <div id="page_data"><?php
					
					if( $page->getContent() != '' ) { ?>
                        <div class="news_title"><?php echo $page->getName(); ?></div>
                        <div class="news_content"><?php echo $page->getContent(); ?></div><?php
                    }
					
					$subPages = $page->getSubPages();
					
					foreach($subPages as $sp) {
						echo '<div class="content_title">' .
							$sp->getLinkedName( array('class' => 'page-link', 'onclick' => 'loadRPContent('. $sp->getId() .'); return false;')) .
							'</div>';
						echo '<div class="news_content">' . $sp->getSummary() . '</div>';
					}
					?>
                </div>


                <div id="sidebar"> 
                    <div id="related_info"><?php
                        
						//echo $site->getMenuManager()->getMenuPagesAsList();
						
                        $relatedPages = $page->getRelatedPages();
                        
                        foreach($relatedPages as $rp) {
                            
                            echo '<ul><li class="rc_title">' . $rp->getLinkedName() . '</li>';
                            echo '<ul class="rc_ul">';
                            
							$subPages = $rp->getSubPages();
							
                            foreach($subPages as $sp) {
								
								echo '<li class="rc_li">' .
									$sp->getLinkedName(
										'page-link', // class
										'onclick="loadRPContent('. $sp->getId() .'); return false;"'
									) .
								'</li>';
                                
                            }
                            
                            echo '</ul>';
                            
                            echo "</ul>";
                        }
						?>
                    </div>
                </div>

            </div>

            <?php include('footer.php'); ?>
        </div>
    </body>
</html>