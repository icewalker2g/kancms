
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
      <title><?php echo $site->getSiteName(); ?> - Home Page</title>
      <?php 
		  echo $site->getHTMLHeadData();
	  ?>
   </head>

   <body>
      <div id="page">
         <?php 
		 	include('header.php');
			include('navigation.php');
		 ?>

         <div id="bannerArea">
            <div id="left">
               <?php $site->renderComponent('spotlight');  ?>
            </div>

            <div id="right">

               <div class="block_title">Upcoming Events</div>
               <div class="right_content">
                  <?php
				  
				  $events = $site->getEvents(0,5);
                  echo '<ul>';

                  if( count($events) > 0 ) {
                     foreach($events as $event) { ?>
                          <li>	
                             <span class="highlight">
                                 <?php echo $event->getStartDate("d-M") . ": "; ?>
                             </span>
        
                             <?php echo $event->getLinkedName(); ?>
                          </li><?php
                     }
                  } else { ?>
                  	 <li>No Upcoming Events Posted.</li><?php
                  }

                  echo "</ul>";

                  ?>
               </div>
            </div>
         </div>

         <div id="content" style="padding: 0px">

            <div id="notices_block">
               <div class="main">
			   	  <?php echo $site->getHomePageText(); ?>
               </div>
               
               <div class="nav">
                  <div class="block_title">In The News</div>
                  <div style="font-family: Tahoma, Geneva, sans-serif; font-size: 11px; clear:both;"><?php
					 
					 $articles = $site->getArticles(0,2);
					 
                     if( count($articles) > 0 ) {

                        foreach($articles as $news) { ?>
                             <div style="font-size: 13px; clear:both;">
                                <?php echo $news->getLinkedTitle(); ?>
                             </div>
                             <div style="clear:both;"><?php
								  if( $news->getImageThumbnail() != "none" && $news->getImageThumbnail() != "" ) {
									 echo '<img src="' . $news->getImageThumbnail() . '" alt="" width="80" align="left" hspace="5"/>';
								  }
	
								  echo $news->getSummary(); ?>
                             </div><?php
                        }
                     } else { ?>
                     	<div>No Recent Articles Have Been Posted</div><?php
                     }

                     ?>
                  </div>

               </div>

               
            </div>
         </div>

         <?php include('footer.php'); ?>
      </div>
   </body>
</html>