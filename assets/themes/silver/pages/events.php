<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title><?php echo isset($event) ? $event->getName() : ""; ?> | <?php echo $site->getSiteName(); ?></title>
        <?php 
			echo $site->getHTMLHeadData(); 
			echo $site->script('common/pageManager.js'); 
		?>
        <script type="text/javascript">
            function loadEvent(id) {
                var query = "id=" + id + "&siteid=<?php echo $site->getSiteIdentifier(); ?>";
                $p.post("<?php echo $site->theme->url("ajax/event_loader.php"); ?>", query, 'page_data');

                return false; // so the page does not follow the HTML Link
            }
        </script>
    </head>

    <body>
        <div id="page">
           <?php 
				include('header.php');
				include('navigation.php');
			?>

            <div id="content">
                <div id="page_data"><?php 

                    if( isset($event) ) { ?>
                    <div class="news_title"><?php echo $event->getName(); ?></div>
                    <div class="news_content"><?php echo $event->getDescription(); ?></div><?php
                    } ?>

                    <div id="more_news">
                        <div class="block_title">Other Upcoming Events</div>
                        <div><?php

                            $events = $site->getEvents(0,10);

                            foreach( $events as $e ) {

								echo '<div style="margin-bottom: 10px; padding: 5px; overflow: hidden;">' ;
								echo '<div><span style="font-size:11px; font-family: tahoma; color: #999">' .
									$e->getStartDate() . ":</span> " .
									$e->getLinkedName( array(
										'id' => 'evl1_' . $e->getId(), 
										'class' => 'event_link', 
										'onclick' => "return loadEvent(' . $e->getId() . ');" // ajax events
									)) . "</div>";
								echo '</div>';
                            }

                            // if the count is not greater than 0 then no event we were loaded, so
                            // we need to provide some feedback

                            if( count($events) == 0 ) {
                                echo "Sorry, there are no upcoming events available at the moment";
                            }
                            ?></div>
                    </div>
                </div>

                <div id="sidebar">
                    <div id="related_info"><?php

                        echo '<ul class="simple-list rc_ul">';
						$categories = $site->getEventsManager()->getCategories();
                        $lastCat = "";

                        foreach($categories as $cat) {

                            
							echo '<h3 class="list-header">' . $cat->getCategoryName() . '</h3>';
                            
							$events = $cat->getEvents(0,5);
							foreach($events as $ev) {

								echo '<li class="rc_li">';
								echo '<span style="color: #999">' . $ev->getStartDate() . ":</span> ";
	
								echo $ev->getLinkedName( array(
									'id' => 'evl2_' . $ev->getId(), // link id
									'class' => 'event_link', // link class
									'onclick' => "return loadEvent(' . $ev->getId() . ');" // ajax events
								));
	
								echo "</li>";
							}

                        }
                        echo "</ul>";

                        ?></div>

                </div>
                <!--<div id="related_info">Related Information Goes Here</div>-->
            </div>

            <?php include('footer.php'); ?>
        </div>
    </body>
</html>