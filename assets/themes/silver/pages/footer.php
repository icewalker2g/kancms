
<div id="footer">
   
   
   <div id="links_area"><?php
   
   	   $links = new LinksManager();
   	   $linkCats = $links->getLinkCategories();
	   
	   for( $i = 0; $i < count($linkCats); $i++ ) {
		   
		   $catName = $linkCats[$i]->getCategoryName();
		   $catId = $linkCats[$i]->getCategoryID();
		   
		   $catLinks = $links->getLinksInCategory( SITE_ID, $catId );
		   
		   if( count($catLinks) == 0 ) continue;
		   
		   echo "
			   <div class='link_group'>
			   	  <div class='link_group_header'>$catName</div>
				  <div class='link_group_content'>
				  	<ul class='link-list'>
		   ";
		   
		   for( $j = 0; $j < count($catLinks); $j++ ) {
			   
			   echo "
			   	       <li>" . $catLinks[$j]->getLinkAsHTMLAnchor() . "</li>
			   ";
		   }
				  
		   echo "
		            </ul>
		   		  </div>
			   </div>
		   ";
	   }

	   
   ?>
   		
        <div class="link_group site_footer_text">
        	<div class="link_group_header">About This Site</div>
        	<div><?php echo $site->getFooterText(); ?></div>
        </div>
   </div>
   
   <!--<div style="font-size: 10px; color:#666; clear:both; text-align: right; padding-top: 10px; position:absolute; width: 950px">KAN Silver Theme Developed By <a href="http://icewalker2g.wordpress.com">Francis Adu-Gyamfi</a>-->
</div>

