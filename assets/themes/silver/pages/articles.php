<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title><?php echo isset($article) ? $article->getTitle() : ""; ?> | <?php echo $site->getSiteName(); ?></title>
        <?php 
		  echo $site->getHTMLHeadData();
	    ?>
    </head>

    <body>
        <div id="page">
            <?php 
				include('header.php');
				include('navigation.php');
			?>

            <div id="content">
					
                <div id="page_data"><?php
				    // if we have an article to display, then load that
					if( isset($article) ) { ?>
                        <div class="news_title"><?php echo $article->getTitle(); ?></div>
                        <div class="news_content">
                        	<?php 
							if( $article->getImage() != "" ) {
							   echo '<img src="' . $article->getImage() . '" alt="" align="left" hspace="5" style="border: solid #999 1px; padding: 2px;" />';
						    }
							
							echo $article->getContent(); ?>
                        </div>
    
                        <?php 
					} else { 
						$articles = $site->getArticles(0,10);
						foreach( $articles as $article ) {
							echo "<div style='clear: both; margin-bottom: 10px; overflow:hidden; '>";
							
							
							echo "<h3 style='margin-bottom: 4px;'>" . $article->getLinkedTitle() ."</h3>";
					
							echo "<div style='padding-top: 10px; overflow: hidden;'>" ;
							echo '<div style="float:left; clear:left; margin-right: 20px; padding: 8px; background-color: #f9f9f9; border-radius: 5px; border: solid #e1e1e1 1px;" title="' . $article->getPostDate("d M, Y") . '" >
							         <div style="font-size:20px; color: #353535; text-transform: uppercase; text-align: center; font-weight: bold; padding: 4px;">' . $article->getPostDate('M') . '</div>
									 <div style="font-size:19px; color: #222; text-align: center; font-weight: bold; padding: 4px;">' . $article->getPostDate('d') . '</div>
							      </div>';
								  
							if( $article->getImageThumbnail() != "none" && $article->getImageThumbnail() != "" ) {
							   echo '<img src="' . $article->getImageThumbnail() . '" alt="" width="150" align="left" hspace="5" style="border: solid #999 1px; padding: 2px;" />';
						    }
							
							echo $article->getSummary() . "</div>";
							echo "</div>";
							
						}
					}
					?>
                </div>

                <div id="sidebar">
                    <div id="related_info">
                    	<h3>More Articles</h3>
                    	<ul class="rc_ul">
						<?php $articles = $site->getArticles(0,10); 
						foreach($articles as $art) {
							echo "<li class='rc_li'>" . $art->getLinkedTitle() . "</li>";	
						}
						?>
                        </ul>
                    </div>
                </div>

                <!--<div id="related_info">Related Information Goes Here</div>-->
            </div>

            <?php include('footer.php'); ?>
        </div>
    </body>
</html>