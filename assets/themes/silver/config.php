<?php

/**
 * This file is required to provide the necessary configuration options
 * available for this theme.
 */
if( !isset($site) ) {
    return;
}

$site->theme->setOptions( array(
    'layouts' => array(
        'sections.php' => 'Default Page Layout'
    ),
    'spotlight_info' => array(
        'width' => '650px',
        'height' => '250px'
    )
));
?>
