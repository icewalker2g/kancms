<?php

/**
 * This file is required to provide the necessary configuration options
 * available for this theme.
 */
if( !isset($site) ) {
    return;
}

$site->theme->setOptions( array(
    'layouts' => array(
        'sections.php' => 'Default Page Layout',
        'layout_1.php' => 'Layout 1 (Sidebar Left)',
        'layout_2.php' => 'Layout 2 (Sidebar Right)',
        'layout_3.php' => 'Layout 3 (No Sidebars)'
    ),
    'spotlight_info' => array(
        'width' => '980px',
        'height' => '300px'
    )
));
?>
