<?php 

    $links = $site->getLinks(0, 5);
	
	echo "<h3>Useful Links</h3>";
	
	echo "<ul>";
	foreach($links as $link ) {
		echo "<li>{$link->render()}</li>";
	} 
	
	echo "</ul>";
?>