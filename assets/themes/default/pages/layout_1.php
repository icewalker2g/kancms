<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title><?php echo $site->getSiteName(); ?> - <?php echo $page->getName();?></title>
        <?php 
			echo $site->getHTMLHeadData(); 
			echo $site->script('jquery/js/jquery.js');
			echo $site->theme->css('prettify/prettify.css');
			echo $site->theme->script('prettify/prettify.js');
		?>
        
        <script type="text/javascript">
			$(document).ready(function(){
				$("#page_data pre").addClass("prettyprint");
				prettyPrint();
				
				$(".page-list a").click(function() {
					document.getElementById("page_data").scrollIntoView();
					$("#page_data").html("<div><b><img src='<?php echo $site->theme->url('css/loadatom.gif'); ?>' alt='' align='absmiddle' /> Loading Content Please Wait....</b></div>");
					
					var id = $(this).attr("data-id");
					var query = "id=" + id + "&siteid=<?php echo $site->getSiteIdentifier(); ?>";
					$.post(
						"<?php echo $site->theme->url('ajax/content.php'); ?>",
						query,
						function(data) {
							$("#page_data").html(data);
							$("#page_data pre").addClass("prettyprint");
							prettyPrint();
						}
					);
					return false;	
				});
			});
        </script>
    </head>

    <body>
        <div id="page" class="layout1">
            <?php include('header.php'); ?>

            <div id="content">
            	<div id="location_bar">
					<?php echo $page->getBreadCrumb("&bull;"); ?>
                </div>
                <div id="page_data">
					<?php 
						include('functions.php');
						renderContent($page); 
					?>
                </div>


                <div id="sidebar">
                    <div id="related_info">
                        <h3>In This Section</h3>
                        <div>
							<?php 
								if( $page->getLevel() <= 2 ) {
									$page->toHTMLList('page-list', 1, $page->getSubPages() );
									
								} else if( $page->getLevel() > 2 ) {
									$parents = $page->getParentPages();
									$parents[1]->toHTMLList('page-list', 4, $parents[1]->getSubPages() );
								} 
							?>
                        </div>
                    </div>
                </div>

            </div>

            <?php include('footer.php'); ?>
        </div>
    </body>
</html>