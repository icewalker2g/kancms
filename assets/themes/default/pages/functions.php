<?php 

function renderContent($page) { ?>
	
    <div class="news_title"><?php echo $page->getName(); ?></div>
    <div class="news_content"><?php echo $page->getContent(); ?></div>
    
    <?php 
    if( $page->getLevel() <= 2 ) { 
        $pages = $page->getSubPages();
        
        for( $i = 0; $i < count($pages); $i++ ) { 
			if( !$page->isPublished() ) continue; ?>
            
            <h3><?php echo $pages[$i]->getLinkedName(); ?></h3>
            <div class="news_content"><?php echo $pages[$i]->getSummary(); ?></div><?php
        }
    }	
}
?>