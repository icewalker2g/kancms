<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
      <title><?php echo $site->getSiteName(); ?> - Home Page</title>
      <?php 
	  		echo $site->getHTMLHeadData();
			echo $site->script('jquery/js/jquery.js');
	  ?>
   </head>

   <body>
      <div id="page">
         <?php include('header.php'); ?>

         <div id="bannerArea">
         	<div id="banner_block"><?php $site->renderComponent('spotlight'); //include('spotlight.php'); ?></div>
         </div>

         <div id="content">
            <div id="home_sidebar">
                <?php include('links.php'); ?>
            </div>
            <div id="home_content">
            	<h3>Welcome To <?php echo $site->getSiteName(); ?></h3>
            	<div id="home_text"><?php echo $site->getHomePageText(); ?></div>
                <?php
					include('article_summary.php');
					renderArticleSummary( "Recent Articles", NULL, 2 );
				?>
            </div>
         </div>
         
		 <?php include('footer.php'); ?>
      </div>
   </body>
</html>