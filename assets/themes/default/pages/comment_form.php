
<form name="comment-form" id="comment-form" action="" >
<div>
	<label for="name">* Name: </label>
    <input type="text" name="name" id="name" required placeholder="Enter Name Here"  />
    <span class="comment-text">Required</span>
</div>

<div>
	<label for="email">* Email: </label>
    <input type="email" name="email" id="email" required placeholder="Enter Address Here" >
    <span class="comment-text">Required</span>
</div>

<div>
	<label for="website">Website: </label>
    <input type="url" name="website" id="website" placeholder="Enter Website Here" />
    <span class="comment-text">Optional</span>
</div>

<div>
	<label for="comment">* Comment</label>
    <textarea name="comment" id="comment" required></textarea>
</div>

<div id="comment-buttons">
	<input type="submit" name="submit" id="submit" value="Post Comment" />
    <input type="reset" name="reset" id="reset" value="Clear" />
    <input type="hidden" name="article_id" id="article_id" value="<?php echo isset($article) ? $article->getId() : -1; ?>"  />
	No HTML Allowed In Comments
</div>
</form>