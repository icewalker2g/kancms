<?php include_once('article_summary.php'); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title><?php echo (isset($article) ? $article->getTitle() . " - " : "") . $site->getSiteName(); ?></title>
        <?php 
			echo $site->getHTMLHeadData(); 
			echo $site->script('jquery/js/jquery.js'); 
			echo $site->theme->script("articles.js"); 
		?>
        
        <script type="text/javascript">
            $(document).ready(function(){
                articles.init({
                    article_id : <?php echo isset($article) ? $article->getId() : -1; ?>,
					url: '<?php echo $site->theme->url('ajax/articles.php'); ?>'
                });
            });
        </script>
    </head>

    <body>
        <div id="page">
            <?php include('header.php'); ?>

            <div id="content"><?php 
				if( isset($article) ) { ?>
                    <div id="location_bar">
                        <?php echo $article->getBreadCrumb("&bull;"); ?>
                    </div>
                    <div id="page_data">
                        
                        <div class="news_title"><?php echo $article->getTitle(); ?></div>
                        <div class="news_content">
                            <?php 
								echo $article->renderImage( array('align' => 'left', 'style' => 'border: solid #ccc 1px; margin-right: 10px;') );
								echo $article->getContent(); 
							?>
                        </div>
                       
                        <?php if( $article->commentsAllowed() ) { ?>
                        <div id="more_news">
                        	<div>
                            	<h3><a name="comments">Have Your Say</a></h3>
                            	<ul id="comment-list"><?php
                                $comments = $article->getApprovedComments();
								
								for($i = 0; $i < count($comments); $i++) { ?>
									<li>
                                    	<!--<div class="comment_title"><?php echo $comments[$i]->getSubject(); ?></div>-->
                                        <div class="comment-info">
                                        	<div class="author"><?php echo $comments[$i]->getLinkedReaderName(); ?> says: </div>
                                            <div class="date"><?php echo $comments[$i]->getPostDate('M d, Y'); ?> at <?php echo $comments[$i]->getPostDate('H:m'); ?></div>
                                        </div>
                                        <div class="comment-message"><?php echo $comments[$i]->getMessage(); ?></div>
                                    </li><?php
								}
								?>
                                </ul>
                            </div>
                            <div class="content_title">Post Comment</div>
                            <div>
                                <?php include('comment_form.php'); ?>
                            </div>
                        </div>
                        <?php } ?>
                        
                    </div><?php 
				} else if( isset($category) ) { ?>
                
                    <div id="location_bar">
                        <?php echo $category->getBreadCrumb("&bull;"); ?>
                    </div>
					<div id="page_data">
                        <?php
							renderArticleSummary(
								$category->getCategoryName(), // title
								$category // articles
							);
						?>
                        
                    </div><?php 
				} else { ?>
                	<div id="location_bar">
                        <a href="<?php echo $site->getHomePageURL(); ?>">Home</a> &bull; Articles
                    </div>
					<div id="page_data">
                        <?php renderArticleSummary("Recent Articles" ); ?>
                    </div><?php 
				} ?>

                <div id="sidebar">
                    <div class="article_list">
					
						<?php
						if( isset($article) || isset($category) ) {
							echo "<h3>More " . $category->getCategoryName() 
							. "<a class='feed-link' href='" . $category->getFeedURL() . "'></a>"
							. "</h3>";
							
							
							echo "<ul>";
							
							$articles = $category->getArticles(0,20);
	
							for( $i = 0; $i < count($articles); $i++ ) {
								$art = $articles[$i];
								
								echo '<li><span class="data_info">' . $art->getPostDate() . ":</span> " 
								   . $art->getLinkedTitle() . "</li>";
							}
							
							echo "</ul>"; 
						}
						?>
                    </div>
                    
                    <div class="article_list">
                    	<h3>
                        	Article Categories
                            <a class="feed-link" href="<?php echo $site->getArticlesManager()->getArticlesFeedURL(); ?>"></a>
                        </h3>
						<?php $site->getArticlesManager()->getCategoriesAsHTMLList('category-list'); ?>
                    </div>
                    
                    <div class="link_list"><?php include('links.php'); ?></div>
                </div>

                <!--<div id="related_info">Related Information Goes Here</div>-->
            </div>

			<?php include('footer.php') ?>
        </div>
    </body>
</html>