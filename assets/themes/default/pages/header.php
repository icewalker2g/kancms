<div id="header">
    <div id="head_comps">

        <div id="title_block">
            <h2><a href='<?php echo $site->getHomePageURL(); ?>'><?php echo $site->getSiteName(); ?></a></h2>
            <h3><?php echo $site->getSiteSlogan(); ?></h3>
        </div>

        <div id="search_area">
            <?php $site->renderComponent('search_box'); ?>
        </div>

    </div>

    <div id="navigation">
        <?php $site->renderMenu('nav', 1); ?>
        </div>
    </div>


<?php if (isset($page)) {
    $rootPage = $page->getRootPage(); ?>
    <div id="sub_nav">
        <div class="sub_page_name"><?php echo $rootPage->getName(); ?></div>
        <div class="list_group"><?php $page->toHTMLList('sub-nav-list', 1, $rootPage->getSubPages()); ?></div>
    </div>
<?php } ?>
