<?php 

function renderArticleSummary($listTitle = "Recent Articles", $category = NULL, $limit = 10) {	
	global $site;
	$articles = NULL;
	$feedURL = NULL;
	$start = isset($_GET['p']) ? $_GET['p'] - 1 : 0;
	$totalPosts = NULL;

	if( $category == NULL ) {
		$articles = $site->getArticlesManager()->getArticles($start, $limit);
	} else {
		$articles = $category->getArticles($start, $limit);	
	}
	
	if( $category == NULL ) {
		$feedURL = $site->getArticlesManager()->getArticlesFeedURL();	
	} else {
		$feedURL = $category->getFeedURL();	
	}
	
	echo "<div class='article_summary'>";
	
	if( $listTitle != "" ) {
		echo "<h2>
				$listTitle
				<a class='feed-link' href='$feedURL'></a>
			 </h2>";
	}

    //echo "<pre>" . print_r($articles) . "</pre>";
    
	for( $i = 0; $i < count($articles); $i++ ) {
		$article = $articles[$i];
		
		$authorsURL = $article->getCategory()->getURL();
		$authorsURL = $authorsURL . (strpos($authorsURL,'?') ? '&' : '?') . "author=" . urlencode($article->getAuthor());
		
		echo "<div class='article_group'>";
		echo "<h3>" . $article->getLinkedTitle() . "</h3>";
		echo "<div class='summary_footer'>"
		   
		   . "<span class='date_info'>" . $article->getPostDate("d M, Y") . "</span> "
		   
		   //. "<span class='cat_name'>" . $article->getCategory()->getLinkedCategoryName() . "</span> "
		   
		   . " By <span><a href='$authorsURL'>" . $article->getAuthor() . "</a></span> "
		   
		   . "<span class='comment_info'><a href='" . $article->getURL() . "#comments'>Leave A Reply (" . $article->getCommentCount() .")</a></span>"
		   
		   . "</div>";
		   
		// create summary content
		echo "<div class='summary_content'>";
		echo $article->renderThumbnail();
		echo $article->getSummary();
		
		echo "</div>";
		
		// bottom of summary
		echo "<div class='summary_footer'>"
		   
		   . "FILED UNDER <span class='cat_name'>" . $article->getCategory()->getLinkedCategoryName() . "</span> "
		   
		   . "</div>";
		   
		// end article group
		echo "</div>";
	}	
	
	if( !isset($category) ) {
		$totalPosts = $site->getArticlesManager()->getTotalArticleCount('approved');
	} else {
		$totalPosts = $category->getTotalArticleCount();	
	}
	
	$lastPage = ceil($totalPosts / $limit);
	
	echo "<div class='navbar'>";
	if( isset($_GET['p']) && $_GET['p'] > 1 ) {
		$page = isset($_GET['p']) ? $_GET['p'] - 1 : 1;
		echo "<a class='newer-posts' href='?page=$page'>Newer Posts</a>";
	}

	// if there are more pages than the current or if not on the last page
	if( (isset($_GET['p']) && $_GET['p'] < $lastPage) || (!isset($_GET['p']) && $lastPage > 1) ) {
		$page = isset($_GET['p']) ? $_GET['p'] + 1 : 2;
		echo "<a class='older-posts' href='?page=$page'>Older Posts</a>";
	}
	echo "</div>";
	
	echo "</div>";
}


?>