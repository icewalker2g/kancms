<?php 
	echo $site->theme->css('spotlight.css');
	echo $site->theme->script('spotlight.js');
?>

<div id='spots'><?php
	
	$spotlights = $site->getSpotlights(0,5);
	$count = 1;
	
	foreach( $spotlights as $spotlight ) { ?>
	
		<div id='spot-<?php echo $count; ?>' class='spot-content'>
			<?php echo $spotlight->render(); ?>
			<div class='spot-desc'><?php echo $spotlight->getDescription(); ?></div>
		</div><?php
			
		$count++;
	} ?>
</div>
