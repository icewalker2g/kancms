<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title><?php echo $site->getSiteName(); ?> - <?php echo $page->getName();?></title>
        <?php 
			echo $site->getHTMLHeadData(); 
		?>
        
    </head>

    <body>
        <div id="page" class="layout3">
            <?php include('header.php'); ?>

            <div id="content">
            	<div id="location_bar">
					<?php echo $page->getBreadCrumb("&bull;"); ?>
                </div>
                <div id="page_data" class="full_width">
					<?php echo $page->getContent(); ?>
                </div>

            </div>

            <?php include('footer.php'); ?>
        </div>
    </body>
</html>