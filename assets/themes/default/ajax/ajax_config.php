<?php
/**
 * This file should be included in all theme AJAX files, to provide a quick way to get the configuration
 * and database connection information for the CMS
 */

// code will find configuration path
$currentPage = $_SERVER['PHP_SELF'];
$endPos = strpos($currentPage, "assets/");
$siteRoot = substr($currentPage, 0, $endPos);

// if the site object is not available, then we need to include the site_selector.php
// file to be able to determine which site to load the event(s) for
if( !isset($site) ) {
    include($_SERVER['DOCUMENT_ROOT'] . $siteRoot . 'core/SiteLoader.php');
}
?>
