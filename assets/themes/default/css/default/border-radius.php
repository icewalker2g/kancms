<?

  header("Content-Type: image/svg+xml;charset=utf-8");

  $radius = $_GET["r"];
  if (!$radius) $radius = "5";

  $color = $_GET["color"];
  if (!$color) $color = "silver";
  if (is_numeric(substr($color, 0, 1))) {
    $color = "rgb(" . $color . ")";
  }

?>
<?= '<?xml version="1.0" ?>' ?>
<svg xmlns="http://www.w3.org/2000/svg">
  <rect fill="white" x="0" y="0" width="100%" height="100%" />
  <rect fill="<?php echo $color ?>" x="0" y="0" width="100%" height="100%" rx="<?php echo $radius ?>px" />
</svg>