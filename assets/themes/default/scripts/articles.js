// JavaScript Document

var articles = {
	
	params: null,
	
	init: function(params) {
		articles.params = params;
		
		$("#comment-form").submit(function(){
			articles.postComment();
			return false;
		});
		
	},
	
	postComment: function() {
		 var query = "action=add_comment&" + $("#comment-form").serialize();

		 $.post( articles.params.url, query, function(data) {
		 	$("#comment-form input[type=text], #comment-form input[type=email], #comment-form input[type=url], #comment-form textarea").val("");
			
			var li = document.createElement("li");
			li.style.display = "none";
			li.innerHTML = '<div class="comment-info">'
						 + '  <div class="moderation">Your Comment Is Awaiting Moderation And Will Show Up Once Approved</div>'
			             + '  <div class="author"><b>' + data.ReaderName + '</b> says </div>'
						 + '</div>'
						 + '<div class="comment-message">' + data.Message + '</div>';
			
			$("#comment-list").append(li);
			$(li).show('slow');
		 },'json');
	}
};