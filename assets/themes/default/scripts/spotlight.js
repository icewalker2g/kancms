// JavaScript Document
$(document).ready(function(){
	var pos = 1;
	var lastSpot = null;
	
	var timer = setInterval(function(){
		if( lastSpot != null ) {
			lastSpot.fadeOut('slow');
		}
		
		lastSpot = $("#spot-" + pos);
		
		if( lastSpot != null ) {
			lastSpot.fadeIn('slow');
			pos++;
		} 
		
		if( pos > $("#spots .spot-content").size() ){
			pos = 1;
		}
	},12000);	
});