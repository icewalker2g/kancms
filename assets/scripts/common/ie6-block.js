/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


var ie6block = {

    vNum: 6, // the version number of the browser

    init: function(minVersion) {
        var x = navigator;
        ie6block.vNum = parseFloat(x.appVersion.split("MSIE")[1]);
        
        if( minVersion ? ie6block.vNum < minVersion : ie6block.vNum < 7 ) {
            ie6block.createBlocker();
        }
    },

    createBlocker: function() {
        var notice = document.createElement("div");
        notice.innerHTML = "<div align=center style='font-size: 18px;'><b>Sorry!</b> Your current web browser <b>Internet Explorer " + ie6block.vNum + "</b> will not render this website "
                         + " correctly.</div><br /><br />"
                         + "<div>Please upgrade your browser to: "
                         + "<ul>";
		
		if( ie6block.vNum < 7 ) {
			notice.innerHTML += "<li><a href='http://www.microsoft.com/downloads/details.aspx?FamilyId=9AE91EBE-3385-447C-8A30-081805B2F90B'>Internet Explorer 7</a> or</li>";
		}
		
		if( ie6block.vNum < 8 ) {
            notice.innerHTML += "<li><a href='http://www.microsoft.com/Windows/internet-explorer/'>Internet Explorer 8</a> (recommended)</li>";
		}
		
        
		notice.innerHTML += "</ul><br />"
                         + "Or try any of these other alternative modern web browsers: "
                         + "<ul>"
                         + "<li><a href='http://www.opera.com/download/'>Opera Web Browser</a> (Recommended)</li>"
                         + "<li><a href='http://www.apple.com/safari/download/'>Safari Web Browser</a></li>"
                         + "<li><a href='http://www.mozilla.com/firefox/'>Mozilla Firefox</a></li>"
                         + "<li><a href='http://www.google.com/chrome'>Google Chrome</a></li>"
                         + "</ul><br />"
                         + "<center>Once you have upgraded or switched your browser, please visit us again. Thank you!</center>"
                         + "</div>";

        var content = document.createElement("div");
        content.style.position = "absolute";
        content.style.left = "26%";
        content.style.marginTop = "100px";
        content.style.marginLeft = "auto";
        content.style.marginRight = "auto";
        content.style.height = "340px";
        content.style.width = "580px";
        content.style.padding = "20px";
        content.style.fontFamily = "Tahoma";
        content.style.fontSize = "13px";
        content.style.backgroundColor = "#FFF";
        content.style.border = "solid #ccc 1px";
        content.style.zIndex = 800;
        content.style.opacity = 1.0;
        content.style.filter = "alpha(opacity=100)";
        content.appendChild( notice );

        var overlay = document.createElement("div");
        overlay.style.position = "absolute";
        overlay.style.width = "100%";
        overlay.style.height = "100%";
        overlay.style.backgroundColor = "#000";
        overlay.style.opacity = 0.7;
        overlay.style.filter = "alpha(opacity=70)";
        overlay.style.zIndex = 600;
		overlay.style.left = "0px";
		overlay.style.top = "0px";

        overlay.appendChild( content );

        var html = document.getElementsByTagName("html")[0];
        html.style.width = "100%";
        html.style.height = "100%";
        html.style.overflow = "hidden";
        
        var bodyEl = document.getElementsByTagName("body")[0];

		if( bodyEl != null ) {
			bodyEl.insertBefore(overlay, bodyEl.firstChild);
			bodyEl.insertBefore(content, overlay);
			bodyEl.style.width = "100%";
			bodyEl.style.height = "100%";
			bodyEl.scroll = "no"; // for IE 5.5
			bodyEl.style.overflow = "hidden"; // for IE 6
		} else {
			alert();	
		}
        
    },

    // Created by: Simon Willison
    // http://simon.incutio.com/archive/2004/05/26/addLoadEvent
    addLoadEvent: function(func) {
        var oldonload = window.onload;
        if (typeof window.onload != 'function') {
            window.onload = func;
        } else {
            window.onload = function() {
                if (oldonload) {
                    oldonload();
                }

                func();
            }
        }
    }
};

/*ie6block.addLoadEvent( function() {
	ie6block.init(); 
});*/
//ie6block.init();