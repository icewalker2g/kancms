// JavaScript Document
// For a more flexible event registration routine, see
// http://simon.incutio.com/archive/2004/05/26/addLoadEvent
//window.onload = attachFormHandlers;

var fieldNameMap;
var errorFields = new Array();
var errorFieldNames = new Array();
var formCheckCallBack;

function attachFormHandlers() {
    // Ensure we're working with a 'relatively' standards
    // compliant browser
    if (document.getElementsByTagName) {
        var objForm = document.getElementsByTagName('form');

        for (var iCounter=0; iCounter<objForm.length; iCounter++) {
            objForm[iCounter].onsubmit = function(){
                return validate(this);
            }
        }
    }
}

function attachCheckFormHandler(form) {
    // Ensure we're working with a 'relatively' standards
    // compliant browser
    if (document.getElementById) {
        var objForm = document.getElementById(form);

        if( objForm != null ) {
            objForm.onsubmit = function(){
                return validate(this);
            }
        }
    }
}

function displayFormErrors() {
    var errors = "<br>";
    for( var i = 0; i < getErrorFields().length; i++ ) {
        errors = errors + "&nbsp;  - " + getErrorFields()[i] + "<br>";
		
        var tempBorder = document.getElementById( getErrorFieldNames()[i] ).style.border;
        var fieldEl = document.getElementById( getErrorFieldNames()[i] );
		
        fieldEl.style.border = "solid red 1px";
        fieldEl.onblur = function() {
            if( this.value != null && this.value != "" ) {
                this.style.border = "solid #bbb 1px";
            }
        };
    }
}

function validate(objForm, map)
{
	
	if( map != null ) {
		setFieldNameMap(map);	
	}
	
    var arClass, bValid;
    var objField = objForm.getElementsByTagName('*');
    var errorCount = 0;
    errorFields = new Array();
    errorFieldNames = new Array();
  
    for (var iFieldCounter=0; iFieldCounter<objField.length; iFieldCounter++)
    {
        // Allow for multiple values being assigned to the class attribute
        arClass = objField[iFieldCounter].className.split(' ');
        for (var iClassCounter=0; iClassCounter<arClass.length; iClassCounter++)
        {
            switch (arClass[iClassCounter])
            {
                case 'required':
                    bValid = isNotEmpty(objField[iFieldCounter].value);
                    break;
                case 'string':
                    bValid = isString(objField[iFieldCounter].value.replace(/^\s*|\s*$/g, ''));
                    break;
                case 'number' :
                    bValid = isNumber(objField[iFieldCounter].value);
                    break;
                case 'email' :
                    bValid = isEmail(objField[iFieldCounter].value);
                    break;
                default:
                    bValid = true;
            }

            if (bValid == false)
            {
		
                // if a field name map exists then user expects to provide extra functionality
                // hence, simply list errors and continue
                if( fieldNameMap != null ) {
                    errorFields[errorCount] = fieldNameMap[objField[iFieldCounter].name];
                    errorFieldNames[errorCount++] = objField[iFieldCounter].name;
                //objField[iFieldCounter].style.border = "solid red 1px";
			
                // If this field is invalid, leave the testing early,
                // and alert the visitor to this error
                } else {
                    alert('Please review the value you provided for ' + objField[iFieldCounter].name);
			
                    objField[iFieldCounter].select();
                    objField[iFieldCounter].focus();
                    return false;
                }
       
            } else {
        //if( objField[iFieldCounter].form != null )	{
        //objField[iFieldCounter].style.border = "solid #999999 1px";
        //}
        }
        }
    }
 
    // if errors have been listed
    if( errorFields.length > 0 ) {
        // new addition: we will now display form errors on the field as well
        // in order to make the errors more visible
        displayFormErrors();
	
        // check if user specified a way to handle the errors
        if( formCheckCallBack != null  ) {
            formCheckCallBack();
		
        // simply display the errors via an alert prompt
        } else {
            var errors = "Please Check The Following Fields For Errors\n"
            for( var i = 0; i < getErrorFields().length; i++ ) {
                errors = errors + "  - " + getErrorFields()[i] + "\n";
            }
		
            alert( errors );
        }
	
        // ensure to indicate that there are errors in the current form
        return false;
    }
  
    return true;
}

function setFieldNameMap(map) {
    fieldNameMap = map;
}

function getFieldNameMap() {
    return fieldNameMap;
}

function getErrorFields() {
    return errorFields;
}

function getErrorFieldNames() {
    return errorFieldNames;
}

function setFormCheckCallbackMethod( methodName ) {
    formCheckCallBack = methodName;
}

function isNotEmpty(strValue) {
    return (strValue != null && strValue != "" && strValue.length != 0);
}

function isString(strValue)
{
    return (typeof strValue == 'string' && strValue != '' && isNaN(strValue));
}

function isNumber(strValue)
{
    return (!isNaN(strValue) && strValue != '');
}

function isEmail(strValue)
{
    var objRE = /^[\w-\.\']{1,}\@([\da-zA-Z-]{1,}\.){1,}[\da-zA-Z-]{2,}$/;

    return (strValue != '' && objRE.test(strValue));
}