<?php
    // code will find configuration path
    $currentPage = $_SERVER['PHP_SELF'];
    $endPos = strpos($currentPage, "components/");
    $siteRoot = substr($currentPage, 0, $endPos);
    require_once($_SERVER['DOCUMENT_ROOT'] . $siteRoot . 'managers/SiteLoader.php'); 

 
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$editFormAction = $_SERVER['PHP_SELF'];
if (isset($_SERVER['QUERY_STRING'])) {
  $editFormAction .= "?" . htmlentities($_SERVER['QUERY_STRING']);
}

if ((isset($_POST["MM_insert"])) && ($_POST["MM_insert"] == "cmt_form")) {
  $insertSQL = sprintf("INSERT INTO comments (SiteID, CommentCat, ItemID, Title, CommentText, Name, Email, `Date`) VALUES (%s, %s, %s, %s, %s, %s, %s, %s)",
                       GetSQLValueString($_POST['siteid'], "int"),
					   GetSQLValueString($_POST['comment_cat'], "text"),
                       GetSQLValueString($_POST['itemID'], "int"),
                       GetSQLValueString( strip_tags(urldecode($_POST['comment_title'])), "text"),
                       GetSQLValueString( strip_tags(urldecode($_POST['comment'])), "text"),
                       GetSQLValueString( strip_tags(urldecode($_POST['name'])), "text"),
                       GetSQLValueString( strip_tags(urldecode($_POST['email'])), "text"),
                       GetSQLValueString( $_POST['comment_date'], "date"));

  mysql_select_db($database_config, $config);
  $Result1 = mysql_query($insertSQL, $config) or die(mysql_error());
}

$colname_rsComments = "-1";
if (isset($_POST['itemID'])) {
  $colname_rsComments = $_POST['itemID'];
}
$com_cat_rsComments = "gallery";
if (isset($_POST['comment_cat'])) {
  $com_cat_rsComments = $_POST['comment_cat'];
}
mysql_select_db($database_config, $config);
$query_rsComments = sprintf("SELECT *, date_format(`Date`, '%%b-%%d') AS CommentDate FROM comments WHERE ItemID = %s AND comments.CommentCat = %s ORDER BY id ASC", GetSQLValueString($colname_rsComments, "int"),GetSQLValueString($com_cat_rsComments, "text"));
$rsComments = mysql_query($query_rsComments, $config) or die(mysql_error());
$row_rsComments = mysql_fetch_assoc($rsComments);
$totalRows_rsComments = mysql_num_rows($rsComments);
?>
<?php if( $totalRows_rsComments > 0 ) { ?>
<div id="comment_list">
	<div id="form_area_title">Comments</div>
	<?php $count = 1; do { ?>
   <div id="comment_group" <?php echo $count % 2 == 0 ? 'class="alt_group"' : ''; ?>>
   	<div id="comment_title"><b><?php echo $row_rsComments['Title']; ?></b> by <?php echo $row_rsComments['Name']; ?> On <?php echo $row_rsComments['CommentDate']; ?></div>
	   <div id="comment_text"><?php echo nl2br(htmlentities($row_rsComments['CommentText'])); ?></div>
   </div>
	<?php $count++;
	} while ($row_rsComments = mysql_fetch_assoc($rsComments)); ?>
</div>
<?php } ?>

<div id="comment_form_area">
	<div id="form_area_title">Post A Comment</div>
	<form name="cmt_form" id="cmt_form" action="<?php echo $editFormAction; ?>" method="POST" enctype="application/x-www-form-urlencoded" onsubmit="return false;" >
      <div>
         <table width="400" border="0" cellspacing="0" cellpadding="4">
            
            <tr>
               <td width="22%" align="right" class="boldText">Name: </td>
               <td width="78%"><input name="name" type="text" id="name" size="30" /></td>
            </tr>
            <tr>
               <td align="right" class="boldText">Email: </td>
               <td><input name="email" type="text" id="email" size="30" /></td>
            </tr>
            <tr>
               <td align="right" class="boldText">Msg Title: </td>
               <td><input type="text" value="" name="comment_title" id="comment_title" size="30" /></td>
            </tr>
            <tr>
               <td align="right" valign="top" class="boldText">Message: </td>
               <td><textarea name="comment" id="comment" cols="30" rows="4" onfocus="hideMsg(this,'Write Comment Here...')" style="overflow:auto;">Write Comment Here...</textarea></td>
            </tr>
            <tr>
               <td align="right" valign="top" class="boldText">&nbsp;</td>
               <td><input name="postcom" type="button" value="Post Comment" onclick="postComment();"></td>
            </tr>
         </table>
      </div>    	
         <input name="comment_cat" type="hidden" value="<?php echo $_POST['comment_cat']; ?>" />
         <input name="itemID" id="itemID" type="hidden" value="<?php echo $_POST['itemID']; ?>" />
         <input name="siteid" id="siteid" type="hidden" value="<?php echo defined("SITE_ID") ? SITE : $_POST['siteid']; ?>" />
         <input type="hidden" name="MM_insert" value="cmt_form" />
      	<input type="hidden" name="comment_date" id="comment_date" value="<?php echo date('Y-m-d'); ?>"  />
   </form>
</div>

<?php
mysql_free_result($rsComments);
?>
