// JavaScript Document

function hideMsg(field,msg) {
	if( field.value == msg ) {
		field.value = "";	
	}
}

function showCommentsForImage(imageID) {
	var poststr = "itemID=" + imageID + "&comment_cat=gallery";
	pageManager.ajax('../components/gallery/comment.php',poststr,'comment_area');
	showCommentsFor(imageID,'gallery');
}

function showCommentsFor(itemID, category) {
	var poststr = "itemID=" + itemID + "&comment_cat=" + category;
	pageManager.ajax('../components/gallery/comment.php',poststr,'comment_area');
}

function postComment() {
	var query = pageManager.buildQuery( document.getElementById("cmt_form") );
	pageManager.post('../components/gallery/comment.php',query,'comment_area');
}

function showNextImage() {
	var pos = ++currentImage;
	if(pos >= pics.length) {
		pos = pics.length - 1;
		currentImage = pos;
		alert("No More Photos In This Gallery. You May View Previous Images");
		clearLoadStatus();
		return;
	}
	
	showImageAt(pos);
}

function showPreviousImage()  {
	var pos = --currentImage;
	if(pos < 0) {
		pos = 0;
		currentImage = pos;
		alert("This Is The First Photo");
		clearLoadStatus();
		return;
	}
	
	showImageAt(pos);
}

function showImageAt(pos) {
	currentImage = pos;
	var imageData = pics[pos].split("|");
	showImage(imageData);
}

function showImage(imageData) {

	if(document.getElementById) {
		document.getElementById("loadStatus").style.display = "block";
		document.getElementById("photo").style.display = "none";
		
		if( imageData != null ) {
			document.getElementById("photo").src = imageData[2];
			document.getElementById("descript").innerHTML = imageData[1];
			document.getElementById("imageName").innerHTML = imageData[0];
			
			showCommentsForImage( imageData[3] );
		}
	}
}

function clearLoadStatus() {
	document.getElementById("loadStatus").style.display = "none";
	document.getElementById("photo").style.display = "block";
}

function showImageNavBar() {
	var navbar = document.getElementById("photo_navbar");
	
	for(var i = 0; i < pics.length; i++) {
		navbar.innerHTML = navbar.innerHTML + "<a href=\"#\" onclick=\"showImageAt(" + i + "); return false;\">" + (i+1) + "</a>";
		
		if(i < pics.length - 1) {
			navbar.innerHTML = navbar.innerHTML + " | ";
		}
	}
}

function checkAnswer() {
	// check supplied value in math field with desired answer
	if(document.getElementById("check").value == "" || document.getElementById("check").value != document.getElementById("checkval").value) {
		alert("The Math answer provided is incorrect");
		//alert("Item ID: " + document.getElementById("itemID").value);
		document.getElementById("check").focus();
		return false;
	}
	
	return true;
}

function showHideSection(secName) {
	if(document.getElementById( secName ).style.display == "none") {
		pageManager.show( secName );
	} else {
		pageManager.hide( secName );
	}
}