<?php

$db = $site->getDatabase();

$editFormAction = $_SERVER['PHP_SELF'];
if (isset($_SERVER['QUERY_STRING'])) {
   $editFormAction .= "?" . htmlentities($_SERVER['QUERY_STRING']);
}

$currentPage = $_SERVER["PHP_SELF"];

$maxRows_rsGalNames = 10;
$pageNum_rsGalNames = 0;
if (isset($_GET['pageNum_rsGalNames'])) {
   $pageNum_rsGalNames = $_GET['pageNum_rsGalNames'];
}
$startRow_rsGalNames = $pageNum_rsGalNames * $maxRows_rsGalNames;

$query_rsGalNames = sprintf("SELECT GalleryID, GalleryName FROM galleries WHERE SiteID = %s ORDER BY id DESC", $db->sanitizeInput(SITE_ID,'int'));
$query_limit_rsGalNames = sprintf("%s LIMIT %d, %d", $query_rsGalNames, $startRow_rsGalNames, $maxRows_rsGalNames);
$rsGalNames = mysql_query($query_limit_rsGalNames) or die(mysql_error());
$row_rsGalNames = mysql_fetch_assoc($rsGalNames);

if (isset($_GET['totalRows_rsGalNames'])) {
   $totalRows_rsGalNames = $_GET['totalRows_rsGalNames'];
} else {
   $all_rsGalNames = mysql_query($query_rsGalNames);
   $totalRows_rsGalNames = mysql_num_rows($all_rsGalNames);
}
$totalPages_rsGalNames = ceil($totalRows_rsGalNames/$maxRows_rsGalNames)-1;


$colname_rsGallery = "-1";
if (isset($_GET['gal'])) {
   $colname_rsGallery = (get_magic_quotes_gpc()) ? $_GET['gal'] : addslashes($_GET['gal']);
}

$query_rsGallery = sprintf("SELECT * FROM images WHERE GalleryID = '%s' ORDER BY id DESC", $colname_rsGallery);
$rsGallery = mysql_query($query_rsGallery) or die(mysql_error());
$row_rsGallery = mysql_fetch_assoc($rsGallery);
$totalRows_rsGallery = mysql_num_rows($rsGallery);

$maxRows_rsComments = 10;
$pageNum_rsComments = 0;
if (isset($_GET['pageNum_rsComments'])) {
   $pageNum_rsComments = $_GET['pageNum_rsComments'];
}
$startRow_rsComments = $pageNum_rsComments * $maxRows_rsComments;

?>
<link rel="stylesheet" type="text/css" href="css/comments.css"/>
<script type="text/javascript" src="<?php echo SCRIPTS_URL . 'common/pageManager.js'; ?>"></script>
<script type="text/javascript" src="scripts/comments.js"></script>
<script type="text/javascript" language="javascript">

   var pics = new Array();
   var comments = new Array();
   var currentImage = -1;

<?php if(isset($_POST['curImage'])) { ?>
   currentImage = <?php echo $_POST['curImage']; ?>;
   <?php }  ?>

<?php $count = 0;
do { ?>
   <?php if( isset($_GET['id']) && !isset($_POST['curImage']) && ($row_rsGallery['id'] == $_GET['id']) ) echo "currentImage = " . ($count - 1) . ";n"; ?>
      pics[<?php echo $count++  ; ?>] = "<?php echo $row_rsGallery['ImageName'] . '|' . $row_rsGallery['ImageDescription'] . '|' . $row_rsGallery['ImageFile'] . '|' . $row_rsGallery['id']; ?>";
   <?php } while ($row_rsGallery = mysql_fetch_assoc($rsGallery)); ?>



</script>
<div id="comp-page">
   <div>
      <div id="titlebar" style="font-family:Tahoma, Geneva, sans-serif; font-size: 21px; font-weight:bold;">
         Photo Gallery
      </div>

      <?php if( $totalRows_rsGallery > 0 || isset($_GET['gal']) ) { ?>
      <div id="comp-page-nav" style="float:right; width: 240px; padding: 5px; border: solid #ccc 1px; font-size: 11px; border-radius: 5px;">
         <div id="navigation_header">
            <strong>Available Galleries</strong>
         </div>

         <div id="navigation_content"><?php
               $count = 1;
               do {
                  if( $row_rsGalNames['GalleryID'] == $_GET['gal'] ) {
                     $gallery = $row_rsGalNames['GalleryName'];
                  } ?>
            <div style="padding-top:5px; padding-bottom: 5px; padding-left: 10px;">
                     <?php $galPath = $_SERVER['PHP_SELF'] . "?siteid=" . $_GET['siteid'] . "&page=" . $_GET['page'] . "&gal=" . $row_rsGalNames['GalleryID']; ?>
                     <?php echo $count . ". "; ?><a href="<?php echo $galPath; ?>" ><?php echo $row_rsGalNames['GalleryName']; ?></a>
            </div><?php
                  $count++  ;
               } while ($row_rsGalNames = mysql_fetch_assoc($rsGalNames)); ?>
         </div>

      </div>


      <div id="page_data" style="float: left; clear: left; width: 640px; padding: 10px;">
         <table width="98%" border="0" align="center" cellpadding="3" cellspacing="0">
            <tr>
               <td colspan="2" valign="top"> Welcome to the Photo Gallery. Select From the available galleries
                  on the left hand side to view the pictures in that section</td>
            </tr>
            <tr>
               <td colspan="2" align="center">&nbsp;</td>
            </tr>
            <tr>
               <td colspan="2" class="sectionTitle1" style="border-bottom-color: #CCC; border-bottom-style: dashed; border-bottom-width: 1px; font-size: 18px; font-weight: bold;"><?php echo $gallery; ?></td>
            </tr>
            <tr>
               <td width="402" height="30" align="left" class="navSec"><strong>Images:</strong>
                  <span id="photo_navbar">&nbsp;</span></td>
               <td width="261"  align="center" class="navSec">&nbsp;«
                  <a href="#" onclick="showImageAt(0); return false;">First</a>
                  &nbsp;&nbsp;
                  <a href="#" onclick="showPreviousImage(); return false;">Previous</a>
                  &nbsp;&nbsp;<a href="#" onclick="showNextImage(); return false;">Next</a>

                  <a href="#" onclick="showImageAt(pics.length - 1); return false;">&nbsp;&nbsp;Last</a>
                  &nbsp;»</td>
            </tr>
            <tr>
               <td colspan="2"><table width="98%" border="0" align="center" cellpadding="5" cellspacing="0">
                     <tr>
                        <td height="30" valign="bottom" class="course_section"><strong>Image:</strong>
                           <span id="imageName" class="course_text">Image Name Goes Here</span></td>
                     </tr>
                     <tr>
                        <td class="course_section"><strong>Description:</strong>
                           <span id="descript" class="course_insidetext">Image Description Comes Here</span></td>
                     </tr>
                     <tr>
                        <td height="360" align="center" valign="middle"><?php if(isset($_GET['gal'])) { ?>
                           <img src="../assets/images/general/loadcircles.gif" alt="" name="photo" id="photo" onload="clearLoadStatus();"/>
                           <div id="loadStatus" style="color: #006699 ; font-weight: bold; font-family:Verdana, Arial, Helvetica, sans-serif; font-size: 10px; position: relative;">
                              <img src="../assets/images/general/loadcircles.gif" alt="loading" width="16" height="16" align="absmiddle" /> Loading Image. Please Wait...<br />
                           </div>
                                 <?php } else { ?>
                           <div class="course_section">
                              Please Select A Gallery Category on the Left
                           </div>
                                 <?php } ?></td>
                     </tr>
                     <tr>
                        <td align="center" valign="middle">&nbsp;</td>
                     </tr>
                     <tr>
                        <td align="left" valign="middle"><div id="comment_area">
                              &nbsp;</div></td>
                     </tr>
                  </table></td>
            </tr>
         </table>
      </div>

      <script type="text/javascript" language="javascript">
         showNextImage();
         showImageNavBar();
      </script><?php

      } else if( !isset($_GET['gal']) ) { ?>

      <div id="page_data">
         Please select a gallery from one of the available galleries below to view the photos. (ordered by recent first)<br /><br />

         <div style="font-size: 15px; font-weight: bold; font-family:Tahoma, Geneva, sans-serif; border-bottom: dashed #ccc 1px; margin-bottom: 10px; padding-bottom: 5px;">
            Available Galleries
         </div><?php

            if( $totalRows_rsGalNames > 0 ) {
               $count = 1;
               do { ?>
         <div style="padding-top: 10px; padding-bottom: 20px; padding-left: 20px; font-size: 13px;">
                     <?php $galPath = (strpos($_SERVER['REQUEST_URI'], "?") > 0 ? "&" : "?") . "gal=" . $row_rsGalNames['GalleryID']; ?>
                     <?php echo $count . ". "; ?><a href="<?php echo $galPath; ?>" ><?php echo $row_rsGalNames['GalleryName']; ?></a>
         </div>
                  <?php $count++  ;
               } while ($row_rsGalNames = mysql_fetch_assoc($rsGalNames));
            } else {
               echo "No Photo Galleries Have Been Created For This Site. Please check back later!";
            }
            ?>
      </div>
      <?php } else { ?>
      
      <div id="page_data">
         No Photos Have Been Posted For This Gallery. Please check back later!
      </div><?php

      } ?>
   </div>
</div>

<?php
mysql_free_result($rsGalNames);

mysql_free_result($rsGallery);
?>
