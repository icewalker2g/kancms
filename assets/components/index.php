<?php 
/**
* This file serves several purposes:
* 1. Prevents direct access to this directory
* 2. Used to display a blank space when a component is not present
* 3. Should be included in all AJAX processing documents to preconfigure them to use 
     the KAN API. This should be done as include(COMPONENTS_PATH . 'index.php');
*/

/**
 * This file should be included in all theme AJAX files, to provide a quick way to get the configuration
 * and database connection information for the CMS
 */

// code will find configuration path
$currentPage = $_SERVER['PHP_SELF'];
$endPos = strpos($currentPage, "assets/components/");
$siteRoot = substr($currentPage, 0, $endPos);

// if the site object is not available, then we need to include the site_selector.php
// file to be able to determine which site to load the event(s) for
if( !isset($site) ) {
    include($_SERVER['DOCUMENT_ROOT'] . $siteRoot . 'core/SiteLoader.php');
}

if( !isAjax() ) {
   echo '<div class="missing_component"></div>';
}

function isAjax() {
	return (isset($_SERVER['HTTP_X_REQUESTED_WITH']) && 
	($_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest'));
}
?>

