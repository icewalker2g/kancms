<?php 

echo $site->script('jquery/js/jquery.js');
echo $site->component->css('sliderviewer.css');
echo $site->component->script('jquery.easing.1.3.js');
echo $site->component->script('jquery.sliderviewer.js');

$manager = new GalleryManager();
$galleries = $manager->getGalleries(); ?>

<div id="comp-page">
	
    <div id='<?php echo !isset($_GET['gal']) ? "gallery-list" : "gallery-list-vertical"; ?>'>
        <ul><?php
			
			for( $j = 0; $j < count($galleries); $j++ ) {
				
				if( !$galleries[$j]->isPublished() )  continue;
				
				$url = $_SERVER['REQUEST_URI'];
				$url .= (strpos($url, '?')) ? "&" : "?"; 
				$url .= "gal=" . $galleries[$j]->getAlias();

				$galName = $galleries[$j]->getName();
				$images = $galleries[$j]->getImages(0,1);
                
                if( count($images) > 0 ) {

                    echo "<li>
                            <a href='$url' title='$galName'>
                                <div class='img_block'><img src='{$images[0]->getThumbImageURL()}' /></div>
                                <div class='title_block'>$galName</div>
                            </a>
                          </li>";	
                }
			}
        ?></ul>
    </div><?php
	
	if( isset($_GET['gal']) ) {
	
		$gallery = $manager->getGallery( kan_get_parameter('gal') ); ?>
		
		<h2><?php echo $gallery->getName(); ?></h2>
        <div class="description"><?php echo nl2br($gallery->getDescription()); ?></div>
		
		<div id='gal-content'>
			<div id="<?php echo $gallery->getAlias(); ?>" class="svw">
				<ul><?php
					
					$images = $gallery->getImages();
					
					for( $j = 0; $j < count($images); $j++ ) {
						echo "<li><img alt='{$images[$j]->getDescription()}' src='{$images[$j]->getImageURL()}' /></li>";	
					}
				?></ul>
			</div>
			<script type="text/javascript">
				$(window).bind("load",function(){
					$(<?php echo "'#". $gallery->getAlias() . "'"; ?>).slideView({toolTip: true, ttOpacity: 0.9})
				});
			</script>
		</div>
		<?php
	}
?>
</div>
