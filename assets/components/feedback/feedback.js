// JavaScript Document

var feedback = {

	submissionURL: null,
	contentArea: null,

	init: function(params) {
		
		if( params.url ) {
			feedback.submissionURL = params.url;	
		}
		
		if( params.area ) {
			feedback.contentArea = params.area;	
		}
		
		var form = document.getElementById("feedback_form");
		
		if( form != null ) {
			
			form.onsubmit = function() {
				var valid = $p(this).validate({
					'Category': "Feedback Category",
					"client_name": "Your Name",
					"client_email" : "Your Email",
					"client_phone" : "Your Phone Number",
					"client_message" : "Your Message"
				});
				
				if( valid ) {
					$p.all("#feedback_form input, #feedback_form textarea", function(el) {
						el.disabled = true;
					});
					
					var query = "ajax=true&" + $p(this).serialize();
					$p.post( feedback.submissionURL, query, function(data) {
						$p(feedback.contentArea).html(data);
						
						$p.all("#feedback_form input, #feedback_form textarea", function(el) {
							el.disabled = false;
						});

						$p("#feedback_form input[type=text], #feedback_form textarea").val("");

					});
				}
				
				return false;
			};	
		} 
	}
};