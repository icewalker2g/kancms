<?php
//
if (!isset($site)) {
    include_once('../index.php');
}

// create an instance of the Feedback Management class
$fm = new FeedbackManager();

// if the submit button has been posted, then we know can process the information
// secondly, we'll only process the submitted information if it was sent via
// the AJAX form submission, else do nothing for now
if (isset($_POST['submit']) && isset($_POST['ajax'])) {

    $error = "";
    if ($_POST['client_name'] == '') {
        $error .= "Client Name Missing <br />";
    }

    // try to clean up user input
    $name = kan_clean_input($_POST['client_name']);
    $email = kan_clean_input($_POST['client_email']);
    $phone = kan_clean_input($_POST['client_phone']);

    // attempt to clean up message and add the necessary allowed data
    // also include line breaks
    $message = kan_clean_input($_POST['client_message']);

    // create the subject line
    $subject = "Feedback From $name";

    $category = isset($_POST['Category']) ? $_POST['Category'] : 'general';
    $fm->addFeedback($category, $name, $email, $phone, $subject, $message);

    // if there is no error, inform the user of a sucessful message submission
    echo '<div id="fb_success" style="padding: 5px; border: solid #ccc 1px; background-color: #ffd; font-weight: bold;">';
    echo 'Your Message Has Been Sucessfully Submittted. You will receive response within 3 working days.</div>';

    if (isset($_POST['ajax'])) {
        return;
    }
}

// get a list of all the feedback categories for this site
$categories = $fm->getFeedbackCategories($site->getSiteID());

?>


<div id="feedform_area">
    <div id="ajax_result"></div>
    <form id="feedback_form" name="feedback_form" action="" enctype="application/x-www-form-urlencoded" method="post">
        <table width="100%" border="0" cellspacing="0" cellpadding="3">
            <tr>
                <td align="right" nowrap="nowrap">
                    Category:
                </td>
                <td width="445">
                    <select name="Category" id="Category" class="required">
                        <option value="">Select Feedback Category</option>
                        <option value="">------------------------</option>
                        <?php
                        for ($i = 0; $i < count($categories); $i++) {
                            $categoryTag = $categories[$i]->getData('CategoryTag');
                            $categoryName = $categories[$i]->getData('CategoryName');

                            echo "<option value='$categoryTag'>$categoryName</option>";
                        } ?>
                    </select>
                </td>
            </tr>
            <tr>
                <td width="138" align="right">
                    Name:
                </td>
                <td>
                    <input name="client_name" type="text" id="client_name" class="required" size="35" />
                </td>
            </tr>
            <tr>
                <td align="right">
                    Email:
                </td>
                <td>
                    <input name="client_email" type="text" id="client_email" class="email" size="35" />
                </td>
            </tr>
            <tr>
                <td align="right">
                    Phone:
                </td>
                <td>
                    <input name="client_phone" type="text" id="client_phone" class="required" size="35" />
                </td>
            </tr>
            <tr>
                <td align="right" valign="top">
                    Request:
                </td>
                <td>
                    <textarea name="client_message" id="client_message" class="required" cols="40" rows="8" ></textarea>
                </td>
            </tr>
            <tr>
                <td align="right" valign="top">&nbsp;
                </td>
                <td>&nbsp;
                </td>
            </tr>
            <tr>
                <td align="right" valign="top">&nbsp;
                </td>
                <td>
                    <input type="hidden" name="siteid" id="siteid" value="<?php echo $site->getSiteIdentifier(); ?>"  />
                    <input type="submit" name="submit" id="submit" value="Send Request" />
                    <input type="reset" name="reset" id="reset" value="Reset" />
                </td>
            </tr>
            <tr>
                <td align="right" valign="top">&nbsp;
                </td>
                <td>&nbsp;
                </td>
            </tr>
        </table>
    </form>
</div>

<?php 
echo $site->script('common/pageManager.js');
echo $site->component->script("feedback.js");
?>
<script type="text/javascript">
    pageManager.addLoadEvent( function() {
        feedback.init({
            url: '<?php echo $site->component->url("feedback.php"); ?>',
            area: '#ajax_result'
        });
    });
</script>