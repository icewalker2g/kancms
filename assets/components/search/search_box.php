<?php 

// if a url has not been passed, then make a url for use
if( !isset($url) ) {
	$url = defined("SEF_URLS") && SEF_URLS ? $site->getHomePageURL() . "p/search_results" : $site->getHomePageURL();
}
?>

<form action="<?php echo $url; ?>" method="get" enctype="application/x-www-form-urlencoded">
	<div class="widget-box">
    	<div class="search-area">
        	<label for="q">Search:</label>
            <input type="text" size="20" name="q" id="search-field" placeholder="Search Here" autocomplete="true" value="<?php echo isset($_GET['q']) ? $_GET['q'] : ''; ?>" />
            <input type="submit" name="go" id="search-go-btn" value="GO" />
            <input type="hidden" name="siteid" id="siteid" value="<?php echo isset($_GET['siteid']) ? $_GET['siteid'] : ""; ?>" />
            <input type="hidden" name="page" id="page-field" value="search_results" />
        </div>
        <div class="search-engines" style="text-align:center"><?php 
			$google = "www.google.com/search"; $google_sel = "";
			$yahoo = "search.yahoo.com/search"; $yahoo_sel = "";
			$bing = "www.bing.com/search"; $bing_sel = "";
			
			$search_engine = isset($_GET['search-engine']) ? $_GET['search-engine'] : "";
			
			switch($search_engine) {
				case $google:
					$google_sel = "checked='checked'";	
					break;		
				case $yahoo:
					$yahoo_sel = "checked='checked'";
					break;
				case $bing:
					$bing_sel = "checked='checked'";
					break;
				default:
					$google_sel = "checked='checked'";
					break;
			}
			?>
        	<input type="radio" value="<?php echo $google; ?>" name="search-engine" <?php echo $google_sel; ?>   /> Google
            <input type="radio" value="<?php echo $yahoo; ?>" name="search-engine" <?php echo $yahoo_sel; ?> /> Yahoo
            <input type="radio" value="<?php echo $bing; ?>" name="search-engine" <?php echo $bing_sel; ?> /> Bing
        </div>
    </div>
</form>