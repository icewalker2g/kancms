<div id="comp-page">
  <?php 
    $q = isset($_GET['q']) ? urlencode($_GET['q']) : '';
    $searchEngine = isset($_GET['search-engine']) ? urldecode($_GET['search-engine']) : "www.google.com/search";
    $page = sprintf("http://%s?q=%s", $searchEngine, $q);

    if( $_SERVER['HTTP_HOST'] != "localhost" ) {
      $page = sprintf("http://%s?q=site:%s+%s", $searchEngine, $_SERVER['HTTP_HOST'], $q) ;
    }
  ?>

  <iframe src="<?php echo $page; ?>" width="100%" height="1600" frameborder=0 ></iframe>
</div>