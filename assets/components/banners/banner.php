<?php 

if( !defined('CORE_PATH') ) {
	echo "Component Failed To Load Due To Missing Configuration Information";
	return;
}

$banners = new BannerManager();
$banner = $banners->randomlySelect();

$scriptPath = '';
if( count($banner) > 0 ) { // banner object has information
	$scriptPath = substr($banner['BannerPath'], 0, strpos($banner['BannerPath'], ".", 5));
	
} else {
	return; // don't bother rendering the component	
}
?>
<?php echo $site->component->script('AC_RunActiveContent.js'); ?>

<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" style="margin:0px;">
    <?php if( $banner['BannerType'] == "swf" ) { ?>
    <tr>
        <td align="center"> 
            <script type="text/javascript">
				AC_FL_RunContent( 'codebase','http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=9,0,28,0','width','<?php echo $banner['Width']; ?>','height','<?php echo $banner['Height']; ?>','src','<?php echo $scriptPath; ?>','quality','high','pluginspage','http://www.adobe.com/shockwave/download/download.cgi?P1_Prod_Version=ShockwaveFlash','wmode','transparent','movie','<?php echo $scriptPath; ?>' ); //end AC code
            </script>
            <noscript>
            <object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=9,0,28,0" width="<?php echo $banner['Width']; ?>" height="<?php echo $banner['Height']; ?>">
                <param name="movie" value="<?php echo $banner['BannerPath']; ?>">
                <param name="quality" value="high">
                <param name="wmode" value="transparent">
                <embed src="<?php echo $banner['BannerPath']; ?>" width="<?php echo $banner['Width']; ?>" height="<?php echo $banner['Height']; ?>" quality="high" pluginspage="http://www.adobe.com/shockwave/download/download.cgi?P1_Prod_Version=ShockwaveFlash" type="application/x-shockwave-flash" wmode="transparent"></embed>
            </object>
            </noscript>
        </td>
    </tr>
    <?php } else { ?>
    <tr>
        <td align="center"> 
        	<a href="<?php echo $banner['BannerURL']; ?>" title="<?php echo $banner['BannerDesc']; ?>">
            	<img src="<?php echo $banner['BannerPath']; ?>" alt="" name="banner" width="<?php echo $banner['Width']; ?>" height="<?php echo $banner['Height']; ?>" border="0"/>
            </a> 
        </td>
    </tr>
    <?php } ?>
</table>
