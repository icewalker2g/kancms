 <style type="text/css">
  .site_map, .site_map ul {
    padding-left: 20px;
  }
</style>
<?php 
   function listPages($pages) {
      
     foreach($pages as $p) {
       echo "<li>" . $p->getLinkedName() . "</li>";
       
       if( $p->hasSubPages() ) {
          echo "<ul>";
          listPages( $p->getSubPages() );
          echo "</ul>";
       }
     }
   }
  
   $pages = $site->getPages( array('parent' => '' ));
  
   echo "<ul class='site_map'>";
   listPages( $pages );
   echo "</ul>";
?> 