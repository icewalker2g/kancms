<?php 
/**
 * To use this component, the following css information needs to be defined in your
 * style sheet in order to configure the component appearance
 * 
 * 1. #jump_menus
 * 2. #jump_menus #menus_header
 * 3. #jump_menus .jump_header
 * 4. #jump_menus .jump_content
 * 5. #jump_menus .jump_content select (optional)
 */

kan_import('LinksManager');

$lm = new LinksManager();
$cats = $lm->getLinkCategories();
?>

<div id="jump_menus">
	<?php 
		for( $i = 0; $i < count($cats); $i++ ) { 
			$cat = $cats[$i]; ?>
            
            <div class="jump_header"><?php echo $cat->getCategoryName(); ?></div>
            <div class="jump_content">
                <select name="colleges" style="width:100%;" onchange="location.href = this.value">
                    <option value="<?php echo $_SERVER['PHP_SELF']; ?>" selected="selected">Select <?php echo $cat->getCategoryName(); ?></option>
                    <option value="<?php echo $_SERVER['PHP_SELF']; ?>">- - - - - - - - - - - - - - -</option><?php 
                
                    $links = $lm->getLinksInCategory( $site->getSiteID(), $cat->getCategoryID() ); 
                    
                    for( $j = 0; $j < count($links); $j++) {
						$link = $links[$j]; ?>        	
                    	<option title="<?php echo $link->getDescription(); ?>" value="<?php echo $link->getURL(); ?>"><?php echo $link->getName(); ?></option><?php
                    } ?>
                
                </select>
            </div><?php 
		} ?>
</div>