<?php 
/**
 * To use this component, the following css information needs to be defined in your
 * style sheet in order to configure the component appearance
 *
 * 1. #ql_sidebar
 * 2. #ql_sidebar .ql_group
 *	3. #ql_sidebar .ql_group .ql_header
 * 4. #ql_sidebar .ql_group .ql_item
 */
kan_import('LinksManager');

$lm = new LinksManager();
$cats = $lm->getLinkCategories();
?>
<div id="ql_sidebar"><?php 
	for( $i = 0; $i < count($cats); $i++ ) { 
		$cat = $cats[$i]; ?>
		
		<div class="ql_header"><?php echo $cat->getCategoryName(); ?></div>
		<div class="ql_group"><?php
			$links = $lm->getLinksInCategory( $site->getSiteID(), $cat->getCategoryID() ); 
				
			for( $j = 0; $j < count($links); $j++) {
				$link = $links[$j]; ?>        	
				<div class="ql_item" ><a href="<?php echo $link->getURL(); ?>" title="<?php echo $link->getDescription(); ?>"><?php echo $link->getName(); ?></a></div><?php
			} ?>
		</div><?php 
	} ?>
</div>
