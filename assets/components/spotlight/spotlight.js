// JavaScript Document

var faderDiv;
var faderObj;
var faderSpeed = 10;
var faderStyle = "OUT";
var faderAmount = 100;
var barTimer;
var showTimer;

var spotlightCount = 1;

var oldSpotlight;
var oldSpotlightMenu;
var oldSpotlightInfo;

function displaySpotlightBar() {
    var secEl = document.getElementById( "spotlight_info" );
	 
    if(secEl.style.display == "none") {
        pageManager.show( "spotlight_info" );
    } else {
        pageManager.hide( "spotlight_info" );
    }
}

function setSpotlightCount(count) {
    spotlightCount = count;
}

function showSpotlightBar(style) {
    this.faderStyle = style;
    this.faderObj = document.getElementById("spotBar");
    if( faderStyle == "IN" ) {
        clearTimeout( barTimer );
        pageManager.show("spotBar");
    } else {
        barTimer = setTimeout( "pageManager.hide('spotBar')", 2000 );
    }
}

function showSpotlight(pos) {
    if( oldSpotlight != null ) {
        pageManager.fadeOut( oldSpotlightMenu, function() {
			pageManager.fadeOut( oldSpotlight );	
		});
        //oldSpotlightMenu.className = "spotlight_menu";
        //pageManager.hide(oldSpotlightInfo);
    }
	
    if( pos > spotlightCount ) {
        pos = 1;
    }
	
    var spotlight = document.getElementById("spotlight_item" + pos);
    var spotlightMenu = document.getElementById("spotlight_menu" + pos);
	
	if( spotlight != null ) {
		pageManager.fadeIn("spotlight_item" + pos, function() {
			pageManager.fadeIn("spotlight_menu" + pos);
		});
		
		oldSpotlight = "spotlight_item" + pos;
        oldSpotlightMenu = "spotlight_menu" + pos;
	}
	
    if( showTimer != null ) {
        clearTimeout(showTimer);
    }
	
    if( spotlightCount > 1 ) {
        showTimer = setTimeout("showSpotlight(" + (++pos) + ")", 20000);
    }
}

function startSpotlightShow() {
    showSpotlight(1);
	
    if( spotlightCount > 1 ) {
        setSpotlightVisible( true );
    } else {
        setSpotlightVisible( false );
    }
}

function setSpotlightVisible(vi) {
    if( document.getElementById("spotlight_menubar") != null ) {
        document.getElementById("spotlight_menubar").style.visibility = (vi) ? "visible" : "hidden";
    } 
}