<?php 

// generate the HTML and CSS needed to render the spotlight component
echo $site->script('common/pageManager.js');
echo $site->component->css('spotlight.css');
echo $site->component->script('spotlight.js');

// retrieve 4 available spotlight images via the site object
$spotlights = $site->getSpotlights(0, 4);
?>

<div id="spot_container">

    <div id="spotlight"><?php 
		$count = 1;
		
		foreach($spotlights as $spotlight) {
			
			echo "<div id='spotlight_item{$count}' class='spotlight_item'>";
			echo $spotlight->render( array('id' => "spot-$count", 'class' => 'my_class' ));
			echo "</div>";
			
			$count++;
		} ?>
    </div>
    <div id="spotlight_menubar">
      	<div class="menu_bg"></div>
        <?php 
			$count = 1;
			
			foreach( $spotlights as $spotlight ) { ?>
                <div id="spotlight_menu<?php echo $count; ?>" class="spotlight_menu" onclick="showSpotlight(<?php echo $count; ?>)">
                    <div class="menu_header"><?php echo $spotlight->getTitle(); ?></div>
                    <div class="menu_subheader"><?php echo $spotlight->getSubTitle(); ?></div>
                    <div class="menu_description"><?php echo $spotlight->getDescription(); ?></div>
                </div><?php 
				$count++; 
			}
		?>
    </div>
</div>
<script type="text/javascript">
	setSpotlightCount(<?php echo count($spotlights); ?>);
	startSpotlightShow();
</script>
