<?php

class Helper {

    /**
     * List of helpers used by this helper
     *
     * @var array
     */
    protected $helpers = array();

	protected $controller = null;

    public function __construct(&$controller = NULL) {
		
		$this->controller = $controller;

        // initiate the helper classes available for use by this Model
        if ($this->helpers != NULL) {
            foreach ($this->helpers as $helper) {
                if (class_exists($helper) && is_subclass_of($helper, 'Helper')) {
                    $this->{$helper} = new $helper($this->controller);
                }
            }
        }
    }

}

?>