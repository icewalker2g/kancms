<?php


/**
 * Description of Assets
 *
 * @author Francis Adu-Gyamfi
 */
class Assets extends Helper {
    
    protected $helpers = array('HTML');
    
    protected function baseURL() {
        return (SITE_ROOT . "assets/");
    }
    
    protected function basePath() {
        return (BASE_PATH . 'assets/');
    }


    public function script($path) {
        return $this->HTML->renderScript( $this->baseURL() . "scripts/", $path);
    }
    
    public function image($url, $options = NULL) {
        return $this->HTML->renderImage( $this->baseURL() . 'images/', $url, $options);
    }
    
    /*public function renderComponent($component) {
        
    }
    
    public function renderMedia() {
        
    }*/
}

?>
