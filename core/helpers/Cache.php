<?php

if (!defined('CORE_PATH'))
    die('Access Denied');

/**
 * Description of Cache
 *
 * @author Francis Adu-Gyamfi
 */
class Cache extends Helper {

    public function beginCaching() {

        /**
         * Begin Caching Script, courtesy of 
         * http://www.addedbytes.com/articles/caching-output-in-php/
         */
        $cacheEnabled = getSetting('CacheEnabled', false);

        // we'll only run the caching when the request method is a GET, hence when users
        // POST data we will skip caching all together and allow the scripts to process the requests
        // normally
        if ($_SERVER['REQUEST_METHOD'] == 'GET' && $cacheEnabled == 'true') {

            // Settings
            $cachedir = kan_fix_path('../assets/cache/'); // Directory to cache files in (keep outside web root)

            if (!is_dir($cachedir)) {
                @mkdir($cachedir);
            }

            $cachetime = 600; // Seconds to cache files for
            $cacheext = 'cache'; // Extension to give cached files (usually cache, htm, txt)
            $siteid = isset($_GET['siteid']) ? kan_clean_input($_GET['siteid']) : "";

            // Ignore List
            $ignore_list = array(
                'xxx'
            );

            // Script
            $page = 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']; // Requested page
            $cachefile = $cachedir . $siteid . '_' . md5($page) . '.' . $cacheext; // Cache file to either load or create

            $ignore_page = false;
            for ($i = 0; $i < count($ignore_list); $i++) {
                $ignore_page = (strpos($page, $ignore_list[$i]) !== false) ? true : $ignore_page;
            }

            $cachefile_created = ((@file_exists($cachefile)) && ($ignore_page === false)) ? @filemtime($cachefile) : 0;
            @clearstatcache();

            // Show file from cache if still valid
            if (time() - $cachetime < $cachefile_created) {

                //ob_start('ob_gzhandler');
                @readfile($cachefile);
                //ob_end_flush();
                exit();
            }

            // If we're still here, we need to generate a cache file
            ob_start();
        }
    }

    public function endCaching() {
        /**
         * End Caching Script, courtesy of 
         * http://www.addedbytes.com/articles/caching-output-in-php/
         */
        // we'll only run the caching when the request method is a GET, hence when users
        // POST data we will skip caching all together and allow the scripts to process the requests
        // normally 
        $cacheEnabled = getSetting('CacheEnabled', false);

        if ($_SERVER['REQUEST_METHOD'] == 'GET' && $cacheEnabled == 'true') {
            $cachedir = kan_fix_path('../assets/cache/'); // Directory to cache files in (keep outside web root)
            $cacheext = 'cache'; // Extension to give cached files (usually cache, htm, txt)
            $page = 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']; // Requested page

            $siteid = isset($_GET['siteid']) ? kan_clean_input($_GET['siteid']) : "";
            $cachefile = $cachedir . $siteid . '_' . md5($page) . '.' . $cacheext; // Cache file to either load or create
            // Now the script has run, generate a new cache file
            $fp = @fopen($cachefile, 'w');

            $cache_track = "\n<!-- Cache Of $page -->";
            $contents = ob_get_contents() . $cache_track;
            // save the contents of output buffer to the file
            @fwrite($fp, $contents);
            @fclose($fp);

            ob_end_flush();
        }
    }

    /**
     * Empties the current system cache completely, or only for the site with
     * the specified tag if supplied
     *
     * @param string $site_tag  the Unique Identifier for the site
     */
    public function clearCache($site_tag = '') {
        // Settings
        $cachedir = kan_fix_path('../assets/cache/'); // Directory to cache files in 
        $handle = @opendir($cachedir);

        if ($handle) {

            while (false !== ($file = @readdir($handle))) {
                if ($file != '.' and $file != '..') {
                    if ($site_tag == '') {
                        //echo $file . ' deleted.<br>';
                        @unlink($cachedir . '/' . $file);
                    } else if (preg_match("/^" . $site_tag . "/", $file)) {
                        //echo $file . ' deleted.<br>';
                        @unlink($cachedir . '/' . $file);
                    }
                }
            }

            @closedir($handle);
        }
    }

}

?>
