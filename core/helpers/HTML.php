<?php

class HTML extends Helper {

    /**
     * Renders an HTML Anchor tag with the name and url specified
     * 
     * @param type $name
     * @param type $url
     * @param type $options
     * @return string 
     */
    public function renderLink($name, $url, $options = NULL) {

        if (!is_string($name) || !is_string($url)) {
            trigger_error("Name Or URL not in correct type format");
            return "";
        }

        $attributes = array('href' => $url);

        if ($options != NULL && is_array($options)) {
            $attributes = array_merge($attributes, $options);
        }

        return $this->renderTag("a", $attributes, $name);
    }

    /**
     * Renders an HTML Image Tag with the specified URL
     * 
     * @param string $url
     * @param array $options
     * @return string 
     */
    public function renderImage($url, $options = NULL) {

        if (is_null($url)) {
            return "";
        }

        if (!is_string($url) && !is_null($url)) {
            trigger_error("URL must be a string");
            return "";
        }

        $attributes = array('src' => $url, 'border' => 0, 'alt' => "");
		
		$path = kan_fix_path($url);
		
		if( file_exists($path) ) {
			list($width, $height, $type, $attr) = getimagesize( $path );
			
			if( isset($width) && isset($height) ) {
				$attributes['width'] = $width;	
				$attributes['height'] = $height;
			}
		}

        if ($options != NULL && is_array($options)) {
            $attributes = array_merge($attributes, $options);
        }
		


        return $this->renderTag("img", $attributes);
    }

    /**
     *
     * @param string $base
     * @param string $path
     * @param array $options
     * @return string 
     */
    public function renderScript($base, $path, $options = NULL) {

        if (is_array($path)) {
            $output = '';
            foreach ($path as $s) {
                $output .= "\n\t" . $this->renderScript($base, $s);
            }

            return $output;
        }

        if (!is_string($path))
            return;

        $pos = strrpos($path, ".js");

        if (!$pos || $pos != strlen($path) - 3) {
            $path .= ".js";
        }

        $path = $base . $path;
        $attributes = array("type" => "text/javascript", "src" => $path);

        if (!is_null($options) && is_array($options)) {
            $attributes = array_merge($attributes, $options);
        }

        return $this->renderTag("script", $attributes) . "\n";
    }

    /**
     *
     * @param string $base
     * @param string $path
     * @param array $options
     * @return string 
     */
    public function renderCSS($base, $path, $options = NULL) {

        if (is_array($path)) {
            $output = '';
            foreach ($path as $s) {
                $output .= "\n\t" . $this->renderCSS($base, $s);
            }

            return $output;
        }

        if (!is_string($path))
            return;

        $pos = strrpos($path, ".css");

        if (!$pos || $pos != strlen($path) - 4) {
            $path .= ".css";
        }

        $path = $base . $path;
        $attributes = array("href" => $path, "type" => "text/css", "rel" => "stylesheet");

        if (!is_null($options)) {
            $attributes = array_merge($attributes, $options);
        }

        return $this->renderTag("link", $attributes) . "\n";
    }

    /**
     * Renders an HTML tag with the specified tagname and attributes.
     * 
     * @param string $tagname
     * @param array $attributes
     * @param string $text
     * @return string 
     */
    private function renderTag($tagname, $attributes, $text = "") {

        $no_text_tags = array("img", "br", "link", "meta", "hr", "input");

        $tagname = strtolower($tagname);
        $tag = "<" . $tagname . " ";

        foreach ($attributes as $key => $value) {
            $tag .= "$key=\"$value\" ";
        }

        if (in_array($tagname, $no_text_tags)) {
            $tag .= " />";
        } else {
            $tag .= ">" . $text . "</$tagname>";
        }

        return $tag;
    }

    /**
     * Returns text without all the HTML characters encoded or removed
     * 
     * @param string $text
     * @return string
     */
    private function plain($text) {
        if (is_null($text)) {
            $text = "";
        }

        return htmlspecialchars($text, ENT_QUOTES, 'UTF-8');
    }

    /**
     * Returns text with all HTML tags removed and text HTML encoded
     * 
     * @param string $text
     * @return string 
     */
    private function clean($text) {
        return nl2br($this->plain(strip_tags(urldecode($text))));
    }
	
	public function url($params = array()) {
		
		$path = CMS_URL . "index.php";
		
		if( is_array($params) ) {
			
			if( isset($params['controller']) ){
				$path .= "?page=" . $params['controller'];
				unset($params['controller']);
			}
			
			else {
				$controller = get_class($this->controller);
				$controller = str_replace(array('Module','Controller'), '', $controller);
				
				$path .= "?page=" . strtolower($controller);	
			}
			
			if( isset($params['action']) ) {
				$path .= "&action=" . $params['action'];
				unset($params['action']);
			}
			
			// append any extra parameters
			foreach($params as $param => $value) {
				$path .= "&" . $param . "=" . urlencode($value);
			}
		}
		
		return $path;
	}

}

?>