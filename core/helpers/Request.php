<?php

/**
 * Description of Request
 *
 * @author Francis Adu-Gyamfi
 */
class Request extends Helper {

    function isAjax() {
        return (isset($_SERVER['HTTP_X_REQUESTED_WITH']) && ($_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest'));
    }

}

?>
