<?php 

class SettingCategory extends Model {

	private $Setting = NULL;

	public function __construct($data = NULL) {
		parent::__construct($data, 'system_settings_categories');	
		$this->Setting = new Setting();
	}	
	
	public function getId() {
		return $this->getData('id');	
	}
	
	public function getCategoryName() {
		return $this->getData('CategoryName');	
	}
	
	public function getCategoryDescription() {
		return $this->getData('CategoryDescription');	
	}
	
	public function getSettings() {
		return $this->Setting->find( array(
			'filter' => array("SettingCategory" => $this->getId())
		));
	}
}

?>