<?php

if( !defined('CORE_PATH')) die('Access Denied');

kan_import('Media');

class MediaCategory extends Model {
    
    /**
     * Media object instance
     * @var Media
     */
    private $Media = NULL;
    
    public function __construct($data = NULL) {
        parent::__construct($data, 'media_categories');
        $this->Media = new Media();
    }
    
    /**
     * Returns an array of Media objects belonging to this category
     * 
     * @return Media[]
     */
    public function getMedia($start = 0, $limit = NULL) {

        return $this->Media->find( array(
            'filter' => array('CategoryID' => $this->getId()),
            'offset' => $start,
            'limit' => $limit
        ));
    }
    
    public function getMediaCount() {

        return $this->Media->count( array(
            'filter' => array('CategoryID' => $this->getId())
        ));
    }
    
    /**
     * Deletes this MediaCategory and all related media files and records in the
     * process
     */
    public function delete() {
        
        $this->Media->deleteAll(array('CategoryID' => $this->getId()));
        
        parent::delete();
    }
    
    public function deleteAll($conditions = array()) {
        $categories = $this->find(array(
            'filter' => $conditions
        ));
        
        foreach($categories as $category) {
            $category->delete();
        }
    }
}

?>
