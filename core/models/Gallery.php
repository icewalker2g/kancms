<?php

if( !defined('CORE_PATH')) die('Access Denied');

/**
 * Represents a record from Gallery table
 */
class Gallery extends Model {

    private $GalleryImage = NULL;

    public function __construct($data = NULL) {
        parent::__construct($data, 'galleries');
        $this->GalleryImage = new GalleryImage();
    }

    public function getId() {
        return $this->getData('id');
    }

    public function getSiteID() {
        return $this->getData('SiteID');
    }

    public function getName() {
        return $this->getData('GalleryName');
    }
	
	public function getLinkedName($options = NULL) {
		$url = $this->getURL();
		$name = $this->getName();
		
		return $this->HTML->renderLink($name, $url, $options);	
	}

    public function getDescription() {
        return $this->getData('GalleryDescription');
    }

    public function getAlias() {
        return $this->getData('GalleryID');
    }

    public function getFolder() {
        return $this->getData('GalleryFolder');
    }

    public function allowsComments() {
        return $this->getData('AllowComments');
    }

    public function getImageSortOrder() {
        return $this->getData('ImageSortOrder');
    }

    public function getImageWidth() {
        return $this->getData('ImageWidth');
    }

    public function getThumbnailWidth() {
        return $this->getData('ThumbnailWidth');
    }

    public function isPublished() {
        return $this->getData('Published') == 1;
    }

    public function getImage($id) {
        return new GalleryImage($id);
    }

    public function getImages($start = 0, $total = NULL) {
		$images = NULL;
		
        if ($this->getImageSortOrder() != "RAND") {
            $images = $this->GalleryImage->find(array(
                'filter' => array("GalleryID" => $this->getId()),
                'order' => array("id" => $this->getImageSortOrder()),
				'limit' => $total,
				'offset' => $start
            ));
        } else {
            $images = $this->GalleryImage->find(array(
                'filter' => array("GalleryID" => $this->getId()),
                'order' => "rand()",
				'limit' => $total,
				'offset' => $start
            ));
        }
		
		return $images;
    }

    /**
     * Returns the total number of images in this gallery
     *
     * @return int total number of images
     */
    public function getImageCount() {

        return $this->GalleryImage->count(array(
            'filter' => array("GalleryID" => $this->getId())
        ));
    }
	
	/**
	 * Returns the URL to the Article to be displayed. If an external URL has been
	 * specified, that will be returned, else an internal URL will be built and returned
	 */
    public function getURL() {
		
		$tag = $this->getAlias();
		$url = "../pages/gallery.php?siteid=" . SITE_TAG . "&gallery=" . $tag;
		
		if (SEF_URLS) {
            $url = "../" . SITE_TAG . "/gallery/" . $tag . "/";
        }
		
		if( defined("IN_CMS") && IN_CMS ) {
			return $url;	
		}

        return kan_fix_url($url);
    }

}
?>
