<?php
if( !defined('CORE_PATH')) die('Access Denied');

kan_import('Gallery');

/**
 * Represents a record from the gallery_images table
 */
class GalleryImage extends Model {

	protected $Gallery = NULL;
	protected $helpers = array('HTML');
	
    public function __construct($data = NULL, $gallery = NULL) {
        parent::__construct($data, 'gallery_images');
		
		if( $gallery != NULL && $gallery instanceof Gallery ) {
			$this->Gallery = $gallery;	
		}
    }

    public function getId() {
        return $this->getData('id');
    }
    
	/**
	 * Returns the Gallery object for this image
	 * @return Gallery
	 */
    public function getGallery() {
		if( $this->Gallery == NULL ) {
			$this->Gallery = new Gallery( $this->getData('GalleryID') );	
		}
		
		return $this->Gallery;
    }
	
	/**
	 * @param $gallery
	 */
	public function setGallery($gallery = NULL) {
		if( $gallery instanceof Gallery ) {
			$this->Gallery = $gallery;	
		} else {
			trigger_error("Supplied parameter must be an instanceof a Gallery");	
		}
	}

    public function getAlias() {
        return $this->getGallery()->getAlias();
    }

    public function getName() {
        return $this->getData('ImageName');
    }

    public function getDescription() {
        return $this->getData('ImageDescription');
    }

    public function getImageURL() {
        return kan_fix_url($this->getData('ImageURL'));
    }

    public function getThumbImageURL() {
        return kan_fix_url($this->getData('ImageThumbURL'));
    }

	/**
	 * Renders the spotlight as an image for viewing. The function takes an array
	 * of <IMG> attributes as key - value pairs. e.g.
	 * render( array('id' => 'an_id', 'class' => 'a_class', 'onclick' => 'someJSFunction') );
	 *
	 * @param array $options a set of options to use for the image rendering
	 */
	public function render($options = NULL) {
		return $this->HTML->renderImage( $this->getImageURL(), $options );
	}
	
	/**
	 * Renders the spotlight as a thumbnail image for viewing. The function takes an array
	 * of <IMG> attributes as key - value pairs. e.g.
	 * renderThumbnail( array('id' => 'an_id', 'class' => 'a_class', 'onclick' => 'someJSFunction') );
	 *
	 * @param array $options a set of options to use for the image rendering
	 */
	public function renderThumbnail($options = NULL) {
		return $this->HTML->renderImage( $this->getThumbImageURL(), $options );
	}
}
?>
