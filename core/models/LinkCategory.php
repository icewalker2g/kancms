<?php
if( !defined('CORE_PATH')) die('Access Denied');

class LinkCategory extends Model {
	
	public static $Link = NULL;
	
	public function __construct($data = NULL) {
		parent::__construct($data, 'quick_links_categories');
		$this->Link = new Link();
	}
	
	public function getCategoryId() {
		return $this->getData('id');	
	}
	
	public function getCategoryName() {
		return $this->getData('CategoryName');
	}
	
	public function getCategoryDescription() {
		return $this->getData('CategoryDescription');
	}
	
	public function getLinks($start = 0, $limit = NULL, $publishedOnly = true) {
		$conditions = array("SiteID" => SITE_ID, "Published" => 'true', 'LinkCategory' => $this->getId() );
        
        if( !$publishedOnly ) {
            $conditions = array("SiteID" => SITE_ID, 'LinkCategory' => $this->getId() );
        }
        
		return $this->Link->find(array(
            'joins' => array(
                array("type" => "inner", "condition" => array("quick_links_categories qlc" => "qlc.id = Link.LinkCategory"))
            ),
            'filter' => $conditions,
            "order" => array("LinkCategory, LinkPosition, LinkName"),
			'offset' => $start,
			'limit' => $limit
        ));
	}
	
	public function getLinksCount($publishedOnly = true) {
		
		$filters = NULL;
		
		if( $publishedOnly ) {
			$filters = array('LinkCategory' => $this->getId(), "Published" => "true");
		} else {
			$filters = array('LinkCategory' => $this->getId());
		}
		
		return $this->Link->count( array(
			'filter' => $filters
		));	
	}
	
	public function listLinks($class = '') {
		$links = $this->getLinks();
		
		echo "<ul class='$class'>";
		
		for($i = 0; $i < count($links); $i++) {
			echo "<li>" . $links[$i]->render() . "</li>";		
		}
		
		echo "</ul>";
	}
}
?>
