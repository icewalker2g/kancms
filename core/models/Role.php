<?php

if (!defined('CORE_PATH'))  die('Access Denied');

/**
 * Description of Role
 *
 * @author Francis Adu-Gyamfi
 */
class Role extends Model {

    private $User = NULL;

    public function __construct($data = NULL) {
        parent::__construct($data, 'user_roles');
        $this->User = new User();
    }

    /**
     *
     * @return string
     */
    public function getName() {
        return $this->getData('role_name');
    }

    /**
     *
     * @return string 
     */
    public function getTag() {
        return $this->getData('role_tag');
    }

    /**
     *
     * @return string 
     */
    public function getDescription() {
        return $this->getData('role_description');
    }

    /**
     *
     * @return User[]
     */
    public function getUsers($start = 0, $limit = NULL, $options = NULL) {
        
        return $this->User->find(array(
            'filter' => array('Role' => $this->getId()),
            'offset' => $start,
            'limit' => $limit
        ));
    }

    /**
     *
     * @return int the total number of users for this role
     */
    public function getUserCount() {
        return $this->User->count(array(
            'filter' => array('Role' => $this->getId())
        ));
    }

}


?>
