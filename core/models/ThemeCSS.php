<?php

if( !defined('CORE_PATH')) die('Access Denied');

/**
 * Theme CSS represents a mapping to the site_themes_css table
 *
 * @author Francis Adu-Gyamfi
 */
class ThemeCSS extends Model {

    protected $tableName = "site_themes_css";

}

?>
