<?php

if( !defined('CORE_PATH')) die("Access Denied");

class Media extends Model {
	
	protected $MediaCategory = NULL;

    public function __construct($data = NULL) {
        parent::__construct($data, 'media');
    }
	
	public function getName() {
		return $this->getData('MediaName');	
	}
	
	public function getArtiste() {
		return $this->getData('MediaArtiste');	
	}
	
	public function getGenre() {
		return $this->getData('MediaGenre');	
	}
	
	public function getDescription() {
		return $this->getData('MediaDesc');	
	}
	
	public function getDuration() {
		return $this->getData('MediaDuration');	
	}
	
	public function getEmbedCode() {
		return $this->getData('MediaEmbedCode');	
	}
	
	public function getLinkedName() {
		$url = $this->getURL();
		$name = $this->getData('MediaName');
		
		return $this->HTML->renderLink($name, $url);
	}
	
	public function getCategory() {
		if( $this->MediaCategory == NULL ) {
			$this->MediaCategory = new MediaCategory( $this->getData('CategoryID') );	
		}
		
		return $this->MediaCategory;	
	}
	
	public function getURL() {
		
		$tag = $this->getCategory()->getData('Alias');
		$url = "../pages/media.php?siteid=" . SITE_TAG . "&category=" . $tag . "&id=" . $this->getId();
		
		if (SEF_URLS) {
            $url = "../" . SITE_TAG . "/media/" . $tag . "/" . $this->getId(); // $this->HTML->slug( $this->getData('MediaName') );
        }
		
		if( defined("IN_CMS") && IN_CMS ) {
			return $url;	
		}

        return kan_fix_url($url);	
	}
	
	public function getPostDate($format = "d-M") {
        return date($format, strtotime($this->getData('DatePosted')));
    }
    
    
    /**
     * Returns the URL to the media item on the system
     * 
     * @return string
     */
    public function getPath() {
        if( defined('IN_CMS') && IN_CMS ) {
            return $this->getData('MediaPath');
        }
        
        return kan_fix_url($this->getData('MediaPath'));
    }
	
	public function getDownloadURL() {
		
		$path = $this->getData('MediaPath');
		$media_id = $this->getId();
		
		$url = kan_fix_url("../pages/media.php?siteid=" . SITE_TAG. "&file=" . $media_id);
		
		if( SEF_URLS ) {
			$url = kan_fix_url("../" . SITE_TAG . "/media/file/" . $media_id );	
		}
		
        return $url;	
	}

    /**
     * Deletes a media record from the database and removes the file from
     * the file system as while
     */
    public function delete() {
        $this->deleteFile();
        parent::delete();
    }
    
    /**
     * Deletes the file representing the media items from the file system
     * 
     * @return bool
     */
    public function deleteFile() {
        $file = kan_fix_path( $this->getData('MediaPath') );
        
        if( file_exists($file) ) {
            @unlink($file);
            return true;
        } else {
            return false;
        }
    }
    
    public function deleteAll($conditions = NULL) {
        $media = $this->find(array(
            'filter' => $conditions
        ));
        
        foreach($media as $m) {
            $m->deleteFile();
        }
        
        parent::deleteAll($conditions);
    }
    
    /**
     * Indicates if the specified Media Item can be viewed.
     * 
     * @return bool
     */
    public function isPublished() {
        return $this->getData('Published') == 1;
    }
}
?>