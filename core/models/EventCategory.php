<?php
if( !defined('CORE_PATH')) die('Access Denied');

kan_import('Event');

class EventCategory extends Model {
	
    private $Event = NULL;
    
	public function __construct($data = NULL) {
		parent::__construct($data, 'event_categories');
        $this->Event = new Event();
	}
	
	public function getId() {
		return $this->getData('id');
	}
	
	public function getSiteID() {
		return $this->getData('SiteID');
	}
	
	public function getCategoryName() {
		return $this->getData('Category');	
	}
	
	public function getLinkedCategoryName($options = NULL) {
		$url = $this->getURL();
		$name = $this->getCategoryName();
		
		return $this->HTML->renderLink($name, $url, $options);
	}
	
	public function getCategoryAlias() {
		return slug($this->getCategoryName());	
	}
	
	public function getCategoryDescription() {
		return $this->getData('CategoryDescription');	
	}
	
	public function getURL() {
		$alias = $this->getCategoryAlias();
		$url = "../pages/events.php?siteid=" . SITE_TAG . "&category=" . $alias;
		
		if( SEF_URLS ) {
			$url = "../" . SITE_TAG . "/events/" . $alias;
		}
		
		return kan_fix_url($url);
	}
	
	public function getEvents($start = 0, $limit = NULL, $publishedOnly = true, $upcomingOnly = true) {
		$db = $this->getDatabase();

        $publishFilter = $publishedOnly ? " AND Published = 'true' " : "";
        $upcomingFilter = $upcomingOnly ? " AND (CURRENT_DATE < EndDate OR CURRENT_DATE < StartDate) " : "";
		
		$query = sprintf("SELECT * FROM events WHERE EventCategory = %s {$publishFilter} {$upcomingFilter} ORDER BY StartDate ASC ",
					$db->sanitizeInput($this->getId(),'int'));	
		
		if( !is_null($limit) ) {
			$query = sprintf("%s LIMIT %s, %s", $query, 
				$db->sanitizeInput($start,'int'), 
				$db->sanitizeInput($limit,'int') 
			);
		}
		
		$db->query($query,true);
		$events = array();
		
		for($i = 0; $i < $db->getResultCount(); $i++) {
			$event = new Event($db->getRow($i));
			$event->setCategory( $this );
			
			array_push($events, $event );	
		}
		
		return $events;
	}
	
	/**
     * Returns an integer representing the total number of news articles in the database
     * for the specified site. If the optional category is specified, only the number of articles
     * in that category will be returned
     *
     * @param $site the ID of the Site
     * @param category the ID of the news_category
     */
    public function getTotalEventCount() {
		$db = $this->getDatabase();
		
        $query = sprintf(
					"SELECT count(*) AS cnt FROM events "
					. "INNER JOIN event_categories ec ON ec.id = events.EventCategory "
					. "WHERE ec.id = %s",
					$db->sanitizeInput($this->getId(), 'text')
				);

        
		$data = $db->query($query)->getRow();
		return isset($data['cnt']) ? $data['cnt'] : 0;
    }

    public function getFeedURL() {
		if( SEF_URLS ) {
			return kan_fix_url("../" . SITE_TAG . "/feed/events/" . $this->getCategoryAlias() );
		}

		return kan_fix_url("../pages/feed.php?siteid=" . SITE_TAG . "&content=events&category=" . $this->getCategoryAlias() );
	}
	
	public function getBreadCrumb($separator = "-") {
		// add home link
        $location = "<a href='" . kan_site_home() . "'>Home</a>";
		
		// get url for articles page
		$art_url = kan_fix_url('../pages/events.php?siteid=' . SITE_TAG );

		// if using Friendly URLS then fix the url for the articles page
        if (SEF_URLS) {
            $art_url = kan_fix_url("../" . SITE_TAG . "/events/" );
        }
		
		// add the articles page element
		$location .= " $separator <a href='$art_url'>Upcoming Events</a> ";

		// add the current category element
        $location .= " $separator " . $this->getLinkedCategoryName();

		// return the finally breadcrumb
        return $location;
    }
}
?>
