<?php

if (!defined('CORE_PATH'))
    die('Access Denied');

class DownloadCategory extends Model {

    public function __construct($data = NULL) {
        parent::__construct($data, 'download_categories');
    }

    public function getId() {
        return $this->getData('id');
    }

    public function getSiteID() {
        return $this->getData('SiteID');
    }

    public function getCategoryName() {
        return $this->getData('CategoryName');
    }

    public function getLinkedCategoryName() {
        $url = "../pages/downloads.php?siteid=" . SITE_TAG . "&category=" . $this->getCategoryAlias();

        if( SEF_URLS ) {
            $url = "../" . SITE_TAG . "/downloads/" . $this->getCategoryAlias() . "/";
        }

        $url = kan_fix_url($url);

        return "<a href='$url' title='{$this->getCategoryDescription()}'>{$this->getCategoryName()}</a>";
    }

    public function getCategoryAlias() {
        return $this->getData('CategoryAlias');
    }

    public function getCategoryImage() {
        return $this->getData('CategoryImage');
    }

    public function getCategoryDescription() {
        return $this->getData('CategoryDescription');
    }

    public function getDownloads($start = 0, $limit = NULL) {

        $download = new Download();
        return $download->find(array(
            'filter' => array("CategoryID" => $this->getId()),
            'order' => array("id DESC"),
            'offset' => $start,
            'limit' => $limit
        ));
    }

    public function getTotalDownloadsCount($show = 'all') {
        $db = $this->getDatabase();

        $filter = "";
        switch ($show) {
            case 'approved':
                $filter = " AND Published = 1 ";
                break;
            case 'unapproved':
                $filter = " AND Published = 0 ";
                break;
            case 'all':
            default:
                $filter = "";
        }

        $selectSQL = sprintf("SELECT count(id) AS cnt FROM downloads WHERE CategoryID = %s {$filter}",
                $db->sanitizeInput($this->getId(), 'int'));

        $row = $db->query($selectSQL, true)->getRow();

        return $row['cnt'];
    }
    
    public function getBreadCrumb($separator = "-") {
		// add home link
        $location = "<a href='" . kan_site_home() . "'>Home</a>";
		
		// get url for articles page
		$art_url = kan_fix_url('../pages/downloads.php?siteid=' . SITE_TAG );

		// if using Friendly URLS then fix the url for the articles page
        if (SEF_URLS) {
            $art_url = kan_fix_url("../" . SITE_TAG . "/downloads/" );
        }
		
		// add the articles page element
		$location .= " $separator <a href='$art_url'>Downloads</a> ";

		// add the current category element
        $location .= " $separator <a href='{$this->getURL()}'>{$this->getCategoryName()}</a>";

		// return the finally breadcrumb
        return $location;
    }
    
    public function getURL() {
		$url = "../pages/downloads.php?siteid=" . SITE_TAG . "&category=" . $this->getCategoryAlias();
		
        if (SEF_URLS) {
            $url = "../" . SITE_TAG . "/downloads/" . $this->getCategoryAlias();
        }
		
		if( defined("IN_CMS") && IN_CMS ) {
			return $url;	
		}

        return kan_fix_url($url);
    }

    public function delete() {
        
        $download = new Download();
        $download->deleteAll(array('CategoryID' => $this->getId()));
        
        parent::delete();
    }
    
    public function deleteAll($conditions = array()) {
        $categories = $this->find( array(
            'filter' => $conditions
        ));
        
        foreach($categories as $category) {
            $category->delete();
        }
        
        parent::deleteAll($conditions);
    }
	
	public function getSiteCategories() {
		return $this->find(array(
			'filter' => array('SiteID' => SITE_ID),
			'order' => array('CategoryName')
		));	
	}
}

?>
