<?php

if( !defined('CORE_PATH')) die('Access Denied');

class Block extends Model {

    private $page = NULL;

    public function __construct($data = NULL, $page = NULL) {
        parent::__construct($data, 'page_blocks');
        $this->setPage($page);
    }

    public function getPageID() {
        return $this->getData('PageID');
    }

    public function getPage() {
        if ($this->page != NULL) {
            return $this->page;
        }

        $db = $this->getDatabase();
        $query = sprintf("SELECT * FROM pages WHERE id = %s", $db->sanitizeInput($this->getPageID(), 'int'));
        $data = $db->query($query)->getRow();

        return new Page($data);
    }

    public function setPage($page) {
        if ($page != NULL && !($page instanceof Page)) {
            die("Invalid Object Passed: " . __FILE__ . ", at Line: " . __LINE__);
        }

        $this->page = $page;
    }

    public function getTitle() {
        return $this->getData('BlockTitle');
    }

    public function getAlias() {
        return $this->getData('BlockAlias');
    }

    public function getDescription() {
        return $this->getData('BlockDescription');
    }

    public function getContentType() {
        return $this->getData('BlockContentType');
    }

    public function getContent() {
        return $this->getData('BlockContent');
    }

}
?>
