<?php

class ArticleCategory extends Model {

	private $tree = NULL;
	private $parentCategory = NULL;
	
    public function __construct($data = NULL, &$tree = NULL) {
        parent::__construct($data, 'article_categories');
		$this->setAdjancencyTree($tree);
    }
	
	public function setAdjancencyTree($tree) {
		$this->tree = $tree;	
	}
	
	public function getAdjacencyTree() {
		return $this->tree;	
	}

    public function getId() {
        return $this->getData('id');
    }

    public function getParentID() {
        return $this->getData('ParentID');
    }

    public function getParent() {
		if( $this->parentCategory == NULL ) {
			$this->parentCategory = new ArticleCategory( $this->getParentID() );	
		}
		
		return $this->parentCategory;
    }
	
	public function getLinkedCategoryName($options = NULL) {
		return $this->HTML->renderLink( $this->getCategoryName(), $this->getURL(), $options );
    }

    public function getCategoryName() {
        return $this->getData('CategoryName');
    }

    public function getCategoryDescription() {
        return $this->getData('CategoryDescription');
    }

    public function getCategoryAlias() {
        return $this->getData('CategoryAlias');
    }

    public function getURL() {
		$url = "../pages/articles.php?siteid=" . SITE_TAG . "&category=" . $this->getCategoryAlias();
		
        if (SEF_URLS) {
            $url = "../" . SITE_TAG . "/articles/" . $this->getCategoryAlias();
        }
		
		if( defined("IN_CMS") && IN_CMS ) {
			return $url;	
		}

        return kan_fix_url($url);
    }

    public function getBreadCrumb($separator = "-") {
		// add home link
        $location = "<a href='" . kan_site_home() . "'>Home</a>";
		
		// get url for articles page
		$art_url = kan_fix_url('../pages/articles.php?siteid=' . SITE_TAG );

		// if using Friendly URLS then fix the url for the articles page
        if (SEF_URLS) {
            $art_url = kan_fix_url("../" . SITE_TAG . "/articles/" );
        }
		
		// add the articles page element
		$location .= " $separator <a href='$art_url'>Articles</a> ";

		// add the current category element
        $location .= " $separator <a href='{$this->getURL()}'>{$this->getCategoryName()}</a>";

		// return the finally breadcrumb
        return $location;
    }

    public function getArticles($start = 0, $limit = -1, $publishedOnly = true) {
        $db = $this->getDatabase();

        $filter = $publishedOnly ? " AND Published = 1 " : "";

        $query = sprintf("SELECT * FROM articles WHERE CategoryID = %s {$filter} ORDER BY id DESC",
                $db->sanitizeInput($this->getId(), 'int'));

        if ($start >= 0 && $limit != -1) {
            $query = sprintf("%s LIMIT %s, %s", $query, $start, $limit);
        }

        $db->query($query, true);

        $articles = array();

        for ($i = 0; $i < $db->getResultCount(); $i++) {
            $article = new Article($db->getRow($i));
            $article->setCategory($this);
			
			if( $publishedOnly && !$article->isPublished() ) {
				continue;
			}

            array_push($articles, $article);
        }

        return $articles;
    }

    public function getTotalArticleCount($show = 'all') {
        $db = $this->getDatabase();

        $filter = "";
		switch($show) {
			case 'published':
				$filter = " AND Published = 1 ";
				break;
			case 'unpublished':
				$filter = " AND Published = 0 ";
				break;
		}

        $selectSQL = sprintf("SELECT count(id) AS cnt FROM articles WHERE CategoryID = %s {$filter}",
                $db->sanitizeInput($this->getId(), 'int'));

        $row = $db->query($selectSQL, true)->getRow();

        return $row['cnt'];
    }
	
	public function getFeedURL() {
		if( SEF_URLS ) {
			return kan_fix_url("../" . SITE_TAG . "/feed/articles/" . $this->getCategoryAlias() );
		}
		
		return kan_fix_url("../pages/feed.php?siteid=" . SITE_TAG . "&content=articles&category=" . $this->getCategoryAlias() );
	}

}
?>
