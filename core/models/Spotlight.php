<?php

if (!defined('CORE_PATH'))
    die('Access Denied');

class Spotlight extends Model {
	
	public function __construct($data = NULL) {
		parent::__construct($data, 'spotlights');	
	}
	
	public function getSiteID() {
		return $this->getData('SiteID');	
	}
	
	public function getTitle() {
		return $this->getData('menu_header');
	}
	
	public function getSubTitle() {
		return $this->getData('menu_subheader');	
	}
	
	public function getDescription() {
		if( defined("IN_CMS") && IN_CMS ) {
			return $this->getData('description');	
		}
		
		return kan_process_html($this->getData('description'));	
	}
	
	/**
	 * Returns the path/url to image to be displayed
	 *
	 * @return string
	 */
	public function getImagePath() {
		if( defined("IN_CMS") && IN_CMS ) {
			return $this->getData('spotlight');	
		}
		
		return kan_fix_url($this->getData('spotlight'));
	}
	
	/**
	 * Returns the URL to content to which this spotlight points
	 *
	 * @return string
	 */
	public function getURL() {
		if( defined("IN_CMS") && IN_CMS ) {
			return $this->getData('spotlight_url');	
		}
		
		return kan_fix_url($this->getData('spotlight_url'));
	}
	
	public function getPostDate($format = 'Y-m-d') {
		return date( $format, strtotime($this->getData('date')) );
	}
	
	public function getPosition() {
		return $this->getData('position');	
	}
	
	/**
	 * Returns true if the Spotlight can be viewed, false otherwise
	 * 
	 * @return bool published state
	 */
	public function isPublished() {
		return $this->getData('approved') == "true";	
	}
    
    /**
     * Publishes or Unpublishes a Spotlight
     * 
     * @param bool $publish 
     */
    public function publish($publish) {
        $this->setData("approved", $publish ? "true" : "false");
        $this->save();
    }
	
	/**
	 * Renders the spotlight as an image for viewing. The function takes an array
	 * of <IMG> attributes as key - value pairs. e.g.
	 * render( array('id' => 'an_id', 'class' => 'a_class', 'onclick' => 'someJSFunction') );
	 *
	 * @param array $options a set of options to use for the image rendering
	 */
	public function render($options = NULL) {
		return $this->HTML->renderImage( kan_fix_url($this->getData('spotlight')), $options );
	}
    
    /**
     * Overrides the parent delete functionality to enable deleting of spotlight files
     * when deleting the records from the database
     */
    public function delete() {
        
        $file_path = kan_fix_path( $this->getImagePath() );
        if( file_exists($file_path) ) {
            @unlink($file_path);
        }
        
        parent::delete();
    }
    
    public function deleteAll($conditions = NULL) {
        $spotlights = $this->find(array(
            'filter' => $conditions
        ));
        
        foreach($spotlights as $spotlight) {
            $spotlight->delete();
        }
        
        parent::deleteAll($conditions);
    }
}
?>
