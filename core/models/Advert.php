<?php

/**
 * Model for the Advert tables
 *
 * @author Francis Adu-Gyamfi
 */
class Advert extends Model {
    
    protected $tableName = "advert_images";
    
    
    public function render() {
        $path = $this->getData("AdvertPath");
        $type = $this->getData("AdvertType");
        
        if( $type == "jpg" || $type == "png" ) {
            $this->HTML->renderImage( kan_fix_url($path) );
        }
    }
    
}

?>
