<?php
if( !defined('CORE_PATH')) die('Access Denied');

class Theme extends Model {
    
    private $themeCSS = NULL;
	
    /**
     *
     * @var array
     */
    private $options = array(
        'layouts' => array(
            'sections.php' => 'Default Page Layout'
        )
    );

    public function __construct($data = NULL) {
        parent::__construct($data, 'site_themes');
        
        $this->themeCSS = new ThemeCSS();
    }

    /**
     * Sets the options available for this theme
     * @param array $options 
     */
    public function setOptions($options) {
        if( $options == NULL || !is_array($options) ) {
            trigger_error("Options Supplied Must Be An Array");
            return;
        }
        
        $this->options = $options;
    }
    
    /**
     * Returns all available options for this theme
     * @return array 
     */
    public function getOptions() {
        return $this->options;
    }
    
    /**
     * Returns an option from the available theme options
     * 
     * @param string $key
     * @return mixed
     */
    public function getOption($key) {
        if( !$key ) {
            trigger_error("Key required to obtain option");
            return NULL;
        }
        
        if( $this->options == NULL ) {
            trigger_error("No options currently available");
            return NULL;
        }
        return $this->options[$key];
    }
    
    /**
     * Adds or replaces an option for this theme
     * 
     * @param string $key
     * @param mixed $value 
     */
    public function setOption($key, $value) {
        
        if( $this->options == NULL ) {
            $this->options = array();
        }
        
        if( $key == NULL || $value == NULL ) {
            trigger_error("The Key and/or Value Cannot Be Null");
            return;
        }
        
        $this->options[$key] = $value;
    }
	
	public function option($key, $value = NULL) {
		if( $this->options == NULL ) {
            $this->options = array();
        }
		
		if( !$key ) {
            trigger_error("Key required to obtain option");
            return NULL;
        }
		
		// we are trying to retrieve a value
		if( $key && !$value && is_string($key) ) {
			return isset($this->options[$key]) ? $this->options[$key] : NULL;
		}
		
		// we are trying to update the options setup
		if( $key && !$value && is_array($key) ) {
			$this->options = $key;
			return;
		}
		
		if( $key && $value ) {
			$this->options[$key] = $value;
		}
	}
	
	public function path($file) {
		if( !is_string($file) ) return "";
		
		$path = kan_fix_path($this->getData('ThemeFolder') . $file);
        
        return $path;
	}
	
	public function url($file) {
		if( !is_string($file) ) return "";
		
		$path = kan_fix_url($this->getData('ThemeFolder') . $file);
        
        return $path;
	}
	
	public function script($path) {
        return $this->HTML->renderScript( kan_fix_url($this->getData('ThemeFolder') . 'scripts/'), $path ); 
    }
    
    public function css($path) {
        return $this->HTML->renderCSS( kan_fix_url($this->getData('ThemeFolder') . 'css/'), $path );
    }
    
    public function getThemeStyleSheets() {
        return $this->themeCSS->find( array(
            'filter' => array("ThemeID" => $this->getId())
        ));
    }
	
	public function delete() {
		
		if( !defined("IN_CMS") || !IN_CMS ) {
			return;	
		}
		
		$this->deleteFiles();
		parent::delete();	
	}
	
	public function deleteFiles() {
        if( !defined("IN_CMS") || !IN_CMS ) {
			return;	
		}
        
		$folder = kan_fix_path($this->getData('ThemeFolder'));
		$fm = new FileManager();
		$fm->deleteDirectory($folder);
	}
}

?>
