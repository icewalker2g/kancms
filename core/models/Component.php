<?php
if( !defined('CORE_PATH')) die('Access Denied');

class Component extends Model {
	
	public function __construct($data = NULL) {
		parent::__construct($data,'components');
	}
    
    private function _baseurl() {
        return COMPONENTS_URL . basename(dirname($this->getData("ComponentPath"))) . "/";
    }
    
    private function _basepath() {
        return COMPONENTS_PATH . basename(dirname($this->getData("ComponentPath"))) . "/";
    }
    
    public function getPath() {
        return kan_fix_path( $this->getData('ComponentPath') );
    }
    
    public function path($file) {
		if( !is_string($file) ) return "";
		
		return $this->_basepath() . $file;
	}
	
	public function url($file) {
		if( !is_string($file) ) return "";
		
		return $this->_baseurl() . $file;
	}
    
    public function script($path) {
        
        if( is_array($path) ) {
            $output = '';
            foreach($path as $s) {
                $output .= "\n\t" . $this->script($s);
            }
            
            return $output;
        }
        
        if( !is_string($path) ) return;
        
        $pos = strrpos($path, ".js");
        
        if( !$pos || $pos != strlen($path) - 3 ) {
            $path .= ".js";
        }
		
		$path = $this->url($path);
        
        $script = "<script type='text/javascript' src='$path'></script>\n";
        
        return $script;
    }
    
    public function css($path) {
        
        if( is_array($path) ) {
            $output = '';
            foreach($path as $s) {
                $output .= "\n\t" . $this->css($s);
            }
            
            return $output;
        }
        
        if( !is_string($path) ) return;
        
        $path = $this->url($path);
        
        $pos = strrpos($path, ".css");
        
        if( !$pos || $pos != strlen($path) - 4 ) {
            $path .= ".css";
        }
        
        $css = "<link type='text/css' href='$path' rel='stylesheet' />\n";
        
        return $css;
    }

}
?>
