<?php 

class Setting extends Model {

	private $category = NULL;
	
	public function __construct($data = NULL, &$category = NULL) {
		parent::__construct($data, 'system_settings');	
		$this->category = $category;
	}
	
	public function getId() {
		return $this->getData('id');
	}
	
	public function getCategoryID() {
		return $this->getData('SettingCategory');	
	}
	
	public function getCategory() {
		if( $this->category == NULL ) {
			$this->category = new SettingCategory( $this->getCategoryID() );
		}	
		
		return $this->category;
	}
	
	public function getKey() {
		return $this->getData('SettingKey');	
	}
	
	public function getValue() {
		return $this->getData('SettingValue');	
	}
	
	public function getType() {
		return $this->getData('SettingType');	
	}
	
	public function getName() {
		return $this->getData('SettingName');	
	}
	
	public function getDescription() {
		return $this->getData('SettingDescription');
	}
}
?>