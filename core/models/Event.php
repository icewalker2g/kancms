<?php
if( !defined('CORE_PATH')) die('Access Denied');

kan_import('EventCategory');

class Event extends Model {
	
	private $eventCat = NULL;
	
	public function __construct($data = NULL) {
		parent::__construct($data, 'events');
	}
	
	public function getId() {
		return $this->getData('id');	
	}
	
	public function getCategoryID() {
		return $this->getData('EventCategory');	
	}
	
	public function getCategory() {
		if( $this->eventCat == NULL ) {
			$this->eventCat = new EventCategory( $this->getCategoryID() );
		}
		
		return $this->eventCat;
	}
	
	public function setCategory($eventCat) {
		if( !($eventCat instanceof EventCategory) ) {
			die("Invalid Object Type. EventCategory expected, received " . gettype($eventCat) );
		}
		
		$this->eventCat = $eventCat;	
	}
	
	public function getName() {
		return $this->getData('EventName');
	}
	
    /**
     * Returns an HTML Link with the EventName as the text. Takes options to use
     * as attributes for the link
     * 
     * @param array $options
     * @return string
     */
	public function getLinkedName( $options = NULL ) {
		$url = $this->getURL();
		$name = $this->getName();
        
        return $this->HTML->renderLink($name, $url, $options);
	}
	
	public function getNameAlias() {
		return $this->getData('FriendlyURL');	
	}
	
	public function getDescription() {
		return kan_process_html($this->getData('EventDesc'));	
	}
	
	public function getTime() {
		return $this->getData('Time');
	}
	
	public function getVenue() {
		return $this->getData('Venue');	
	}
	
	public function getStartDate($format = 'd M') {
		
		if( $this->getData('StartDate') == "0000-00-00" ) {
			return "";	
		}
        
        if( $format != NULL ) {
			return date( $format, strtotime($this->getData('StartDate')) );	
		}
		
		return $this->getData('StartDate');
	}
	
	public function getEndDate($format = 'd M') {
		
		if( $this->getData('EndDate') == "0000-00-00" ) {
			return NULL;	
		}
        
        if( $format != NULL ) {
			return date( $format, strtotime($this->getData('EndDate')) );	
		}
		
		return $this->getData('EndDate');	
	}
	
	public function getURL() {
		if( $this->getData('RedirectURL') != NULL ) {
			return kan_fix_url($this->getData('RedirectURL'));	
		}
		
		if( SEF_URLS ) {
			$category = $this->getCategory();
			return kan_fix_url("../" . SITE_TAG . "/events/" . $category->getCategoryAlias() . "/" . $this->getNameAlias() );		
		}
		
		return kan_fix_url("../pages/events.php?id=" . $this->getId() );
	}
	
	public function isPublished() {
		$published = $this->getData('Published');
		
		return $published == "true" || $published == 1;
	}
	
	public function isPast() {
		$startDate = strtotime($this->getData('StartDate'));
		$endDate = strtotime($this->getData('EndDate'));
		
		return time() > $endDate && time() > $startDate;
	}
	
	public function getBreadCrumb($separator = "-") {
        // add home link
        $location = "<a href='" . kan_site_home() . "'>Home</a>";
		
		// get url for articles page
		$art_url = kan_fix_url('../pages/events.php?siteid=' . SITE_TAG );

		// if using Friendly URLS then fix the url for the articles page
        if (SEF_URLS) {
            $art_url = kan_fix_url("../" . SITE_TAG . "/events/" );
        }
		
		// add the articles page element
		$location .= " $separator <a href='$art_url'>Events</a> ";

		// get the category for this article and build the link for it
        $category = $this->getCategory();
        $location .= " $separator " . $category->getLinkedCategoryName();

		// add the element for the current article
        $location .= " $separator " . $this->getLinkedName();

		// return the breadcrumb location
        return $location;
    }
}
?>
