<?php
if (!defined('CORE_PATH'))
    die('Access Denied');

class Image extends Model {

	public function __construct($data = NULL) {
		parent::__construct($data, 'images');	
	}	
    
    
    /**
     * Deletes a media record from the database and removes the file from
     * the file system as while
     */
    public function delete() {
        $this->deleteFile();
        parent::delete();
    }
    
    /**
     * Deletes the file representing the media items from the file system
     * 
     * @return bool
     */
    public function deleteFile() {
        $file = kan_fix_path( $this->getData('ImageFile') );
        $thumb = kan_fix_path( $this->getData('ThumbPath') );
        
        if( file_exists($file) ) {
            @unlink($file);
            @unlink($thumb);
            return true;
        } else {
            return false;
        }
    }
    
    public function deleteAll($conditions = array()) {
        $images = $this->find( array(
            'filter' => $conditions
        ));
        
        foreach($images as $image) {
            $image->deleteFile();
        }
        
        parent::deleteAll($conditions);
    }
	
	public function getImageURL() {
        return kan_fix_url($this->getData('ImageFile'));
    }

    public function getThumbImageURL() {
        return kan_fix_url($this->getData('ThumbPath'));
    }

	/**
	 * Renders the spotlight as an image for viewing. The function takes an array
	 * of <IMG> attributes as key - value pairs. e.g.
	 * render( array('id' => 'an_id', 'class' => 'a_class', 'onclick' => 'someJSFunction') );
	 *
	 * @param array $options a set of options to use for the image rendering
	 */
	public function render($options = NULL) {
		return $this->HTML->renderImage( $this->getImageURL(), $options );
	}
	
	/**
	 * Renders the spotlight as a thumbnail image for viewing. The function takes an array
	 * of <IMG> attributes as key - value pairs. e.g.
	 * renderThumbnail( array('id' => 'an_id', 'class' => 'a_class', 'onclick' => 'someJSFunction') );
	 *
	 * @param array $options a set of options to use for the image rendering
	 */
	public function renderThumbnail($options = NULL) {
		return $this->HTML->renderImage( $this->getThumbImageURL(), $options );
	}
}
?>
