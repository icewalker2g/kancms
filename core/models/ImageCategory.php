<?php
if (!defined('CORE_PATH'))
    die('Access Denied');

class ImageCategory extends Model {
	
	public function __construct($data = NULL) {
		parent::__construct($data, 'image_categories');	
	}
	
	public function getImages($start = 0, $limit = NULL, $options = NULL) {
		$image = new Image();
		
		$default = array(
			'filter' => array('UseSection' => $this->field('CategoryCode'))
		);
		
		if( is_array($options) ) {
			$default = array_merge($default, $options);	
		}
		
		return $image->paginate($start, $limit, $default);	
	}
}
?>
