<?php

/**
 * Represents a record in the article_comments table
 */
class ArticleComment extends Model {
	
	private $article = NULL;
	
	public function __construct($data = NULL) {
		parent::__construct($data, 'article_comments');	
	}
	
	public function getId() {
		return $this->getData('id');	
	}
	
	public function getArticleID() {
		return $this->getData('ArticleID');	
	}
    
    public function setArticle($article) {
        $this->article = $article;
    }
	
	public function getArticle() {
		if( $this->article == NULL ) {
            $this->article = new Article( $this->getArticleID() );
		}
		
		return $this->article;
	}
	
	public function getArticleTitle() {
		return $this->getArticle()->getData('ArticleTitle');	
	}
	
	public function getReaderName() {
		return $this->getData('ReaderName');	
	}
	
	/**
	 * Returns the ReaderName as a HTML Anchor if the user specified a valid
	 * website address
	 */
	public function getLinkedReaderName() {
		if( substr($this->getReaderWebsite(),0,7) == "http://" ) {
			return "<a href='" . kan_make_plain($this->getReaderWebsite()) . "'>" . kan_make_plain($this->getReaderName()) . "</a>";
		}
		
		return kan_make_plain($this->getReaderName());
	}
	
	public function getReaderEmail() {
		return $this->getData('ReaderEmail');	
	}
	
	public function getReaderPhone() {
		return $this->getData('ReaderPhone');	
	}
	
	public function getReaderWebsite() {
		return $this->getData('ReaderWebsite');	
	}
	
	public function getSubject() {
		return $this->getData('Subject');	
	}
	
	public function getMessage() {
		return $this->getData('Message');	
	}
	
	public function getPostDate($format = 'd M, Y - h:i a') {
		return date( $format, strtotime($this->getData('PostDate')));
	}
	
	public function isApproved() {
		return $this->getData('Approved') == 1;
	}
}
?>
