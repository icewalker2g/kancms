<?php

if (!defined('CORE_PATH'))  die('Access Denied');

/**
 * Description of User
 *
 * @author Francis Adu-Gyamfi
 */
class User extends Model {

    public function __construct($data = NULL) {
        parent::__construct($data, 'users');
    }

    public function getFirstName() {
        return $this->getData('FirstName');
    }

    public function getLastName() {
        return $this->getData('LastName');
    }

    public function getFullName() {
        return $this->getData('FullName');
    }

    public function getLevel() {
        return $this->getData('Level');
    }

    public function getRole() {
        return $this->getData('Role');
    }

    public function getUsername() {
        return $this->getData('Username');
    }

    public function getPassword() {
        return $this->getData('Password');
    }

    public function getEmail() {
        return $this->getData('Email');
    }

    public function getLastAccessDate($format = 'Y-m-d') {
        return date($format, strtotime($this->getData('LastAccess')));
    }

    public function isActivated() {
        return $this->getData('Activated') == 1;
    }

    /**
     * Changes the password associated with this current User
     * 
     * @param string $old
     * @param string $new
     * @return bool 
     */
    public function changePassword($old, $new) {
        if (!defined('IN_CMS') || !IN_CMS) {
            return;
        }


        $oldPassHash = $this->getData('Password');
        $oldSalt = $this->getData('PassSalt');
        $confirmOldHash = md5($old . $oldSalt);

        // confirm that the supplied password is a valid
        if ($confirmOldHash != $oldPassHash) {
            tigger_error("The Old Password Supplied Is Not Valid. Please Check and Correct");
            return false;
        }

        $newSalt = rand(1, 100000);
        $newPassHash = md5($new . $newSalt);

        $this->setData('Password', $newPassHash);
        $this->setData('PassSalt', $newSalt);
        $this->save();

        //"Password Change Successful";
        return true;
    }

    public function getUserSites() {
        return Site::getInstance()->find(array(
            'filter' => array("id IN" => sprintf("SELECT Site FROM users_sites WHERE User = %s", $this->getId())),
            'order' => array('SiteName')
        ));
    }

}

?>
