<?php

class Download extends Model {

    private $category = NULL;

    public function __construct($data = NULL, &$category = NULL) {
        parent::__construct($data, 'downloads');

        if ($category != NULL && $category instanceof DownloadCategory) {
            $this->category = $category;
        }
    }

    public function getId() {
        return $this->getData('id');
    }

    public function getCategoryID() {
        return $this->getData('CategoryID');
    }

    public function getCategory() {
        if ($this->category == NULL) {
            $this->category = new DownloadCategory( $this->getCategoryID() );
        }

        return $this->category;
    }

    public function getName() {
        return $this->getData('Name');
    }
	
	public function getLinkedName() {
		$category = $this->getCategory();
		$url = "../pages/index.php?siteid=" . SITE_TAG . "&category=" . $category->getCategoryAlias() . "&id=" . $this->getId();
		
		if( SEF_URLS ) {
			$url = "../" . SITE_TAG . "/downloads/" . $category->getCategoryAlias() . "/" . $this->getAlias();	
		}
		
		$url = kan_fix_url($url);
		
		return "<a href='$url'>" . $this->getName() . "</a>";
	}

    public function getAlias() {
        return $this->getData('Alias');
    }

    public function getDescription() {
        return $this->getData('Description');
    }

    public function getThumbnail() {
        return $this->getData('Thumbnail');
    }
	
	public function getURL() {
		$catAlias = $this->getCategory()->getCategoryAlias();
		$url = kan_fix_url("../pages/downloads.php?siteid=" . SITE_TAG. "&category=" . $catAlias . "&id=" . $this->getId() );
		
		if( SEF_URLS ) {
			$url = kan_fix_url("../" . SITE_TAG . "/downloads/{$catAlias}/" . $this->getAlias() );	
		}
		
        return $url;	
	}

    public function getDownloadURL() {
		$enc = $this->getData('EncryptedURL');
		$url = kan_fix_url("../pages/downloads.php?siteid=" . SITE_TAG. "&file=" . $enc);
		
		if( SEF_URLS ) {
			$url = kan_fix_url("../" . SITE_TAG . "/downloads/file/" . $enc );	
		}
		
        return $url;
    }
	
	public function getFilePath() {
		return kan_fix_path( $this->getData('URL') );	
	}

    public function getDownloadCount() {
        return $this->getData('TotalDownloads');
    }
	
	public function incrementDownloadCount() {
		$this->setData("TotalDownloads", intval($this->getData('TotalDownloads')) + 1);
		$this->save();	
	}

    public function getPostDate($format = 'Y-m-d') {
        return date($format, strtotime($this->getData('PostDate')));
    }

    public function isPublished() {
        return $this->getData('Published') == 1;
    }

    public function getBreadCrumb($separator = "-") {
        // add home link
        $location = "<a href='" . kan_site_home() . "'>Home</a>";
		
		// get url for articles page
		$art_url = kan_fix_url('../pages/download.php?siteid=' . SITE_TAG );

		// if using Friendly URLS then fix the url for the articles page
        if (SEF_URLS) {
            $art_url = kan_fix_url("../" . SITE_TAG . "/downloads/" );
        }
		
		// add the articles page element
		$location .= " $separator <a href='$art_url'>Downloads</a> ";

		// get the category for this article and build the link for it
        $category = $this->getCategory();
        $location .= " $separator <a href='" . $category->getURL() . "'>" . $category->getCategoryName() . "</a>";

		// add the element for the current article
        $location .= " $separator <a href='" . $this->getURL() . "'>" . $this->getName() . "</a>";

		// return the breadcrumb location
        return $location;
    }
    
    public function delete() {
        $file_path = $this->getFilePath();

        if (file_exists($file_path)) {
            @unlink($file_path);
        }
        
        parent::delete();
    }
	
    public function deleteAll($conditions = array()) {
        $downloads = $this->find(array(
            'filter' => $conditions
        ));

        foreach ($downloads as $download) {

            $file_path = $download->getFilePath();

            if (file_exists($file_path)) {
                @unlink($file_path);
            }
        }
        
        parent::deleteAll($conditions);
    }
}
?>
