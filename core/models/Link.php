<?php

if( !defined('CORE_PATH')) die('Access Denied');

class Link extends Model {
	
	private $LinkCategory = NULL;
	
	public function __construct($data = NULL) {
		parent::__construct($data, 'quick_links');
	}
	
	public function getCategory() {
		if( $this->LinkCategory == NULL ) {
			$this->LinkCategory = new LinkCategory( $this->getCategoryID() );	
		}
		
		return $this->LinkCategory;
	}
	
	function getCategoryID() {
		return $this->getData('LinkCategory');	
	}
	
	function getCategoryName() {
		return $this->getData('CategoryName');	
	}

    /**
     * Returns the name of the current link item
     * 
     * @return string the link name
     */
    function getName() {
        return $this->getData('LinkName');
    }

    function getDescription() {
        return $this->getData('LinkDesc');
    }

    function getURL() {
        return $this->getData('LinkURL');
    }

    public function getTarget() {
        return $this->getData('LinkTarget');
    }

    public function getPosition() {
        return $this->getData('LinkPosition');
    }
	
	public function getLinkedName() {
		return $this->getLinkAsHTMLAnchor();	
	}
	
	public function getLinkAsHTMLAnchor($id = '', $class = '', $events = '') {
		$url = $this->getURL();
		$target = $this->getTarget();
		$name = $this->getName();
		$desc = $this->getDescription();
		
		return "<a id='$id' class='$class' href='$url' title='$desc' target='$target' $events >$name</a>";
	}
    
    public function isPublished() {
        return $this->getData("Published") == "true";
    }
    
    public function publish($publish) {
        $this->setData("Published", $publish ? 'true': 'false');
        $this->save();
    }
	
	public function render($options = NULL) {
		$url = $this->getURL();
		$target = $this->getTarget();
		$name = $this->getName();
		$desc = $this->getDescription();
		
		$default = array('target' => $target, 'title' => $desc);
		
		if( $options ) {
			$default = array_merge($options, $default);	
		}
		
		return $this->HTML->renderLink($name, $url, $default);
	}	
}
?>
