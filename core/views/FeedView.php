<?php

/**
 * Description of FeedView
 *
 * @author Francis Adu-Gyamfi
 */
class FeedView extends View {

    public function __construct() {
        
    }

    public function render() {
        global $site;

        // get an instance of the feed generator
        $feed = new FeedWriter();
        //Use core setChannelElement() function for other optional channels
        $feed->setChannelElement('language', 'en-us');
        $feed->setChannelElement('pubDate', date(DATE_RSS, time()));

        // get the section of the site to provide feeds for
        if (isset($_GET['content']) && $_GET['content'] == "articles") {

            // get an instance of the article manager
            $artManager = new ArticlesManager();
            $articles = array();

            // if providing a feed for a specific category then get the category
            if (isset($_GET['category']) && !empty($_GET['category'])) {
                // get the category alias
                $category_alias = kan_clean_input($_GET['category']);

                // get the actual category
                $category = $artManager->getCategory($category_alias);

                // get the 20 most recent articles
                $articles = $category->getArticles(0, 20);

                // create the feed
                $feed->setTitle($site->getSiteName() . ": Articles - " . $category->getCategoryName());
                $feed->setLink($_SERVER['REQUEST_URI']);
                $feed->setDescription($category->getCategoryDescription());
            }

            // if this is the feed for all articles 
            else {
                // get the 20 most recent articles
                $articles = $artManager->getArticles(0, 20);

                // create the feed
                $feed->setTitle($site->getSiteName() . ": Articles");
                $feed->setLink($_SERVER['REQUEST_URI']);
                $feed->setDescription($site->getSiteName() . " Articles");
            }

            // create the feeditems
            for ($i = 0; $i < count($articles); $i++) {
                $article = $articles[$i];

                $feedItem = $feed->createNewItem();
                $feedItem->setTitle($article->getTitle());
                $feedItem->setLink($article->getURL());

                $img = "<img src='{$article->getImageThumbnail()}' align=left hspace=10 alt='' />";
                $desc = $article->getImage() != "" ? $img . $article->getSummary() : $article->getSummary();

                $feedItem->setDescription($desc);
                $feedItem->setDate(time());
                $feedItem->addElement('author', $article->getAuthor());
                $feedItem->addElement('guid', $article->getURL(), array('isPermaLink' => 'true'));

                $feed->addItem($feedItem);
            }
        }


        // finally generate the feed for the user
        $feed->genarateFeed();
    }

}

?>
