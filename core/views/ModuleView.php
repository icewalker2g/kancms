<?php 

class ModuleView extends View {

	public function __construct($controller, $template = NULL) {
        if (!($controller instanceof Module)) {
            trigger_error("ModuleView Can Only Be Used With An Instance of a Module");
            return;
        }

        parent::__construct($controller, $template);
    }
	
	public function script($path_or_array) {
		return $this->HTML->renderScript( $this->_baseurl(), $path_or_array);
	}
	
	public function css($path_or_array) {
		return $this->HTML->renderCSS( $this->_baseurl(), $path_or_array);
	}
	
	private function _basepath() {
		$module = str_replace("Module", "", get_class($this->controller) );
        $module_folder = $this->camelToLowerWithUnderscores($module);
		$base_path = CORE_PATH . 'modules/' . $module_folder . '/';
		
        return $base_path;
    }
	
	private function _baseurl() {
		$module = str_replace("Module", "", get_class($this->controller) );
        $module_folder = $this->camelToLowerWithUnderscores($module);
		$base_url = CORE_URL . 'modules/' . $module_folder . '/';
		
        return $base_url;
    }
    
    public function path($file) {
		if( !is_string($file) ) return "";
		
		return $this->_basepath() . $file;
	}
	
	public function url($file) {
		if( !is_string($file) ) return "";
		
		return $this->_baseurl() . $file;
	}
	
	/**
     * Loads the global site object and renders the controller data to the template
     * in the theme
     * 
     * @global Site $site 
     */
    public function render($actionName = NULL) {
        $layout_content = NULL;
		
        if( $actionName != NULL ) {
        
            $controller = str_replace("Module", "", get_class($this->controller) );
            $controller_folder = $this->camelToLowerWithUnderscores($controller);
            $action_file = $this->camelToLowerWithUnderscores($actionName);

			// determine if a core module exists
            $path = CORE_PATH . "modules/" . $controller_folder . "/views/" . $action_file . ".php";
			
			// if no core module exists, determine if user defined modules are present
			if( !file_exists($path) ) {
				$path = ASSETS_PATH . "modules/" . $controller_folder . "/views/" . $action_file . ".php";
			}

            if( file_exists($path) ) {
                // Load variables
                foreach ($this->variables as $key => $value) {
                    $$key = $value;
                }

				if( $this->controller->autoRender ) {
					ob_start();
					include_once($path);
					$layout_content = ob_get_contents();
					ob_end_clean();
					
					if( $this->controller->autoLayout ) {
						include_once(CMS_PATH . $this->template);
					}
					
					else {
						echo $layout_content;	
					}
				}
				
            } else {
                trigger_error("Action File Not Found At $path");
            }
        }
    }
    
    private function camelToLowerWithUnderscores($str) {
        return strtolower(preg_replace('/(?<=[a-z])([A-Z])/', '_$1', $str));
    }	
}
?>