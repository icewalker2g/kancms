<?php

class GalleryView extends View {

    protected $template = "gallery.php";

    public function __construct($controller, $template = NULL) {
        if (!($controller instanceof GalleryManager)) {
            trigger_error("GalleryView Can Only Be Used With An Instance of a GalleryManager");
            return;
        }

        parent::__construct($controller, $template);
    }

    public function render() {
        global $site;

        $gallery = NULL;
        $image = NULL;

        if (isset($_GET['gallery'])) {
            $gallery = $this->controller->getGallery(kan_get_parameter('category'));
            $site->setData('SiteDescription', strip_tags($gallery->getDescription()));
        }

        if (isset($_GET['id'])) {
            $image = $this->controller->getGalleryImage(kan_get_parameter('id'));
        } else if (isset($_GET['url'])) {
            // the url parameters
            $parts = explode("/", kan_get_parameter('url'));

            // if the url parameters contains more than two parameters then the url is
            // valid and the article can be loaded. e.g. news/article-title
            if (count($parts) >= 1 && !empty($parts[0])) {
                $gallery = $this->controller->getGallery(kan_clean_input($parts[0]));
                $site->setData('SiteDescription', strip_tags($gallery->getDescription()));
            }

            if (count($parts) >= 2 && !empty($parts[1])) {
                $image = $this->controller->getGalleryImage(kan_clean_input($parts[1]));
            }
        }

        $galleries = $this->controller->getGalleries();

        $this->set('gallery', $gallery);
        $this->set('image', $image);
        $this->set('galleries', $galleries);

        parent::render();
    }

}

?>