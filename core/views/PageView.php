<?php

/**
 * Description of PageView
 *
 * @author Francis Adu-Gyamfi
 */
class PageView extends View {

    /**
     * The controller must be an instance of a PageManager
     * 
     * @var PageManager
     */
    protected $controller;

    public function __construct($controller, $template = NULL) {
        if (!($controller instanceof PageManager)) {
            trigger_error("PageView Can Only Be Used With An Instance of a PageManager");
            return;
        }

        parent::__construct($controller, $template);
    }

    /**
     * Renders the pages requested
     */
    public function render() {
        global $site;

        // we create simple page title for statistics purposes
        $pageTitle = "Home Page";

        // get path to theme index.php file
        $index_file = $site->getTheme()->path('pages/index.php');

        if (isset($_GET['page'])) {

            // try to provide a modecum of security by removing any unwanted data
            // passed via the $_GET['page']
            $param = kan_get_parameter('page');

            $pages = $this->controller;

            // if we are using friendly URLs, we need to reform the URLs correctly
            if (SEF_URLS && strpos($param, "/") >= 0) {
                $param = explode("/", $param);
                //array_shift($param); // remove site ID
                //array_shift($param); // remove content type parameter

                $param = "/" . implode("/", $param); // form the page path as in db
            }

            // check if the current url is a valid page
            if ($pages->isSitePage($param)) {

                // get the current page based on the alias
                $page = $pages->getPage($param);
				
				if( $page ) {
                	$site->setData('SiteDescription', strip_tags($page->getDescription()));
					
					$this->set("page", $page);

					// get the page title for statistical purposes
					$pageTitle = $page->getName();
	
					// get the potential layouts to use for the page
					$view_file = $page->getPageLayout() != "" ? $page->getPageLayout() : "sections.php";
					$layout_file = $site->getTheme()->path("pages/" . $view_file);
	
					// check if layout file exists first incase a layout was chosen
					// for a previous theme and is not currently available with the theme change.
					if (file_exists($layout_file)) {
						$this->template = $view_file;
					} else {
						$this->template = "index.php";
					}
				}

                

                // else default to the index page
            } else {
                $this->template = "index.php";
            }
        } else {
            $this->template = "index.php";
        }


        // use the parent render method
        parent::render($this->variables);
    }

}

?>
