<?php 

/**
 * Base class for all Modules to be developed for the CMS
 */
abstract class Module extends Controller {

	protected $template = "cms_layout.php";
	
	public function __construct($basedir = NULL) {

		$this->db = new Database();
        
        // initiate the models as class variables
        if( $this->models != NULL ) {
			if( $basedir == NULL ) {
				$basedir = CORE_PATH;	
			}
			
            foreach($this->models as $model) {
				$modelPath = $basedir . "/models/" . $model . ".php";
				
                if( file_exists($modelPath) && !class_exists($model) ) {
					include_once($modelPath);
					
                    $this->{$model} = new $model();
                }
				
				else if( class_exists($model) ) {
					$this->{$model}	= new $model();
				}
            }
        }
		
		// initiate the helper classes available for use by this Controller
		if( $this->helpers != NULL ) {
            foreach($this->helpers as $helper) {
                if( class_exists($helper) && is_subclass_of($helper, 'Helper') ) {
                    $this->{$helper} = new $helper($this);
                }
            }
        }
        
        // initiate the view instance
        if( $this->view == NULL ) {
            $this->view = new View($this, $this->template);
        }
	}

	public function index() {
        
    }
	
	public function beforeFilter() {
			
	}
    
    public function fireAction($actionName, $params = array()) {
        
		// call any pre action function
		$this->beforeFilter();

		// filter and call the function
        call_user_func_array(array($this,$actionName), $params);
        
        if( $this->autoRender ) {
            $view = new ModuleView($this, $this->template);
            $view->render($actionName);
        }
		
		// call post action function
		$this->afterFilter();
    }
	
	public function afterFilter() {
		
	}
	
	public function beforeRender() {
		
	}

    /**
     * Renders the contents of the Controller to the view
     */
    public function render() {
	
		// call any callbacks defined
		$this->beforeRender();
		
		// render the view
		$view = new ModuleView($this, $this->template);
		$view->render();
		
		// call any callbacks defined
		$this->afterRender();
    }
    
    public function afterRender() {
		
	}
	
	public function redirect($params = array()) {
		
		$path = CMS_URL . "index.php";
		
		if( is_array($params) ) {
			
			if( isset($params['page']) ) {
				$path .= "?page=" . $params['page'];
				unset($params['page']);
			}
			
			else if( isset($params['module']) ) {
				$path .= "?page=" . $params['module'];
				unset($params['module']);
			}
			
			else if( isset($params['controller']) ){
				$path .= "?page=" . $params['controller'];
				unset($params['controller']);
			}
			
			else {
				$path .= "?page=" . strtolower(get_class($this));	
			}
			
			if( isset($params['action']) ) {
				$path .= "&action=" . $params['action'];
				unset($params['action']);
			}
			
			// append any extra parameters
			foreach($params as $param => $value) {
				$path .= "&" . $param . "=" . urlencode($value);
			}
		}
		
		header("Location: $path");
	}
}
?>