<?php 

if( !defined('CORE_PATH')) die('Access Denied');

kan_import('Link');

/***
 * Provides an implementation of the base Manager class for the purpose of managing
 * the quick_links database table.
 */
class LinksManager extends Manager {

    private $result;
    private $row;
    private $total;
    
    protected $models = array('Link','LinkCategory');

    /**
     * Constructs the LinksManager class and generates the initial number of records
     * to be loaded from the database
     *
     * @param int $total the total number of records to generate
     */
    function __construct($siteid = '', $total = NULL) {
		parent::__construct('quick_links','quick_links_categories');
		$this->setItemClassName('Link');
		$this->setCategoryClassName('LinkCategory');
    }
    
    /**
     * Returns a link from the database
     * 
     * @param mixed $id
     * @return Link
     */
    public function getLink($id) {
        return $this->Link->findFirst(array(
            'filter' => array(
                'OR' => array(
                    'id' => $id
                )
            )
        ));
    }
	
	public function getLinks($start = 0, $limit = NULL) {
        
        return $this->getItems($start, $limit, array(
            'joins' => array(
                array("type" => "inner", "condition" => array("quick_links_categories qlc" => "qlc.id = Link.LinkCategory"))
            ),
            'filter' => array("SiteID" => SITE_ID, "Published" => 'true'),
            "order" => array("LinkCategory, LinkPosition, LinkName")
        ));
    }

    /**
     * Returns an Array of LinkCategory objects containing the data for category objects
     * 
     * @param int $siteid the database record ID of the site to load the categories for
     * @return array an Array of LinkCategory objects
     */
	public function getLinkCategories() {
        return $this->LinkCategory->find( array(
            'filter' => array(
                'SiteID' => SITE_ID
            ),
            'order' => array('CategoryName' => 'ASC')
        ));
	}
	
	public function createCategory() {
		if( !defined("IN_CMS") || IN_CMS ) return;
		
		$linkCat = new LinkCategory(array(
			'SiteID' => SITE_ID,
			'CategoryName' => $_POST['CategoryName'],
			'CategoryDescription' => $_POST['CategoryDescription']
		));
		
		$linkCat->save();
	}
	
	public function updateCategory() {
		if( !defined("IN_CMS") || IN_CMS ) return;
		
		$linkCat = new LinkCategory(array(
			'SiteID' => SITE_ID,
			'CategoryName' => $_POST['CategoryName'],
			'CategoryDescription' => $_POST['CatDesc'],
			'id' => $_POST['EditCatID']
		));
		
		$linkCat->save();
	}

	public function createLink() {
		if( !defined("IN_CMS") || !IN_CMS ) return;
		
		$link = new Link(array(
			'LinkCategory' => $_POST['LinkCategory'],
			'LinkName' => $_POST['LinkName'],
			'LinkDesc' => $_POST['LinkDesc'],
			'LinkURL' => $_POST['LinkURL'],
			'LinkTarget' => $_POST['LinkPosition'],
			'Published' => isset($_POST['Published']) ? 'true' : 'false'
		));		
		$link->save();
	}

	public function updateLink() {
		if( !defined("IN_CMS") || !IN_CMS ) return;
		
		$link = new Link(array(
			'LinkCategory' => $_POST['LinkCategory'],
			'LinkName' => $_POST['LinkName'],
			'LinkDesc' => $_POST['LinkDesc'],
			'LinkURL' => $_POST['LinkURL'],
			'LinkTarget' => $_POST['LinkPosition'],
			'Published' => isset($_POST['Published']) ? 'true' : 'false',
			'id' => $_POST['id']
		));		
		$link->save();
	}
}








?>