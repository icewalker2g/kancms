<?php

if( !defined("CORE_PATH")) die("Accessed Denied");

/**
 * Description of AdvertController
 *
 * @author Francis Adu-Gyamfi
 */
class AdvertsManager extends Manager {
    
    protected $models = array("Advert");
    
    public function __construct() {
        parent::__construct("advert_images");
        $this->setItemClassName("Advert");
    }
    
    public function getAdverts($start = 0, $limit = NULL) {
        return $this->getItems($start, $limit);
    }
}

?>
