<?php

if (!defined('CORE_PATH'))
    die('Access Denied');

class ImageManager extends Manager {

    protected $models = array("Image", "ImageCategory");
    
    public function __construct() {
        parent::__construct('images', 'image_categories');
        $this->setItemClassName('Image');
        $this->setCategoryClassName('ImageCategory');
    }
    
    /**
     * Returns an image from the database based on its ID information
     * 
     * @param mixed $data
     * @return Image 
     */
    public function getImage($data) {
        return $this->getItem($data);
    }
	
	public function getImageCategory($id) {
		return $this->ImageCategory->findFirst(array(
			'filter' => array(
				'OR' => array('id' => $id, 'CategoryCode' => $id)
			)
		));	
	}

    /**
     * Adds a new Image to database, this code is only run within the CMS
     * 
     */
    public function addImage() {
        if (!defined("IN_CMS") && !IN_CMS) {
            return;
        }

        // fix the destination folder for the upload
        $_POST['destFolder'] = kan_fix_path($_POST['destFolder']);
        
        // include file upload processor
        include(CMS_PATH . 'utils/fileUpload.php');
        include(CMS_PATH . 'utils/thumbgen.php');

        // resize the image if it is to be used for a news article
        if ($_POST['UseSection'] == "articles") {
            $fpath = generateThumb($_POST['Picture'], $_POST['destFolder'], 320);
            
        }

        $image = new Image(array(
            "SiteID" => SITE_ID,
            "UseSection" => $_POST['UseSection'],
            "ImageName" => $_POST['ImageName'],
            "ImageDescription" => $_POST['ImageDescription'],
            "ImageFile" => $_POST['Picture'],
            "ThumbPath" => $fpath
        ));
        
        $image->save();
    }
    
    /**
     * Updates the information for the image from the database
     * 
     */
    public function updateImage() {
        if (!defined("IN_CMS") && !IN_CMS) {
            return;
        }
        
        $image = new Image(array(
            "id" => $_POST['id'],
            "UseSection" => $_POST['UseSection'],
            "ImageName" => $_POST['ImageName'],
            "ImageDescription" => $_POST['ImageDescription']
        ));
        
        $image->save();
    }

    /**
     * Deletes the image with the specified ID from the system
     * 
     * @param int $id 
     */
    public function deleteImage($id) {
        if( is_int($id) || intval($id) > 0 ) {
            $this->deleteImages( array($id) );
        }
    }
    
    /** 
     * Deletes all images from the databases and removes their files from the system
     * 
     * @param array $ids 
     */
    public function deleteImages($ids) {
        
        if( is_array($ids) ) {
            $images = $this->Image->find( array(
                'filter' => array("id IN" => $ids)
            ));
            
            foreach($images as $image) {
                $image->deleteFile();
            }
            
            $this->deleteItems($ids);
        }
    }
}

?>