<?php

if( !defined('CORE_PATH')) die("Access Denied");

/**
 * A class to manage all Banners in the KAN Database
 */
class BannerManager extends Manager {

    /**
     * Constructs the BannerManager object and returns all the Banners in the database
     * if the number of banners to return is not specified
     *
     * @param int $total the number of banners to return
     */
    public function __construct($total = NULL) {
		parent::__construct('banner_images');
		$this->setItemClassName('Banner');

    }

    /**
     * Returns an array containing the database records for the banners for the
     * specified site.
     *
     * @param int $siteid the database record ID of the site
     * @param int $total the total number of records to load
     * @return array an array of database records
     */
    public function getBanners($siteid = -1, $total = NULL) {
        $db = $this->getDatabase();
        $query = "SELECT * FROM banner_images WHERE SiteID = %s AND Published = 1 ORDER BY id DESC ";
        $query = sprintf($query, $db->sanitizeInput( $siteid, "int") );

        if( $total != NULL ) {
            $query .= sprintf(" LIMIT 0, %s", $db->sanitizeInput($total, 'int'));
        }

        return $db->query($query)->getResult();
    }
	
	/**
     * Returns an array containing the information about the Banner with the specified id
     * 
     * @param int $id the records ID of the banner
     * @return array an array containing the banner information
     */
	public function getBanner($id) {
		$db = $this->getDatabase();
		$query = sprintf("SELECT * FROM banner_images WHERE id = %s ", $db->sanitizeInput($id,'int') );
		
		return $db->query($query)->getResult();
	}

    /**
     * Returns a randomly selected banner from the database
     * 
     * @return array an array containing the banner information
     */
	public function randomlySelect() {
		if( defined('SITE_ID') ) {
			$db = $this->getDatabase();
			$query = sprintf("SELECT * FROM banner_images WHERE SiteID = %s AND Published = 1 ORDER BY rand() ASC LIMIT 1 ",
					$db->sanitizeInput(SITE_ID,'int'));
			
			return $db->query($query)->getResult();
		}
		
		return array();
	}
}

class Banner extends Model {
	
	public function __construct($data = NULL) {
		parent::__construct($data, 'banner_images');	
	}
}

?>