<?php 

if( !defined('CORE_PATH')) die('Access Denied');

/**
 * Base class for handling all General System related activities such as
 * getting and setting system settings
 */
class SystemManager extends Manager {
	
	// a cache for all retrieved settings, so we don't have to query the 
	// database again if the setting has already been requested for previously
	private $settings_cache = array();

	public function __construct() {
		parent::__construct('system_settings','system_settings_categories');
		$this->setItemClassName('Setting');
		$this->setCategoryClassName('SettingCategory');
	}
	
	/**
	 * Returns an array of SettingCategory objects
	 *
	 * @return array an array of SettingCategory objects
	 */
	public function getSettingCategories() {
		return $this->getCategories();
	}
	
	public function getSettings($category_id = NULL) {
		$db = $this->getDatabase();
		
		$query = sprintf("SELECT * FROM system_settings ");
		
		if( $category_id != NULL ) {
			$query .= sprintf(" WHERE SettingCategory = %s", $db->sanitizeInput($category_id,'int'));	
		}
		
		$db->query($query);
		$settings = array();
		
		for($x = 0; $x < $db->getResultCount(); $x++) {
			array_push($settings, new Setting( $db->getRow($x) ) );	
		}
		
		return $settings;
	}

    /**
     * Returns the current value of the specified setting
     *
     * @param mixed $settingKey the key to use to determine the setting
     * @return mixed the value of the setting
     */
	public function getSetting($settingKey) {
	   // first check if the setting has been previously requested for and cached
	   // in this current page session
	   if( isset($this->settings_cache[$settingKey]) ) {
	       return $this->settings_cache[$settingKey];
	   }
		
	   $db = $this->getDatabase();
	   $query = sprintf("SELECT * FROM system_settings WHERE SettingKey = %s",
	   				$db->sanitizeInput($settingKey, 'text'));
					
	   $row_rsSetting = $db->query($query)->getRow();
	   $setting = isset($row_rsSetting['SettingValue']) ? $row_rsSetting['SettingValue'] : NULL;
	   
	   if( $setting != NULL ) {
	   	   $this->settings_cache[$settingKey] = $setting;
	   }
	   
	   return $setting;
	}

    /**
     * Changes the value of the specified setting
     *
     * @param mixed $settingKey the key of the setting
     * @param mixed $settingValue the value of the setting
     */
	public function saveSetting($settingKey, $settingValue) {
		if( !defined("IN_CMS") || !IN_CMS ) {
			trigger_error("System Settings Should Not Be Changed From The Front End");
			return;
		}

		$db = $this->getDatabase();
		$query = sprintf("UPDATE system_settings SET SettingValue = %s WHERE SettingKey = %s", 
						 $db->santizeInput($settingValue, "text"),
						 $db->santizeInput($settingKey, "text") );
						 	
		$db->query($query);
	}
	
	public function saveSettings() {
		if( !defined("IN_CMS") || !IN_CMS ) {
			trigger_error("System Settings Should Not Be Changed From The Front End");
			return;
		}
		
		$db = $this->getDatabase();
		
		foreach($_POST as $key => $value) {
			if( $key != "action" ) {
				$updateSQL = sprintf("UPDATE system_settings SET SettingValue = %s WHERE SettingKey = %s",
							$db->sanitizeInput($value, 'text'),
							$db->sanitizeInput($key,'text'));
							
				$db->query($updateSQL,true);
			}	
		}
	}
}




?>