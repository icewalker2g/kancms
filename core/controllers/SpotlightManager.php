<?php

if (!defined('CORE_PATH'))  die('Access Denied');

class SpotlightManager extends Manager {
    
    protected $models = array('Spotlight');
    
    function  __construct($total = NULL) {
		parent::__construct('spotlights');
		$this->setItemClassName('Spotlight');
    }
    
    /**
     * Returns a Spotlight object based on the ID or array options specified
     * 
     * @param mixed $data
     * @return Spotlight
     */
    public function getSpotlight($data) {
		if( intval($data) > 0 ) {
			$spotlight = new Spotlight($data);
			return $spotlight;
		}
		
		if( is_array($data) ) {
        	return $this->Spotlight->findFirst($data);
		}
    }
    
    /**
     * 
     */
    public function deleteSpotlight($id) {
        if( !is_int($id) ) {
            trigger_error("ID Must Be An Integer");
            return;
        }
        
        $spotlight = new Spotlight($id);
        $spotlight->delete();
    }

    /**
     * Returns an Array of Spotlight objects for the site
     *
     * @param int $siteid ID of the site
     * @param int $start the start index to load the data from
     * @param int $limit the total number of records to load
     * @return array an array of Spotlight objects
     */
    public function getSpotlights($start = 0, $limit = NULL, $options = NULL) {
		
        $default = array(
            'filter' => array(
                'SiteID' => SITE_ID,
                'approved' => 'true'
            ),
            'order' => array('id' => 'desc'),
            'offset' => $start,
            'limit' => $limit
        );
        
        if( isset($options) && is_array($options) ) {
            $default = array_merge($default, $options);
        }
        
        return $this->Spotlight->find( $default );
    }
    
    public function createSpotlight() {
		
		if( !defined('IN_CMS') || !IN_CMS ) return;
		
		$_POST['destFileName'] = "spotlight" . date('YmdHis');
		$_POST['destFolder'] = "../assets/images/spotlight/";
		$_POST['allowOverwrite'] = "yes";
		$_POST['Picture'] = $_POST['destFolder'] . $_POST['destFileName'] . $_POST['ext'];
		$_POST['ImageFile'] = "spotlight" . date('YmdHis');
		$_POST['maxlimit'] = "500000";
		$_POST['allowedExt'] = "jpg,jpeg,png,swf,jar";
		
		include('utils/fileUpload.php');
		
		$spotlight = new Spotlight(array(
			'SiteID' => SITE_ID,
			'menu_header' => $_POST['menu_header'],
			'menu_subheader' => $_POST['menu_subheader'],
			'spotlight' => strtolower($_POST['Picture']),
			'description' => $_POST['description'],
			'spotlight_url' => $_POST['spotlight_url'],
			'date' => date('Y-m-d'),
			'approved' => isset($_POST['approved']) ? "true" : "false"
		));
		
		$spotlight->save();
			
	}
	
	public function updateSpotlight() {
		if( !defined('IN_CMS') || !IN_CMS ) return;
		
		$spotlight = new Spotlight(array(
			'SiteID' => SITE_ID,
			'menu_header' => $_POST['menu_header'],
			'menu_subheader' => $_POST['menu_subheader'],
			'spotlight' => $_POST['spotlight'],
			'description' => $_POST['description'],
			'spotlight_url' => $_POST['spotlight_url'],
			'date' => date('Y-m-d'),
			'approved' => isset($_POST['approved']) ? "true" : "false",
			'id' => $_POST['id']
		));
		
		$spotlight->save();
	}
}




?>