<?php 

if( !defined('CORE_PATH')) die("Access Denied");

class FileManager extends Controller {


	public function __construct() {
		
	}
	
	public function deleteFile($file) {
		
		if( !file_exists($file) ) {
			return;	
		}
		
		try {
			unlink($file);
		} catch(Exception $e) {
			echo $e;
		}
	}
	
	public function deleteDirectory($dirname) {
	   // Sanity check
	   if (!file_exists($dirname) ) {
		  return false;
	   }
	
	   // Simple delete for a file
	   if (is_file($dirname)) {
		  return unlink($dirname);
	   }
	
	   // Loop through the folder
	   $dir = dir($dirname);
	   while (false !== $entry = $dir->read()) {
		  // Skip pointers
		  if ($entry == '.' || $entry == '..') {
			 continue;
		  }
	
		  // Recurse
		  $this->deleteDirectory("$dirname/$entry");
	   }
	
	   // Clean up
	   $dir->close();
	   return rmdir($dirname);
	}
	
	public function extractZipArchive($filePath, $destFolder = '') {
		// create object
		$zip = new ZipArchive() ;
		
		// open archive
		if ($zip->open( $filePath ) !== TRUE) {
			die ('Could not open archive');
		}
		
		// if the destFolder is not supplied, then we'll need to extract the file to
		// same directory.
		if( $destFolder == '') {
			$destFolder = dirname($filePath);	
		}
		
		// extract contents to destination directory
		$zip->extractTo($destFolder);
		
		// close archive
		$zip->close();   
   }
   
   /** 
     * Copy file or folder from source to destination, it can do 
     * recursive copy as well and is very smart 
     * It recursively creates the dest file or directory path if there weren't exists 
     * Situtaions : 
     * - Src:/home/test/file.txt ,Dst:/home/test/b ,Result:/home/test/b -> If source was file copy file.txt name with b as name to destination 
     * - Src:/home/test/file.txt ,Dst:/home/test/b/ ,Result:/home/test/b/file.txt -> If source was file Creates b directory if does not exsits and copy file.txt into it 
     * - Src:/home/test ,Dst:/home/ ,Result:/home/test/** -> If source was directory copy test directory and all of its content into dest      
     * - Src:/home/test/ ,Dst:/home/ ,Result:/home/**-> if source was direcotry copy its content to dest 
     * - Src:/home/test ,Dst:/home/test2 ,Result:/home/test2/** -> if source was directoy copy it and its content to dest with test2 as name 
     * - Src:/home/test/ ,Dst:/home/test2 ,Result:->/home/test2/** if source was directoy copy it and its content to dest with test2 as name 
     * @todo 
     *     - Should have rollback technique so it can undo the copy when it wasn't successful 
     *  - Auto destination technique should be possible to turn off 
     *  - Supporting callback function 
     *  - May prevent some issues on shared enviroments : http://us3.php.net/umask 
     * @param $source //file or folder 
     * @param $dest ///file or folder 
     * @param $options //folderPermission,filePermission 
     * @return boolean 
     */ 
   public function smartCopy($source, $dest, $options=array('folderPermission'=>0755,'filePermission'=>0755)) 
    { 
        $result=false; 
        
        if (is_file($source)) { 
            if ($dest[strlen($dest)-1]=='/') { 
                if (!file_exists($dest)) { 
                    cmfcDirectory::makeAll($dest,$options['folderPermission'],true); 
                } 
                $__dest=$dest."/".basename($source); 
            } else { 
                $__dest=$dest; 
            } 
            $result=copy($source, $__dest); 
            chmod($__dest,$options['filePermission']); 
            
        } elseif(is_dir($source)) { 
            if ($dest[strlen($dest)-1]=='/') { 
                if ($source[strlen($source)-1]=='/') { 
                    //Copy only contents 
                } else { 
                    //Change parent itself and its contents 
                    $dest=$dest.basename($source); 
                    @mkdir($dest); 
                    chmod($dest,$options['filePermission']); 
                } 
            } else { 
                if ($source[strlen($source)-1]=='/') { 
                    //Copy parent directory with new name and all its content 
                    @mkdir($dest,$options['folderPermission']); 
                    chmod($dest,$options['filePermission']); 
                } else { 
                    //Copy parent directory with new name and all its content 
                    @mkdir($dest,$options['folderPermission']); 
                    chmod($dest,$options['filePermission']); 
                } 
            } 

            $dirHandle=opendir($source); 
            while($file=readdir($dirHandle)) 
            { 
                if($file!="." && $file!="..") 
                { 
                     if(!is_dir($source."/".$file)) { 
                        $__dest=$dest."/".$file; 
                    } else { 
                        $__dest=$dest."/".$file; 
                    } 
                    //echo "$source/$file ||| $__dest<br />"; 
                    $result = $this->smartCopy($source."/".$file, $__dest, $options); 
                } 
            } 
            closedir($dirHandle); 
            
        } else { 
            $result=false; 
        } 
        return $result; 
    }
	
	public function encodeUrlParam ( $string ) {
	  $string = trim($string);
		
	  if ( ctype_digit($string) ) {
		return $string;
		
	  } else {      
		// replace accented chars
		$accents = '/&([A-Za-z]{1,2})(grave|acute|circ|cedil|uml|lig);/';
		$string_encoded = htmlentities($string,ENT_NOQUOTES,'UTF-8');
	
		$string = preg_replace($accents,'$1',$string_encoded);
		  
		// clean out the rest
		$replace = array('([\40])','([^a-zA-Z0-9-])','(-{2,})');
		$with = array('-','','-');
		$string = preg_replace($replace,$with,$string); 
	  } 
	
	  return strtolower($string);
	}

}
?>