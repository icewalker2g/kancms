<?php

/**
 * Include the base KAN classes required for the system to work and configure the KAN
 * database connection. If this configuration fails, KAN will fail to load
 */

include_once('kan.inc');

$siteManager = new SitesManager();

// create the identifier variable
$site_identifier = '';
$site = NULL;

// if a site id has not been defined either via a GET or a POST operation, we'll use the default site
if (!isset($_GET['siteid']) && !isset($_POST['siteid'])) {

    /**
     * For multi-site support, using subdomains, we will try to use the the HTTP_HOST
     * information for this site and load the specified site based on that information
     */
    // get HTTP_HOST information supplied for this site
    $http_host = $_SERVER['HTTP_HOST'];

    // if the host information references do not reference the localhost installation, then
    // we will try to determine which site to load based on the SiteDomain information for the site
    if ($http_host != "localhost") {
        $sites = $siteManager->find('Site', array(
            'filter' => array(
                'OR' => array(
                    'SiteDomain' => $http_host,
                    'SiteType' => 'main'
                )
            )
        ));
        
        // only load the site information if there is actually information
        if ( count($sites) > 0) {
            $site = $sites[0];
            $_GET['siteid'] = $site_identifier = $site->getData('SiteIdentifier');
        } 

        // if this is the localhost installation, then just load the main site
    } else {

        $sites = $siteManager->find('Site', array(
            'filter' => array('SiteType' => 'main')
        ));
        
        if( count($sites) > 0 ) {
            $site = $sites[0];
            $_GET['siteid'] = $site_identifier = $site->getData('SiteIdentifier');
        }

        
    }
} else {
    
    if (isset($_POST['siteid'])) {
        $site_identifier = strip_tags($_POST['siteid']);
    } else if (isset($_GET['siteid'])) {
        $site_identifier = strip_tags($_GET['siteid']);
    }
    
    // retrieve and config the site for use
    $site = $siteManager->getSite($site_identifier);
}



// define a global site path
if( isset($site) ) {
    if (!defined("SITE_ID")) define("SITE_ID", $site->getId());
    if (!defined("SITE_TAG")) define("SITE_TAG", $site->getSiteIdentifier() );
}

// the main site could be determined, exit the system
if (!isset($site)) {
    echo "Could Not Determine The Default Site For This Installation. Please Contact The Site Administrator!";
    exit();
    
} else if (!$site->isActive() && !(isset($_GET['preview']) && $_GET['preview'] === 'true') ) {
    echo "The Site You Are Requesting Is Currently Not Active. Please check back later or Contact Site Administrator";
    exit();

} else if( isset($_GET['preview']) && $_GET['preview'] === 'true' ) { // set the system in preview mode
    if( !defined("PREVIEW_MODE") ) define("PREVIEW_MODE", true);
}



?>