<?php

    // we are simply using this as a redirection page, then redirect early
    if (isset($_GET['goto'])) {
        include_once("../core/kan.inc");
        $goTo = strip_tags(urldecode($_GET['goto']));

        // ensure that this is an internal link, since go to can be exploited
        // for redirection to an external resource
        if (substr($goTo, 0, 3) == '../' || stristr(kan_fix_url($goTo), SITE_ROOT)) {
            header("Location: " . $goTo);
        }
    }

    include_once('../core/SiteLoader.php');

    $pm = new PageManager();
    $pm->render();
?>
