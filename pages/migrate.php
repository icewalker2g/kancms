<?php 

require_once("../core/kan.inc");
$db = new Database();

// migrate the menus
$query = sprintf("SELECT id, SiteID, MenuAlias, MenuName, ShowMenu FROM section_menus");
$db->query($query,true);
$rowsCount = $db->getResultCount();
$rows = $db->getResult();

// iterate the menus
for($i = 0; $i < $db->getResultCount($rows); $i++) {
	$row = $db->getRow($rows, $i);
	$menu_id = $rows[$i]['id'];
	$menu_alias = slug( $row["MenuName"] );
	
	$menuTrans = sprintf("INSERT INTO pages (SiteID, PagePosition, PageName, PageNameAlias, PagePath, PageContent, RedirectURL, PageContentType, Published)
			SELECT SiteID, MenuPos, MenuName, %s AS Alias, (%s) AS alias, MenuContent, TargetPage, 'html' AS type, %s AS pub FROM section_menus WHERE id = %s",
		
		$db->sanitizeInput( $menu_alias, 'text'),
		$db->sanitizeInput( "/" . $menu_alias, 'text'),
		$db->sanitizeInput( $row['ShowMenu'] == 'true' ? 1 : 0, 'int' ),
		$db->sanitizeInput( $menu_id, 'int') );
	
	echo $menuTrans . "<br / >";	
	$db->query($menuTrans, true);
	$menu_page_id = $db->getInsertId();
	//$new_page_id = $i;
	
	// iterate the pages
	$query = sprintf("SELECT id, SectionAlias, SectionTitle, Published FROM section_pages WHERE MenuID = %s",
				$db->sanitizeInput( $menu_id, 'int') );
	$pages = $db->query($query,true)->getResult();
	
	for($j = 0; $j < $db->getResultCount($pages); $j++) {
		$page_row = $db->getRow($pages, $j);
		$page_id = $page_row['id'];
		$page_alias = slug( $page_row['SectionTitle'] );
		
		$pageTrans = sprintf("INSERT INTO pages (SiteID, ParentID, PagePosition, PageName, PageNameAlias, PageDescription, PagePath, PageContent, RedirectURL, PageContentType, Published)
			SELECT %s AS SiteID, %s AS parent, MenuPos, SectionTitle, %s AS alias, Description, (%s) AS path, Content, AlternativeURL, ContentType, %s AS pub FROM section_pages WHERE id = %s",
		
		$db->sanitizeInput( $row['SiteID'], 'int' ),
		$db->sanitizeInput( $menu_page_id, 'int' ),
		$db->sanitizeInput( $page_alias, 'text'),
		$db->sanitizeInput( "/" . $menu_alias . "/" . $page_alias, 'text'),
		$db->sanitizeInput( $page_row['Published'] == 'true' ? 1 : 0, 'int' ),
		$db->sanitizeInput( $page_id, 'int') );
		
		echo $pageTrans . "<br />";
		$db->query($pageTrans, true);
		$section_page_id = $db->getInsertId();
		
		// iterate the sub page content
		$query = sprintf("SELECT id, ContentAlias, ContentTitle, Published FROM section_page_content WHERE SectionID = %s",
					$db->sanitizeInput( $page_id, 'int') );
		$content = $db->query($query,true)->getResult();
		
		for($k = 0; $k < $db->getResultCount($content); $k++) {
			$content_row = $db->getRow($content, $k);
			$content_id = $content_row['id'];
			$content_alias = slug( $content_row['ContentTitle'] );
			
			$contentTrans = sprintf("INSERT INTO pages (SiteID, ParentID, PagePosition, PageName, PageNameAlias, PageSummary, PagePath, PageContent, RedirectURL, PageContentType, Published)
				SELECT %s AS SiteID, %s AS parent, SectionPos, ContentTitle, %s AS alias, Summary, (%s) AS path, Content, RedirectURL, ContentType, %s AS pub FROM section_page_content WHERE id = %s",
			
			$db->sanitizeInput( $row['SiteID'], 'int' ),
			$db->sanitizeInput( $section_page_id, 'int' ),
			$db->sanitizeInput( $content_alias, 'text'),
			$db->sanitizeInput( "/" . $menu_alias . "/" . $page_alias . "/" . $content_alias, 'text'),
			$db->sanitizeInput( $content_row['Published'] == 'true' ? 1 : 0, 'int' ),
			$db->sanitizeInput( $content_id, 'int') );
			
			echo $contentTrans . "<br />";
			$db->query($contentTrans, true);
			$content_page_id = $db->getInsertId();
		}
	}
	
	echo "<br />";
}

// ********************************
// next migrate the news to the articles system
// **********************************

// create the news category
$query = sprintf("SELECT id, SiteID, Category, CategoryDescription FROM news_categories");
$db->query($query,true);
$categories = $db->getResult();

for( $i = 0; $i < $db->getResultCount($categories); $i++) {
	$cat_row = $db->getRow($categories, $i);
	$cat_id = $cat_row['id'];
	$cat_tag = slug( $cat_row['Category'] );
	
	$insertSQL = sprintf("INSERT INTO article_categories (SiteID, CategoryName, CategoryAlias, CategoryDescription) VALUES(%s, %s, %s, %s)",
		$db->sanitizeInput($cat_row['SiteID'],'int'),
		$db->sanitizeInput($cat_row['Category'],'text'),
		$db->sanitizeInput($cat_tag,'text'),
		$db->sanitizeInput($cat_row['CategoryDescription'],'text'));
		
	$db->query($insertSQL,true);
	$new_cat_id = $db->getInsertId();
	
	// get the articles in the old category and save them under the new article category
	$selectSQL = sprintf("SELECT * FROM news WHERE CategoryID = %s", $db->sanitizeInput($cat_id,'int'));
	$articles = $db->query($selectSQL,true)->getResult();
	
	for($j = 0; $j < $db->getResultCount($articles); $j++) {
		$article = $db->getResult($articles, $j);
		$article_slug = slug( $article['Title'] );
		
		$insertSQL = sprintf("INSERT INTO articles (CategoryID, SiteID, ArticleTitle, ArticleTitleAlias, ArticleSummary, ArticleContent, ArticleSource, ArticleImage, ArticleThumbnail, ArticleURL, ArticleGallery, PostDate, Published)
					VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s) ",
					$db->sanitizeInput($new_cat_id, 'int'),
					$db->sanitizeInput($cat_row['SiteID'],'int'),
					$db->sanitizeInput($articles['Title'],'text'),
					$db->sanitizeInput($article_slug,'text'),
					$db->sanitizeInput($articles['Summary'],'text'),
					$db->sanitizeInput($articles['Content'],'text'),
					$db->sanitizeInput($articles['ContentOwner'],'text'),
					$db->sanitizeInput($articles['Picture'],'text'),
					$db->sanitizeInput($articles['ThumbPath'],'text'),
					$db->sanitizeInput($articles['RedirectURL'],'text'),
					$db->sanitizeInput($articles['AssociatedGallery'],'text'),
					$db->sanitizeInput($articles['PostDate'],'text'),
					$db->sanitizeInput($articles['Published'] == "true" ? 1 : 0,'int'));
					
		$db->query($insertSQL);
	}
}
?>