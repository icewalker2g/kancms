<?php 
if( !isset($_GET['page']) ) 
	header("Location: sites.php"); 

require_once("accesscheck.php"); 


if( isset($_GET['page']) ) {
	$page = 'forms/' . kan_clean_input($_GET['page']) . '.php';
	$module = CORE_PATH . 'modules/' . kan_clean_input($_GET['page']) . '/index.php';
	
	if( !file_exists($module) ) {
		$module = ASSETS_PATH . 'modules/' . kan_clean_input($_GET['page']) . '/index.php';
	}

	if( file_exists($page) ) {
		include_once($page);	
	}

	else if( file_exists($module) ) {
		include_once($module);	
		
		$string = new String(); // initiate a string helper instance
		$controller = $string->toCamel($_GET['page'] . "_module");
		
		if( class_exists($controller) ) {
			$controller = new $controller();
			$controller->set('site',$site); // pass the global site object
			
			if( isset($_GET['action']) ) {
				$action = NULL;
				
				// pass any other parameters that remain from the $_GET array
				// might not be the best approach for now until we switch to 
				// clean URLs
				unset($_GET['page']);
								
				if( method_exists($controller, $_GET['action']) ) {
					$action = $_GET['action'];
					
				} else {
					$underscored = $string->camelToLowerWithUnderscores($_GET['action']);
					$camelled = $string->toCamel( $underscored );
					
					if( method_exists($controller, $underscored) ) {
						$action = $underscored;
					}
					
					else if( method_exists($controller, $camelled) ) {
						$action = $camelled;	
					}
				}
				
				if( $action == NULL ) {
					trigger_error("Action " . $_GET['action'] . " Or Case Variant Not Found");	
				} else {			
					unset($_GET['action']);
					$controller->fireAction($action, $_GET);
				}
				
			} else {
				$controller->fireAction("index");
			}
		}
	}
}
?>