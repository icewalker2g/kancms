<?php 
require("accesscheck.php"); 
require_once("utils/pui_content_table_renderer.php");
include_once("utils/file_utils.php");

$tm = new ThemeManager();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/cms.dwt.php" codeOutsideHTMLIsLocked="false" -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<!-- InstanceBeginEditable name="doctitle" -->
<title><?php echo getSetting('CMSTitle','KAN Content Management System'); ?></title>
<!-- InstanceEndEditable -->
<link rel="shortcut icon" type="image/png" href="../favicon.png" />
<link rel="stylesheet" type="text/css" href="css/widestyles.css"/>
<link rel="stylesheet" type="text/css" href="css/cms.css"/>
<link rel="stylesheet" type="text/css" href="css/cms-ui.css"/>
<link rel="stylesheet" type="text/css" href="css/util.css"/>

<script type="text/javascript" src="../assets/scripts/jquery/js/jquery.js"></script>
<script type="text/javascript" src="../assets/scripts/jquery/js/jquery-ui.js"></script>
<script type="text/javascript" src="../assets/scripts/common/pageManager.js"></script>
<script type="text/javascript" src="scripts/system.js"></script>
<script type="text/javascript" src="scripts/ui.js"></script>

<!-- InstanceBeginEditable name="head" -->
<link rel="stylesheet" type="text/css" href="css/themes.css"/>
<?php $fileUtils->printScripts(); ?>
<style type="text/css">
.css_table td, .css_table th {
	font-size: 11px;
    font-family: Tahoma, Geneva, sans-serif;    
}

div#theme-description ul, div#theme-description ol {
    margin-left: 10px;
	padding-left: 15px;    
}

div#theme-description p {
	margin-top: 0px;    
}

div#theme-description {
	overflow: hidden;    
}
</style>
<!-- InstanceEndEditable -->
</head>

<body>

<div id="page">
	<div id="header">
    	<?php include('logo_header.php'); ?>
    </div>
    
    <?php if( isset($_SESSION['CMS_Username']) ) { ?>
    <div id="site-info-bar">
		<?php include('system_menu.php'); ?>
    </div>
    <?php } ?>
    
    
    <div id="content-wrapper">
    	<div id="content-header">
        	<span>
			<!-- InstanceBeginEditable name="section title" -->CONTENT MANAGEMENT SECTION<!-- InstanceEndEditable -->
            </span>
        </div>
    	<div id="content">
			<!-- InstanceBeginEditable name="content" -->
            <div id="nav">
                <?php include('nav_section.php'); ?>
            </div>
            
            <div id="main">
				
                <!--<div class="cms-content-header">Themes</div>-->
                
                
                <div id="content-pane" class="cms-content-pane">
                    <div class="articles-pane">
                        <div id="art-pane-header" class="pane-header">Themes</div>
                        <div class="pane-sub-header">
                        	<a id="add-link" href="?create"><img src="images/icons/css_add.png" alt="theme" width="16" height="16" border="0" align="absmiddle" /> Create New Theme</a>
                            <a id="theme-install-link" href="?add"><img src="images/icons/page_white_compressed.png" alt="install" width="16" height="16" border="0" align="absmiddle" /> Install New Theme</a>
                            <a id="delete-link" href="#"><img src="images/icons/newspaper_delete.png" alt="del" width="16" height="16" border="0" align="absmiddle" /> Delete Selected</a>
                        </div>
                        <div id="articles-display" class="pane-content">
                            <div class="split_view">
                                <div class="left_content">
									<div class="row header">
                                    	<input type="checkbox" name="row-check" id="all-checker" />
                                        Installed Themes
                                    </div>
                                    
                                    <div id="theme-rows" style="max-height:550px; overflow: auto;">
									<?php 
										$themes = $tm->getThemes();
										$count = 1;
										
										foreach($themes as $theme) { ?>
											<div id="row-<?php echo $theme->getId(); ?>" class="row">
												<input type="checkbox" name="row-check" class="row-checker" id="checker" value="<?php echo $theme->getId(); ?>" />
												<a class="theme-name-link" data-id="<?php echo $theme->getId(); ?>" href="#" title="Preview Theme On Right">
													<?php echo $count++ . ". " . $theme->getData('ThemeName'); ?>
												</a>
												
												<!--<a href="#" class="edit-link" data-id="<?php echo $theme->getId(); ?>" title="Edit Theme"></a>
												<a href="#" class="delete-link" data-id="<?php echo $theme->getId(); ?>" title="Delete Theme"></a>-->
											</div>											
											<?php
										}
									?>
                                    </div>

                                </div>
                                <div class="right_content">
                                	<div class="row header" style="padding: 9px;">
                                        <span>
                                            Theme Details
                                        </span>
                                        <span class="nav">
                                            <a id="theme-edit-link" class="nav-link" href="#"><img src="images/icons/page_edit.png" alt="del" width="16" height="16" border="0" align="absmiddle" /> Edit</a>
                                            <!--<a id="css-install-link" href="#" ><img src="images/icons/page_white_compressed.png" alt="del" width="16" height="16" border="0" align="absmiddle" /> Install CSS</a>-->
                                            <a id="theme-duplicate-link" class="nav-link" href="#"><img src="images/icons/page_copy.png" alt="del" width="16" height="16" border="0" align="absmiddle" /> Duplicate</a>
                                            <a id="theme-export-link" class="nav-link" href="#"><img src="images/icons/page_white_compressed.png" alt="del" width="16" height="16" border="0" align="absmiddle" /> Export</a>
                                            <a id="theme-delete-link" class="nav-link" href="#"><img src="images/icons/page_delete.png" alt="del" width="16" height="16" border="0" align="absmiddle" /> Delete</a>
                                        </span>
                                    </div>
                                    <div id="info_view" style="padding:10px;">
                                    	<div >Select Theme On Left To View Details Here</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                       
                    </div>
                </div>
            </div>
            
            <script type="text/javascript" async="async" src="scripts/tiny_mce/tiny_mce.js"></script>
            <script type="text/javascript" src="scripts/themes.js"></script>
            <script type="text/javascript">
            	pageManager.addLoadEvent(function(){
					themes.init();
				});
            </script>
            <!-- InstanceEndEditable -->
        </div>
    </div>
    
    <div id="footer">
    	<?php include('footer.php'); ?>
    </div>
</div>

</body>
<!-- InstanceEnd --></html>