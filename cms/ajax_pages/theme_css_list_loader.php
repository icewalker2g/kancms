<?php
require_once('../accesscheck.php');

$sm = new SitesManager();
$themeCSS = $sm->find('ThemeCSS', array(
    'filter' => array(
        'ThemeID' => isset($_GET['themeid']) ? $_GET['themeid'] : -1
    )
    ));
?>
<select name="SiteThemeCSSID" id="SiteThemeCSSID">
    <?php
    foreach ($themeCSS as $css) {
        $selected = $css->getData('id') == $_GET['cssid'] ? "selected=selected" : "";
        ?>
        <option value="<?php echo $css->getData('id') ?>"<?php echo $selected; ?>><?php echo $css->getData('CSSName'); ?></option>
    <?php } ?>
</select>
