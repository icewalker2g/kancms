<?php 

require_once('../accesscheck.php');

kan_import('GalleryManager');
$gm = new GalleryManager();

if( isset($_POST['action']) ) {

	$action = kan_get_parameter('action');
	
	switch($action) {
		
		case 'add_gal':
			$gallery = $gm->createNewGallery(); // returns a Gallery object
			//header( "Location: ../galleries_alt.php" );
			echo json_encode( $gallery->getDataArray() );
			break;
			
		case 'edit_gal':
			$gm->updateEditedGallery();
			header( "Location: ../galleries.php" );	
			break;	
		
		case 'delete-gal':
			$gm->deleteSelectedGalleries( explode(",", kan_get_parameter('gal-ids')) );
			break;
			
		case 'delete-images':
			$gm->deleteGalleryImages( explode(",", kan_get_parameter('img-ids')) );
			break;
			
		case 'upload-image':
			$image = $gm->uploadAndInsertImage();
			echo json_encode( $image->getDataArray() );
			break;
			
		case 'load-image-data':
			$id = $_POST['id'];
			$image = $gm->getImage( $id );
			echo json_encode($image->getDataArray());
			break;
			
		case 'save-image-data':
			$gm->updateEditedImage();
			break;
			
		case 'show-images':
			$gallery_id = $_POST['gallery_id'];
			$page = $_POST['page'];
			$total = $_POST['total'];
			showImagesOnPage( $gallery_id, $page, $total );
			break;
			
		case 'publish':
		case 'unpublish':
			$publish = $_POST['action'] == 'publish';
			$gm->publishGallery( $_POST['id'], $publish );
			break;
			
		case 'check-tag':
			$unused = $gm->isTagUnused( $_POST['tag'] );
			echo $unused ? 'true' : 'false';
			break;
	}
	
	
}

function showImagesOnPage($gallery_id, $page, $total) { 
	global $gm;
	
	$gallery = $gm->getGallery( $gallery_id ); 
	$images = $gallery->getImages( $page * $total, $total ); ?>

    <ul><?php
        
        for( $i = 0; $i < count($images); $i++ ) { ?>
            <li id="img-item-<?php echo $images[$i]->getId(); ?>">
                <div class="image_box">
                    <img src="<?php echo $images[$i]->getThumbImageURL(); ?>" alt="<?php echo $images[$i]->getName(); ?>" data-url="<?php echo $images[$i]->getImageURL(); ?>" />
                </div>
                <div class="options_box">
                    <div class="name">
                        <?php echo $images[$i]->getName(); ?>
                    </div>
                    <div class="links">
                        <input type="checkbox" name="item2" id="item" value="<?php echo $images[$i]->getId(); ?>"  />
                        <a class='edit-link' href="#" data-id="<?php echo $images[$i]->getId(); ?>" data-name="<?php echo $images[$i]->getName(); ?>"><img src="images/icons/image_edit.png" alt="edit" width="16" height="16" border="0" align="absmiddle" /> Edit</a>
                        <a class="delete-link" href="#" data-id="<?php echo $images[$i]->getId(); ?>" data-name="<?php echo $images[$i]->getName(); ?>"><img src="images/icons/image_delete.png" alt="del" width="16" height="16" border="0" align="absmiddle" /> Delete</a>
                    </div>
                </div>
            </li><?php
        }
        
        if( count($images) == 0 ) {
            echo "<div>There are no images on this page.</div>";
        } ?>
    </ul><?php
}
?>