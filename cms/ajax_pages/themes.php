<?php 

	include_once('../accesscheck.php');
	kan_import('ThemeManager');
	
	$themeManager = new ThemeManager();
	
	if( isset($_POST['action']) || isset($_GET['action']) ) {
	
		$action = kan_get_parameter('action');
		
		switch($action) {
			
			case 'export_theme':
				$themeManager->exportTheme( kan_get_parameter('theme_id') );
				break;
			
			case 'export_css':
				$themeManager->exportStyle( kan_get_parameter('cssid') );
				break;
				
			case 'import_theme':
				$themeManager->importTheme();
				//$updateGoTo = "../themes.php";
		  		//header(sprintf("Location: %s", $updateGoTo));
				break;
				
			case 'load_info':
				$theme_id = $_POST['theme_id'];
				$theme = $themeManager->getTheme($theme_id);
				
				include("../forms/theme_view_form.php");
				break;
				
			case 'edit_theme':
				$theme_id = $_POST['theme_id'];
				$theme = $themeManager->getTheme($theme_id);
				
				include("../forms/theme_update_form.php");
				break;
				
			case 'create_theme':
				$themeManager->createTheme();
				break;
				
			case 'update_theme':
				$themeManager->updateTheme();
				break;
				
			case 'delete_theme':
				$theme_id = $_POST['theme_id'];
				$themeManager->deleteTheme($theme_id);
				break;
				
			case 'delete_themes':
				$ids = explode(",",$_POST['ids']);
				$themeManager->deleteThemes($ids);
				break;
            
            case 'duplicate':
                $theme_id = $_POST['theme_id'];
                $theme = $themeManager->duplicateTheme( $theme_id );
                
                echo $theme->toJSON();
                break;
		}
	}
?>