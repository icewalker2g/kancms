<?php

require_once("../accesscheck.php");
require_once("../utils/pui_content_table_renderer.php");

$manager = new DownloadsManager();
$renderer = new ContentTableRenderer("Downloads");
$renderer->setItemsTitle("Download Name / Title");

$action = $_POST['action'];

switch ($action) {

    case 'load_items':
        $category_id = intval($_POST['category']);
        $limit = intval($_POST['limit']);
        $start = isset($_POST['start']) ? $_POST['start'] : 0;
        $category = $manager->getDownloadCategory($category_id);
        $downloads = $category->getDownloads($start, $limit); // start, limit

        // get the total number of articles in this category and set that as the
        // the total content count for this load
        $renderer->setTotalItemsCount($category->getTotalDownloadsCount());

        $downloadsData = array();

        for ($i = 0; $i < count($downloads); $i++) {
            $title = $downloads[$i]->getName();
            $id = $downloads[$i]->getId();
            $url = $downloads[$i]->getURL();
            $published = $downloads[$i]->isPublished();

            array_push($downloadsData, array(
                "title" => $title,
                "id" => $id,
                "url" => $url,
                "published" => $published,
                "download_count" => $downloads[$i]->getDownloadCount()
            ));
        }

        $renderer->setItemsArray($downloadsData);
        $renderer->addColumn( array('header' => 'Downloaded', 'field' => 'download_count'));
        $renderer->render();
        break;

    case 'load_category':
        $category_id = $_POST['category_id'];
        $category = $manager->getDownloadCategory($category_id);

        if ($category) {
            echo json_encode($category->getDataArray());
        }

        break;

    case 'add_category':
        $category = $manager->addCategory();

        if ($category) {
            echo json_encode($category->getDataArray());
        }

        break;

    case 'update_category':
        $category = $manager->updateCategory();

        if ($category) {
            echo json_encode($category->getDataArray());
        }

        break;

    case 'delete_category':
        $category_id = $_POST['category_id'];
        $manager->deleteDownloadCategory($category_id);
        break;

    case 'delete_items':
        $manager->deleteDownloads(explode(",", $_POST['item_ids']));
        break;

    case 'publish_item':
        $id = $_POST['item_id'];
        $publish = $_POST['publish'] == "true";

        $manager->publishDownload($id, $publish);
        break;

    case 'move_items':
        $ids = $_POST['item_ids'];
        $category_id = $_POST['category_id'];
        $manager->moveDownloads(explode(",", $ids), $category_id);
        break;

    case 'load_editor':
        include('../forms/downloads_form.php');
        break;

    case 'add_download':
        $manager->addDownload();
        break;

    case 'update_download':
        $manager->updateDownload();
        break;
}
?>