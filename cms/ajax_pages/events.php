<?php

/***
 * This page is accessed via an AJAX call to manipulate News Data, ie
 * deleting, moving, etc.
 */

// ensure this file is only executed once we have been logged in
include("../accesscheck.php");
include('../../core/controllers/EventsManager.php');

// create a copy of the manager
$events = new EventsManager();
$db = $events->getDatabase();

// if an action has been requested, check the action and carry out the request
// if all required parameters are met
if( isset($_POST['action']) && stristr($_POST['action'],"publish") != '' && isset($_POST['id']) ) {

	$id = $_POST['id'];
	$publish = $_POST['action'] == "publish" ? 'true' : 'false';
	$updateSQL = "UPDATE events SET Published = '$publish' WHERE id = $id";
	$result = $db->query($updateSQL,true);

} else if( isset($_POST['action']) && isset($_POST['image_id']) ) {

	$image_id = $_POST['image_id'];
	$query = "SELECT ImageFile, ThumbPath FROM images WHERE id = $image_id ";
	$imageData = $db->query($query,true)->getRow();

	echo json_encode($imageData);

} else if( isset($_POST['action']) && $_POST['action'] == "delete_articles" ) {
	$article_ids = $_POST['article_ids'];
	$query = "DELETE FROM events WHERE id IN ($article_ids) ";
	$result = $db->query($query,true);

} else if( isset($_POST['action']) && $_POST['action'] == "add_category" ) {
	$categoryName = $_POST['CategoryName'];
	$categoryDesc = $_POST['CategoryDesc'];

	$query = sprintf("INSERT INTO event_categories (SiteID, Category, CategoryAlias, CategoryDescription) VALUES(%s, %s, %s, %s) ",
					$db->sanitizeInput($siteid, "int"),
					$db->sanitizeInput($categoryName, "text"),
					$db->sanitizeInput(slug($categoryName), 'text' ),
					$db->sanitizeInput($categoryDesc, "text"));

	// get the id of the newly insert category
	$id = $db->query($query,true)->getInsertId();

	// echo the result
	echo json_encode( array(
		"id" => $id,
		"categoryName" => $categoryName,
		"categoryDesc" => $categoryDesc
	));

} else if( isset($_POST['action']) && $_POST['action'] == "delete_category" ) {
	$category_id = $_POST['category_id'];
	$query = "DELETE FROM event_categories WHERE id = $category_id ";
	$db->query($query,true);

} else if( isset($_POST['action']) && $_POST['action'] == "load_events" ) {

   // get the number of records to be returned and the position to start returning
   // them from limit for the number of records to returned
   $limit = isset($_POST['limit']) ? $_POST['limit'] : 20;
   $start = isset($_POST['start']) ? $_POST['start'] * $limit : 0;

   // get the category to which the articles belong
   $cat = $_POST['category'];

   // get a category object
   $category = $events->getEventCategory($cat);
   
   // retrieve the articles in the specified category
   $catEvents = $category->getEvents($start, $limit, false, false);

   // get the total number of articles in this category
   $total_articles = $category->getTotalEventCount();

   if( $total_articles == 0 ) {
      echo "No Events Have Been Posted In This Category For This Site";
      return;
   }

   echo "
		<table border=0 cellspacing=0 cellpadding=4 width=100% class='cms-data-table'>
			<tr>
				<th align=right><input type='checkbox' value='' id='checkAll' name='checkAll' /></th>
				<th align='left'>Event Name</th>
				<th>Options</th>
			</tr>
	";

   for($i = 0, $j = $start; $i < count($catEvents); $i++, $j++) {
	  $event = $catEvents[$i];
	  
      $title = $event->getName();
      $id = $event->getId();
	  $published = $event->isPublished();
	  $isPast = $event->isPast();
	  
	  $pub_link_class = $published ? 'cms-publish-link' : 'cms-unpublish-link';
	  $pub_link_title = $published ? 'Published (Click To Unpublish)' : 'Unpublised (Click To Publish Now)';
	  
	  $valid_event_class = $isPast ? 'event-past-icon' : 'event-upcoming-icon';
	  $upcoming_row_class = $isPast ? 'event-past' : 'event-upcoming';
	  $valid_event_title = ($isPast ? 'Event Is Past - ' : 'Event Is Upcoming - ') . $event->getStartDate('d M, Y');
	  
	  $short_title = (strlen($title) > 55) ? (substr($title, 0, 55) . "...") : $title;

      echo "
			<tr id='article-row-$id' class='$upcoming_row_class'>
				<td align=right width='25'><input type='checkbox' value='$id' /></td>
				<td>" . ($j + 1) . ". <a id='$id' class='edit-link' href='?edit&id=$id' title='$title'>$short_title</a></td>
				<td align=center width='35%'>
					<a id='$id' class='edit-link' href='?edit&id=$id'><img src='images/icons/newspaper.png' alt='' align='absmiddle' border=0 /> Edit</a>
					<a id='$id' class='delete-link' href='#'><img src='images/icons/newspaper_delete.png' alt='' align='absmiddle' border=0 /> Delete</a>
					<a id='$id' class='$valid_event_class' href='#' title='$valid_event_title'></a>
					<a id='$id' class='$pub_link_class' href='#' title='$pub_link_title'></a>
				</td>
			</tr>
         ";
   }

   echo "
		</table>
	";

	// echo out the values of the total article count as hidden field in order to be accessed
	// by the javascript processor
	// we'll do this if only the first page is being loaded
   if( !isset($_POST['no-total']) ) {
	   echo "
			<input type='hidden' id='articles-total' value='$total_articles' />
	   ";
   }
}
?>
