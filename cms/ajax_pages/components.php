<?php 

	include_once('../accesscheck.php');
	
	$compManager = new ComponentsManager();
	
	if( kan_get_parameter('action') ) {
	
		$action = kan_get_parameter('action');
		
		switch($action) {
			
			case 'export_component':
				$compManager->exportComponent( kan_get_parameter('component_id') );
				break;
				
			case 'create_component':
				$compManager->createNewComponent();
				
				$updateGoTo = "../components.php";
		  		header(sprintf("Location: %s", $updateGoTo));
				break;
				
			case 'update_component':
				$compManager->updateComponent();
				
				$updateGoTo = "../components.php";
		  		header(sprintf("Location: %s", $updateGoTo));
				break;
				
			case 'import_component':
				$compManager->importComponent();
				
				$updateGoTo = "../components.php";
		  		header(sprintf("Location: %s", $updateGoTo));
				break;
				
			case 'delete_component':
				$id = intval($_POST['id']);
				$compManager->deleteComponent($id);
				break;
				
		}
	}
?>