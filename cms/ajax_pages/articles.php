<?php 

/****
 * articles.php
 * 2010-04-27 Francis Adu-Gyamfi
 *
 * Handles all AJAX process requests from the main articles manager interface
 *
 */
require_once("../accesscheck.php"); 
kan_import('ArticlesManager');

$articles = new ArticlesManager();
$db = $articles->getDatabase();

/***********************************************************
 * AJAX Content Loading
 */
if( isset($_POST['action']) && $_POST['action'] == "load_articles" ) {

   $cat = $_POST['category'];
   $category = $articles->getCategory($cat);
   
   // get the number of records to be returned and the position to start returning
   // them from limit for the number of records to returned
   $limit = isset($_POST['limit']) ? $_POST['limit'] : 25;
   $start = isset($_POST['start']) ? $_POST['start'] * $limit : 0;
   
   $data = $category->getArticles($start,$limit,false);

   if( count($data) == 0 ) {
      echo "No Articles Have Been Posted In This Category For This Site";
      return;
   }
   
   // get the total number of articles in this category
   $total_articles = $category->getTotalArticleCount();

   echo "
		<table border=0 cellspacing=0 cellpadding=4 width=100% class='cms-data-table'>
			<tr>
				<th align=right><input type='checkbox' value='' id='checkAll' name='checkAll' /></th>
				<th align='left'>Article Title</th>
				<th>Options</th>
			</tr>
	";

   for($i = 0; $i < count($data); $i++) {
      $title = $data[$i]->getTitle();
      $id = $data[$i]->getId();
	  $url = $data[$i]->getURL();
	  
	  $pub_link_class = $data[$i]->isPublished() ? 'cms-publish-link' : 'cms-unpublish-link';
	  $pub_link_title = $data[$i]->isPublished() ? 'Published (Click To Unpublish)' : 'Unpublised (Click To Publish Now)';
	  
      echo "
			<tr id='article-row-$id'>
				<td align=right width='25'><input type='checkbox' value='$id' /></td>
				<td>" . ($i + 1) . ". <a id='$id' class='edit-link' href='?edit&id=$id'>$title</a></td>
				<td align=center width='34%'>
					<a id='$id' class='edit-link' href='?edit&id=$id'><img src='images/icons/newspaper.png' alt='' align='absmiddle' border=0 /> Edit</a>
					<a id='$id' class='delete-link' href='#'><img src='images/icons/newspaper_delete.png' alt='Delete' align='absmiddle' border=0 /> Delete</a>
					<a id='$id' class='preview-link' href='$url' target='_blank'><img src='images/icons/magnifier.png' alt='Preview' align='absmiddle' border=0 /></a>
					<a id='$id' class='$pub_link_class' href='#' title='$pub_link_title'></a>
				</td>
			</tr>
         ";
   }

   echo "
		</table>
	";

	// echo out the values of the total article count as hidden field in order to be accessed
	// by the javascript processor
	// we'll do this if only the first page is being loaded
   if( !isset($_POST['no-total']) ) {
	   echo "
			<input type='hidden' id='articles-total' value='$total_articles' />
	   ";
   }
} 

/*************************************************
 * AJAX Processing
 */ 
if( isset($_POST['action']) ) {
	
	switch($_POST['action']) {
		case 'load_category':
			$category_id = $_POST['category_id'];
			$category = $articles->getCategory($category_id);
			echo $category->toJSON();
			break;
			
		case 'add_category':
			$category = $articles->addCategory();
			echo $category->toJSON();
			break;
		
		case 'update_category':
			$category = $articles->updateCategory();
			echo $category->toJSON();
			break;
			
		case 'delete_category':
			$category_id = $_POST['category_id'];
			$articles->deleteCategory($category_id);
			break;
		
		case 'add_article':
		case 'edit_article':
			// include the form for the articles editor display
   			include("../forms/articles_form.php");
			break;
			
		case 'create_article':
			// create a new article
		   $articles->createNewArticle();
		   break;
		
		case 'update_article':
			$articles->updateArticle();
			break;
			
		case 'publish_article':
			$articles->publishArticle( $_POST['article_id'], $_POST['publish'] == 'true' );
			break;
			
		case 'delete_articles':
			$articles->deleteArticles( explode(",",$_POST['article_ids']) );
			break;
			
		case 'move_articles':
			$articles->moveArticles( explode(",",$_POST['msg_ids']), $_POST['category_id'] );
			break;
			
		case "approve_comments":
			$articles_ids = explode(",",$_POST['ids']);
			$articles->approveComments($articles_ids, $_POST['approve']);
			break;
			
		case "delete_comments":
			$articles_ids = explode(",",$_POST['ids']);
			$articles->deleteComments($articles_ids);
			break;
			
		case "update_comment_text":
			$comment_id = $_POST['comment_id'];
			$comment_text = $_POST['comment_text'];
			
			$db = $articles->getDatabase();
			$query = sprintf("UPDATE article_comments SET Message = %s WHERE id = %s",
					$db->sanitizeInput($comment_text, 'text'),
					$db->sanitizeInput($comment_id,'int'));
					
					
			$db->query($query,true);
			break;
	}
}
?>	
