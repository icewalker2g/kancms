<?php 

require_once("../accesscheck.php"); 

// get an instance of the stats manager
$stats = new StatsManager();

// get an instance of the sites manager
$sites = new SitesManager();


// check if we are clear site specific statistics
if( isset($_POST['action']) && $_POST['action'] == "clear_site_stats" ) {

	$range = $_POST['range'];
	$site_id = $_POST['site_id'];
	
	if( $range == "day" ) {
		$clearDate = $_POST['date'];
		$cleared = $stats->clearSiteStatsData($site_id, $clearDate);
		
		return $cleared;
		
	} else if( $range == "all" ) {
		$cleared = $stats->clearAllSiteStatsData($site_id);
		
		return $cleared;
	}
	
	
} else if( isset($_POST['action']) && $_POST['action'] == 'load_field_helps' ) {
		
	// query for all the help information specific to database fields
	$query = "SELECT * FROM system_help WHERE FieldID <> '' ";
	$result = mysqli_query($config, $query) or die(mysqli_error());
	
	$str = "{ \"fields\" : [ ";
	while( $row_result = mysqli_fetch_assoc($result) ) {
		$field_id = $row_result['FieldID'];
		$helpText = $row_result['HelpText'];
		
		$str .= "{\"field_id\" :\"$field_id\"},";
	}
	
	$str .= "] }";
	
	// remove the last comma if exist
	$str = str_replace(",]", "]", $str);
	
	// output the JSON object
	echo $str;
	
} else if( isset($_POST['action']) && $_POST['action'] == "load_help" ) {
	$field = $_POST['field'];
	$query = "SELECT * FROM system_help WHERE FieldID = '$field' ";
	$result = mysqli_query($config, $query) or die(mysqli_error());
	$row_result = mysqli_fetch_assoc($result);
	
	header("Expires: Mon, 26 Jul 1997 05:00:00 GMT"); // Date in the past
	header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT"); // always modified
	header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
	header("Pragma: no-cache"); // HTTP/1.0
	header("Content-Type: text/javascript");
	
	echo "<div style='padding:10px;'><b>{$row_result['HelpTitle']}</b><br />";
	echo "{$row_result['HelpText']}</div>";
	
	
} else if( isset($_POST['action']) && $_POST['action'] == 'load_site_links' ) {
	
	// siteid variable available from 'accesscheck.php'
	$links = $sites->getSiteLinksArray($siteid);
	$str = "<div class='cms-tree' style='height: 200px; overflow: auto;'>";
	$str .= "<ul>";
	
	for($i = 0; $i < count($links); $i++) {
		$link_group = $links[$i];
		$group_title = $link_group['group_title'];
		
		$str .= "
			<li class='dir_name'><span>$group_title</span>
				<ul style='display:none;'>";
		
		$group_links = $link_group['links'];
		
		for($j = 0; $j < count($group_links); $j++) {
			
			$title = $group_links[$j]['title']; 
			$href = $group_links[$j]['href']; 
			$str .= "<li class='file_name'><a href='#' title='$title' data-href='$href' >$title</a></li>";
		}
		
		$str .= "
				</ul>
			</li>";
	}
	
	$str .= "</ul>
		</div>";
	
	echo $str;
	
} else if( isset($_POST['action']) && $_POST['action'] == "save_settings") {
	
	// save the settings via the global system manager
	$system->saveSettings();
	header("Location: ../system.php?saved");
}

?>