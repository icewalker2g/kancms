<?php 

/***
 * This page is accessed via an AJAX call to manipulate News Data, ie
 * deleting, moving, etc.
 */

// ensure this file is only executed once we have been logged in
include("../accesscheck.php");

$currentPage = $_SERVER["PHP_SELF"];

// if an action has been requested, check the action and carry out the request
// if all required parameters are met
if( isset($_POST['action']) && stristr($_POST['action'],"publish") != '' && isset($_POST['id']) ) {
	
	$id = $_POST['id'];
	$publish = $_POST['action'] == "publish" ? 'true' : 'false';
	$updateSQL = "UPDATE section_menus SET ShowMenu = '$publish' WHERE id = $id";
	$result = mysql_query($updateSQL, $config);
	
}

?>