<?php 

/***
 * This page is accessed via an AJAX call to manipulate News Data, ie
 * deleting, moving, etc.
 */

// ensure this file is only executed once we have been logged in
include("../accesscheck.php");
include('../../core/controllers/LinksManager.php');

// if an action has been requested, check the action and carry out the request
// if all required parameters are met
if( isset($_POST['action']) && stristr($_POST['action'],"publish") != '' && isset($_POST['id']) ) {
	
    $link = new Link($_POST['id']);
    $link->publish($_POST['action'] == "publish");
	
} else if( isset($_POST['action']) && $_POST['action'] == "delete_links" ) { 
    
    $link = new Link();
    $link->deleteAll(array('id IN' => explode(",",$link_ids)));

} else if( isset($_POST['action']) && $_POST['action'] == "add_category" ) { 

	$categoryName = isset($_POST['CategoryName']) ? $_POST['CategoryName'] : "";
	$categoryDesc = isset($_POST['CategoryDesc']) ? $_POST['CategoryDesc'] : "";
    
    $category = new LinkCategory(array(
        'SiteID' => SITE_ID,
        'CategoryName' => $categoryName,
        'CategoryDescription' => $categoryDesc
    ));
    $category->save();
	
	// echo the result
	echo json_encode( array(
		"id" => $category->getId(), 
		"categoryName" => $categoryName,
		"categoryDesc" => $categoryDesc
	));

} else if( isset($_POST['action']) && $_POST['action'] == "delete_category" ) { 
    
    $category = new LinkCategory($_POST['category_id']);
    $category->delete();

} else if( isset($_POST['action']) && $_POST['action'] == "move_links" ) {
    
    $category_id = $_POST['category_id'];
    $msg_ids = $_POST['msg_ids'];
    
    $link = new Link();
    $link->updateAll(array('LinkCategory' => $category_id), array('id IN' => explode(",", $msg_ids)));

} else if( isset($_POST['action']) && $_POST['action'] == "load_links" ) {

   // create a copy of the manager
   $lm = new LinksManager();
   $db = $lm->getDatabase();
   
   // get the number of records to be returned and the position to start returning
   // them from limit for the number of records to returned
   $limit = isset($_POST['limit']) ? $_POST['limit'] : 20;
   $start = isset($_POST['start']) ? $_POST['start'] * $limit : 0;
   
   // get the category to which the articles belong
   $cat = $_POST['category'];
   $category = $lm->getCategory($cat);
   
   // retrieve all the links in the specified category
   $links = $category->getLinks($start, $limit, false);
   
   // get the total number of articles in this category
   $total_links = $category->getLinksCount(false);

   if( $total_links == 0 ) {
      echo "No Links Have Been Created In This Category For This Site";
      return;
   }

   echo "
		<table border=0 cellspacing=0 cellpadding=4 width=100% class='cms-data-table'>
			<tr>
				<th align=right width='25'><input type='checkbox' value='' id='checkAll' name='checkAll' /></th>
				<th align='left'>Article Title</th>
				<th>Options</th>
			</tr>
	";

   for($i = 0, $j = $start; $i < count($links); $i++, $j++) {
      $title = $links[$i]->getData('LinkName');
      $id = $links[$i]->getData('id');
	  $published = $links[$i]->getData('Published');
	  
	  $pub_link_class = $published == "true" ? 'cms-publish-link' : 'cms-unpublish-link';
	  $pub_link_title = $published == "true" ? 'Published (Click To Unpublish)' : 'Unpublised (Click To Publish Now)';
	  $short_title = (strlen($title) > 55) ? (substr($title, 0, 55) . "...") : $title;
	  
      echo "
			<tr id='article-row-$id'>
				<td align=right width='25'><input type='checkbox' value='$id' /></td>
				<td>" . ($j + 1) . ". <a id='$id' class='edit-link' href='?edit&id=$id' title='$title'>$short_title</a></td>
				<td align=center width='30%'>
					<a id='$id' class='edit-link' href='?edit&id=$id'><img src='images/icons/link_edit.png' alt='' align='absmiddle' border=0 /> Edit</a>
					<a id='$id' class='delete-link' href='#'><img src='images/icons/link_delete.png' alt='' align='absmiddle' border=0 /> Delete</a>
					<a id='$id' class='$pub_link_class' href='#' title='$pub_link_title'></a>
				</td>
			</tr>
         ";
   }

   echo "
		</table>
	";

	// echo out the values of the total article count as hidden field in order to be accessed
	// by the javascript processor
	// we'll do this if only the first page is being loaded
   if( !isset($_POST['no-total']) ) {
	   echo "
			<input type='hidden' id='link-total' value='$total_links' />
	   ";
   }
}
?>