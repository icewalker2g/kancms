<?php

require_once('../accesscheck.php');
require_once("../utils/pui_content_table_renderer.php");

$manager = new MediaManager();
$renderer = new ContentTableRenderer("Media");
$renderer->setItemsTitle("Name");

$action = $_POST['action'];

switch ($action) {

    case 'delete_media':
        $id = intval($_POST['id']);
        $manager->deleteMedia($id);
        break;

    case 'load_items':
        $category_id = intval($_POST['category']);
        $limit = intval($_POST['limit']);
        $start = isset($_POST['start']) ? $_POST['start'] : 0;
        $category = $manager->getCategory($category_id);
        $media = $category->getMedia($start, $limit); // start, limit
        // get the total number of articles in this category and set that as the
        // the total content count for this load
        $renderer->setTotalItemsCount($category->getMediaCount());

        $mediaData = array();
        
        for ($i = 0; $i < count($media); $i++) {
            $title = $media[$i]->getData('MediaName');
            $id = $media[$i]->getId();
            $url = kan_fix_url($media[$i]->getPath());
            $published = $media[$i]->isPublished();

            array_push($mediaData, array(
                "title" => $title,
                "id" => $id,
                "url" => $url,
                "published" => $published,
            ));
        }

        $renderer->setItemsArray($mediaData);
        $renderer->render();
        break;
        
    case 'delete_category':
        $category_id = $_POST['category_id'];
        $manager->deleteCategory($category_id);
        break;

    case 'delete_items':
        $manager->deleteMedia(explode(",", $_POST['item_ids']));
        break;

    case 'publish_item':
        $id = $_POST['item_id'];
        $publish = $_POST['publish'] == "true";

        $manager->publishitem($id, $publish);
        break;
	
	case 'move_items':
        $ids = $_POST['item_ids'];
        $category_id = $_POST['category_id'];
        $manager->moveItems(explode(",", $ids), $category_id);
        break;
	
	case 'load_category':
        $category_id = $_POST['category_id'];
        $category = $manager->getCategory($category_id);

        if ($category) {
            $data = array(
				'id' => $category->getId(),
				'CategoryName' => $category->getData('Name'),
				'CategoryAlias' => $category->getData('Alias'),
				'CategoryDescription' => $category->getData('Description'),
				'Position' => $category->getData('Position')
			);
            echo json_encode($data);
        }

        break;
		
	case 'add_category':
        $category = $manager->addCategory();

        if ($category) {
			$data = array(
				'id' => $category->getId(),
				'CategoryName' => $category->getData('Name'),
				'CategoryDescription' => $category->getData('Description'),
				'Position' => $category->getData('Position')
			);
            echo json_encode($data);
        }

        break;
		
	case 'update_category':
        $category = $manager->updateCategory();

        if ($category) {
			$data = array(
				'id' => $category->getId(),
				'CategoryName' => $category->getData('Name'),
				'CategoryDescription' => $category->getData('Description'),
				'Position' => $category->getData('Position')
			);
            echo json_encode($data);
        }

        break;
		
	case 'load_editor':
		include('../forms/media_form.php');
        break;
		
	case 'add_media':
		$media = $manager->addMedia();
		
		if( $media ) {
			echo $media->toJSON();	
		}
		break;
		
	case 'update_media':
		$media = $manager->updateMedia();
		
		if( $media ) {
			echo $media->toJSON();	
		}
		break;
}
?>
