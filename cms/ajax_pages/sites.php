<?php 

/***
 * This page is accessed via an AJAX call to manipulate Site Data, ie
 * deleting, moving, etc.
 */

// ensure this file is only executed once we have been logged in
include("../accesscheck.php");
kan_import('SitesManager');

// get a reference to the SitesManager class.
$sites = new SitesManager();

// it is possible to delete site via a URL parameter
// if an id has been supplied with the delete option also specified, then delete
// the site with the specifed ID
/*if ((isset($_GET['id'])) && ($_GET['id'] != "") && (isset($_GET['delete']))) {
    $sites->deleteSite($_GET['id']);
}*/

// deleting of sites via AJAX is done with post data
// hence, if a list of items have been posted, with a delete option specified, run through
// and delete the specified site files
if( isset($_POST['items']) && isset($_POST['action']) && $_POST['action'] == "delete" ) {
    $count = $_POST['items'];
	
    for( $i = 1; $i <= $count; $i++) {
        if( isset($_POST['item' . $i]) && intval($_POST['item' . $i]) > 0 ) {
            $sites->deleteSite($_POST['item' . $i]);
        }
    }
}

// sets the main site by first making all sites sub sites, and then updating the specific
// site as the main site

if( isset($_POST['action']) && $_POST['action'] == "set_default" ) {
    $id = $_POST['id'];

    $sites->setSiteAsDefault( $id );
}

// changes the activate status of the specified site

if( isset($_POST['action']) && $_POST['action'] == "set_active" ) {
    $ids = $_POST['ids'];
    $activate = $_POST['state'] == "active" ? 1 : 0;

    $sites->setSiteAsActive( explode(",", $ids), $activate );
}

if( isset($_POST['action']) && $_POST['action'] == "clear_cache") {
    $ids = $_POST['ids'];

    $sites->clearSiteCache( explode(",", $ids) );
}
?>