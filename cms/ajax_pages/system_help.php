<?php 

	//require_once('../accesscheck.php');
	require_once('../../assets/config/config.php');
	
	// query for all the help information specific to database fields
	if( isset($_POST['action']) && $_POST['action'] == 'load_field_helps' ) {
		$query = "SELECT * FROM system_help WHERE FieldID <> '' ";
		$result = mysqli_query($config, $query) or die(mysqli_error());
		
		$str = "{ \"fields\" : [ ";
		while( $row_result = mysqli_fetch_assoc($result) ) {
			$field_id = $row_result['FieldID'];
			$helpText = $row_result['HelpText'];
			
			$str .= "{\"field_id\" :\"$field_id\"},";
		}
		
		$str .= "] }";
		
		// remove the last comma if exist
		$str = str_replace(",]", "]", $str);
		
		// output the JSON object
		echo $str;
		
	} else if( isset($_POST['action']) && $_POST['action'] == "load_help" ) {
		$field = $_POST['field'];
		$query = "SELECT * FROM system_help WHERE FieldID = '$field' ";
		$result = mysqli_query($config, $query) or die(mysqli_error());
		$row_result = mysqli_fetch_assoc($result);
		
		header("Expires: Mon, 26 Jul 1997 05:00:00 GMT"); // Date in the past
		header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT"); // always modified
		header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
		header("Pragma: no-cache"); // HTTP/1.0
		header("Content-Type: text/javascript");
		
		echo "<div style='padding:10px;'><b>{$row_result['HelpTitle']}</b><br />";
		echo "{$row_result['HelpText']}</div>";
	}
?>