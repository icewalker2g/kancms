<?php 

/***
 * AJAX HANDLER FOR SPOTLIGHTS
 */
 
require_once('../accesscheck.php');

$sm = new SpotlightManager();
$action = isset($_POST['action']) ? $_POST['action'] : "";

switch($action) {
    
    case 'preview':
        $spotlight = $sm->getSpotlight( $_POST['id'] );
        echo $spotlight->render();
        break;
    
    case 'delete_spotlight':
        $sm->deleteSpotlight( intval($_POST['id']) );
        echo json_encode( array('id' => $_POST['id']) );
        break;
    
    case 'publish':
    case 'unpublish':
        $publish = $_POST['action'] == "publish";
        $sm->getSpotlight( $_POST['id'] )->publish($publish);
        break;
}
?>