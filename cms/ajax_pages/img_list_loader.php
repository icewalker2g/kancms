<?php 

include("../accesscheck.php"); 
$im = new ImageManager();

if( isset($_POST['action']) && $_POST['action'] == "delete_images" ) {
	$img_ids = $_POST['img_ids'];
	
	// get the information about the images selected, so as to be able to delete them
	$query = "SELECT * FROM images WHERE id IN ($img_ids)";
	$rsImages = mysqli_query($config, $query) or die(mysqli_error());
	
	// loop through and delete the selected images
	while( $row_rsImages = mysqli_fetch_assoc($rsImages) ) {
		$imagePath = $row_rsImages['ImageFile'];
		$thumbPath = $row_rsImages['ThumbPath'];
		
		// try to delete the files, and do not throw any errors if we
		// do not have permissions or files are not found
		@unlink($imagePath);
		@unlink($thumbPath);
	}
	
	// once images have been hopefully deleted from the server, we'll delete
	// the records from the database
	$query = "DELETE FROM images WHERE id IN ($img_ids) ";
	$result = mysqli_query($config, $query) or die(mysqli_error());
	
	// update the action variable to indicate that we should load images now
	$_POST['action'] == "load_images";
}

if( isset($_POST['action']) && $_POST['action'] == 'load_images' ) {

	$startPos = $_POST['startPos'];
	$limit = $_POST['limit'];
	
	$colname_rsCMSImages = "-1";
	if (isset($_POST['cat'])) {
	  $colname_rsCMSImages = $_POST['cat'];
	}
	
	$category = $im->getImageCategory($_POST['cat']);
	$images = $category->getImages($startPos, $limit); ?>

    <form id="delForm" name="delForm" method="post" action="utils/del.php?delete&amp;tbl=images&amp;ref=../image_manager.php&amp;ea=deleteFiles" style="margin:0px; position:relative;">
       
       <?php 
       if( count($images) > 0 ) { ?>
           <div id="area_header">
                <input type="checkbox" name="checker" id="checker" onclick="checkAll(this.checked)" />
                <?php echo $category->field('CategoryName'); ?> Images
           </div>
           
           <div id="img_area">
            <?php $count = 0;
			
			foreach($images as $img) { 
				$row_rsCMSImages = $img->getDataArray(); ?>
                
              <div class="img_box">
                
                <div class="img_con"><?php 
                    if( file_exists('../' . $row_rsCMSImages['ThumbPath']) ) {
                        list($srcW, $srcH, $srcType, $srcAttr) = getimagesize('../' . $row_rsCMSImages['ThumbPath']);
                    } ?>
                    <a href="?edit&amp;id=<?php echo $row_rsCMSImages['id']; ?>"><img alt="" src="<?php echo $row_rsCMSImages['ThumbPath']; ?>" name="prevImg" <?php if( isset($srcH) && $srcH >= 110 ) echo 'height="110"'; ?> id="prevImg" border="0" /></a>
                </div>
                 
                 
                 <div class="img_options">
                    <div class="img_name" align="center"><?php echo $row_rsCMSImages['ImageName']; ?></div>
                    <input name="item<?php echo $count; ?>" type="checkbox" id="item<?php echo $count; ?>" value="<?php echo $row_rsCMSImages['id']; ?>" />
                    <input name="ftd<?php echo $count; ?>" type="hidden" id="ftd<?php echo $count; ?>" value="<?php echo $row_rsCMSImages['ImageFile']; ?>" />
                    <input name="ftd2<?php echo $count; ?>" type="hidden" id="ftd2<?php echo $count; ?>" value="<?php echo $row_rsCMSImages['ThumbPath']; ?>" />
                    <a href="?edit&amp;id=<?php echo $row_rsCMSImages['id']; ?>"><img src="images/icons/image_edit.png" alt="edit" width="16" height="16" border="0" align="absmiddle" /> Edit</a>
                       &nbsp;&nbsp;<img src="images/icons/image_delete.png" alt="delete" width="16" height="16" border="0" align="absmiddle" />
                    <a href="#" onclick="return image_manager.deleteImage(this, <?php echo $row_rsCMSImages['id']; ?>);"> Delete</a>
                 </div>
              </div>
              <?php 
                $count++; 
              } ?>
              <input name="items" id="items" type="hidden" value="<?php echo $count; ?>" />
           </div>
       <?php 
       } else  { ?>
            <div style="clear:both">No Images Available In The Selected Section</div>
       <?php 
       } ?>
       
       
    </form>
<?php

}
?>
