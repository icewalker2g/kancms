<?php include("accesscheck.php"); 


$editFormAction = $_SERVER['PHP_SELF'];
if (isset($_SERVER['QUERY_STRING'])) {
  $editFormAction .= "?" . htmlentities($_SERVER['QUERY_STRING']);
}

if ((isset($_POST["MM_update"])) && ($_POST["MM_update"] == "form2")) {
  $updateSQL = sprintf("UPDATE advert_images SET AdvertName=%s, AdvertDesc=%s, AdvertPath=%s, AdvertType=%s, Width=%s, Height=%s WHERE id=%s",
                       GetSQLValueString($_POST['AdvertName'], "text"),
                       GetSQLValueString($_POST['AdvertDesc'], "text"),
                       GetSQLValueString($_POST['AdvertPath'], "text"),
                       GetSQLValueString($_POST['AdvertType'], "text"),
                       GetSQLValueString($_POST['Width'], "int"),
                       GetSQLValueString($_POST['Height'], "int"),
                       GetSQLValueString($_POST['id'], "int"));

  mysql_select_db($database_config, $config);
  $Result1 = mysql_query($updateSQL, $config) or die(mysql_error());

  $updateGoTo = "adverts.php";
  header(sprintf("Location: %s", $updateGoTo));
}

if ((isset($_POST["MM_insert"])) && ($_POST["MM_insert"] == "form1")) {
  include('utils/fileUpload.php');

  $insertSQL = sprintf("INSERT INTO advert_images (AdvertName, AdvertDesc, AdvertPath, AdvertType, Width, Height) VALUES (%s, %s, %s, %s, %s, %s)",
                       GetSQLValueString($_POST['AdvertName'], "text"),
                       GetSQLValueString($_POST['AdvertDesc'], "text"),
                       GetSQLValueString($_POST['AdvertPath'], "text"),
                       GetSQLValueString($_POST['AdvertType'], "text"),
                       GetSQLValueString($_POST['Width'], "int"),
                       GetSQLValueString($_POST['Height'], "int"));

  mysql_select_db($database_config, $config);
  $Result1 = mysql_query($insertSQL, $config) or die(mysql_error());

  $insertGoTo = "adverts.php";
  header(sprintf("Location: %s", $insertGoTo));
}

mysql_select_db($database_config, $config);
$query_rsAdverts = "SELECT advert_images.id, advert_images.AdvertName, advert_images.AdvertDesc, advert_images.AdvertPath, advert_images.Width, advert_images.Height, advert_images.AdvertType, advert_types.TypeName FROM advert_images , advert_types WHERE advert_types.TypeExtension =  advert_images.AdvertType";
$rsAdverts = mysql_query($query_rsAdverts, $config) or die(mysql_error());
$row_rsAdverts = mysql_fetch_assoc($rsAdverts);
$totalRows_rsAdverts = mysql_num_rows($rsAdverts);

mysql_select_db($database_config, $config);
$query_rsAdvertTypes = "SELECT * FROM advert_types ORDER BY advert_types.TypeName";
$rsAdvertTypes = mysql_query($query_rsAdvertTypes, $config) or die(mysql_error());
$row_rsAdvertTypes = mysql_fetch_assoc($rsAdvertTypes);
$totalRows_rsAdvertTypes = mysql_num_rows($rsAdvertTypes);

$colname_rsAdvertEdit = "-1";
if (isset($_GET['bid'])) {
  $colname_rsAdvertEdit = $_GET['bid'];
}
mysql_select_db($database_config, $config);
$query_rsAdvertEdit = sprintf("SELECT * FROM advert_images WHERE id = %s", GetSQLValueString($colname_rsAdvertEdit, "int"));
$rsAdvertEdit = mysql_query($query_rsAdvertEdit, $config) or die(mysql_error());
$row_rsAdvertEdit = mysql_fetch_assoc($rsAdvertEdit);
$totalRows_rsAdvertEdit = mysql_num_rows($rsAdvertEdit);

mysql_select_db($database_config, $config);
$query_rsAdTypeCats = "SELECT * FROM advert_types_cats";
$rsAdTypeCats = mysql_query($query_rsAdTypeCats, $config) or die(mysql_error());
$row_rsAdTypeCats = mysql_fetch_assoc($rsAdTypeCats);
$totalRows_rsAdTypeCats = mysql_num_rows($rsAdTypeCats);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/cms.dwt.php" codeOutsideHTMLIsLocked="false" -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<!-- InstanceBeginEditable name="doctitle" -->
<title><?php echo getSetting('CMSTitle','KAN Content Management System'); ?></title>
<!-- InstanceEndEditable -->
<link rel="shortcut icon" type="image/png" href="../favicon.png" />
<link rel="stylesheet" type="text/css" href="css/widestyles.css"/>
<link rel="stylesheet" type="text/css" href="css/cms.css"/>
<link rel="stylesheet" type="text/css" href="css/cms-ui.css"/>
<link rel="stylesheet" type="text/css" href="css/util.css"/>

<script type="text/javascript" src="../assets/scripts/jquery/js/jquery.js"></script>
<script type="text/javascript" src="../assets/scripts/jquery/js/jquery-ui.js"></script>
<script type="text/javascript" src="../assets/scripts/common/pageManager.js"></script>
<script type="text/javascript" src="scripts/system.js"></script>
<script type="text/javascript" src="scripts/ui.js"></script>

<!-- InstanceBeginEditable name="head" -->
<script type="text/javascript" src="scripts/adverts.js"></script><script type="text/javascript">
function MM_validateForm() { //v4.0
  if (document.getElementById){
    var i,p,q,nm,test,num,min,max,errors='',args=MM_validateForm.arguments;
    for (i=0; i<(args.length-2); i+=3) { test=args[i+2]; val=document.getElementById(args[i]);
      if (val) { nm=val.name; if ((val=val.value)!="") {
        if (test.indexOf('isEmail')!=-1) { p=val.indexOf('@');
          if (p<1 || p==(val.length-1)) errors+='- '+nm+' must contain an e-mail address.\n';
        } else if (test!='R') { num = parseFloat(val);
          if (isNaN(val)) errors+='- '+nm+' must contain a number.\n';
          if (test.indexOf('inRange') != -1) { p=test.indexOf(':');
            min=test.substring(8,p); max=test.substring(p+1);
            if (num<min || max!=num) errors+='- '+nm+' must contain a number between '+min+' and '+max+'.\n';
      } } } else if (test.charAt(0) == 'R') errors += '- '+nm+' is required.\n'; }
    } if (errors) alert('The following error(s) occurred:\n'+errors);
    document.MM_returnValue = (errors == '');
} }

</script>
<!-- InstanceEndEditable -->
</head>

<body>

<div id="page">
	<div id="header">
    	<?php include('logo_header.php'); ?>
    </div>
    
    <?php if( isset($_SESSION['CMS_Username']) ) { ?>
    <div id="site-info-bar">
		<?php include('system_menu.php'); ?>
    </div>
    <?php } ?>
    
    
    <div id="content-wrapper">
    	<div id="content-header">
        	<span>
			<!-- InstanceBeginEditable name="section title" -->Manage Advertisements<!-- InstanceEndEditable -->
            </span>
        </div>
    	<div id="content">
			<!-- InstanceBeginEditable name="content" -->
        <div id="nav">
            <?php include('nav_section.php'); ?>
        </div>
        <div id="main">
            <table width="98%" border="0" align="center" cellpadding="3" cellspacing="0" class="newsTbl">
                <?php if( isset($_GET['edit']) ) { ?>
                <tr>
                    <td class="sqrtab">Edit Advert INformation</td>
                </tr>
                <tr>
                    <td class="newsSummary">
                        <div class="cms-form-message">
                            Use this form to edit the information about the selected advert
                        </div>
                        <form action="<?php echo $editFormAction; ?>" method="post" name="form2" id="form2">
                            <table width="381" align="center" cellpadding="5" cellspacing="0">
                                <tr >
                                    <td width="104" align="right" nowrap="nowrap" class="sectionTitle1">Advert Name:</td>
                                    <td colspan="3">
                                        <input type="text" name="AdvertName" value="<?php echo htmlentities($row_rsAdvertEdit['AdvertName'], ENT_COMPAT, 'iso-8859-1'); ?>" size="32" />
                                    </td>
                                </tr>
                                <tr >
                                    <td align="right" nowrap="nowrap" class="sectionTitle1">Advert Desc:</td>
                                    <td colspan="3">
                                        <input type="text" name="AdvertDesc" value="<?php echo htmlentities($row_rsAdvertEdit['AdvertDesc'], ENT_COMPAT, 'iso-8859-1'); ?>" size="32" />
                                    </td>
                                </tr>
                                <tr  style="display:none;">
                                    <td align="right" nowrap="nowrap" class="sectionTitle1">Advert Type:</td>
                                    <td colspan="3">
                                        <select name="AdvertType" class="small">
                                            <?php 
do {  
?>
                                            <option value="<?php echo $row_rsAdvertTypes['TypeExtension']?>" <?php if (!(strcmp($row_rsAdvertTypes['TypeExtension'], htmlentities($row_rsAdvertEdit['AdvertType'], ENT_COMPAT, 'iso-8859-1')))) {echo "SELECTED";} ?>><?php echo $row_rsAdvertTypes['TypeName']?></option>
                                            <?php
} while ($row_rsAdvertTypes = mysql_fetch_assoc($rsAdvertTypes));
?>
                                        </select>
                                    </td>
                                </tr>
                                <tr >
                                    <td align="right" nowrap="nowrap" class="sectionTitle1">Width:</td>
                                    <td width="74">
                                        <input type="text" name="Width" value="<?php echo htmlentities($row_rsAdvertEdit['Width'], ENT_COMPAT, 'iso-8859-1'); ?>" size="6" />
                                    </td>
                                    <td width="60"><span class="sectionTitle1">Height:</span></td>
                                    <td width="101">
                                        <input name="Height" type="text" id="Height" value="<?php echo htmlentities($row_rsAdvertEdit['Height'], ENT_COMPAT, 'iso-8859-1'); ?>" size="6" />
                                    </td>
                                </tr>
                                <tr >
                                    <td align="right" nowrap="nowrap" class="sectionTitle1">&nbsp;</td>
                                    <td colspan="3">&nbsp;</td>
                                </tr>
                                <tr >
                                    <td colspan="4" align="center" nowrap="nowrap">
                                        <input type="submit" value="Update Info" />
                                        <input type="button" name="button2" id="button2" value="Cancel Update" onclick="cancelFormSubmit('Cancel Update Of Advert Information?', 'adverts.php') " />
                                        &nbsp;&nbsp;&nbsp;
                                        <input type="button" name="button4" id="button4" value="Delete Image" 
                            onclick="if(confirm('Delete Current Image From Database and Server?')) location.href='utils/del.php?del=advert_images&amp;id=<?php echo $_GET['bid']; ?>&amp;ref=../adverts.php&amp;ea=deleteFile&amp;fpath=<?php echo $row_rsAdvertEdit['AdvertPath']; ?>';"/>
                                    </td>
                                </tr>
                            </table>
                            <input type="hidden" name="AdvertPath" value="<?php echo htmlentities($row_rsAdvertEdit['AdvertPath'], ENT_COMPAT, 'iso-8859-1'); ?>" />
                            <input type="hidden" name="MM_update" value="form2" />
                            <input type="hidden" name="id" value="<?php echo $row_rsAdvertEdit['id']; ?>" />
                        </form>
                    </td>
                </tr>
                <?php } ?>
                <?php if( isset($_GET['upload']) ) { ?>
                <tr>
                    <td class="sqrtab">Upload Advert IMage</td>
                </tr>
                <tr>
                    <td class="newsSummary">
                        <div class="cms-form-message">
                            Fill the fields below and hit Upload Advert to add a new advert to the site
                            <ul>
                                <li>
                                    <strong>Note:</strong> if the <strong>Advert Dimensions</strong> dont match the default Width and Height, please specify the new values to prevent image distortion&nbsp;
                                </li>
                            </ul>
                        </div>
                        <form action="<?php echo $editFormAction; ?>" method="post" enctype="multipart/form-data" name="form1" id="form1" onsubmit="MM_validateForm('Height','','RisNum','AdvertName','','R','Width','','RisNum');return document.MM_returnValue">
                            <table align="center" cellpadding="5" cellspacing="0">
                                <tr >
                                    <td width="95" align="right" nowrap="nowrap" class="sectionTitle1">Advert Name:</td>
                                    <td colspan="3">
                                        <input name="AdvertName" type="text" class="sectionText1" id="AdvertName" value="" size="32" />
                                    </td>
                                </tr>
                                <tr >
                                    <td align="right" nowrap="nowrap" class="sectionTitle1">Description:</td>
                                    <td colspan="3">
                                        <input name="AdvertDesc" type="text" class="sectionText1" value="" size="50" />
                                    </td>
                                </tr>
                                <tr >
                                    <td align="right" nowrap="nowrap" class="sectionTitle1">Advert Type:</td>
                                    <td colspan="3">
                                        <select name="AdvertType" id="AdvertType" class="sectionText1" onchange="setTypeCatsEnabled(this.options[this.selectedIndex].value)">
                                            <?php 
do {  
?>
                                            <option value="<?php echo $row_rsAdvertTypes['TypeExtension']?>" ><?php echo $row_rsAdvertTypes['TypeName']?></option>
                                            <?php
} while ($row_rsAdvertTypes = mysql_fetch_assoc($rsAdvertTypes));
?>
                                        </select>
                                    </td>
                                </tr>
                                <tr >
                                    <td align="right" nowrap="nowrap" class="sectionTitle1">Advert Type Category</td>
                                    <td colspan="3">
                                        <label>
                                            <select name="AdvertTypeCat" class="sectionText1" id="AdvertTypeCat" onchange="populateWidthAndHeight(this.options[this.selectedIndex].value)">
                                                <?php
do {  
?>
                                                <option value="<?php echo $row_rsAdTypeCats['CategoryCode']?>"><?php echo $row_rsAdTypeCats['CategoryName']?></option>
                                                <?php
} while ($row_rsAdTypeCats = mysql_fetch_assoc($rsAdTypeCats));
  $rows = mysql_num_rows($rsAdTypeCats);
  if($rows > 0) {
      mysql_data_seek($rsAdTypeCats, 0);
	  $row_rsAdTypeCats = mysql_fetch_assoc($rsAdTypeCats);
  }
?>
                                            </select>
                                        </label>
                                    </td>
                                </tr>
                                <tr >
                                    <td align="right" nowrap="nowrap" class="sectionTitle1">&nbsp;</td>
                                    <td colspan="3">&nbsp;</td>
                                </tr>
                                <tr >
                                    <td align="right" nowrap="nowrap" class="sectionTitle1">File To Upload:</td>
                                    <td colspan="3">
                                        <input name="uploadFile" type="file" class="course_insidetext" id="uploadFile" size="45" onchange="setDestinationFileName(this)" />
                                    </td>
                                </tr>
                                <tr >
                                    <td align="right" nowrap="nowrap" class="sectionTitle1">&nbsp;</td>
                                    <td colspan="3" >
                                        <input name="destFileName" type="hidden" id="destFileName" value="<?php echo "advert" . $totalRows_rsAdverts; ?>" />
                                        <input name="destFolder" type="hidden" id="destFolder" value="../assets/images/adverts/" />
                                        <input name="allowOverwrite" type="hidden" value="yes" />
                                        <input name="AdvertPath" type="hidden" id="AdvertPath" value="../assets/images/adverts/<?php echo "advert" . $totalRows_rsAdverts . ".jpg"; ?>" />
                                        <input name="redirect" type="hidden" id="redirect" value="<?php echo $editFormAction; ?>" />
                                        <input name="ImageFile" type="hidden" id="ImageFile" value="<?php echo "advert" . $totalRows_rsAdverts . ".jpg"; ?>" />
                                        <input name="maxlimit" type="hidden" id="maxlimit" value="300000" />
                                        <input name="allowed_ext" type="hidden" id="allowed_ext" value="jpg,png,swf,jar" />
                                    </td>
                                </tr>
                                <tr >
                                    <td align="right" nowrap="nowrap" class="sectionTitle1">Width:</td>
                                    <td width="98">
                                        <input name="Width" type="text" class="small" id="Width" value="468" size="6" />
                                    </td>
                                    <td width="44" class="sectionTitle1">Height:</td>
                                    <td width="189">
                                        <input name="Height" type="text" class="small" id="Height" value="80" size="6" />
                                    </td>
                                </tr>
                                <tr >
                                    <td nowrap="nowrap" align="right">&nbsp;</td>
                                    <td colspan="3">&nbsp;</td>
                                </tr>
                                <tr >
                                    <td nowrap="nowrap" align="right">&nbsp;</td>
                                    <td colspan="3">
                                        <input type="submit" value="Upload Advert" />
                                        <input type="button" name="button" id="button" value="Cancel Upload" onclick="cancelFormSubmit('Cancel Advert Upload Process?', 'adverts.php')" />
                                    </td>
                                </tr>
                            </table>
                            <input type="hidden" name="MM_insert" value="form1" />
                        </form>
                    </td>
                </tr>
                <?php } 
							
							if( !isset($_GET['upload']) && !isset($_GET['edit']) ) { ?>
                <tr>
                    <td class="sqrtab">Site Adverts</td>
                </tr>
                <tr>
                    <td class="newsHeader"><a href="adverts.php?upload"><img src="images/icons/image_add.png" width="16" height="16" border="0" align="absmiddle" /> Upload New</a><a href="#" onclick="deletePagesIn('banform','adverts.php','advert_images', 'deleteFiles'); return false;"><img src="images/icons/image_delete.png" alt="del" width="16" height="16" border="0" align="absmiddle" /> Delete Selected</a></td>
                </tr>
                <tr>
                    <td class="newsSummary">
                        <form id="banform" name="banform" method="post" action="" style="margin:0px;">
                            <table width="100%" border="0" cellpadding="3" cellspacing="0" class="cms-data-table">
                                <tr>
                                    <th colspan="2" align="left">Images / Media</th>
                                    <th width="25%">Options</th>
                                </tr>
                                <?php 
					  if( $totalRows_rsAdverts > 0 ) { 
					  	$lastExt = ""; $count = 0; 
						do { 
							if( $lastExt != $row_rsAdverts['TypeName'] ) { ?>
                                <tr>
                                    <td colspan="3" align="left" class="row_header"><?php echo $row_rsAdverts['TypeName']; ?></td>
                                </tr>
                                <?php 
					  			$lastExt = $row_rsAdverts['TypeName']; 
							} ?>
                                <tr>
                                    <td width="5%" align="center">
                                        <input name="<?php echo 'item' . $count; ?>" type="checkbox" id="<?php echo 'item' . $count; ?>" value="<?php echo $row_rsAdverts['id']; ?>" />
                                        <input name="<?php echo 'ftd' . $count; ?>" type="hidden" id="<?php echo 'ftd' . $count; ?>" value="<?php echo $row_rsAdverts['AdvertPath']; ?>" />
                                    </td>
                                    <td width="70%"><a href="?edit&amp;bid=<?php echo $row_rsAdverts['id']; ?>"><?php echo $row_rsAdverts['AdvertName']; ?></a></td>
                                    <td align="center"><a href="?edit&amp;bid=<?php echo $row_rsAdverts['id']; ?>"><img src="images/icons/image_edit.png" alt="edit" width="16" height="16" border="0" align="absmiddle" /> Edit</a>&nbsp;&nbsp; <a href="utils/del.php?del=advert_images&amp;id=<?php echo $row_rsAdverts['id']; ?>&amp;ref=<?php echo $_SERVER['PHP_SELF']; ?>&amp;ea=deleteFile&amp;fpath=<?php echo $row_rsAdverts['AdvertPath']; ?>"><img src="images/icons/image_delete.png" alt="delete" width="16" height="16" border="0" align="absmiddle" /> Delete</a></td>
                                </tr>
                                <?php $count++; 
					  	} while ($row_rsAdverts = mysql_fetch_assoc($rsAdverts)); ?>
                                <input type="hidden" name="items" id="items" value="<?php echo $count; ?>" />
                                <?php 
					  } 
					  
					  if( $totalRows_rsAdverts == 0 ) { ?>
                                <tr>
                                    <td colspan="2">No Advert Images Available</td>
                                    <td>&nbsp;</td>
                                </tr>
                                <?php } // end if( $totalRows_rsAdverts ... ) ?>
                            </table>
                        </form>
                    </td>
                </tr>
                <?php } // end if( !isset($_GET['upload'] ...) ) ?>
            </table>
        </div>
        <!-- InstanceEndEditable -->
        </div>
    </div>
    
    <div id="footer">
    	<?php include('footer.php'); ?>
    </div>
</div>

</body>
<!-- InstanceEnd --></html>
<?php
mysql_free_result($rsAdverts);

mysql_free_result($rsAdvertTypes);

mysql_free_result($rsAdvertEdit);

mysql_free_result($rsAdTypeCats);
?>
