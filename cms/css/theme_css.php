<?php  
// include the access checker script so we can determine which site we are working with
// based on the session information
include_once('../accesscheck.php');

$site = new Site($siteid);
$theme_css = new ThemeCSS( $site->field('SiteThemeCSSID') );

// get the contents of the css file
$cssData = file_get_contents( kan_fix_path($theme_css->field('CSSURL')) );

/*header("Expires: Mon, 26 Jul 1997 05:00:00 GMT"); // Date in the past
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT"); // always modified
header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
header("Pragma: no-cache"); // HTTP/1.0*/
header("Content-Type: text/css");

echo $cssData;
?>