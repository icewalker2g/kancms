<div id="ajax-progress">
    <img src="../assets/images/general/loadcircles.gif" width="16" height="16" alt="progress" align="absmiddle" />
    <span id="progress-text">Processing...</span>
</div>

<div id="nav-wrapper">
    <ul>
        <?php if (function_exists('isAdminUser') && isAdminUser()) { ?>
            <li>
                <a href="#"><img src="images/icons/application_view_list.png" alt="home" width="16" height="16" border="0" align="absmiddle" /> System Menu</a>
                <ul>
                    <li><a href="users.php"><img src="images/icons/user.png" alt="users" width="16" height="16" border="0" align="absmiddle" /> Manage Users</a></li>

                    <li><a href="components.php"><img src="images/icons/page.png" alt="comps" width="16" height="16" border="0" align="absmiddle" /> Manage Components</a></li>
                    <li><a href="themes.php"><img src="images/icons/css.png" alt="themes" width="16" height="16" border="0" align="absmiddle" /> Manage Themes</a></li>
                    <!--<li><a href="#">Create New Custom Manager</a></li>-->
                    <li class="divider"></li>
                    <li>
                        <a href="system.php"><img src="images/icons/cog_edit.png" alt="config" width="16" height="16" border="0" align="absmiddle" /> System Configuration</a>
                    </li>
                    <li>
                        <a href="index.php?page=system_folders"><img src="images/icons/folder_explore.png" alt="folders" width="16" height="16" border="0" align="absmiddle" /> File &amp; Folder Permissions</a>
                    </li>
                    <li><a href="index.php?page=system_stats"><img src="images/icons/report.png" alt="folders" width="16" height="16" border="0" align="absmiddle" /> Usage Statistics</a></li>
                    <li class="divider"></li>
                    <li><a href="about_cms.php"><img src="images/icons/help_16.png" alt="help" width="17" height="17" border="0" align="absmiddle" /> About KAN CMS</a></li>
                </ul>
            </li>
        <?php } ?>

        <li>&nbsp;</li>
        <li>
            <a href="sites.php"><img src="images/icons/house.png" alt="home" width="16" height="16" border="0" align="absmiddle" /> Sites Home</a>

            <ul>
                <?php
                $sm = new SitesManager();
                $sites = $sm->getSites();

// provide easy site context switching via drop down menu
                if (count($sites) > 1) {
                    ?>

                    <li><a href="sites.php"><img src="images/icons/house.png" alt="home" width="16" height="16" border="0" align="absmiddle" /> Go To Home</a></li>
                    <li class="divider"></li>
                    <?php
                    foreach ($sites as $s) {
                        if (isset($site) && !isGeneralSitePage($_SERVER['SCRIPT_NAME']) && $s->equals($site))
                            continue;
                        ?>

                        <li>
                            <a href="sites.php?launch&id=<?php echo $s->getId(); ?>">
                                <img src="<?php echo $s->isActive() ? "images/icons/accept.png" : "images/icons/stop.png"; ?>" alt="home" width="16" height="16" border="0" align="absmiddle" />
                                <?php echo $s->getSiteName(); ?>
                            </a>
                        </li>
                        <?php
                    }
                }
                ?> 
            </ul>  
        </li>

        <?php
        if (function_exists("isGeneralSitePage") && !isGeneralSitePage($_SERVER['SCRIPT_NAME'])) {
            if (isset($_SESSION['SiteName'])) {
                ?>
                <li>&raquo;</li>
                <li class="sitename">
                    <a href="#"><?php echo $site->getSiteName(); ?></a>

                    <ul>
                        <li><a href="sites.php?edit&id=<?php echo $site->getId(); ?>&ref=<?php echo $_SERVER['SCRIPT_NAME']; ?>"><img src="images/icons/page_edit.png" alt="edit" width="16" height="16" border="0" align="absmiddle" /> Edit Site Definition</a></li>
                        <li><a href="<?php echo $site->getHomePageURL(); ?>" target="_blank"><img src="images/icons/magnifier.png" alt="view" width="16" height="16" border="0" align="absmiddle" /> View Site</a></li>
                        <li><a href="index.php?page=system_stats&site=<?php echo $site->getId(); ?>"><img src="images/icons/report.png" alt="folders" width="16" height="16" border="0" align="absmiddle" /> Site Statistics</a></li>
                        <!--<li><a href="#">Export Site</a></li>-->
                    </ul>
                </li><?php
    }
}
        ?>

    </ul>
</div>