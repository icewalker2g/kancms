<?php

class ContentTableRenderer {

    private $itemsTitle = "Item";
    private $itemsArray = array();
    private $totalItemsCount = 1;
    private $contentName = "Items";
	private $columns = array();

    public function __construct($name) {
        if ($name != NULL) {
            $this->contentName = $name;
        }
    }

    public function getContentName() {
        return $this->contentName;
    }

    public function setItemsTitle($title) {
        $this->itemsTitle = $title;
    }

    public function getItemsTitle() {
        return $this->itemsTitle;
    }

    public function setItemsArray($array) {
        $this->itemsArray = $array;
    }

    public function getItemsArray() {
        return $this->itemsArray;
    }

    public function setTotalItemsCount($count) {
        $this->totalItemsCount = $count;
    }

    public function getTotalItemsCount() {
        return $this->totalItemsCount;
    }
	
	/**
	 * A column specification must contain a title and a field title from
	 * the set of items
	 */
	public function addColumn($column = array()) {
		$this->columns[] = $column;
	}
	
	public function getColumns() {
		return $this->columns;	
	}

    public function render() {
        if (count($this->getItemsArray()) == 0) {
            echo "No <b>" . $this->getContentName() . "</b> Have Been Posted In This Category For This Site";
            return;
        }

        echo "
			<table border=0 cellspacing=0 cellpadding=4 width=100% class='cms-data-table'>
				<tr>
					<th align=right><input type='checkbox' value='' id='checkAll' name='checkAll' /></th>
					<th align='left'>" . $this->getItemsTitle() . "</th>";
		
		// add any extra columns
		for($x = 0; $x < count($this->getColumns()); $x++) {
			$header = $this->columns[$x]['header'];
			echo "<th align='left'>" . $header . "</th>";	
		}
					
		echo "
					<th>Options</th>
				</tr>
		";

        for ($i = 0; $i < count($this->getItemsArray()); $i++) {
            $title = $this->itemsArray[$i]['title'];
            $id = isset($this->itemsArray[$i]['id']) ? $this->itemsArray[$i]['id'] : NULL;
            $url = isset($this->itemsArray[$i]['url']) ? $this->itemsArray[$i]['url'] : NULL;

            $published = isset($this->itemsArray[$i]['published']);
            
            echo "
				<tr id='article-row-$id'>
					<td align=right width='25'><input type='checkbox' value='$id' /></td>
					<td>" . ($i + 1) . ". <a id='$id' class='edit-link' href='?edit&id=$id'>$title</a></td>";
			
			// add any extra columns
			for($x = 0; $x < count($this->getColumns()); $x++) {
				$field = $this->columns[$x]['field'];
				$fieldValue = isset($this->itemsArray[$i][$field]) ? $this->itemsArray[$i][$field] : NULL;
				echo "<td>" . $fieldValue . "</td>";	
			}
					
			echo
					"<td align=center width='34%'>
						<a id='$id' class='edit-link' href='?edit&id=$id'><img src='images/icons/newspaper.png' alt='' align='absmiddle' border=0 /> Edit</a>
						<a id='$id' class='delete-link' href='#'><img src='images/icons/newspaper_delete.png' alt='Delete' align='absmiddle' border=0 /> Delete</a>";

            if ($url) {
                echo "<a id='$id' class='preview-link' href='$url' target='_blank'><img src='images/icons/magnifier.png' alt='Preview' align='absmiddle' border=0 /></a>";
            }

            if ($published) {
                $pub_link_class = $this->itemsArray[$i]['published'] ? 'cms-publish-link' : 'cms-unpublish-link';
                $pub_link_title = $this->itemsArray[$i]['published'] ? 'Published (Click To Unpublish)' : 'Unpublised (Click To Publish Now)';
                echo "<a id='$id' class='$pub_link_class' href='#' title='$pub_link_title'></a>";
            }

            echo "
					</td>
				</tr>
			 ";
        }

        echo "
			</table>
		";

        // echo out the values of the total article count as hidden field in order to be accessed
        // by the javascript processor
        // we'll do this if only the first page is being loaded
        if( !isset($_POST['no-total']) ) {
            echo "
				<input type='hidden' id='items-total' value='{$this->getTotalItemsCount()}' />
		   ";
        }
    }

}

?>