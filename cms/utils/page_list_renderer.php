<?php 

function listPages($pages) {
	
	foreach( $pages as $page ) {  ?>
                                            
        <li draggable="true" id="page-li-<?php echo $page->getId(); ?>" data-id="<?php echo $page->getId(); ?>" class="<?php echo $page->hasSubPages() ? 'folder' : 'file'; ?>" data-position="<?php echo $page->getPosition(); ?>" data-name="<?php echo $page->getName(); ?>">
            <div class="row">
                <div class='name_col'>
                    <input type="checkbox" id="item" name="item" value="<?php echo $page->getId(); ?>" />
                    <a class="edit-link page-name" href="?edit&amp;id=<?php echo $page->getId(); ?>" data-id="<?php echo $page->getId(); ?>">
                        <?php echo $page->getName(); ?>
                    </a>
                </div>
                <div class='options_col'>
                    <a class="edit-link" href="?edit&amp;id=<?php echo $page->getId(); ?>" data-id="<?php echo $page->getId(); ?>">
                        <img src="images/icons/page_edit.png" alt="edit" width="16" height="16" border="0" align="absmiddle" /> Edit
                    </a>
                    
                    <a href="#" class="delete-link" data-id="<?php echo $page->getId(); ?>">
                        <img src="images/icons/page_delete.png" alt="delete" width="16" height="16" border="0" align="absmiddle" /> Delete
                    </a>
                    
                    <a class="preview-link" href="<?php echo $page->getURL(); ?>" target="_blank" title="Preview Page" data-id="<?php echo $page->getId(); ?>" >
                        <img src="images/icons/magnifier.png" alt="view" width="16" height="16" border="0" align="absmiddle" />
                        <!--Preview-->
                    </a>
                    
                    <?php 
                        $page_id = $page->getId();
                        $published = $page->isPublished();
                        $publish_class = $published ? "cms-publish-link" : "cms-unpublish-link";
                        $publish_title = $published ? "Published (Click To Unpublish)" : "Unpublished (Click To Publish Now)";
                        
                        echo "<a id='$page_id' href='#' class='$publish_class' title='$publish_title'></a>";
                    ?>
                </div>
            </div>
        
        </li><?php
    }
}
?>