<?php 

function deleteTheme($themeDir) {
   deleteDirectory($themeDir);
}

function deleteThemeStyleSheet($styleSheetDir) {
   deleteDirectory($styleSheetDir);
}

function deleteDirectory($dirname) {
   // Sanity check
   if (!file_exists($dirname) ) {
      return false;
   }

   // Simple delete for a file
   if (is_file($dirname)) {
      return unlink($dirname);
   }

   // Loop through the folder
   $dir = dir($dirname);
   while (false !== $entry = $dir->read()) {
      // Skip pointers
      if ($entry == '.' || $entry == '..') {
         continue;
      }

      // Recurse
      deleteDirectory("$dirname/$entry");
   }

   // Clean up
   $dir->close();
   return rmdir($dirname);
}

function uploadAndInstallCSS($themeFolder, $insertid) {
   // get a reference to the db connection
   global $config;

   foreach ($_FILES as $key => $value) {
      if ($_FILES[$key]["error"] == 0) {
         // get the name of the file uploaded
         $fileName = $_FILES[$key]["name"];
         $destFolder = $themeFolder . "css/";

         // if the file is not a zip file, then continue to the next file
         if( strpos($fileName, ".zip") == -1 || strpos($fileName, ".zip") == "" || strpos($fileName, ".zip") == NULL ) {
            continue;
         }

         $destZipFile = $destFolder . $_FILES[$key]["name"];

         // move the file to the CSS folder
         move_uploaded_file($_FILES[$key]["tmp_name"], $destZipFile );

         $cssFolderName = substr($fileName, 0, strpos($fileName,".") );

         // create object
         $zip = new ZipArchive() ;

         // open archive
         if ($zip->open( $destZipFile ) !== TRUE) {
            die ('Could not open archive');
         }

         // extract contents to destination directory
         $zip->extractTo($destFolder);

         // close archive
         $zip->close();

         unlink( $destZipFile );

         // if we managed to upzip successfully, then we must update the db record with the
         // new path
         $sheetFolder = $destFolder . $cssFolderName . "/";
         $sheetPath = $sheetFolder . $cssFolderName . ".css" ;

         $themeCSS = new ThemeCSS(array(
             'id' => $insertid,
             'CSSFolder' => $sheetFolder,
             'CSSURL' => $sheetPath
         ));
         $themeCSS->save();
      }
   }
}

function uploadAndInstallTheme($insertid) {
   // get a reference to the db connection
   global $config;

   foreach ($_FILES as $key => $value) {
      if ($_FILES[$key]["error"] == 0) {
         // get the name of the file uploaded
         $fileName = $_FILES[$key]["name"];

         // if the file is not a zip file, then continue to the next file
         if( strpos($fileName, ".zip") == -1 || strpos($fileName, ".zip") == "" || strpos($fileName, ".zip") == NULL ) {
            continue;
         }

         $destFolder = "../themes/";

         $destZipFile = $destFolder . $_FILES[$key]["name"];

         // move the file to the CSS folder
         move_uploaded_file($_FILES[$key]["tmp_name"], $destZipFile );

         $themeFolderName = substr($fileName, 0, strpos($fileName,".") );

         // create object
         $zip = new ZipArchive() ;

         // open archive
         if ($zip->open( $destZipFile ) !== TRUE) {
            die ('Could not open archive');
         }

         // extract contents to destination directory
         $zip->extractTo($destFolder);

         // close archive
         $zip->close();

         // delete zip file
         @unlink( $destZipFile );

         // if we managed to upzip successfully, then we must update the db record with the
         // new path
         $themeFolder = $destFolder . $themeFolderName . "/";
		 $sheetFolder = $themeFolder . "css/default/";
         $sheetPath = $themeFolder . "css/default/default.css" ;

         $theme = new Theme(array(
             'id' => $insertid,
             'ThemeFolder' => $themeFolder
         ));
         $theme->save();
         
         $themeCSS = new ThemeCSS(array(
             'ThemeID' => $theme->getId(),
             'CSSName' => 'Default Theme CSS',
             'CSSFolder' => $sheetFolder,
             'CSSURL' => $sheetPath,
             'InUse' => 'true',
             'DateAdded' => date('Y-m-d')
         ));
         $themeCSS->save();
      }
   }
}


function createNewTheme($folderPath) {
	global $fileUtils;
	
	/*if( !is_dir($folderPath) ) {
		echo("Invalid Theme Folder Specified: <b>$folderPath</b> " );
		return false;
	}*/
	
	// create the folder and do not report if it exists or not
	if( !@mkdir($folderPath) ) {
		echo "<div class='cms-warning'>Failed To Create The Theme Folder: $folderPath. Please check permissions of the <b>../themes/</b> directory. </div>";
		//return false;
	}
	
	// copy the contents of the temp theme folder to the new specified folder
	include_once('file_utils.php');
	$fileUtils->smartCopy('../cms/utils/temp_theme/', $folderPath);
	
	// return true to indicate that the process completed successfully
	return true;
}

?>