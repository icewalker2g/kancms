<?php 
include_once('ajax_config.php');
kan_import('ArticlesManager');

if( isset($_POST['action']) ) {
	$manager = new ArticlesManager();
	
	$action = $_POST['action'];
	switch($action) {
	
		case 'add_comment':
			$article_id = $_POST['article_id'];
			$comment_data = array(
				'ReaderName' => kan_get_parameter('name'),
				'ReaderEmail' => kan_get_parameter('email'),
				'ReaderWebsite' => kan_get_parameter('website'),
				//'Subject' => kan_get_parameter('Subject'),
				'Message' => kan_get_parameter('comment')
			);
			
			$manager->addArticleComment( $article_id, $comment_data );		
			
			echo json_encode($comment_data);
			break;	
	}
}
?>