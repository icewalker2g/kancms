<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title><?php echo $page->getName() . " | " . $site->getSiteName() ;?></title>
        <?php echo $site->getHTMLHeadData(); ?>
        
    </head>

    <body>
        <div id="page">
            <?php include('header.php'); ?>

            <div id="content">
                <div id="page_data">
                    <div class="news_title"><?php echo $page->getName(); ?></div>
                    <div class="news_content"><?php echo $page->getContent(); ?></div>
                </div>


                <div id="sidebar"> 
                    <div id="related_info">
						<?php echo $page->getRootPage()->toHTMLList(); ?>
                    </div>
                </div>

            </div>

            <?php include('footer.php'); ?>
        </div>
    </body>
</html>