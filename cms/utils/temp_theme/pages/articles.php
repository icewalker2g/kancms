<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title><?php echo $site->getSiteName(); ?> - Articles</title>
        <?php 
			echo $site->getHTMLHeadData(); 
			echo $site->script('jquery/js/jquery.js');
			echo $site->theme->script('articles.js');
		?>
    </head>

    <body>
        <div id="page">
            <?php include('header.php'); ?>

            <div id="content">
                <div id="page_data"><?php
					if( isset($article) ) { ?>
                        <div class="news_title"><?php echo $article->getTitle(); ?></div>
                        <div class="news_content"><?php echo $article->getContent(); ?></div>
    
    					<h3><a name="comments">Comments</a></h3>
                        <ul id="comment-list"><?php
                        $comments = $article->getApprovedComments();
                        
                        for($i = 0; $i < count($comments); $i++) { ?>
                            <li>
                                <!--<div class="comment_title"><?php echo $comments[$i]->getSubject(); ?></div>-->
                                <div class="comment-info">
                                    <div class="author"><?php echo $comments[$i]->getLinkedReaderName(); ?> says: </div>
                                    <div class="date"><?php echo $comments[$i]->getPostDate('M d, Y'); ?> at <?php echo $comments[$i]->getPostDate('H:m'); ?></div>
                                </div>
                                <div class="comment-message"><?php echo $comments[$i]->getMessage(); ?></div>
                            </li><?php
                        }
                        ?>
                        </ul>
                        
                        <?php 
						if( $article->commentsAllowed() ) { ?>
                            <div id="comments">
                                <?php include('comments_form.php'); ?>
                            </div><?php
						}
					} ?>
                </div>

                <div id="sidebar">
                    <div id="related_info">
						<ul><?php
							// get the 10 most recent articles
							$articles = $site->getArticles(0, 10);
							
							for( $i = 0; $i < count($articles); $i++ ) {
								$art = $articles[$i];
	
								echo '<li class="rc_li"><span style="font-size:11px; font-family: tahoma; color: #999">' .
									$art->getPostDate() . ":</span> " . $art->getLinkedTitle() . "</li>";
							}?>
                        </ul>
                    </div>
                </div>

            </div>

            <?php include('footer.php'); ?>
        </div>
    </body>
</html>