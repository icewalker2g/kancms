
<div id="header">
   <div id="head_comps">
		<div id="logo_area"><?php
			if( $site->getSiteLogo() != "" ) { ?>
				<a href="<?php echo $site->getHomePageURL(); ?>">
					<span id="site_logo">
					<img src="<?php echo $site->getSiteLogo(); ?>" alt="" border=0 />
					</span>
				</a><?php
			} else { ?>
				<a href="<?php echo $site->getHomePageURL(); ?>">
					<span id="site_title">
					<?php echo $site->getSiteName(); ?><br />
					<span style="font-size:13px; margin-left: 20px;"><?php echo $site->getSiteSlogan(); ?></span>
					</span>
				</a><?php
			} ?>
     </div>
     
     <div id="search_area">
     	  <?php $site->renderComponent('search_box'); ?>
     </div>
   
   </div>
</div>

<div id="navigation">
	<?php echo $site->renderMenu('nav', 2); // HTML UL List Of Pages, Two Levels Deep ?>
</div>
