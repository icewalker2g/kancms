<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
      <title><?php echo $site->getSiteName(); ?> - Home Page</title>
      <?php echo $site->getHTMLHeadData(); ?>
   </head>

   <body>
      <div id="page">
         <?php include('header.php'); ?>
         
         <div id="content">
            <div id="notices_block">
               <div class="nav">
                  Sidebar Content Here
               </div>

               <div class="main">
			   	  <?php echo $site->getHomePageText(); ?>
               </div>
            </div>
         </div>

         <?php include('footer.php'); ?>
      </div>
   </body>
</html>