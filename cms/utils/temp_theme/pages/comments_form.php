
<form name="comment-form" id="comment-form" action="">
<div>
	<label for="name">* Name: </label>
    <input type="text" name="name" id="name" value="Enter Name Here"  />
    <span class="comment-text">Required</span>
</div>

<div>
	<label for="email">* Email: </label>
    <input type="text" name="email" id="email" value="Enter Address Here" >
    <span class="comment-text">Required</span>
</div>

<div>
	<label for="website">Website: </label>
    <input type="text" name="website" id="website" value="Enter Website Here" />
    <span class="comment-text">Optional</span>
</div>

<div>
	<label for="comment">* Comment</label>
    <textarea name="comment" id="comment"></textarea>
</div>

<div id="comment-buttons">
	<input type="button" name="submit" id="submit" value="Post Comment" />
    <input type="button" name="submit" id="submit" value="Clear" />
    <input type="hidden" name="article_id" id="article_id" value="<?php echo isset($article) ? $article->getId() : -1; ?>"  />
	No HTML Allowed In Comments
</div>
</form>