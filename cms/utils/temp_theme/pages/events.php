<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title><?php echo "Events | " . $site->getSiteName(); ?></title>
        <?php echo $site->getHTMLHeadData(); ?>
    </head>

    <body>
        <div id="page">
            <?php include('header.php'); ?>

            <div id="content">
                <div id="page_data"><?php 

                    if( isset($event) ) { ?>
                    <div class="news_title"><?php echo $event->getName(); ?></div>
                    <div class="news_content"><?php echo $event->getDescription(); ?></div><?php
                    } ?>

                    <div id="more_news">
                        <div class="block_title">Other Upcoming Events</div>
                        <div><?php

                            $events = $site->getEvents(0,10);

                            for( $i = 0; $i < count($events); $i++ ) {

								echo '<div style="margin-bottom: 10px; padding: 5px; overflow: hidden;">' ;
								echo '<div><span style="font-size:11px; font-family: tahoma; color: #999">' .
									$events[$i]->getStartDate() . ":</span> " .
									$events[$i]->getLinkedName() . "</div>";
								echo '</div>';
                                
                            }

                            // if the count is not greater than 0 then no event we were loaded, so
                            // we need to provide some feedback

                            if( $i == 0 ) {
                                echo "Sorry, there are no upcoming events available at the moment";
                            }
                            ?></div>
                    </div>
                </div>

                <div id="sidebar">
                    <div id="related_info"><?php

                        echo '<ul class="rc_ul">';

                        $lastCat = "";

                        for( $i = 0; $i < count($events); $i++ ) {

                            if( $lastCat != $events[$i]->getCategoryID() ) {
                                echo '<li><h3>' . $events[$i]->getCategory()->getCategoryName() . '</h3></li>';
                                $lastCat = $events[$i]->getCategoryID();
                            }

                            
                              echo '<li>';
                              echo '<span style="font-size:11px; font-family: tahoma; color: #999">' .
                                  $events[$i]->getStartDate() . ":</span> ";

                              echo $events[$i]->getLinkedName();

                              echo "</li>";
                            
                        }
                        echo "</ul>";

                        ?></div>

                </div>
            </div>

            <?php include('footer.php'); ?>
        </div>
    </body>
</html>