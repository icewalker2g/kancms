<?php 

class File {

	var $folderName;
	var $folderPath;
	var $recommendedPerms = "0775";
	
	public function __construct($folderName, $folderPath, $recommendedPerms = '0775') {
		$this->folderName = $folderName;
		$this->folderPath = $folderPath;
		$this->recommendedPerms = $recommendedPerms;
	}
	
	public function getFilePermissions() {
		if( is_file( $this->folderPath ) ) {
			return substr(decoct( @fileperms($this->folderPath) ), 2);	
		}
		
		return substr(decoct( @fileperms($this->folderPath) ), 1);
	}
	
	public function getFileName() {
		return $this->folderName;	
	}
	
	public function getFilePath() {
		return $this->folderPath;	
	}
	
	public function getRecommendedPerms() {
		return substr($this->recommendedPerms,0);
	}
}

?>