<?php
	
	//
	require_once('../../assets/config/config.php');
 // Comment: this code deletes a specific record from a specified table
 
 /* Parameters:
    	del: 	the table to delete information from 
 		id: 	the id of the item to delete from the table
		ref: 	the page to redirect to after deletion has completed
 */
 
 
 function deleteFile($filePath, $canRemDir = false) {
 	if( file_exists($filePath) ) {
		if( is_dir($filePath) && canRemDir ) {
			rmdirr($filePath);
		} else if(is_file($filePath)) {
			unlink($filePath);
		}
	}
 }
 
 function rmdirr($dirname) {
	// Sanity check
	if (!file_exists($dirname)) {
		return false;
	}
	
	// Simple delete for a file
	if (is_file($dirname)) {
		return unlink($dirname);
	}
	
	// Loop through the folder
	$dir = dir($dirname);
	while (false !== $entry = $dir->read()) {
		// Skip pointers
		if ($entry == '.' || $entry == '..') {
			continue;
		}
		
		// Recurse
		rmdirr("$dirname/$entry");
	}
	
	// Clean up
	$dir->close();
	return rmdir($dirname);
 }
 
 if( isset($_GET['del']) && isset($_GET['id']) && isset($_GET['ref']) ) {
	 $id = $_GET['id'];
	 $del = $_GET['del'];
	 $referer = $_GET['ref'];
	 
	 // perform extra action if set
	 if( isset($_GET['ea']) ) {
	 	
		// delete file action
	 	if( $_GET['ea'] == "deleteFile" && isset($_GET['fpath']) ) {
			deleteFile($_GET['fpath']);
			
			// delete multiple files that may have been added
			$count = 1;
			while($count++ < 5) {
				if( isset($_GET['fpath' . $count]) ) {
					deleteFile($_GET['fpath' . $count]);
				}
			}
			
		} else if($_GET['ea'] == "deleteRelated" && isset($_GET['relatedID']) && isset($_GET['relatedTbl']) &&
									isset($_GET['relatedCol']) ) {
			
			$relatedTbl = $_GET['relatedTbl'];
			$relatedID = $_GET['relatedID'];
			$relatedCol = $_GET['relatedCol'];
			
			$query_rspics = "delete FROM $relatedTbl WHERE $relatedCol = $relatedID";
			$rspics = mysqli_query($config,$query_rspics) or die(mysqli_error());
			
		} else {
			die("File To Delete Not Specified");
		}
	 }
	 
  	 $query_rspics = "delete FROM $del where id = '$id' ";
	 $rspics = mysqli_query($config,$query_rspics) or die(mysqli_error());
	
	 // cms_list_gen no longer required since image list is generated on the fly
	
	 $link = $referer;
	 header("Location: ". $link);	
	 
 } else if( isset($_GET['delete']) && isset($_GET['tbl']) && isset($_GET['ref']) ) {
	$table = $_GET['tbl'];
	$checkCount = $_POST['items'];
	
	for($i = 0; $i < $checkCount; $i++) {
		if( isset($_POST['item'.$i]) && ($_POST['item'.$i] != '-1') ) {
		
			// check if any extra action is to be performed
			if( isset($_GET['ea']) ) {
				
				// if the extra action to be performed is deletion  of files, then we
				// need to ensure that a file reference has been passed
				if( $_GET['ea'] == "deleteFiles" && isset($_POST['ftd' . $i]) ) {
					deleteFile($_POST['ftd' . $i]);
					
					// included to cater for thumbnail image deletion in tables such as the images
					// table. a better approach may have to be devised for the "file to delete 2"
					if( isset($_POST['ftd2' . $i]) ) {
						deleteFile($_POST['ftd2' . $i]);
					}
				}
			}
			
			$deleteSQL = sprintf("DELETE FROM $table WHERE id = %s LIMIT 1", GetSQLValueString($_POST['item'.$i], "int"));
					   
		    $Result1 = mysqli_query($config,$deleteSQL) or die(mysqli_error());
		}
	}
	
	$deleteGoTo = $_GET['ref']; // referral page
	
	header(sprintf("Location: %s", $deleteGoTo));
	
 } else {
	die("Error: Could not delete specified record(s)!"); 
 }
?>