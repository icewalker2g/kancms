<?php 

	$uploadFileAction = $_SERVER['PHP_SELF'];
	if(isset($_SERVER['QUERY_STRING'])) {
		$uploadFileAction .= "?" . $_SERVER['QUERY_STRING'];
	}
	
	$deleteFileAction = $_SERVER['PHP_SELF'];
	if(isset($_SERVER['QUERY_STRING'])) {
		$deleteFileAction .= "?" . $_SERVER['QUERY_STRING'];
	}
					
	$error = "";
	$folder = "../assets/images/"; // Folder in which to store files
	
	if(isset($_POST['destFolder'])) {
		$folder = $_POST['destFolder'];
		
		if(!file_exists($folder)) {
			mkdir($folder, 0777, true);
		}
	}
	
	// Set maximum file limit (in bits)
	$maxlimit = 500000; 
	
	// Set allowed extensions (split using comma)
	$allowed_ext = "jpg,gif,png,jpeg,swf,pdf,zip,jar,rar,mp3,mp4,m4v,flv,amr,midi,mpg,mpeg,wmv,divx,wma,sql,htm,html,xml,js,doc,xml,ppt,xls,mdb,docx,xlsx,pptx"; 

	// Allow file overwrite? yes/no
	$overwrite = "no"; 
	
	// if an specific overwrite permission has been posted, use that instead
	if(isset($_POST['allowOverwrite'])) {
		$overwrite = $_POST['allowOverwrite'];
	}
	
	// set the maximum upload limit based on specification
	if(isset($_POST['maxlimit'])) {
		$maxlimit = $_POST['maxlimit'];
	}
	
	// set the allowed upload extensions based on user's specification if available
	if(isset($_POST['allowed_ext'])) {
		$allowed_ext = $_POST['allowed_ext'];
	}
	
	// set specific destination file name based on user supplied data
	if(isset($_POST['destFileName'])) {		
		$match = ""; // Clear match variable; for security purposes
		$filesize = $_FILES['uploadFile']['size']; // Get file size (in bits)
		$filename = strtolower($_FILES['uploadFile']['name']); // Get file name; make it all lowercase
		$destFileName = $filename;

		if(isset($_POST['destFileName'])) {
			$destFileName = strtolower($_POST['destFileName']);
		}
		
		if(!$filename || $filename==""){ // File not selected
		   $error = "- No file selected for upload.<br />";
		} else if(file_exists($folder.$filename) && $overwrite=="no"){ // Check if file exists
		   $error = "- File already exists: $filename<br />";
		}
		
		// Check if file size
		if($filesize < 1){ // File is empty
		   $error .= "- File size is empty.<br />";
		}elseif($filesize > $maxlimit){ // File is more than maximum
		   $error .= "- File size is too big.<br />";
		}
		
		$file_ext = substr($filename, strrpos($filename,".") + 1 ); // Split filename at period (name.ext)
		
		if(!isset($file_ext) ) {
			$error .= "- Please Specify A File<br>";
		}
		
		$allowed_ext = preg_split("/\,/",$allowed_ext); // Create array of extensions
		foreach($allowed_ext as $ext){
		   if(isset($file_ext) && $ext == $file_ext) {
				$match = "1";
				if(isset($_POST['destFileName'])) {
					$destFileName .= ".".$ext;
				}
		   } // File is allowed 
		}
		
		// File extension not allowed
		if(!$match){
		   $error .= "- File type isn't allowed: $filename<br />";
		}
		
		if($error){
		   print "Error trying to upload file:<br /> $error"; // Display error messages
		 
		}else{
			if(isset($_POST['destFileName'])) {
				if( isset($_POST['checkdim']) ) {
					//$error = "Image Dimension Info";
					list($srcW, $srcH, $srcType, $srcAttr) = getimagesize($_FILES['uploadFile']['tmp_name']);
					$dimensions = split( ",", $_POST['checkdim'] );
					$width = $dimensions[0];
					$height = $dimensions[1];
					
					if( $srcW < $width || $srcH < $height || $srcW > $width || $srcH > $height ) {
						$error .= "- Image Dimensions Not Accurate. Please Resize To " . $width . " x " . $height . "px and Re-Upload<br>";
						return;
					} 
				}
			
				if(move_uploaded_file($_FILES['uploadFile']['tmp_name'], $folder.$destFileName) ) {
					// if we manage to upload the file, then clear all errors
					$error = "";
					
					if(isset($_POST['createThumbnail']) && $_POST['createThumbnail'] == "yes") {
						$imagePath = $folder.$destFileName;
						
						// check if the thumbnail output directory exists, else notify
						if(isset($_POST['tnOutputDir'])) {
							$tndir = $_POST['tnOutputDir'];
							
							// generate the thumbnail image
							include('thumbgen.php');
							generateThumb($imagePath, $tndir, 150);
						} else {
							$error .= "- Cannot create thumbnail. No target directory set<br />";
						}
					}
				}
			} else{
			  $error .= "Error! File size might exceed upload limit of server. Try again.<br />"; // Display error
		   }
		}
	}
?>
