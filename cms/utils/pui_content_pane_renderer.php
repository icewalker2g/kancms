<?php
 
// prevent direct access
if( !defined("SITE_ID") ) {
	die("Access Denied");	
}


class PUIContentPaneRenderer {

    private $categoryTitle = "Categories";
	private $itemTitle = "Item";
    private $categoryArray = array();
    private $contentName = "Items";
	private $totalItemsCount = 0;
	private $categoryFormPath = "forms/category_form.php";
	private $renderCategoryPane = true;
    private $renderOptions = array(
        'category_pane' => true,
        'category_title' => 'Categories',
        'category_actions' => true,
        'category_form_path' => "forms/category_form.php",
        'item_name' => 'Item',
        'content_name' => 'Items',
        'total_item_count' => 0
    );

    public function __construct($name) {
        $this->setContentName($name);
		$this->setItemTitle($name);
    }

    public function getContentName() {
        return $this->contentName;
    }

    public function setContentName($name) {
        $this->contentName = $name;
        $this->setRenderOption('content_name', $name);
    }

    public function setCategoryTitle($title) {
        $this->categoryTitle = $title;
        $this->setRenderOption('category_title', $title);
    }

    public function getCategoryTitle() {
        return $this->categoryTitle;
    }

    public function setCategoryArray($array) {
        $this->categoryArray = $array;
    }

    public function getCategoryArray() {
        return $this->categoryArray;
    }

    public function setTotalItemsCount($count) {
        $this->totalItemsCount = $count;
        $this->setRenderOption('total_item_count', $count);
    }

    public function getTotalItemsCount() {
        return $this->totalItemsCount;
    }
    public function getItemTitle() {
        return $this->itemTitle;
    }

    public function setItemTitle($itemTitle) {
        $this->itemTitle = $itemTitle;
        $this->setRenderOption('content_name', $itemTitle);
    }
	
	public function getCategoryFormPath() {
		return $this->categoryFormPath;	
	}
	
	public function setCategoryFormPath($path) {
		$this->categoryFormPath = $path;
        $this->setRenderOption('category_form_path', $path);
	}
	
	public function setRendererCategoryPane($bool) {
		$this->renderCategoryPane = $bool;
        $this->renderOptions['category_pane'] = $bool;
	}
	
	public function getRenderCategoryPane() {
		return $this->renderCategoryPane;	
	}
    
    public function setRenderOption($key, $value = NULL) {
        if( is_array($key) ) {
            $this->renderOptions = array_merge($this->renderOptions, $key);
        }
        
        if( is_string($key) && !is_null($value) ) {
            $this->renderOptions[$key] = $value;
        }
    }

    public function render() { ?>
        <div class="cms-content-header"><?php echo $this->renderOptions['content_name']; ?></div>
        
        <div id="content-pane" class="cms-content-pane"><?php
			if( $this->renderOptions['category_pane'] ) { ?>      
            <div class="category-pane">        
                <div class="pane-header">        
                    <?php echo $this->renderOptions['category_title']; ?>
                    
                    <?php if( $this->renderOptions['category_actions']) { ?>
                    <div style="width: 66px; position: absolute; right: 10px; top: 2px; text-align:right;">        
                        <a id="add-cat-link" href="#" title="Add Category"><img src="images/icons/add.png" width="16" height="16" alt="add" align="absmiddle" border="0" /></a>        
                        <a id="edit-cat-link" href="#" title="Add Category"><img src="images/icons/pencil.png" width="16" height="16" alt="add" align="absmiddle" border="0" /></a>        
                        <a id="del-cat-link" href="#" title="Delete Selected Category"><img src="images/icons/delete.png" width="16" height="16" alt="add" align="absmiddle" border="0" /></a>        
                    </div>
                    <?php } ?>
                </div>        
                <div class="pane-content">        
                    <ul class="category-list">        
                    <?php
                    $categories = $this->getCategoryArray();

                    for ($i = 0; $i < count($categories); $i++) {
                        $cat_id = $categories[$i]['id'];
                        $cat = $categories[$i]['name'];
                        $desc = $categories[$i]['description'];
                        echo "<li><a id='cat-$cat_id' href='#' title='$desc' data-id='$cat_id' >$cat</a></li>";
                    }
                    ?>
                    </ul>
                </div>
            </div><?php
			} ?>
            
            <div class="articles-pane">
                <div id="art-pane-header" class="pane-header"><?php echo $this->renderOptions['content_name']; ?> In</div>
                <div class="pane-sub-header">
                    <a id="add-link" href="#"><img src="images/icons/newspaper_add.png" alt="add" width="16" height="16" border="0" align="absmiddle" /> Add New <?php echo $this->itemTitle; ?></a>
                    <a id="move-link" href="#" ><img src="images/icons/newspaper_go.png" alt="del" width="16" height="16" border="0" align="absmiddle" /> Move Selected</a>
                    <a id="delete-link" href="#"><img src="images/icons/newspaper_delete.png" alt="del" width="16" height="16" border="0" align="absmiddle" /> Delete Selected</a>
                </div>
                <div id="articles-display" class="pane-content"></div>
                <div class="pane-footer">
                    Pages:
                </div>
            </div>


            <div id="cat-add-dialog" class="cms-dialog">
                <div class="cms-dialog-title">Add Category</div>
                <div class="cms-dialog-content">
                    <?php include( $this->renderOptions['category_form_path'] ); ?>
                </div>
            </div>
        </div>

        <div id="editor-pane" class="cms-content-pane" style="border:solid #ccc 1px; padding: 0px; border-spacing: 0px; display: none;">
            <div class="pane-header">Item Editor</div>
            <div id="pane-content" style="padding: 10px;">

            </div>
        </div> 
		
		<script type="text/javascript" src="../assets/scripts/jquery/js/jquery.js"></script>
		<script type="text/javascript" src="scripts/friendurl/jquery.friendurl.min.js"></script>
        <script type="text/javascript" src="scripts/tiny_mce/tiny_mce.js"></script>
        <script type="text/javascript" src="scripts/pane_ui.js"></script>
		<?php
    }

}
    ?>