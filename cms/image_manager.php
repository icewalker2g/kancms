<?php 
include("accesscheck.php"); 
$im = new ImageManager();

$editFormAction = $_SERVER['PHP_SELF'];
if (isset($_SERVER['QUERY_STRING'])) {
   $editFormAction .= "?" . htmlentities($_SERVER['QUERY_STRING']);
}

if ((isset($_POST["MM_update"])) && ($_POST["MM_update"] == "form2")) {
   $im->updateImage();

   $updateGoTo = $_SERVER['PHP_SELF'];
   header(sprintf("Location: %s", $updateGoTo));
}

if ((isset($_POST["MM_insert"])) && ($_POST["MM_insert"] == "img_upload")) {
   $im->addImage();

   // redirect the page to referal page or back to uploader to generate image list
   if( isset($_GET['rdir']) ) {
      $redirectPage = urldecode($_GET['rdir']);
      header(sprintf("Location: %s", $redirectPage));
   } else {
      $redirectPage = $_SERVER['PHP_SELF'];

      if( isset($_GET['us']) ) {
         $redirectPage .= "?us=" . $_GET['us'];
      }
      
      header(sprintf("Location: %s", $redirectPage));
   }
}

$query_rsImgCats = "SELECT * FROM {$im->ImageCategory->getTableName()} ";
$rsImgCats = mysqli_query($config, $query_rsImgCats) or die(mysqli_error());
$row_rsImgCats = mysqli_fetch_assoc($rsImgCats);
$totalRows_rsImgCats = mysqli_num_rows($rsImgCats);

$image = $im->getImage(isset($_GET['id']) ? $_GET['id'] : -1);
$row_rsImgUpdate = $image->getDataArray();

?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/cms.dwt.php" codeOutsideHTMLIsLocked="false" -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<!-- InstanceBeginEditable name="doctitle" -->
      <title><?php echo getSetting('CMSTitle','KAN Content Management System'); ?></title>
      <!-- InstanceEndEditable -->
<link rel="shortcut icon" type="image/png" href="../favicon.png" />
<link rel="stylesheet" type="text/css" href="css/widestyles.css"/>
<link rel="stylesheet" type="text/css" href="css/cms.css"/>
<link rel="stylesheet" type="text/css" href="css/cms-ui.css"/>
<link rel="stylesheet" type="text/css" href="css/util.css"/>

<script type="text/javascript" src="../assets/scripts/jquery/js/jquery.js"></script>
<script type="text/javascript" src="../assets/scripts/jquery/js/jquery-ui.js"></script>
<script type="text/javascript" src="../assets/scripts/common/pageManager.js"></script>
<script type="text/javascript" src="scripts/system.js"></script>
<script type="text/javascript" src="scripts/ui.js"></script>

<!-- InstanceBeginEditable name="head" -->
      <link rel="stylesheet" type="text/css" href="css/tabs.css"/>
      <link rel="stylesheet" type="text/css" href="css/images.css"/>
      <link rel="stylesheet" type="text/css" href="utils/file_utils.css" />
      <script type="text/javascript" src="scripts/tabs.js"></script>
      <script type="text/javascript" src="scripts/image_manager.js"></script>
      <script type="text/javascript" src="utils/file_utils.js"></script>
      <script type="text/javascript">

         pageManager.addLoadEvent( function() {
            image_manager.init({
               use_section: "<?php echo (isset($_GET['us']) ?  $_GET['us'] : 'cms') ; ?>"
            });
         });

      </script>

      <!-- InstanceEndEditable -->
</head>

<body>

<div id="page">
	<div id="header">
    	<?php include('logo_header.php'); ?>
    </div>
    
    <?php if( isset($_SESSION['CMS_Username']) ) { ?>
    <div id="site-info-bar">
		<?php include('system_menu.php'); ?>
    </div>
    <?php } ?>
    
    
    <div id="content-wrapper">
    	<div id="content-header">
        	<span>
			<!-- InstanceBeginEditable name="section title" -->Manage Site Images<!-- InstanceEndEditable -->
            </span>
        </div>
    	<div id="content">
			<!-- InstanceBeginEditable name="content" -->
            <div id="nav"><?php include('nav_section.php'); ?></div>
            <div id="main">
            	<table border="0" align="center" cellpadding="3" cellspacing="0" class="newsTbl">

                   <?php if( !isset($_GET['upload']) && !isset($_GET['edit']) ) { ?>
                   <tr>
                      <td class="sqrtab">Available Images</td>
                   </tr>
                   <tr>
                      <td class="newsSummary">
                         <div id="tab_section">
                            <div id="tab_row"><?php

							  $count = 0;
							  mysqli_data_seek($rsImgCats, 0);
							  while( $row_rsImgCats = mysqli_fetch_assoc($rsImgCats) ) { ?>
								   <div class="tab">
									  <a href="#" onclick="image_manager.showImages('<?php echo $row_rsImgCats['CategoryCode']; ?>', <?php echo $count++; ?>); return false;"><?php echo $row_rsImgCats['CategoryName']; ?></a>
								   </div><?php
							  } ?>
                              
                            </div>

                            <div id="tab_area">
                               <!-- Selection Options -->
                               <div id="tab_options" class="tab_options">
                                  <div id="links">
                                     <a href="?upload&amp;us="><img src="images/icons/image_add.png" alt="add" width="16" height="16" border="0" align="absmiddle" /></a>
                                     <a href="#" id="upload_link" >Upload Image</a>
                                     |
                                     <a href="#" id="delete_link" ><img src="images/icons/image_delete.png" alt="del" width="16" height="16" border="0" align="absmiddle" /> Delete Selected</a>
                                  </div>

                                  <div id="search">
                                     &nbsp;
                                  </div>
                               </div>

                               <!-- Main Content Area -->
                               <div id="tab_content">Preparing Content Management System. Please Wait....</div>

                               <!-- Pagination Section -->
                               <div id="tab_bottom" class="tab_options">
                                  <div id="links">
                                     <a href="#" id="prev_link" >&laquo; Previous Page</a> |
                                     <a href="#" id="next_link" >Next Page &raquo;</a>
                                  </div>

                                  <div id="search">
                                     <form id="limit_form" action="" onsubmit="return false;">
                                        Showing <input type="text" size="4" name="limit" id="limit" value="30" style="text-align:center;" title="Enter Value And Hit Enter To Reload. Leave Field To Simply Set Limit Without Reload" />
                                        Records
                                     </form>
                                  </div>
                               </div>

                            </div>
                            <div id="loader" style="display:none;"><img src="images/loadSnake.gif" alt="loading" hspace="5" align="absmiddle" />Loading Selected Section Information. Please Wait...</div>
                         </div>
                      </td>
                   </tr>
                      <?php } ?>

                   <?php if( isset($_GET['edit']) ) { ?>
                   <tr>
                      <td class="sqrtab">Image Information Update</td>
                   </tr>
                   <tr>
                      <td valign="top" class="newsSummary">Complete the fields belows to update the information for the selected image&nbsp;
                         <form action="<?php echo $editFormAction; ?>" method="post" name="form2" id="form2">
                            <table align="center" cellpadding="5" cellspacing="0">

                               <tr valign="baseline">
                                  <td colspan="2" align="center" valign="middle" nowrap="nowrap" class="sectionTitle1"><img src="<?php echo $row_rsImgUpdate['ImageFile']; ?>" alt="Image" name="image" border="1" id="image" /></td>
                               </tr>

                               <tr valign="baseline">
                                  <td align="right" nowrap="nowrap" class="sectionTitle1">Use Section:</td>
                                  <td>
                                     <select name="UseSection" id="UseSection" class="sectionText1">
                                           <?php
                                           do {
                                              ?>
                                        <option value="<?php echo $row_rsImgCats['CategoryCode']?>"<?php if (!(strcmp($row_rsImgCats['CategoryCode'], $row_rsImgUpdate['UseSection']))) {
                                                 echo "selected=\"selected\"";
                                                      } ?>><?php echo $row_rsImgCats['CategoryName']?></option>
                                                      <?php
                                                   } while ($row_rsImgCats = mysqli_fetch_assoc($rsImgCats));
                                                   $rows = mysqli_num_rows($rsImgCats);
                                                   if($rows > 0) {
                                                      mysqli_data_seek($rsImgCats, 0);
                                                      $row_rsImgCats = mysqli_fetch_assoc($rsImgCats);
                                                   }
                                                   ?>
                                     </select>
                                  </td>
                               </tr>

                               <tr valign="baseline">
                                  <td align="right" nowrap="nowrap" class="sectionTitle1">Image Name:</td>
                                  <td><input type="text" name="ImageName" value="<?php echo htmlentities($row_rsImgUpdate['ImageName'], ENT_COMPAT, 'iso-8859-1'); ?>" size="32" /></td>
                               </tr>
                               <tr valign="baseline">
                                  <td align="right" nowrap="nowrap" class="sectionTitle1">Image Description:</td>
                                  <td><input type="text" name="ImageDescription" value="<?php echo htmlentities($row_rsImgUpdate['ImageDescription'], ENT_COMPAT, 'iso-8859-1'); ?>" size="32" /></td>
                               </tr>
                               <tr valign="baseline">
                                  <td nowrap="nowrap" align="right">&nbsp;</td>
                                  <td>&nbsp;</td>
                               </tr>
                               <tr valign="baseline">
                                  <td colspan="2" align="center" nowrap="nowrap"><input name="Submit" type="submit" value="Update Info" />
                                     <input type="button" name="button3" id="button3" value="Cancel Update" onclick="if(confirm('Cancel Image Information Update?')) location.href = 'image_manager.php';"/> &nbsp;&nbsp;
                                     <input type="button" name="button4" id="button4" value="Delete Image" onclick="if(confirm('Delete Current Image From Database and Server?')) location.href='utils/del.php?del=images&id=<?php echo $_GET['id']; ?>&ref=../image_manager.php&ea=deleteFile&fpath=<?php echo $row_rsImgUpdate['ImageFile']; ?>';"/>
                                  </td>
                               </tr>
                            </table>
                            <input type="hidden" name="MM_update" value="form2" />
                            <input type="hidden" name="id" value="<?php echo $row_rsImgUpdate['id']; ?>" />
                         </form>                    </td>
                   </tr>
                      <?php } ?>

                   <?php if( isset($_GET['upload']) ) { ?>
                   <tr>
                      <td class="sqrtab">Image Upload Controller</td>
                   </tr>
                   <tr>
                      <td class="newsSummary"><form action="<?php echo $editFormAction; ?>" method="POST" enctype="multipart/form-data" name="img_upload" id="img_upload">
                            <table width="98%" border="0" cellspacing="0" cellpadding="3">
                               <tr valign="baseline">
                                  <td width="24%" align="right" nowrap="nowrap">&nbsp;</td>
                                  <td colspan="2">&nbsp;</td>
                               </tr>
                               <tr valign="baseline">
                                  <td align="right" nowrap="nowrap" class="sectionTitle1"><strong>Image Name:</strong></td>
                                  <td colspan="2">
                                     <label>
                                        <input name="ImageName" type="text" id="ImageName" size="40" />
                                     </label>
                                  </td>
                               </tr>
                               <tr valign="baseline">
                                  <td align="right" nowrap="nowrap" class="sectionTitle1"><strong>Image Description: </strong></td>
                                  <td colspan="2">
                                     <span class="course_text">
                                     <input name="ImageDescription" type="text" id="ImageDescription" size="40" />
                                     </span>
                                  </td>
                               </tr>
                               <tr valign="baseline">
                                  <td align="right" nowrap="nowrap" class="sectionTitle1">&nbsp;</td>
                                  <td colspan="2">&nbsp;</td>
                               </tr>
                               <tr valign="baseline">
                                  <td align="right" valign="middle" nowrap="nowrap" class="sectionTitle1"><strong>Image Use Section:</strong></td>
                                  <td width="33%" valign="baseline">
                                     <select name="UseSection" class="sectionText1" id="UseSection">
                                         <option value="" <?php if (!(strcmp("", $_GET['cat']))) {echo "selected=\"selected\"";} ?>>Select Image Category</option>
                                         <?php
do {  
?>
                                         <option value="<?php echo $row_rsImgCats['CategoryCode']?>"<?php if (!(strcmp($row_rsImgCats['CategoryCode'], $_GET['cat']))) {echo "selected=\"selected\"";} ?>><?php echo $row_rsImgCats['CategoryName']?></option>
                                         <?php
} while ($row_rsImgCats = mysqli_fetch_assoc($rsImgCats));
$rows = mysqli_num_rows($rsImgCats);
if($rows > 0) {
mysqli_data_seek($rsImgCats, 0);
$row_rsImgCats = mysqli_fetch_assoc($rsImgCats);
}
?>
                                     </select>
                                  </td>
                                  <td width="43%" valign="middle">&nbsp;</td>
                               </tr>
                               <tr valign="baseline">
                                  <td align="right" nowrap="nowrap" class="sectionTitle1">&nbsp;</td>
                                  <td colspan="2">&nbsp;</td>
                               </tr>
                               <tr valign="baseline">
                                  <td align="right" nowrap="nowrap" class="sectionTitle1"><strong>Image To Upload:</strong></td>
                                  <td colspan="2">
                                       <!--<input name="uploadFile" type="file" class="course_insidetext" id="uploadFile" size="45" onchange="setDestinationFileName(this)" />-->
                                     <input name="uploadFile" type="file" class="course_insidetext" id="uploadFile" size="45" />
                                  </td>
                               </tr>
                               <tr valign="baseline">
                                   <td nowrap="nowrap" align="right">&nbsp;</td>
                                   <td colspan="2" valign="middle" class="small">&nbsp;</td>
                               </tr>
                               <tr>
                                   <td colspan="3" align="center">
                                       <input type="submit" name="button" id="button" value="Upload" />
                                       <input type="button" name="button2" id="button2" value="Cancel Upload" onclick="if( confirm('Cancel Image Upload Process?') ) location.href = '<?php echo $_SERVER['PHP_SELF']; ?>' " />                                               
                                       <input name="destFileName" type="hidden" id="destFileName" value="<?php echo "img"; ?>" />
                                       <input name="destFolder" type="hidden" id="destFolder" value="../assets/images/pics/" />
                                       <input name="allowOverwrite" type="hidden" value="yes" />
                                       <input name="Picture" type="hidden" id="Picture" />
                                       <input name="redirect" type="hidden" id="redirect" value="<?php echo $editFormAction; ?>" />
                                       <input name="maxlimit" type="hidden" id="maxlimit" value="2097152" />
                                       <input name="allowed_ext" type="hidden" id="allowed_ext" value="jpg,gif,png" />
                                       <input name="thumbFolder" type="hidden" id="thumbFolder" checked="checked" value="../assets/images/news/thumbs/" />                                           </td>
                                   <?php
                                     $randNum = rand(1,100);
                                     $date_info = date("dmY_His");
                                     ?>
                               </tr>
                               </table>
                            <input type="hidden" name="MM_insert" value="img_upload" />
                         </form>                  </td>
                   </tr>
                      <?php } // end if() ?>
                </table>
            </div>
               
               <!-- InstanceEndEditable -->
        </div>
    </div>
    
    <div id="footer">
    	<?php include('footer.php'); ?>
    </div>
</div>

</body>
<!-- InstanceEnd --></html>
<?php
mysqli_free_result($rsImgCats);
?>
