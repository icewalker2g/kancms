<?php 
include('accesscheck.php'); 

$im = new ImageManager();

if( isset($_POST['picup_Picture']) ) {
	// prevent generated page from caching, especially with image generated
	header ("Expires: Mon, 26 Jul 1997 05:00:00 GMT"); // Date in the past
	header ("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT"); // always modified
	header ("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
	header ("Pragma: no-cache"); // HTTP/1.0
	
	$_POST['Picture'] = $_POST['picup_Picture'];
	$pic = $_POST['Picture'];
	$_POST['maxlimit'] = 3000000;
	$_POST['destFileName'] = substr($pic, strrpos($pic,"/"), strrpos($pic, ".") - strrpos($pic,"/")  );
	$_POST['destFolder'] = $_POST['destFolder'];
	$_POST['thumbFolder'] = $_POST['thumbFolder'];
	
	// include file for picture upload, which should upload image to destFolder
	include("utils/fileUpload.php"); 
	
	// include file for thumbnail generation
	include("utils/thumbgen.php");

	// generate a thumbnail of applicant photo using uploaded image, target destination folder and specified width
	$thumbPath = generateThumb( $_POST['Picture'], $_POST['thumbFolder'], 150);
	
	// resize the image if it is to be used for a news article
	if( $_POST['useSection'] == "articles" ) {
	  $fpath = generateThumb( $_POST['Picture'], $_POST['destFolder'], 285);
	} else if( $_POST['useSection'] == "sidebar" ) {
	  $fpath = generateThumb( $_POST['Picture'], $_POST['destFolder'], 200);
	} else if( $_POST['useSection'] == "media" ) {
	  $fpath = generateThumb( $_POST['Picture'], $_POST['destFolder'], 200);
	} else if( $_POST['useSection'] == "cms" ) {
		// we cannot safely say that every image must be resized but we'll use this
		// option for now to ensure that uploaded images are not so large when displayed
	  $fpath = generateThumb( $_POST['Picture'], $_POST['destFolder'], 640); // will possible remove this options later
	} else {
		// if any other use section is specified simply use the CMS specification
		$fpath = generateThumb( $_POST['Picture'], $_POST['destFolder'], 640);
	}
	
	// generate a thumbnail image for the news article and update database with path
	if($_POST['useSection'] != "sidebar" ) {
		$fpath = generateThumb( $_POST['Picture'], $_POST['thumbFolder'], 150);
		
		if( $fpath == "" || $fpath == NULL ) {
			$fpath = $_POST['Picture'];
		}
	}
	
	// return image to be displayed
	if( $error != "" ) {
		echo '<script>alert("' . $error . '")</script>';
	} else {
		
		// even though a thumbnail will be generated to the destination folder,
		// we'll still use the original generated ImagePath to ensure the right path is obtained
		// since $fpath can be replaced by another path
		$imageFile = $_POST['Picture'];		
        $image = new Image();
		
        $totalRows_result = $image->count(array(
            'filter' => array('ImageFile' => $imageFile)
        ));
		
        $row_result2 = $image->count(array(
            'filter' => array('ImageName LIKE' => '%Uploaded%')
        ));
		
		$imgName = "Uploaded Image " . ($row_result2['cnt'] + 1);
		$useSection = $_POST['useSection'];
		
		if( $totalRows_result == 0 ) {
            $image->setData(array(
                'SiteID' => $siteid,
                'UseSection' => $useSection,
                'ImageName' => $imgName,
                'ImageFile' => $imageFile,
                'ThumbPath' => $thumbPath,
                'DateAdded' => date('Y-m-d H:i:s')
            ));
            $image->save();
		}
		
		echo '<img id="uploadedPic" name="uploadedPic" width="150" src="' . strtolower($_POST['Picture']) . '?' . rand(1,100) . '">';
		echo "<input type=\"hidden\" id=\"picup_imgPath\" name=\"picup_imgPath\" value=\"$imageFile\" />";
		echo "<input type=\"hidden\" id=\"picup_thumb\" name=\"picup_thumb\" value=\"$thumbPath\" />";
	}

} 

else if( isset($_POST['action']) && $_POST['action'] == "load_images" ) {
	$useSection = $_POST['use_section'];
	$start = isset($_POST['start']) ? $_POST['start'] : 0;
	$limit = isset($_POST['limit']) ? $_POST['limit'] : 20;
	$siteid = isset($_POST['siteid']) ? $_POST['siteid'] : SITE_ID;
	
    $image = new Image();
    $count = $image->count(array(
        'filter' => array(
            'UseSection' => $useSection,
            'SiteID' => $siteid
        )
    ));
    
	$pageCount = ceil($count/$limit);
	
	?>
    <div class="nav-block">
    	<div class="select-box">
        	<input type="checkbox" name="selectall" id="selectall"  />
            Select All
        </div>
        
        <div class="links-box"><?php
			for($i = 1; $i <= $pageCount; $i++) { ?>
				<a href="#" data-page="<?php echo $i; ?>" <?php if( $limit * ($i - 1) == $start) echo 'class="selected"'; ?>><?php echo $i; ?></a><?php
			} ?>
        </div>
    </div>
    <?php
	$images = $image->find(array(
        'filter' => array(
            'SiteID' => SITE_ID,
            'UseSection' => $useSection
        ),
        'order' => array('id' => 'desc'),
        'offset' => $start,
        'limit' => $limit
    ));
	
	
	foreach($images as $image) { ?>
        
		<div class="img-box" id="img-box-<?php echo $image->getId(); ?>">
        	<div>
        		<img src="<?php echo $image->getData('ThumbPath'); ?>" alt="<?php echo $image->getData('ImageDescription'); ?>" 
                	data-url="<?php echo $image->getData('ImageFile'); ?>"
                    data-thumb-url="<?php echo $image->getData('ThumbPath'); ?>" />
            </div>
            <div class="image-name"><?php echo $image->getData('ImageName'); ?></div>
            <div>
            	<!--<a class="edit-link" href="#">Edit</a>-->
                <a class="delete-link" data-id="<?php echo $image->getData('id'); ?>" href="#">Delete</a>
            </div>
            
        </div>
    	<?php
	}
	
} else if( isset($_POST['action']) && $_POST['action'] == "delete_images" ) {
	
	$id = isset($_POST['id']) ? $_POST['id'] : -1;
	
	$im->deleteImage($id);
}
?>