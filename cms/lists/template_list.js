// This list may be created by a server logic page PHP/ASP/ASPX/JSP in some backend system.
// There templates will be displayed as a dropdown in all media dialog if the "template_external_list_url"
// option is defined in TinyMCE init.

var tinyMCETemplateList = [
	// Name, URL, Description
	["One Column Liquid, Centered, Header and Footer", "templates/one_col_fluid_hf.htm", "Two Column Liquid, Header and Footer"],
	["Two Column Liquid, Left Sidebar", "templates/two_col_fluid_ls.htm", "Two Column Liquid Layout"],
	["Two Column Liquid, Left Sidebar, Header and Footer", "templates/two_col_fluid_lshf.htm", "Two Column Liquid, Header and Footer"],
	["Two Column Liquid, Right Sidebar", "templates/two_col_fluid_rs.htm", "Two Column Liquid Layout"],
	["Two Column Liquid, Right Sidebar, Header and Footer", "templates/two_col_fluid_rshf.htm", "Two Column Liquid, Header and Footer"],
	["Three Column Liquid", "templates/three_col_fluid.htm", "Three Column Liguid Layout"],
	["Three Column Liquid, Header and Footer", "templates/three_col_fluid_hf.htm", "Three Column Liguid Layout, Header and Footer"]
];