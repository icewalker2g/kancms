<?php 

include_once('../accesscheck.php');

// use the GET Variable for the site id if variable instead of SESSION if specified
if( isset($_GET['siteid']) ) {
	$siteid = $_GET['siteid'];
}

/**
 * Generates the list of page links in a representational tree format
 * 
 * @param string $str
 * @param array $page_data
 * @param int $level 
 */
function buildPageTree(&$str, $page_data, $level = 1) {
    
    foreach($page_data as $data) {
        
        $page = new Page($data);
        
        $dashes = str_repeat("--", $level);
        $str .= ",['$dashes " . addslashes($page->getName()) . "','{$page->getURL()}']";
        
        if( isset($data['children']) && count($data['children']) > 0 ) {
            buildPageTree($str, $data['children'], ++$level);
        }
    }
}

$pm = new PageManager();
$pages = $pm->getAdjacencyTree()->getFullNodes( array('id','PageName','PageNameAlias','PageDescription','PagePath','RedirectURL'));


$str = "var tinyMCELinkList = new Array(\n";
$str .= "['CMS QUICK LINKS','#'],"
	  . "['------------------','#'],"
	  . "['CMS Home', '../pages/index.php?goto=../cms/index.php'],"
	  . "[' ','#'],"
	  . "['SITE PAGES','#'],"
	  . "['------------------','#']";

// generate the page list
buildPageTree($str, $pages);

$str .= ");";



/*header("Expires: Mon, 26 Jul 1997 05:00:00 GMT"); // Date in the past
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT"); // always modified
header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
header("Pragma: no-cache"); // HTTP/1.0
header("Content-Type: text/javascript");*/

echo $str;
?>