<?php 

include('../accesscheck.php');

// use the GET Variable for the site id if variable instead of SESSION if specified
if( $siteid == -1 && isset($_GET['siteid']) ) {
	$siteid = $_GET['siteid'];
} 

$query_rsMedia = "SELECT * FROM media WHERE SiteID = $siteid ORDER BY MediaName ASC";
$rsMedia = mysqli_query($config, $query_rsMedia) or die(mysqli_error());
$row_rsMedia = mysqli_fetch_assoc($rsMedia );
$totalRows_rsMedia = mysqli_num_rows($rsMedia );

$str = "var tinyMCEMediaList = [";
for($i = 0; $i < $totalRows_rsMedia; $i++) {
	$str .= "\t['". addslashes($row_rsMedia['MediaName']) . "','" . $row_rsMedia['MediaPath'] . "']"; 
	
	if( $i <= ($totalRows_rsMedia - 2) ) {
		$str .= ",\n";
		$row_rsMedia = mysqli_fetch_assoc($rsMedia );
	} else {
		$str .= "\n";
	}
}

$str .= "];";

header("Expires: Mon, 26 Jul 1997 05:00:00 GMT"); // Date in the past
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT"); // always modified
header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
header("Pragma: no-cache"); // HTTP/1.0
header("Content-Type: text/javascript");

echo $str;

mysqli_free_result($rsMedia);
?>