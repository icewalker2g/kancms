<?php 

include_once('../accesscheck.php');

// use the GET Variable for the site id if variable instead of SESSION if specified
if( isset($_GET['siteid']) ) {
	$siteid = $_GET['siteid'];
} 

$query_rsImages = "SELECT * FROM images WHERE UseSection = 'cms' AND SiteID = $siteid ORDER BY ImageName ASC";
$rsImages = mysqli_query($config, $query_rsImages) or die(mysqli_error());
$row_rsImages = mysqli_fetch_assoc($rsImages );
$totalRows_rsImages = mysqli_num_rows($rsImages );

//$imageList = fopen('lists/image_list.js', "w+");

$str = "var tinyMCEImageList = new Array(\n";
for($i = 0; $i < $totalRows_rsImages; $i++) {
	$str .= "\t['". addslashes($row_rsImages['ImageName']) . "','" . $row_rsImages['ImageFile'] . "']"; 
	
	if( $i <= ($totalRows_rsImages - 2) ) {
		$str .= ",\n";
		$row_rsImages = mysqli_fetch_assoc($rsImages );
	} else {
		$str .= "\n";
	}
}

$str .= ")";

header("Expires: Mon, 26 Jul 1997 05:00:00 GMT"); // Date in the past
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT"); // always modified
header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
header("Pragma: no-cache"); // HTTP/1.0
header("Content-Type: text/javascript");

echo $str;

mysqli_free_result($rsImages );
?>