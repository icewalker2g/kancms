<?php
// *** obtain a connection to the global KAN configuration
require_once("../core/kan.inc");

// let libraries know we are in the CMS section
define("IN_CMS", true);

// *** Logout the current user.
$logoutGoTo = "login.php";
if (!isset($_SESSION)) {
    session_start();
}

if (isset($_GET['logout'])) {
    $_SESSION['CMS_Username'] = NULL;
    $_SESSION['CMS_UserGroup'] = NULL;
    $_SESSION['User'] = NULL;
    $_SESSION['UserID'] = NULL;
    $_SESSION['Level'] = NULL;
    $_SESSION['User_Role'] = NULL;
    $_SESSION['LastAccess'] = NULL;
    $_SESSION['siteid'] = NULL;
    $_SESSION['PrevUrl'] = NULL;

    unset($_SESSION['CMS_Username']);
    unset($_SESSION['CMS_UserGroup']);
    unset($_SESSION['User']);
    unset($_SESSION['UserID']);
    unset($_SESSION['Level']);
    unset($_SESSION['User_Role']);
    unset($_SESSION['LastAccess']);
    unset($_SESSION['siteid']);
    unset($_SESSION['PrevUrl']);

    if ($logoutGoTo != "") {
        header("Location: $logoutGoTo");
        exit;
    }
}

// *** get a database instance
$db = new Database();

// *** Validate request to login to this site.
if (!isset($_SESSION)) {
    session_start();
}

$loginFormAction = $_SERVER['PHP_SELF'];
if (isset($_GET['accesscheck'])) {
    $_SESSION['PrevUrl'] = $_GET['accesscheck'];
}

if (isset($_POST['Username'])) {
    $um = new UserManager();
    
    $loginUsername = strip_tags($_POST['Username']);
    $password = strip_tags($_POST['Password']);
    $cmsUserAuthorization = "Level";
    $cmsRedirectSuccess = "sites.php";
    $cmsRedirectLoginFailed = "login.php?error";
    $cmsRedirectToReferer = true;

    $user = $um->getUserByUsername($_POST['Username']);

    if (isset($user)) { // if the specified user name exist, then we need to ensure the password also matches
        $dbPassword = $user->getData('Password');
        $dbPassSalt = $user->getData('PassSalt');

        $confirmationHash = md5($password . $dbPassSalt);

        if ($dbPassword != $confirmationHash) {
            $loginFoundUser = false;
        } 
        
        else {
            $loginFoundUser = true;
        }
    }

    if ($loginFoundUser) {

        $loginStrGroup = $user->getData('Level');
        $loginUserRole = $user->getData('Role');
        $loginAccess = $user->getData('LastAccess');
        $userFirstName = $user->getData( 'FirstName');
        $userID = $user->getData('id');

        //declare two session variables and assign them
        $_SESSION['CMS_Username'] = $loginUsername;
        $_SESSION['User'] = $userFirstName;
        $_SESSION['UserID'] = $userID;
        $_SESSION['CMS_UserGroup'] = $loginStrGroup;
        $_SESSION['Level'] = $loginStrGroup;
        $_SESSION['User_Role'] = $loginUserRole;
        $_SESSION['LastAccess'] = $loginAccess;

        $user->setData(array(
            'LastAccess' => date('Y-m-d H:i:s')
        ));
        $user->save();

        header("Location: " . $cmsRedirectSuccess);
    } else {
        header("Location: " . $cmsRedirectLoginFailed);
    }
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/cms.dwt.php" codeOutsideHTMLIsLocked="false" -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<!-- InstanceBeginEditable name="doctitle" -->
        <title><?php echo getSetting('CMSTitle', 'KAN Content Management System'); ?></title>
        <!-- InstanceEndEditable -->
<link rel="shortcut icon" type="image/png" href="../favicon.png" />
<link rel="stylesheet" type="text/css" href="css/widestyles.css"/>
<link rel="stylesheet" type="text/css" href="css/cms.css"/>
<link rel="stylesheet" type="text/css" href="css/cms-ui.css"/>
<link rel="stylesheet" type="text/css" href="css/util.css"/>

<script type="text/javascript" src="../assets/scripts/jquery/js/jquery.js"></script>
<script type="text/javascript" src="../assets/scripts/jquery/js/jquery-ui.js"></script>
<script type="text/javascript" src="../assets/scripts/common/pageManager.js"></script>
<script type="text/javascript" src="scripts/system.js"></script>
<script type="text/javascript" src="scripts/ui.js"></script>

<!-- InstanceBeginEditable name="head" -->

        <script type="text/javascript">
            window.onload = function() {
                document.getElementById("Username").focus();								   
            };
        </script>
        <style type="text/css">
            #content-header {
                display: none;    
            }
            
            .nice_header {
                color: #fff;
                padding: 10px;
            	background: rgb(130,20,13); /* Old browsers */
                background: -moz-linear-gradient(top, rgba(130,20,13,1) 0%, rgba(96,3,1,1) 100%); /* FF3.6+ */
                background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,rgba(130,20,13,1)), color-stop(100%,rgba(96,3,1,1))); /* Chrome,Safari4+ */
                background: -webkit-linear-gradient(top, rgba(130,20,13,1) 0%,rgba(96,3,1,1) 100%); /* Chrome10+,Safari5.1+ */
                background: -o-linear-gradient(top, rgba(130,20,13,1) 0%,rgba(96,3,1,1) 100%); /* Opera11.10+ */
                background: -ms-linear-gradient(top, rgba(130,20,13,1) 0%,rgba(96,3,1,1) 100%); /* IE10+ */
                filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#82140d', endColorstr='#600301',GradientType=0 ); /* IE6-9 */
                background: linear-gradient(top, rgba(130,20,13,1) 0%,rgba(96,3,1,1) 100%); /* W3C */    
            }
        </style>

        <!-- InstanceEndEditable -->
</head>

<body>

<div id="page">
	<div id="header">
    	<?php include('logo_header.php'); ?>
    </div>
    
    <?php if( isset($_SESSION['CMS_Username']) ) { ?>
    <div id="site-info-bar">
		<?php include('system_menu.php'); ?>
    </div>
    <?php } ?>
    
    
    <div id="content-wrapper">
    	<div id="content-header">
        	<span>
			<!-- InstanceBeginEditable name="section title" -->CONTENT MANAGEMENT LOGIN<!-- InstanceEndEditable -->
            </span>
        </div>
    	<div id="content">
			<!-- InstanceBeginEditable name="content" -->
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td height="420" align="center">

<?php if (isset($_GET['error'])) { ?>
                                    <div class="small" style="color:#900; margin-bottom: 15px; font-weight:bold">
                                        (Invalid Username Or Password Entered or User Account May Not Be Active)
                                    </div>
<?php } ?>
                                <form id="login" name="login" method="post" action="<?php echo $loginFormAction; ?>">
                                    <table width="440" border="0" cellpadding="10" cellspacing="0" bgcolor="#F9FCF8" style="border: solid #bbb 1px">
                                        <tr valign="middle">
                                            <td colspan="3" align="center" class="nice_header">Authentication Required</td>
                                        </tr>
                                        <tr>
                                            <td colspan="3" align="center" class="sectionTitle1">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td width="91" rowspan="3" align="center" class="sectionTitle1"><img src="images/Keychain.png" alt="lock" height="100" /></td>
                                            <td width="130" align="right" nowrap="nowrap" class="sectionTitle1">Username / Email</td>
                                            <td width="137" align="left"><input name="Username" type="text" id="Username" size="28"  autofocus="autofocus" /></td>
                                        </tr>
                                        <tr>
                                            <td align="right" class="sectionTitle1">Password</td>
                                            <td align="left"><input name="Password" type="password" id="Password" size="28" /></td>
                                        </tr>
                                        <tr>
                                            <td align="center" valign="middle"><p>&nbsp;</p></td>
                                            <td align="left" valign="middle">
                                                <input name="Submit" type="submit" class="sectionTitle1" id="button" value="Login" style="padding: 6px 20px" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="3" align="center" valign="top"><a href="#" class="small">Forgot Password?</a> | <a href="../pages/index.php" class="small">Go To Default Site</a></td>
                                        </tr>
                                    </table>
                            </form></td>
                        </tr>
                    </table>
                    <!-- InstanceEndEditable -->
        </div>
    </div>
    
    <div id="footer">
    	<?php include('footer.php'); ?>
    </div>
</div>

</body>
<!-- InstanceEnd --></html>