<?php 
require_once("accesscheck.php");
include_once('utils/file_utils.php');

$gm = new GalleryManager();
$galleries = $gm->getGalleries();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/cms.dwt.php" codeOutsideHTMLIsLocked="false" -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<!-- InstanceBeginEditable name="doctitle" -->
<title><?php echo getSetting('CMSTitle','KAN Content Management System'); ?></title>
<!-- InstanceEndEditable -->
<link rel="shortcut icon" type="image/png" href="../favicon.png" />
<link rel="stylesheet" type="text/css" href="css/widestyles.css"/>
<link rel="stylesheet" type="text/css" href="css/cms.css"/>
<link rel="stylesheet" type="text/css" href="css/cms-ui.css"/>
<link rel="stylesheet" type="text/css" href="css/util.css"/>

<script type="text/javascript" src="../assets/scripts/jquery/js/jquery.js"></script>
<script type="text/javascript" src="../assets/scripts/jquery/js/jquery-ui.js"></script>
<script type="text/javascript" src="../assets/scripts/common/pageManager.js"></script>
<script type="text/javascript" src="scripts/system.js"></script>
<script type="text/javascript" src="scripts/ui.js"></script>

<!-- InstanceBeginEditable name="head" -->
<link rel="stylesheet" type="text/css" href="css/gallery.css"/>
<link rel="stylesheet" type="text/css" href="../assets/scripts/jquery/css/ui-lightness/jquery-ui.css"/>
<script type="text/javascript" src="../assets/scripts/jquery/js/jquery.js"></script>
<script type="text/javascript" src="../assets/scripts/jquery/js/jquery-ui.js"></script>
<script type="text/javascript" src="scripts/gallery.js"></script>
<?php echo $fileUtils->printScripts(); ?>
<script type="text/javascript">
	pageManager.addLoadEvent( function() {
		gallery.init();
	});
</script>
<!-- InstanceEndEditable -->
</head>

<body>

<div id="page">
	<div id="header">
    	<?php include('logo_header.php'); ?>
    </div>
    
    <?php if( isset($_SESSION['CMS_Username']) ) { ?>
    <div id="site-info-bar">
		<?php include('system_menu.php'); ?>
    </div>
    <?php } ?>
    
    
    <div id="content-wrapper">
    	<div id="content-header">
        	<span>
			<!-- InstanceBeginEditable name="section title" -->CONTENT MANAGEMENT SECTION<!-- InstanceEndEditable -->
            </span>
        </div>
    	<div id="content">
			<!-- InstanceBeginEditable name="content" -->
            <div id="nav">
                <?php include('nav_section.php'); ?>
            </div>
            <div id="main">
                <table width="100%" border="0" cellpadding="4" cellspacing="0" class="newsTbl" align="center">
                    <?php 
					if( isset($_GET['edit']) || isset($_GET['add']) ) { 
				
						if( isset($_GET['edit']) ) {
							$gallery = $gm->getGallery(kan_get_parameter('id'));
						} ?>
                    <tr>
                        <td class="sqrtab">
                            <?php echo isset($_GET['edit']) ? "Edit Selected Gallery" : "Create New Gallery" ?>
                        </td>
                    </tr>
                    <tr>
                        <td class="newsContent">
                            <div id="gallery-tabs" class="ui-tabs" style="background: transparent;">
                                <ul class="ui-tabs-nav">
                                    <li class="ui-tabs-selected">
                                        <a href="#gallery-info">Gallery Definition</a>
                                    </li>
                                    <li id="images-tab" <?php echo isset($_GET['add']) ? 'style="visibility:hidden;"' : '' ?>>
                                        <a href="#gallery-images">Gallery Images</a>
                                    </li>
                                </ul>
                                <div id="gallery-info" class="ui-tabs-panel">
                                    <form id="gallery_form" name="form1" method="post" action="ajax_pages/galleries.php" onsubmit="return false;">
                                        <table width="650" border="0" align="center" cellpadding="5" cellspacing="0">
                                            <tr>
                                                <th colspan="4" align="left" class="sectionTitle3"> Gallery Definition</th>
                                            </tr>
                                            <tr>
                                                <td width="139" align="right">
                                                    * Gallery Name:
                                                </td>
                                                <td colspan="3">
                                                    <input class="required" name="GalleryName" type="text" id="GalleryName" size="40" value="<?php echo isset($gallery) ? $gallery->getName() : ""; ?>" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="right">
                                                    * Gallery Tag / Alias:
                                                </td>
                                                <td colspan="3">
                                                    <input class="required" name="GalleryTag" type="text" id="GalleryTag" size="15" value="<?php echo isset($gallery) ? $gallery->getAlias() : ""; ?>" />
                                                    <span id="tag_info" class="small">(a simple tag to identify this gallery, no spaces allowed) </span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="right" valign="top">
                                                    Gallery Description:
                                                </td>
                                                <td colspan="3">
                                                    <textarea name="GalleryDescription" id="GalleryDescription" cols="45" rows="5"><?php echo isset($gallery) ? $gallery->getDescription() : ""; ?></textarea>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="right">
                                                    * Gallery Folder:
                                                </td>
                                                <td colspan="3">
                                                    <input class="required" name="GalleryFolder" type="text" id="GalleryFolder" value="<?php echo isset($gallery) ? $gallery->getFolder() : "../assets/images/gallery/"; ?>" size="40" readonly="readonly" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="4">&nbsp;
                                                </td>
                                            </tr>
                                            <tr>
                                                <th colspan="4" align="left" class="sectionTitle3"> Gallery Options </th>
                                            </tr>
                                            <tr>
                                                <td align="right">
                                                    Image Sort Order:
                                                </td>
                                                <td colspan="3">
                                                    <select name="ImageSortOrder" id="ImageSortOrder">
                                                        <option value="DESC" <?php echo isset($gallery) && $gallery->getImageSortOrder() == "DESC" ? "selected='selected'" : "" ?> selected="selected">Recent Images First</option>
                                                        <option value="ASC" <?php echo isset($gallery) && $gallery->getImageSortOrder() == "ASC" ? "selected='selected'" : "" ?>>Oldest Images First</option>
                                                        <option value="RAND" <?php echo isset($gallery) && $gallery->getImageSortOrder() == "RAND" ? "selected='selected'" : "" ?>>Sort Images Randomly</option>
                                                    </select>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="right">
                                                    Allow Comments:
                                                </td>
                                                <td width="71">
                                                    <input type="checkbox" name="AllowComments" id="AllowComments" <?php echo isset($gallery) && $gallery->allowsComments() ? "checked=checked" : ""; ?> />
                                                </td>
                                                <td width="119" align="right">
                                                    Published:
                                                </td>
                                                <td width="281">
                                                    <input type="checkbox" name="Published" id="Published" <?php echo (isset($gallery) && $gallery->isPublished()) ? "checked=checked" : ""; ?> />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="right">&nbsp;
                                                </td>
                                                <td colspan="3">&nbsp;
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="4" class="cms-warning">
                                                    The following dimension settings 
                                                    will only apply when images are being uploaded to the gallery. Previously uploaded images will not be affected when
                                                these are changed. Note, however, that galleries may choose to use these settings when displaying images, which may cause the images to distort/degrade if they are smaller than the specified sizes.
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="4" align="right">&nbsp;
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="4" class="sectionTitle3">
                                                    Image Resize Options
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="right">
                                                Image Width:
                                                </td>
                                                <td>
                                                    <input class="number" name="ImageWidth" type="text" id="ImageWidth" style="text-align:center;" value="<?php echo isset($gallery) ? $gallery->getImageWidth() : '600'; ?>" size="5" />
                                                </td>
                                                <td align="right">
                                                    Thumbnail Width:
                                                </td>
                                                <td>
                                                    <input class="number" name="ThumbnailWidth" type="text" id="ThumbnailWidth" value="<?php echo isset($gallery) ? $gallery->getThumbnailWidth() : '150'; ?>" size="5" style="text-align:center;" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="right">&nbsp;
                                                </td>
                                                <td colspan="3">&nbsp;
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="4" align="center">
                                                    <input name="action" type="hidden" id="action" value="<?php echo isset($_GET['edit']) ? 'edit_gal' : 'add_gal'; ?>" />
                                                    <input type="hidden" name="id" id="id" value="<?php echo isset($gallery) ? $gallery->getId() : ""; ?>" />
                                                    <input type="submit" name="Submit" id="Submit" value="<?php echo (isset($_GET['edit']) ? "Update" : "Create") . " Gallery"; ?>" />
                                                    <input type="button" name="Cancel" id="Cancel" value="Cancel" onclick="windows.location.href = '<?php echo $_SERVER['PHP_SELF']; ?>';" />
                                                </td>
                                            </tr>
                                        </table>
                                    </form>
                                </div>
                                <div id="gallery-images" class="ui-tabs-panel">
                                	<div class="cms-form-message">
                                	Use the links and controls below to <strong>Add</strong>, <strong>Edit</strong> and <strong>Delete</strong> images to this gallery. Once done, click the <strong>Save and Close Gallery</strong> button to return to the <em>Gallery List</em>, or you can go back to the <em>Gallery Definition</em> section
                                    to update the gallery.
                                	</div>
                                    <table width="100%" border="0" cellpadding="4" cellspacing="0" class="newsTbl">
                                        <tr>
                                            <td class="newsHeader">
                                            	<input type="checkbox" name="image-checker" id="image-checker" />
                                                <label for="image-checker">Select All</label>
                                                
                                                &nbsp;&nbsp;
                                                <a id="image-add-photo" href="#">
                                                <img src="images/icons/image_add.png" alt="add" width="16" height="16" border="0" align="absmiddle" /> Add Photo To Gallery</a>
                                                <a id="image-delete-selected" href="#"><img src="images/icons/image_delete.png" alt="del" width="16" height="16" border="0" align="absmiddle" /> Delete Selected</a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td id="image-content" class="newsContent">
                                                <ul><?php
												if( isset($_GET['edit']) ) {
													$images = $gallery->getImages(0, 15);
													
													for( $i = 0; $i < count($images); $i++ ) { ?>
                                                        <li id="img-item-<?php echo $images[$i]->getId(); ?>">
                                                            <div class="image_box">
                                                                <img src="<?php echo $images[$i]->getThumbImageURL(); ?>" alt="<?php echo $images[$i]->getName(); ?>" data-url="<?php echo $images[$i]->getImageURL(); ?>" />
                                                            </div>
                                                            <div class="options_box">
                                                                <div class="name">
                                                                    <?php echo $images[$i]->getName(); ?>
                                                                </div>
                                                                <div class="links">
                                                                    <input type="checkbox" name="item2" id="item" value="<?php echo $images[$i]->getId(); ?>"  />
                                                                    <a class='edit-link' href="#" data-id="<?php echo $images[$i]->getId(); ?>" data-name="<?php echo $images[$i]->getName(); ?>"><img src="images/icons/image_edit.png" alt="edit" width="16" height="16" border="0" align="absmiddle" /> Edit</a>
                                                                    <a class="delete-link" href="#" data-id="<?php echo $images[$i]->getId(); ?>" data-name="<?php echo $images[$i]->getName(); ?>"><img src="images/icons/image_delete.png" alt="del" width="16" height="16" border="0" align="absmiddle" /> Delete</a>
                                                                </div>
                                                            </div>
                                                        </li><?php
													}
													
													if( count($images) == 0 ) {
														echo "<div>There are no images in this gallery.</div>";
													}
												} ?>
                                                </ul>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td id="images-footer" class="newsContent">
                                                <strong>Pages:</strong><?php
												
												if( isset($_GET['edit']) ) {
													$total = ceil($gallery->getImageCount() / 15);
													
													for( $i = 0, $j = 1; $i < $total; $i++, $j++ ) {
														$selected = $i == 0 ? "selected" : "";
														$gal_id = $gallery->getId();
														
														echo "<a class='pager-link $selected' data-page='$i' data-gallery-id='$gal_id' href='#'>$j</a>";
													}
												}
												?>
                                            </td>
                                        </tr>
                                    </table>
                                    
                                    <form>
                                    <div align="center" style="padding: 10px;"><input type="button" name="save" id="save" value="Save And Close Gallery" onclick="window.location = '<?php echo $_SERVER['PHP_SELF']; ?>';"  /></div>
                                    </form>
                                </div>
                            </div>
                            
                            <div id="image-uploader-form" style="display:none;">

                                <div class="cms-form-message">Enter The <strong>Name</strong> and a <strong>Description</strong> for the photo to be added to the gallery. Then select the photo file to be uploaded</div>
                                <form action="" method="post" enctype="multipart/form-data" name="frmAddPhoto" id="frmAddPhoto">
                                    <table width="600" align="center" cellpadding="5" cellspacing="0">
                                        <tr>
                                            <td width="101" nowrap="nowrap" class="course_section">
                                                <strong>* Image Name:</strong>
                                            </td>
                                            <td width="476">
                                                <input name="ImageName" type="text" class="required" value="" size="50" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td nowrap="nowrap" class="course_section" valign="top">
                                                <strong>* Description:</strong>
                                            </td>
                                            <td>
                                                <textarea name="ImageDescription" id="ImageDescription" cols="50" rows="4" class="required"></textarea>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td nowrap="nowrap" class="bluetooth">&nbsp;
                                            </td>
                                            <td>&nbsp;
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2" nowrap="nowrap" class="small">
                                                <strong>Select The Image To Upload (Max Limit: 2 MB)</strong>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td nowrap="nowrap" class="course_section">
                                                <strong>* Image File:</strong>
                                            </td>
                                            <td>
                                                <input name="uploadFile" type="file" class="required" id="uploadFile" multiple="true" size="50" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td nowrap="nowrap" class="bluetooth">&nbsp;
                                            </td>
                                            <td>
                                                <?php 
													$randNum = rand(1,100);
													$imageName = "img" . date("Ymd_his");
												?>
                                                <input name="destFileName" type="hidden" id="destFileName" value="<?php echo $imageName ?>" />
                                                <input name="destFolder" type="hidden" id="destFolder" value="<?php if( isset($gallery) ) echo $gallery->getFolder() . "pics/"; ?>" />
                                                <input name="allowOverwrite" type="hidden" value="yes" />
                                                <input name="ImageFile" type="hidden" id="ImageFile" value="<?php if( isset($gallery) ) echo $gallery->getFolder() . 'pics/' . $imageName . ".jpg"; ?>" />
                                                <input name="ThumbPath" type="hidden" id="ThumbPath" value="<?php if( isset($gallery) ) echo $gallery->getFolder() . 'thumbs/' . $imageName . ".jpg"; ?>" />
                                                <input name="maxlimit" type="hidden" id="maxlimit" value="2097152" />
                                                <input name="thumbFolder" type="hidden" id="thumbFolder" value="<?php if( isset($gallery) ) echo $gallery->getFolder() . 'thumbs/'; ?>" />
                                            </td>
                                        </tr>
                                    </table>
                                    <input type="hidden" name="action" id="action" value="upload-image"  />
                                    <input name="GalleryID" type="hidden" id="GalleryID" value="<?php if( isset($gallery) ) echo $gallery->getId(); ?>" />
                                    <input type="hidden" name="MM_insert" value="frmAddPhoto" />
                                </form>
                            </div>
                            
                            <div id="image-data-editor" style="display:none;">
                            	<form>
                                	<table width="100%" border="0" cellspacing="0" cellpadding="5">
                                        <tr>
                                            <td width="180" rowspan="2" align="center">
                                                <img src="" alt="" name="GalleryImage" width="150" id="GalleryImage" style="border: solid #ccc 1px; padding: 1px;" />
                                            </td>
                                            <td width="23%" align="right">
                                                <strong>Image Name:</strong>
                                            </td>
                                            <td>
                                                <input name="ImageName" type="text" id="ImageName" size="40" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right" valign="top">
                                                <strong>Description:</strong>
                                            </td>
                                            <td>
                                                <textarea name="ImageDescription" cols="40" rows="5" id="ImageDescription"></textarea>
                                            </td>
                                        </tr>
                                    </table>
                                    <input type="hidden" name="id" id="id" value=""  />

                                </form>
                            </div>
                        </td>
                    </tr>
                    <?php } else { ?>
                    <tr>
                        <td class="sqrtab">
                            Available Galleries
                        </td>
                    </tr>
                    <tr>
                        <td class="newsHeader">
                            <a href="?add"><img src="images/icons/image_add.png" alt="add'" width="16" height="16" border="0" align="absmiddle" /> Create New Gallery </a>
                            <a id="delete-selected-link" href="#"><img src="images/icons/image_delete.png" alt="del" width="16" height="16" border="0" align="absmiddle" /> Delete Selected </a>
                        </td>
                    </tr>
                    <tr>
                        <td class="newsContent">
                            <table width="100%" border="0" cellpadding="4" cellspacing="0" class="cms-data-table">
                                <tr>
                                    <th align="center" scope="col">
                                        <input type="checkbox" name="item" id="checker" value="" />
                                    </th>
                                    <th align="left" scope="col">Gallery Name</th>
                                    <th width="27%" scope="col">Options</th>
                                </tr>
                                <?php
								
								for($i = 0; $i < count($galleries); $i++) {
									$gallery = $galleries[$i]; ?>
                                    <tr id="<?php echo 'gal-row-' . $gallery->getId(); ?>">
                                        <td width="5%" align="center">
                                            <input type="checkbox" name="item" id="item" value="<?php echo $gallery->getId(); ?>" />
                                        </td>
                                        <td width="68%">
                                            <a href='?edit&id=<?php echo $gallery->getId(); ?>'><?php echo ($i + 1) . ". " . $gallery->getName(); ?></a>
                                        </td>
                                        <td align="center">
                                            <a href="?edit&amp;id=<?php echo $gallery->getId(); ?>"><img src="images/icons/image_add.png" alt="add" width="16" height="16" border="0" align="absmiddle" /> Edit</a>
                                            
                                            <a class="delete-link" href="#" data-id='<?php echo $gallery->getId(); ?>' data-galname='<?php echo $gallery->getName(); ?>' >
                                            	<img src="images/icons/image_delete.png" alt="delete" width="16" height="16" border="0" align="absmiddle" /> Delete
                                            </a>
                                            
                                            <?php 
												$id = $gallery->getId();
												$pub_link_title = $gallery->isPublished() ? 'Published (Click To Unpublish)' : 'Unpublised (Click To Publish Now)';
												$pub_link_class = $gallery->isPublished() ? 'cms-publish-link' : 'cms-unpublish-link';
												echo "<a id='$id' class='$pub_link_class' href='#' title='$pub_link_title'></a>";
											?>
                                        </td>
                                    </tr><?php
								} ?>
                            </table>
                        </td>
                    </tr>
                    <?php } ?>
                </table>
            </div>
            <!-- InstanceEndEditable -->
        </div>
    </div>
    
    <div id="footer">
    	<?php include('footer.php'); ?>
    </div>
</div>

</body>
<!-- InstanceEnd --></html>