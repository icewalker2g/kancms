<?php
require_once("accesscheck.php");
include_once('../core/controllers/ArticlesManager.php');

$articles = new ArticlesManager();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/cms.dwt.php" codeOutsideHTMLIsLocked="false" -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<!-- InstanceBeginEditable name="doctitle" -->
        <title><?php echo getSetting('CMSTitle', 'KAN Content Management System'); ?>- Articles</title>
        <!-- InstanceEndEditable -->
<link rel="shortcut icon" type="image/png" href="../favicon.png" />
<link rel="stylesheet" type="text/css" href="css/widestyles.css"/>
<link rel="stylesheet" type="text/css" href="css/cms.css"/>
<link rel="stylesheet" type="text/css" href="css/cms-ui.css"/>
<link rel="stylesheet" type="text/css" href="css/util.css"/>

<script type="text/javascript" src="../assets/scripts/jquery/js/jquery.js"></script>
<script type="text/javascript" src="../assets/scripts/jquery/js/jquery-ui.js"></script>
<script type="text/javascript" src="../assets/scripts/common/pageManager.js"></script>
<script type="text/javascript" src="scripts/system.js"></script>
<script type="text/javascript" src="scripts/ui.js"></script>

<!-- InstanceBeginEditable name="head" -->
        
        <!-- General Page Scripts -->
        <style type="text/css">
#cat-add-dialog {
    left: 150px;
    top: 25px;
}
</style>
        <!-- InstanceEndEditable -->
</head>

<body>

<div id="page">
	<div id="header">
    	<?php include('logo_header.php'); ?>
    </div>
    
    <?php if( isset($_SESSION['CMS_Username']) ) { ?>
    <div id="site-info-bar">
		<?php include('system_menu.php'); ?>
    </div>
    <?php } ?>
    
    
    <div id="content-wrapper">
    	<div id="content-header">
        	<span>
			<!-- InstanceBeginEditable name="section title" -->Site Articles<!-- InstanceEndEditable -->
            </span>
        </div>
    	<div id="content">
			<!-- InstanceBeginEditable name="content" -->
                    <div id="nav">
                    <?php include('nav_section.php'); ?>
                </div>
                    <div id="main">
                    <div class="cms-content-header">Articles</div>
                    <div id="content-pane" class="cms-content-pane">
                            <div class="category-pane">
                            <div class="pane-header"> Categories
                                    <div style="width: 66px; position: absolute; right: 10px; top: 2px; text-align:right;"> <a id="add-cat-link" href="#" title="Add Category"><img src="images/icons/add.png" width="16" height="16" alt="add" align="absmiddle" border="0" /></a> <a id="edit-cat-link" href="#" title="Add Category"><img src="images/icons/pencil.png" width="16" height="16" alt="add" align="absmiddle" border="0" /></a> <a id="del-cat-link" href="#" title="Delete Selected Category"><img src="images/icons/delete.png" width="16" height="16" alt="add" align="absmiddle" border="0" /></a> </div>
                                </div>
                            <div class="pane-content">
                                    <?php $articles->getCategoriesAsHTMLList('category-list'); ?>
                                </div>
                        </div>
                            <div class="articles-pane">
                            <div id="art-pane-header" class="pane-header">Articles In</div>
                            <div class="pane-sub-header"> 
                            	<a id="add-arts-link" href="#">
                                	<img src="images/icons/newspaper_add.png" alt="add" width="16" height="16" border="0" align="absmiddle" /> Add New Article
                                </a>
                                <a id="move-link" href="#" >
                                	<img src="images/icons/newspaper_go.png" alt="del" width="16" height="16" border="0" align="absmiddle" /> Move Selected
                                </a>
                                <a id="del-arts-link" href="#">
                                	<img src="images/icons/newspaper_delete.png" alt="del" width="16" height="16" border="0" align="absmiddle" /> Delete Selected
                                </a>
                            </div>
                            <div id="articles-display" class="pane-content"></div>
                            <div class="pane-footer"> Pages: </div>
                        </div>
                            <div id="cat-add-dialog" class="cms-dialog">
                            <div class="cms-dialog-title">Add Article Category</div>
                            <div class="cms-dialog-content">
                                    <?php include('forms/article_category_form.php'); ?>
                                </div>
                        </div>
                        </div>
                    <div id="editor-pane" class="cms-content-pane" style="border:solid #ccc 1px; padding: 0px; border-spacing: 0px; display: none;">
                            <div class="pane-header">Article Editor</div>
                            <div id="pane-content" style="padding: 10px;"> </div>
                        </div>
                </div>
                    <script type="text/javascript" src="scripts/articles.js"></script> 
                    <script type="text/javascript" src="../assets/scripts/jquery/js/jquery.js"></script> 
                    <script type="text/javascript" src="scripts/tiny_mce/tiny_mce.js"></script> 
                    <script type="text/javascript" src="scripts/imgupload/picup.js"></script> 
                    <script type="text/javascript" src="scripts/friendurl/jquery.friendurl.min.js"></script> 
                    <script type="text/javascript">
                        pageManager.addLoadEvent( function() {
                            articles.init();
                        });
                    </script> 
                    <!-- InstanceEndEditable -->
        </div>
    </div>
    
    <div id="footer">
    	<?php include('footer.php'); ?>
    </div>
</div>

</body>
<!-- InstanceEnd --></html>
