<?php 
require_once("accesscheck.php"); 

include_once('../core/controllers/FeedbackManager.php');

$feedback = new FeedbackManager();

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/cms.dwt.php" codeOutsideHTMLIsLocked="false" -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<!-- InstanceBeginEditable name="doctitle" -->
<title>User Feedback Management</title>
<!-- InstanceEndEditable -->
<link rel="shortcut icon" type="image/png" href="../favicon.png" />
<link rel="stylesheet" type="text/css" href="css/widestyles.css"/>
<link rel="stylesheet" type="text/css" href="css/cms.css"/>
<link rel="stylesheet" type="text/css" href="css/cms-ui.css"/>
<link rel="stylesheet" type="text/css" href="css/util.css"/>

<script type="text/javascript" src="../assets/scripts/jquery/js/jquery.js"></script>
<script type="text/javascript" src="../assets/scripts/jquery/js/jquery-ui.js"></script>
<script type="text/javascript" src="../assets/scripts/common/pageManager.js"></script>
<script type="text/javascript" src="scripts/system.js"></script>
<script type="text/javascript" src="scripts/ui.js"></script>

<!-- InstanceBeginEditable name="head" -->
<link rel="stylesheet" type="text/css" href="css/cms-ui.css" />
<link rel="stylesheet" type="text/css" href="css/feedback.css" />
<link rel="stylesheet" type="text/css" href="css/tabs.css" />
<script type="text/javascript" src="scripts/tabs.js"></script>
<script type="text/javascript" src="scripts/ui.js"></script>
<script type="text/javascript" src="scripts/feedback.js"></script>
<!-- InstanceEndEditable -->
</head>

<body>

<div id="page">
	<div id="header">
    	<?php include('logo_header.php'); ?>
    </div>
    
    <?php if( isset($_SESSION['CMS_Username']) ) { ?>
    <div id="site-info-bar">
		<?php include('system_menu.php'); ?>
    </div>
    <?php } ?>
    
    
    <div id="content-wrapper">
    	<div id="content-header">
        	<span>
			<!-- InstanceBeginEditable name="section title" -->User Feedback Management<!-- InstanceEndEditable -->
            </span>
        </div>
    	<div id="content">
			<!-- InstanceBeginEditable name="content" -->
            <div id="nav">
                <?php include('nav_section.php'); ?>
            </div>
            <div id="main">
                <div class="cms-content-pane" style="height: 200px;">
                	
                        
                    <div class="category-pane">
                        <div class="pane-header">
                            Feedback Category
                            <div class="cat-options">
                                <!-- Opera chokes on the layout of these if the empty spaces "&nbsp;" are not added to the links --> 
                                <a id="add-cat-link" href="#" title="Add Category">&nbsp;</a>
                                <a id="edit-cat-link" href="#" title="Edit Selected Category">&nbsp;</a>
                                <a id="del-cat-link" href="#" title="Delete Selected Category">&nbsp;</a>
                            </div>
                        </div>
                        <div class="pane-content">
                            <ul class="category-list">
                                <?php 
							
								$categories = $feedback->getFeedbackCategories( $siteid );
								
								for($i = 0; $i < count($categories); $i++) {
									
									$id = $categories[$i]->getId();
									$categoryName = $categories[$i]->getData('CategoryName');
									$categoryDesc = $categories[$i]->getData('CategoryDesc');
									
									echo "<li><a id='$id' href='#' title='$categoryDesc'>$categoryName</a></li>";
								} ?>
                            </ul>
                        </div>
                    </div>
                    <div class="articles-pane">
                        <div class="pane-header">
                            Messages In
                        </div>
                        <div class="pane-sub-header">
                        	<a id="actions-link" href="#"><img src="images/icons/email.png" alt="delete" width="16" height="16" border="0" align="absmiddle" /> Actions</a>
                            <!--<a id="unread-link" href="#"><img src="images/icons/email.png" alt="delete" width="16" height="16" border="0" align="absmiddle" /> Unread</a>-->
                            <a id="move-link" href="#"><img src="images/icons/email_go.png" alt="delete" width="16" height="16" border="0" align="absmiddle" /> Move</a>
                            <a id="delete-link" href="#"><img src="images/icons/email_delete.png" alt="delete" width="16" height="16" border="0" align="absmiddle" /> Delete</a>
                        </div>
                        <div id="msg_list_header">
                            <table width="100%" border="0" cellpadding="1" cellspacing="0">
                                <tr>
                                    <th width="40" align="center">
                                        <input type="checkbox" name="checker" id="checker"  />
                                    </th>
                                    <th width="170" align="left">Name</th>
                                    <th width="200" align="left">Feedback Subject</th>
                                    <th align="left">Date</th>
                                </tr>
                            </table>
                        </div>
                        <div class="pane-content" id="msg_list_content" style="height: 135px; overflow: auto;">
                            Messages List Here
                        </div>
                    </div>
                </div>
                <div id="msg_pane" class="cms-content-pane" style="border: solid #d5d5d5 1px; border-collapse: collapse; margin: 2px;">
                    <div style="text-align:center; padding: 10px;">
                        Message Will Appear Here
                    </div>
                </div>
                
                <!-- add category dialog -->
                <div id="cat-add-dialog" class="cms-dialog">
                    <div class="cms-dialog-title">
                        Add Category
                    </div>
                    <div class="cms-dialog-content">
                        <form action="" name="cat-add-form" id="cat-add-form" onsubmit="return false;">
                            <table width="100%" border="0" cellspacing="0" cellpadding="6">
                                <tr>
                                    <td align="left" nowrap="nowrap"><strong>Category Name:</strong></td>
                                    <td>
                                        <input name="CategoryName" type="text" id="CategoryName" class="required" size="35" />
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left"><strong>Category Tag:</strong></td>
                                    <td>
                                        <input name="CategoryTag" type="text" id="CategoryTag" size="20" class="required" maxlength="20" />
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" valign="top"><strong>Description:</strong></td>
                                    <td>
                                        <textarea name="CategoryDesc" cols="35" rows="2" id="CategoryDesc"></textarea>
                                    </td>
                                </tr>
                                <tr>
                                    <td nowrap="nowrap"><strong>Forward To Email:</strong></td>
                                    <td>
                                        <select name="FwdToEmail" id="FwdToEmail">
                                            <option value="true">Yes</option>
                                            <option value="false" selected="selected">No</option>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left"><strong>Email Address:</strong></td>
                                    <td align="left">
                                        <input name="FwdEmail" type="text" id="FwdEmail" size="35" />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" align="center">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td colspan="2" align="center">
                                        <input type="button" name="cat-add-btn" id="cat-add-btn" value="Add Category" />
                                        <input type="button" name="cat-cancel-btn" id="cat-cancel-btn" value="Cancel" />
                                    </td>
                                </tr>
                            </table>
                        </form>
                    </div>
                </div>
                <!--  end add category dialog -->
            </div>
            <!-- InstanceEndEditable -->
        </div>
    </div>
    
    <div id="footer">
    	<?php include('footer.php'); ?>
    </div>
</div>

</body>
<!-- InstanceEnd --></html>