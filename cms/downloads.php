<?php
require_once("accesscheck.php");
include_once('utils/file_utils.php');

$manager = new DownloadsManager();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/cms.dwt.php" codeOutsideHTMLIsLocked="false" -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<!-- InstanceBeginEditable name="doctitle" -->
        <title><?php echo getSetting('CMSTitle', 'KAN Content Management System'); ?></title>
        <!-- InstanceEndEditable -->
<link rel="shortcut icon" type="image/png" href="../favicon.png" />
<link rel="stylesheet" type="text/css" href="css/widestyles.css"/>
<link rel="stylesheet" type="text/css" href="css/cms.css"/>
<link rel="stylesheet" type="text/css" href="css/cms-ui.css"/>
<link rel="stylesheet" type="text/css" href="css/util.css"/>

<script type="text/javascript" src="../assets/scripts/jquery/js/jquery.js"></script>
<script type="text/javascript" src="../assets/scripts/jquery/js/jquery-ui.js"></script>
<script type="text/javascript" src="../assets/scripts/common/pageManager.js"></script>
<script type="text/javascript" src="scripts/system.js"></script>
<script type="text/javascript" src="scripts/ui.js"></script>

<!-- InstanceBeginEditable name="head" -->
		<?php echo $fileUtils->printScripts(); ?>
        <!-- InstanceEndEditable -->
</head>

<body>

<div id="page">
	<div id="header">
    	<?php include('logo_header.php'); ?>
    </div>
    
    <?php if( isset($_SESSION['CMS_Username']) ) { ?>
    <div id="site-info-bar">
		<?php include('system_menu.php'); ?>
    </div>
    <?php } ?>
    
    
    <div id="content-wrapper">
    	<div id="content-header">
        	<span>
			<!-- InstanceBeginEditable name="section title" -->CONTENT MANAGEMENT SECTION<!-- InstanceEndEditable -->
            </span>
        </div>
    	<div id="content">
			<!-- InstanceBeginEditable name="content" -->
                    <div id="nav"><?php include('nav_section.php'); ?></div>

                    <div id="main">
                        <?php
                        include('utils/pui_content_pane_renderer.php');
                        $categories = $manager->getDownloadCategories();
                        $categoryData = array();

                        for ($i = 0; $i < count($categories); $i++) {
                            array_push($categoryData, array(
                                "id" => $categories[$i]->getId(),
                                "name" => $categories[$i]->getCategoryName(),
                                "description" => $categories[$i]->getCategoryDescription()
                            ));
                        }

                        $renderer = new PUIContentPaneRenderer("Downloads");
                        $renderer->setItemTitle("Download");
                        $renderer->setCategoryArray($categoryData);
                        $renderer->render();
                        ?>
                    </div>

                    <script type="text/javascript" src="scripts/downloads.js"></script>
                    <script type="text/javascript">
                        $p.addLoadEvent(function(){
                            downloads.init();
                        });
                    </script>
                    <!-- InstanceEndEditable -->
        </div>
    </div>
    
    <div id="footer">
    	<?php include('footer.php'); ?>
    </div>
</div>

</body>
<!-- InstanceEnd --></html>