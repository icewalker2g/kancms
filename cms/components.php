<?php 

require_once("accesscheck.php"); 
include("utils/file_utils.php");

$cm = new ComponentsManager();

$query_rsComponents = "SELECT id, ComponentType, ComponentName, ComponentPath, DateAdded, Component FROM {$cm->Component->getTableName()} ORDER BY ComponentType ASC, Component ASC";
$rsComponents = mysqli_query($config, $query_rsComponents) or die(mysqli_error());
$row_rsComponents = mysqli_fetch_assoc($rsComponents);
$totalRows_rsComponents = mysqli_num_rows($rsComponents);

$colname_rsEditComponent = "-1";
if (isset($_GET['id'])) {
   $colname_rsEditComponent = $_GET['id'];
}

$query_rsEditComponent = sprintf("SELECT * FROM {$cm->Component->getTableName()} WHERE id = %s", GetSQLValueString($colname_rsEditComponent, "int"));
$rsEditComponent = mysqli_query( $config, $query_rsEditComponent) or die(mysqli_error());
$row_rsEditComponent = mysqli_fetch_assoc($rsEditComponent);
$totalRows_rsEditComponent = mysqli_num_rows($rsEditComponent);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/cms.dwt.php" codeOutsideHTMLIsLocked="false" -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<!-- InstanceBeginEditable name="doctitle" -->
<title><?php echo getSetting('CMSTitle','KAN Content Management System'); ?></title>
<!-- InstanceEndEditable -->
<link rel="shortcut icon" type="image/png" href="../favicon.png" />
<link rel="stylesheet" type="text/css" href="css/widestyles.css"/>
<link rel="stylesheet" type="text/css" href="css/cms.css"/>
<link rel="stylesheet" type="text/css" href="css/cms-ui.css"/>
<link rel="stylesheet" type="text/css" href="css/util.css"/>

<script type="text/javascript" src="../assets/scripts/jquery/js/jquery.js"></script>
<script type="text/javascript" src="../assets/scripts/jquery/js/jquery-ui.js"></script>
<script type="text/javascript" src="../assets/scripts/common/pageManager.js"></script>
<script type="text/javascript" src="scripts/system.js"></script>
<script type="text/javascript" src="scripts/ui.js"></script>

<!-- InstanceBeginEditable name="head" -->
<?php
// include the file utilities information
$fileUtils->printScripts();
?>
<script type="text/javascript" src="scripts/tiny_mce/tiny_mce.js"></script>
<script type="text/javascript" src="scripts/components.js"></script>
<script type="text/javascript">				
	pageManager.addLoadEvent( function() {
		components.init() 
	});
</script>
<!-- InstanceEndEditable -->
</head>

<body>

<div id="page">
	<div id="header">
    	<?php include('logo_header.php'); ?>
    </div>
    
    <?php if( isset($_SESSION['CMS_Username']) ) { ?>
    <div id="site-info-bar">
		<?php include('system_menu.php'); ?>
    </div>
    <?php } ?>
    
    
    <div id="content-wrapper">
    	<div id="content-header">
        	<span>
			<!-- InstanceBeginEditable name="section title" -->Custom Component MANAGEMENT<!-- InstanceEndEditable -->
            </span>
        </div>
    	<div id="content">
			<!-- InstanceBeginEditable name="content" -->
            <div id="nav">
                <?php include('nav_section.php'); ?>
            </div>
            <div id="main">
                <table width="100%" border="0" cellpadding="4" cellspacing="0" class="newsTbl">
                    <?php if( isset($_GET['import']) ) { ?>
                    <tr>
                        <td class="sqrtab">
                            Import Component To KAN
                        </td>
                    </tr>
                    <tr>
                        <td class="newsSummary">
                            <!--<form action="" method="post" enctype="multipart/form-data" name="import-form" id="import-form">-->
                            <form action="ajax_pages/components.php" method="post" enctype="multipart/form-data" name="import-form" id="import-form">
                                <p>Use this form to upload and import a component package. The component package must be zip file format only. The contents of this package will be extracted to the &quot;/components/&quot; folder on the server. Please ensure the folder has write permissions before uploading the package.</p>
                                <table width="96%" border="0" align="center" cellpadding="4" cellspacing="0">
                                    <tr>
                                        <td align="right">&nbsp;
                                        </td>
                                        <td class="cms-file-readonly">
                                            Maximum Upload File Size 500KB (ZIP File Format Only)
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="23%" align="right">
                                            Component Package:
                                        </td>
                                        <td width="77%">
                                            <input name="uploadFile" type="file" id="uploadFile" size="35" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>&nbsp;
                                        </td>
                                        <td>&nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" align="center">
                                            <input type="hidden" id="destFolder" name="destFolder" value="../components/"  />
                                            <input type="hidden" id="destFileName" name="destFileName" value=""  />
                                            <input type="hidden" id="ArchivePath" name="ArchivePath" value=""  />
                                            <input type="hidden" id="allowed_ext" name="allowed_ext" value="zip" />
                                            <input type="hidden" id="maxlimit" name="maxlimit" value="5120000" />
                                            <input type="hidden" name="import" id="import" value="import"  />
                                            <input type="submit" name="submit" id="submit" value="Upload and Import" />
                                            <input type="button" name="Cancel" id="Cancel" value="Cancel" onclick="history.back()" />
                                            <input type="hidden" name="action" value="import_component"  />
                                        </td>
                                    </tr>
                                </table>
                            </form>
                        </td>
                    </tr>
                    <?php } ?>
                    <?php if( isset($_GET['create']) ) { ?>
                    <tr>
                        <td class="sqrtab">
                            Create New Component
                        </td>
                    </tr>
                    <tr>
                        <td class="newsSummary">
                            <p class="cms-form-message">Complete the form below to create new component in KAN&nbsp;
                            </p>
                            <!--<form action="<?php echo $editFormAction; ?>" method="post" name="comp-create-form" id="comp-create-form">-->
                            <form action="ajax_pages/components.php" method="post" name="comp-create-form" id="comp-create-form">
                                <table width="96%" align="center" cellpadding="4" cellspacing="0">
                                    <tr valign="baseline">
                                        <td width="20%" align="right" nowrap="nowrap">
                                            <strong>Component Type:</strong>
                                        </td>
                                        <td width="80%">
                                            <select id="ComponentType" name="ComponentType" class="small required">
                                                <option value="">Select Component Type</option>
                                                <option value="page">Full Page Component</option>
                                                <option value="widget">Widget Component</option>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr valign="baseline">
                                        <td nowrap="nowrap" align="right">
                                            <strong>Component ID:</strong>
                                        </td>
                                        <td>
                                            <input type="text" id="Component" name="Component" value="" size="25" class="required" />
                                            <span class="cms-file-readonly">(no spaces allowed)</span>
                                        </td>
                                    </tr>
                                    <tr valign="baseline">
                                        <td nowrap="nowrap" align="right">
                                            <strong>Component Name:</strong>
                                        </td>
                                        <td>
                                            <input type="text" id="ComponentName" name="ComponentName" value="" size="60" class="required" />
                                        </td>
                                    </tr>
                                    <tr valign="baseline">
                                        <td nowrap="nowrap" align="right" valign="top">
                                            <strong>Description:</strong>
                                        </td>
                                        <td>
                                            <textarea id="Description" name="Description" cols="65" rows="10"></textarea>
                                        </td>
                                    </tr>
                                    <tr valign="baseline">
                                        <td nowrap="nowrap" align="right">&nbsp;
                                        </td>
                                        <td>&nbsp;
                                        </td>
                                    </tr>
                                    <tr valign="baseline">
                                        <td nowrap="nowrap" align="right">&nbsp;
                                        </td>
                                        <td>
                                            <span class="small">(Complete the path to the component file)</span>
                                        </td>
                                    </tr>
                                    <tr valign="baseline">
                                        <td nowrap="nowrap" align="right">
                                            <strong>Component Path:</strong>
                                        </td>
                                        <td>
                                            <input type="text" id="ComponentPath" name="ComponentPath" value="../assets/components/&lt;comp-dir&gt;/&lt;comp-file&gt;.php" size="50" />
                                        </td>
                                    </tr>
                                    <tr valign="baseline">
                                        <td nowrap="nowrap" align="right">&nbsp;
                                        </td>
                                        <td>&nbsp;
                                        </td>
                                    </tr>
                                    <tr valign="baseline">
                                        <td align="right" valign="top" nowrap="nowrap">
                                            <strong>Component Code:</strong>
                                        </td>
                                        <td>
                                            <div style="border: solid #ccc 1px;">
                                                <?php $fileUtils->getEditorForContent("<?php \n\t//Your PHP Code Goes Here\n?>"); ?>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr valign="baseline">
                                        <td nowrap="nowrap" align="right">&nbsp;
                                        </td>
                                        <td>&nbsp;
                                        </td>
                                    </tr>
                                    <tr valign="baseline">
                                        <td colspan="2" align="center" nowrap="nowrap">
                                            <input type="submit" class="form_button" value="Create Component" />
                                            &nbsp;
                                            <input name="canc" type="button" class="form_button" id="canc" onclick="window.location.href = '<?php echo $_SERVER['PHP_SELF']; ?>';" value="Cancel" />
                                        </td>
                                    </tr>
                                </table>
                                <input type="hidden" name="DateAdded" value="<?php echo date('Y-m-d'); ?>" />
                                <input type="hidden" name="MM_insert" value="form2" />
                                <input type="hidden" name="action" value="create_component"  />
                            </form>
                        </td>
                    </tr>
                    <?php } ?>
                    <?php 
                                    if( isset($_GET['edit']) ) { ?>
                    <tr>
                        <td class="sqrtab">
                            Edit Selected Component
                        </td>
                    </tr>
                    <tr>
                        <td class="newsSummary">
                            <div class="cms-form-message">Complete the form below to edit the information about this component.&nbsp;
                            </div>
                            <!--<form action="<?php echo $editFormAction; ?>" method="post" name="form1" id="form1">-->
                            <form action="ajax_pages/components.php" method="post" name="form1" id="form1">
                                <table width="96%" align="center" cellpadding="5" cellspacing="0">
                                    <tr valign="baseline">
                                        <td nowrap="nowrap" align="right">
                                            <strong>Component Type:</strong>
                                        </td>
                                        <td>
                                            <select name="ComponentType" class="small" id="ComponentType">
                                                <option value="" <?php if (!(strcmp("", $row_rsEditComponent['ComponentType']))) {echo "selected=\"selected\"";} ?>>Select Component Type</option>
                                                <option value="page" <?php if (!(strcmp("page", $row_rsEditComponent['ComponentType']))) {echo "selected=\"selected\"";} ?>>Full Page Component</option>
                                                <option value="widget" <?php if (!(strcmp("widget", $row_rsEditComponent['ComponentType']))) {echo "selected=\"selected\"";} ?>>Widget Component</option>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr valign="baseline">
                                        <td align="right" valign="top" nowrap="nowrap">
                                            <strong>Component ID:</strong>
                                        </td>
                                        <td>
                                            <input type="text" name="Component" value="<?php echo htmlentities($row_rsEditComponent['Component']); ?>" size="25" />
                                        </td>
                                    </tr>
                                    <tr valign="baseline">
                                        <td align="right" valign="top" nowrap="nowrap">
                                            <strong>Component Name:</strong>
                                        </td>
                                        <td>
                                            <input type="text" name="ComponentName" value="<?php echo htmlentities($row_rsEditComponent['ComponentName']); ?>" size="50" />
                                        </td>
                                    </tr>
                                    <tr valign="baseline">
                                        <td align="right" valign="top" nowrap="nowrap">
                                            <strong>Description:</strong>
                                        </td>
                                        <td>
                                            <textarea id="Description" name="Description" cols="60" rows="8"><?php echo $row_rsEditComponent['Description']; ?></textarea>
                                        </td>
                                    </tr>
                                    <tr valign="baseline">
                                        <td align="right" valign="top" nowrap="nowrap">
                                            <strong>Component Path:</strong>
                                        </td>
                                        <td>
                                            <input name="ComponentPath" type="text" value="<?php echo htmlentities($row_rsEditComponent['ComponentPath']); ?>" size="50" readonly="readonly" />
                                            <?php
                                                         $fileUtils = new FileUtils();
                                                         //$fileUtils->getFileEditInfo( $row_rsEditComponent['ComponentPath'], 'text/php');
                                                      ?>
                                        </td>
                                    </tr>
                                    <tr valign="baseline">
                                        <td colspan="2" align="center" nowrap="nowrap">
                                        </td>
                                    </tr>
                                    <tr valign="baseline">
                                        <td align="right" valign="top" nowrap="nowrap">
                                            <strong>Component Folder:</strong>
                                        </td>
                                        <td align="left" valign="top" nowrap="nowrap">
                                            <?php 
													    $folder = dirname( $row_rsEditComponent['ComponentPath'] ) . '/';
														$fileUtils->generateFolderTree($folder); 
													?>
                                        </td>
                                    </tr>
                                    <tr valign="baseline">
                                        <td colspan="2" align="center" nowrap="nowrap">&nbsp;
                                        </td>
                                    </tr>
                                    <tr valign="baseline">
                                        <td colspan="2" align="center" nowrap="nowrap">
                                            <input type="submit" class="form_button" value="Save Changes" />
                                            &nbsp;
                                            <input name="can" type="button" class="form_button" id="can" onclick="window.location.href = '<?php echo $_SERVER['PHP_SELF']; ?>';" value="Cancel" />
                                        </td>
                                    </tr>
                                </table>
                                <input type="hidden" name="DateAdded" value="<?php echo htmlentities($row_rsEditComponent['DateAdded']); ?>" />
                                <input type="hidden" name="MM_update" value="form1" />
                                <input type="hidden" name="id" value="<?php echo $row_rsEditComponent['id']; ?>" />
                                <input type="hidden" name="action" value="update_component"  />
                            </form>
                        </td>
                    </tr>
                    <?php } ?>
                    <?php if( !isset($_GET['import']) && !isset($_GET['edit']) && !isset($_GET['create']) ) { ?>
                    <tr>
                        <td class="sqrtab">
                            Components Available
                        </td>
                    </tr>
                    <tr>
                        <td class="newsHeader">
                            <a href="?create">
                                <img src="images/icons/page_white_add.png" alt="comp-add" width="16" height="16" border="0" align="absmiddle" /> Create New Component</a>
                            <a href="?import" ><img src="images/icons/page_white_compressed.png" alt="import" width="16" height="16" border="0" align="absmiddle" /> Import Component</a>
                        </td>
                    </tr>
                    <tr>
                        <td class="newsSummary">
                            <table width="100%" border="0" cellpadding="4" cellspacing="0" class="cms-data-table">
                                <tr>
                                    <th colspan="2" align="left">Component Name</th>
                                    <th align="left">Component ID</th>
                                    <th width="29%">Options</th>
                                </tr>
                                <?php 
									$components = $cm->getComponents(0, 30, array('order' => 'ComponentName'));
									$count = 1;
									foreach($components as $component) { ?>
                                <tr id="comp-row-<?php echo $component->getId(); ?>">
                                    <td width="5%" align="right">
                                        <?php echo $count++ . '. '; ?>
                                    </td>
                                    <td width="33%">
                                        <?php echo $component->getData('ComponentName'); ?>
                                    </td>
                                    <td width="33%" align="left" class="small">
                                        <?php echo $component->getData('Component'); ?>
                                    </td>
                                    <td align="center">
                                        <a href="?edit&amp;id=<?php echo $component->getId(); ?>">
                                            <img src="images/icons/page_white_code.png" alt="code" width="16" height="16" border="0" align="absmiddle" /> Edit
                                        </a> 
                                        &nbsp;<!-- onclick="components.exportComponent(<?php echo $component->getId(); ?>);" -->
                                        <a href="ajax_pages/components.php?action=export_component&component_id=<?php echo $component->getId(); ?>" >
                                            <img src="images/icons/page_white_compressed.png" alt="code" width="16" height="16" border="0" align="absmiddle" /> Export
                                        </a>
                                        &nbsp;
                                        <a href="#" class="delete-link" data-id="<?php echo $component->getId(); ?>">
                                            <img src="images/icons/page_white_delete.png" alt="delete" width="16" height="16" border="0" align="absmiddle" /> Delete
                                        </a>
                                    </td>
                                </tr>
                                <?php } ?>
                            </table>
                        </td>
                    </tr>
                    <?php 
					} ?>
                </table>
            </div>
            <!-- InstanceEndEditable -->
        </div>
    </div>
    
    <div id="footer">
    	<?php include('footer.php'); ?>
    </div>
</div>

</body>
<!-- InstanceEnd --></html>
<?php
mysqli_free_result($rsComponents);

mysqli_free_result($rsEditComponent);
?>
