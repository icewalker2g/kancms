<?php

$installFolder = "../install";

if (file_exists($installFolder)) {
    echo "<h3>Please Delete The KAN Installation Folder Before Accessing The CMS</h3>";
    echo "Leaving it posses a potential security risk to your site.";
    exit;
}

if (!isset($_SESSION)) {
    session_start();
}

// authorized user categories
$authorizedUsers = "admin,editor,super,specific";
$donotCheckaccess = "false";

// code will find configuration path
$currentPage = $_SERVER['PHP_SELF'];
$endPos = strpos($currentPage, "/cms/");
$siteRoot = substr($currentPage, 0, $endPos);
$basePath = $_SERVER['DOCUMENT_ROOT'] . $siteRoot;

$restrictGoTo = $siteRoot . "/cms/login.php";

if (!((isset($_SESSION['CMS_Username'])) && (isAuthorized("", $authorizedUsers, $_SESSION['CMS_Username'], $_SESSION['CMS_UserGroup'])))) {
    $qsChar = "?";
    $referrer = $_SERVER['PHP_SELF'];
    if (strpos($restrictGoTo, "?"))
        $qsChar = "&";
    if (isset($QUERY_STRING) && strlen($QUERY_STRING) > 0)
        $referrer .= "?" . $QUERY_STRING;
    $restrictGoTo = $restrictGoTo . $qsChar . "accesscheck=" . urlencode($referrer);
    header("Location: " . $restrictGoTo);
    exit;
}

// include general file utilities 
include_once( $basePath . '/core/kan.inc');

// determine which site we are working with. URL / FORM parameters
// should take presedence over the SESSION variables so that we can
// serve dynamic pages better
$siteid = kan_get_parameter('siteid', -1);
$site_tag = kan_get_parameter('SiteIdentifier', -1);
define("SITE_ID", $siteid);
define("SITE_TAG", $site_tag);
define("IN_CMS", true);

$sites = new SitesManager();

if ( isset($_SESSION['siteid']) ) {    
    $site = $sites->getSite($_SESSION['siteid']);
    
} else if( $siteid != -1 ) {
    $site = $sites->getSite($siteid);
} 

// *** Restrict Access To Page: Grant or deny access to this page
function isAuthorized($strUsers, $strGroups, $UserName, $UserGroup) {
    // For security, start by assuming the visitor is NOT authorized. 
    $isValid = False;

    // When a visitor has logged into this site, the Session variable CMS_Username set equal to their username. 
    // Therefore, we know that a user is NOT logged in if that Session variable is blank. 
    if (!empty($UserName)) {
        // Besides being logged in, you may restrict access to only certain users based on an ID established when they login. 
        // Parse the strings into arrays. 
        $arrUsers = Explode(",", $strUsers);
        $arrGroups = Explode(",", $strGroups);
        if (in_array($UserName, $arrUsers)) {
            $isValid = true;
        }
        // Or, you may restrict access to only certain users based on their username. 
        if (in_array($UserGroup, $arrGroups)) {
            $isValid = true;
        }
        if (($strUsers == "") && false) {
            $isValid = true;
        }
    }
    return $isValid;
}

// provide functionality for escaping and formating SQL Values for SQL Query Strings
if (!function_exists("GetSQLValueString")) {

    function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") {
		global $config;
		
        if (PHP_VERSION < 6) {
            $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
        }

        $theValue = function_exists("mysqli_real_escape_string") ? mysqli_real_escape_string($config, $theValue) : mysqli_escape_string($config, $theValue);

        switch ($theType) {
            case "text":
                $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
                break;
            case "long":
            case "int":
                $theValue = ($theValue != "") ? intval($theValue) : "NULL";
                break;
            case "double":
                $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
                break;
            case "date":
                $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
                break;
            case "defined":
                $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
                break;
        }
        return $theValue;
    }

}

function isGeneralSitePage($scriptName) {
    $generalPages = array('sites.php', 'users.php', 'users_new.php', 'themes.php', 'components.php', 'about_cms.php', 'system_check.php', 'system.php');

    foreach ($generalPages as $key => $value) {
        if (stristr($scriptName, $value)) {
            return true;
        }
    }

    return false;
}

function isAdminUser() {
    return ($_SESSION['CMS_UserGroup'] == "admin" || $_SESSION['CMS_UserGroup'] == "super");
}

?>
