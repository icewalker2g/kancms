<style type="text/css">
    .nav_header {
        font-family: Tahoma, Geneva, sans-serif;
        font-size: 11px;
        font-weight: bold;
        border-bottom: solid #eee 1px;
        padding-top: 20px;
    }

    .nav_group {
        font-family: Tahoma, Geneva, sans-serif;
        margin-bottom: 10px;
        border: solid #eee 1px;
        width: 94%;
        margin-left: auto;
        margin-right: auto;
    }

    .nav_group .header {
        color:#333;
        font-size: 11px;
        font-weight: bold;
        padding: 5px;
        border-bottom: solid #eee 1px;
        cursor: pointer;
    }

    .nav_group .header:hover {
        color: #900;
    }

    .nav_group .content {
        font-family: Tahoma, Geneva, sans-serif;
        font-size: 13px;
        padding: 0px;
        overflow: hidden;
    }

    .nav_group .content a {
        font-family: Tahoma, Geneva, sans-serif;
        font-size: 13px;
        text-decoration:none;
    }

    .nav_group .content ul {
        list-style: none;
        margin: 0px;
        padding: 0px;
    }

    .nav_group .content li {
        list-style: none;
        margin: 0px;
        padding: 5px;
    }

    .nav_group .content li:hover { background-color: #f9f9f9; }

    .head-new {
        color: #900;
    }
</style>

<script type="text/javascript">
    pageManager.addLoadEvent( function() {

        if( !document.querySelectorAll ) {
            return;
        }

        var group_headers = document.querySelectorAll(".nav_group .header");
   
        for(var i = 0; i < group_headers.length; i++) {
            var header = group_headers[i];
            header.contentId = i;
            header.onclick = function() {
                var content = this.parentNode.querySelector(".content");
                $p(content).toggleSlide();
            };
        }
    });
</script>

<?php
$db = new Database();

$isGeneral = isGeneralSitePage($_SERVER['SCRIPT_NAME']);

$selectSQL = sprintf("SELECT m.*, mc.CategoryName, mc.CategoryType FROM managers m 
			  INNER JOIN manager_categories mc ON mc.id = m.CategoryID 
              INNER JOIN user_role_menus rm ON rm.menu_id = mc.id
			  WHERE CategoryType = '" . ($isGeneral ? 'general' : 'site_specific') . "' 
			  AND mc.Activated = 1 AND m.Activated = 1 AND rm.role_id = %s
			  ORDER BY mc.Position ASC, m.id ", $db->sanitizeInput($_SESSION['User_Role'], 'int'));

$db->query($selectSQL, true);

if( $db->getResultCount() == 0 ) {
	echo '
			<div class="nav_group"> 
				<div class="header">Site Management</div>
				<div class="content">
				   <ul>
				   	<li><a href="sites.php">Sites Home</a></li>
				   </ul>
				</div>
			</div>
		';
	
	return;
}

$lastCat = "";
for ($i = 0; $i < $db->getResultCount(); $i++) {
    $row = $db->getRow($i);
    $catName = $row['CategoryName'];

    if ($catName != $lastCat) {
        if ($i > 0) {
            echo '
						</ul>
					</div>
				 </div>
				';
        }

        // cache the new group
        $lastCat = $catName;


        echo '
				<div class="nav_group"> 
					<div class="header">' . $row['CategoryName'] . '</div>
					<div class="content">
					   <ul>
			';
    }

    if ($catName == $lastCat) {
        echo '
				<li><a href="' . $row['ManagerCMSFile'] . '">&raquo; ' . $row['ManagerName'] . '</a></li>
			';
    }

    // end and close
    if ($i == $db->getResultCount() - 1) {
        echo '
					</ul>
				</div>
			 </div>
			';
        break;
    }
}

return;
?>

<table width="100%" border="0" cellspacing="0" cellpadding="5" align="center">


    <tr>
        <td>
            <?php if (isGeneralSitePage($_SERVER['SCRIPT_NAME'])) { ?>
                <div class="nav_group">
                    <div class="header">CMS Management Options</div>
                    <div class="content">
                        <ul>
                            <li><a href="sites.php">&raquo; Sites Homepage</a></li>

                            <?php if (isAdminUser()) { ?>
                                <li><a href="users.php"></a><a href="components.php">&raquo; Manage Components</a></li>
                                <li><a href="themes.php">&raquo; Manage Themes</a></li>
                                <li><a href="users.php">&raquo; Manage Users</a></li>
                            <?php } ?>

                        </ul>
                    </div>
                </div>
            <?php } ?>

            <?php if (!isGeneralSitePage($_SERVER['SCRIPT_NAME'])) { ?>
                <div class="nav_group"> <!--  style="margin-top: 30px;" -->
                    <div class="header">
                        Site Layout and Content (<span class="head-new">Beta</span>)
                    </div>
                    <div class="content">
                        <ul>
                            <li><a href="pages.php">&raquo; Site Pages</a></li>
                            <li><a href="blocks.php">&raquo; Content Blocks</a></li>
                            <li><a href="articles.php">&raquo; Articles</a></li>
                            <li><a href="events.php">&raquo; Events &amp; Notices</a></li>
                            <li><a href="quick_links.php">&raquo; Quick Links</a></li>
                            <li><a href="downloads.php">&raquo; Downloads</a></li>
                        </ul>
                    </div>
                </div>

                <div class="nav_group"> <!--  style="margin-top: 30px;" -->
                    <div class="header">
                        Site Layout and Content (<span class="head-new">Pre-Beta</span>)
                    </div>
                    <div class="content">
                        <ul>
                            <li><a href="menus.php">&raquo; Navigation Menus</a></li>
                            <li><a href="pages_old.php">&raquo; Site Pages</a></li>
                            <li><a href="quick_links.php">&raquo; Quick Links</a></li>
                            <li><a href="news.php">&raquo; News Articles</a></li>
                            <li><a href="events.php">&raquo; Events &amp; Notices</a></li>
                            <li><a href="downloads.php">&raquo; Downloads</a></li>
                        </ul>
                    </div>
                </div>

                <?php include('custom_menu.php'); ?>

                <div class="nav_group">
                    <div class="header">Site UI Management</div>
                    <div class="content">
                        <ul>
                            <li>
                                <a href="banners.php">&raquo; Banners</a>
                            </li>
                            <li>
                                <a href="spotlight.php">&raquo; Spotlight Management</a>
                            </li>
                            <li>
                                <a href="custompages.php" style="display:none;">&raquo; Custom Pages</a>
                            </li>
                        </ul>
                    </div>
                </div>

                <div class="nav_group">
                    <div class="header">Media Management</div>
                    <div class="content">
                        <ul>
                            <li><a href="adverts.php">&raquo; Advertisements</a></li>
                            <li><a href="image_manager.php">&raquo; Manage Images</a></li>
                            <li><a href="media.php">&raquo; Other Media</a></li>
                            <li><a href="galleries.php">&raquo; Photo Gallery</a></li>
                        </ul>
                    </div>
                </div>

                <div class="nav_group">
                    <div class="header">Feedback Management</div>
                    <div class="content">
                        <ul>
                            <li><?php
                $db = new Database();
                $selectSQL = "SELECT count(status) AS Unread FROM feedback fb "
                    . "INNER JOIN feedback_categories fc ON fc.id = fb.CategoryID "
                    . "WHERE fb.status = 'unread' AND fc.SiteID = $siteid ";

                $row_result = $db->query($selectSQL)->getRow();
                $unread = $row_result['Unread'];
                ?>

                                <a href="feedback.php">&raquo; Site Feedback (<?php echo $unread . ' Unread'; ?>)</a>
                            </li>
                            <li><?php
                            $selectSQL = "SELECT count(c.id) as cnt FROM article_comments c "
                                . "INNER JOIN articles a ON a.id = c.ArticleID "
                                . "WHERE a.SiteID = " . SITE_ID . " AND Approved = 0";

                            $row = $db->query($selectSQL, true)->getRow();
                ?>
                                <a href="comments.php">&raquo; Article Comments (<?php echo $row['cnt'] . ' Pending'; ?>)</a>
                            </li>
                        </ul>
                    </div>
                </div>
            <?php } ?>

        </td>
    </tr>

</table>

