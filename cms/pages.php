<?php 
include("accesscheck.php");
include('utils/file_utils.php'); 
include('utils/page_list_renderer.php');

kan_import('PageManager');
$pm = new PageManager();
$edit_page = NULL;

if( isset($_GET['edit']) && isset($_GET['id']) ) {
	$edit_page = $pm->getPage($_GET['id']);
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/cms.dwt.php" codeOutsideHTMLIsLocked="false" -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<!-- InstanceBeginEditable name="doctitle" -->
<title><?php echo getSetting('CMSTitle','KAN Content Management System'); ?></title>
<!-- InstanceEndEditable -->
<link rel="shortcut icon" type="image/png" href="../favicon.png" />
<link rel="stylesheet" type="text/css" href="css/widestyles.css"/>
<link rel="stylesheet" type="text/css" href="css/cms.css"/>
<link rel="stylesheet" type="text/css" href="css/cms-ui.css"/>
<link rel="stylesheet" type="text/css" href="css/util.css"/>

<script type="text/javascript" src="../assets/scripts/jquery/js/jquery.js"></script>
<script type="text/javascript" src="../assets/scripts/jquery/js/jquery-ui.js"></script>
<script type="text/javascript" src="../assets/scripts/common/pageManager.js"></script>
<script type="text/javascript" src="scripts/system.js"></script>
<script type="text/javascript" src="scripts/ui.js"></script>

<!-- InstanceBeginEditable name="head" -->
<link rel="stylesheet" type="text/css" href="../assets/scripts/jquery/css/ui-lightness/jquery-ui.css"/>
<link rel="stylesheet" type="text/css" href="css/pages.css"/>
<!-- InstanceEndEditable -->
</head>

<body>

<div id="page">
	<div id="header">
    	<?php include('logo_header.php'); ?>
    </div>
    
    <?php if( isset($_SESSION['CMS_Username']) ) { ?>
    <div id="site-info-bar">
		<?php include('system_menu.php'); ?>
    </div>
    <?php } ?>
    
    
    <div id="content-wrapper">
    	<div id="content-header">
        	<span>
			<!-- InstanceBeginEditable name="section title" -->CONTENT MANAGEMENT SECTION<!-- InstanceEndEditable -->
            </span>
        </div>
    	<div id="content">
			<!-- InstanceBeginEditable name="content" -->
            <div id="nav">
                <?php include('nav_section.php'); ?>
            </div>
            <div id="main">
                
            	<div class="editor-block">
                    <div class="cms-content-header">Add / Edit Page</div>
                    <div class="cms-content-pane" id="editor-content">
                        <?php include("forms/pages_form.php"); ?>
                    </div>
                </div>
            	
                <table border="0" cellpadding="4" cellspacing="0" align="center" class="newsTbl">
                
                	<?php if( !isset($_GET['add']) && !isset($_GET['edit']) && !isset($_GET['edit_home']) ) { ?>
                    <tr>
                        <td class="sqrtab">Site Pages</td>
                    </tr>
                    <tr>
                        <td class="newsHeader">
                            <a id="add-page-link" href="?add"><img src="images/icons/page_add.png" alt="add" width="16" height="16" border="0" align="absmiddle" /> Add New Page</a>
                            <a id="delete-selected-link" href="#"><img src="images/icons/page_delete.png" alt="del" width="16" height="16" border="0" align="absmiddle" /> Delete Selected</a>
                            
                        </td>
                    </tr>
                    <tr>
                        <td class="newsContent">
                        	<div class="cms-pages">
                                <div class="cms-list-header">
                                    <div class="page-name-col">
                                    	<input type="checkbox" id="checkall" name="item" value="" />
                                        Page Name
                                    </div>
                                    <div class="options-col">Options</div>
                                </div>
                                
                                <div class="home-page-block">
                                	<div class="page-name-col">
                                    	<a class="edit-home-link" href="?edit_home"><img src="images/icons/page_white_code.png" alt="home" width="16" height="16" border="0" align="absmiddle" /> Home Page Text</a>
                                	</div>
                                    <div class="options-col">
                                    	<a class="edit-home-link" href="?edit_home"><img src="images/icons/page_white_code.png" alt="edit" width="16" height="16" border="0" align="absmiddle" /> Edit Block</a>
                                    	&nbsp;
                                    	<img src="images/icons/house_go.png" alt="prev" width="16" height="16" border="0" align="absmiddle" />
                                        <a href="<?php echo $site->getHomePageURL(); ?>" target="_blank">Preview</a>
                                    </div>
                                </div>
                                
                                <div class="pages-block">
                               		<div class="tree-instruction">Click The Folder Icons To Collapse/Expand The List</div>
                                    	<ul id="cms-page-list" class='cms-page-list'>
										<?php 
                                            $pages = $pm->getPages( array("parent" => '') );
                                            listPages($pages);
                                        ?>
                                	</ul>
                                </div>
                                
                                <div class="footer-block">
                                    <div class="page-name-col">
                                        <a class="edit-footer-link" href="?edit_footer"><img src="images/icons/page_white_code.png" alt="home" width="16" height="16" border="0" align="absmiddle" /> Footer Text</a>
                                    </div>
                                    <div class="options-col">
                                        <a class="edit-footer-link" href="?edit_footer"><img src="images/icons/page_white_code.png" alt="edit" width="16" height="16" border="0" align="absmiddle" /> Edit Block</a>
                                    </div>
                                </div>
                            </div>
                            
                        </td>
                    </tr>
                    <?php } ?>
                </table>
            </div>
            
            <script type="text/javascript" src="scripts/pages.js"></script>
            <script type="text/javascript">
                pageManager.addLoadEvent( function() {
                    pages.setSiteIdentifier('<?php echo $_SESSION['SiteIdentifier']; ?>');
                    pages.init();
                });
            </script>
            
            <!-- delay page specific library loading to improve page load performance -->
            <script type="text/javascript" src="scripts/tiny_mce/tiny_mce.js"></script>
            <script type="text/javascript" src="scripts/friendurl/jquery.friendurl.min.js"></script>
            <?php $fileUtils->printScripts(); ?>
            <!-- InstanceEndEditable -->
        </div>
    </div>
    
    <div id="footer">
    	<?php include('footer.php'); ?>
    </div>
</div>

</body>
<!-- InstanceEnd --></html>