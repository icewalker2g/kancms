<?php include("accesscheck.php"); 
kan_import('ArticlesManager');

$manager = new ArticlesManager();
$start = isset($_GET['page']) ? $_GET['page'] - 1 : 0;
$limit = 20;
$comments = array();

if( isset($_GET['article_id']) ) {
	$article = $manager->getArticle($_GET['article_id']);
	$comments = $article->getComments($start * $limit, $limit);
	
} else {
	$comments = $manager->getArticleComments(SITE_ID, $start * $limit, $limit, isset($_GET['filter']) ? $_GET['filter'] : "");
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/cms.dwt.php" codeOutsideHTMLIsLocked="false" -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<!-- InstanceBeginEditable name="doctitle" -->
<title><?php echo getSetting('CMSTitle','KAN Content Management System'); ?></title>
<!-- InstanceEndEditable -->
<link rel="shortcut icon" type="image/png" href="../favicon.png" />
<link rel="stylesheet" type="text/css" href="css/widestyles.css"/>
<link rel="stylesheet" type="text/css" href="css/cms.css"/>
<link rel="stylesheet" type="text/css" href="css/cms-ui.css"/>
<link rel="stylesheet" type="text/css" href="css/util.css"/>

<script type="text/javascript" src="../assets/scripts/jquery/js/jquery.js"></script>
<script type="text/javascript" src="../assets/scripts/jquery/js/jquery-ui.js"></script>
<script type="text/javascript" src="../assets/scripts/common/pageManager.js"></script>
<script type="text/javascript" src="scripts/system.js"></script>
<script type="text/javascript" src="scripts/ui.js"></script>

<!-- InstanceBeginEditable name="head" -->
<link rel="stylesheet" type="text/css" href="css/comments.css"/>
<script type="text/javascript" src="scripts/ui.js"></script>
<script type="text/javascript" src="scripts/comments.js"></script>
<script type="text/javascript">
pageManager.addLoadEvent(function(){
	comments.init();	
});
</script>

<!-- InstanceEndEditable -->
</head>

<body>

<div id="page">
	<div id="header">
    	<?php include('logo_header.php'); ?>
    </div>
    
    <?php if( isset($_SESSION['CMS_Username']) ) { ?>
    <div id="site-info-bar">
		<?php include('system_menu.php'); ?>
    </div>
    <?php } ?>
    
    
    <div id="content-wrapper">
    	<div id="content-header">
        	<span>
			<!-- InstanceBeginEditable name="section title" -->MANAGE USER COMMENTS<!-- InstanceEndEditable -->
            </span>
        </div>
    	<div id="content">
			<!-- InstanceBeginEditable name="content" -->
            <div id="nav">
                <?php include('nav_section.php'); ?>
            </div>
            <div id="main">
                <table width="100%" border="0" cellpadding="4" cellspacing="0" class="newsTbl">
                    <tr>
                        <td class="sqrtab">
                            Article Comments
                        </td>
                    </tr>
                    <tr>
                        <td class="newsHeader">
                            <a id="show-link" href="#"><img src="images/icons/comment.png" alt="status" width="16" height="16" border="0" align="absmiddle" /> Show Comments</a>
                            <a id="selected-link" href="#"><img src="images/icons/comment_edit.png" alt="status" width="16" height="16" border="0" align="absmiddle" /> With Selected</a>
                            <a id="delete-selected-link" href="#"><img src="images/icons/comment_delete.png" alt="delete" width="16" height="16" border="0" align="absmiddle" /> Delete Selected </a>
                            <span id="ajax-progress" style="margin-left: 30px; visibility: hidden;">
                           		<img src="../assets/images/general/loadcircles.gif" width="16" height="16" alt="progress" align="absmiddle" />
                                <span id="progress-text">Processing...</span>
                            </span>
                        </td>
                    </tr>
                    <tr>
                        <td class="newsHeader">
                        <strong>Showing:</strong> <?php
						 
						 if( isset($_GET['article_id']) ) {
							 echo "Comments On <b>" . $comments[0]->getArticleTitle() . "</b>";
						 } else {
							 echo "All Comments";	 
						 }
                        ?>
                        </td>
                    </tr>
                    <tr>
                        <td class="newsContent">
                            <table id="comments-table" width="100%" border="0" cellpadding="6" cellspacing="0" class="cms-data-table">
                                <tr>
                                    <th align="left">
                                        <input type="checkbox" name="checkall" id="checkall" />
                                    </th>
                                    <th align="left">Author</th>
                                    <th width="48%" align="left">Comment</th>
                                    <th colspan="2">In Response To</th>
                                </tr>
                                <?php
					
								for( $i = 0; $i < count($comments); $i++ ) { ?>
                                <tr id="comment-row-<?php echo $comments[$i]->getId(); ?>" class="<?php echo $comments[$i]->isApproved() ? 'approved' : 'pending'; ?>">
                                    <td width="4%" valign="top">
                                        <input type="checkbox" name="item" id="item" class="checkitem" value="<?php echo $comments[$i]->getId(); ?>" />
                                    </td>
                                    <td width="23%" valign="top">
                                        <div class="author-name">
                                            <?php echo $comments[$i]->getLinkedReaderName(); ?>
                                        </div>
                                        <div class="author-address">
                                            <a href='mailto:<?php echo $comments[$i]->getReaderEmail(); ?>'><?php echo $comments[$i]->getReaderEmail(); ?></a>
                                        </div>
                                        <div class="author-website">
                                        </div>
                                    </td>
                                    <td valign="top">
                                        <div class="post-date">
                                            <?php echo $comments[$i]->getPostDate(); ?>
                                        </div>
                                        <div class="comment-text" id="comment-text-<?php echo $comments[$i]->getId(); ?>" data-id="<?php echo $comments[$i]->getId(); ?>"><?php echo $comments[$i]->getMessage(); ?></div>
                                        <div class="options-group">
                                            <a class="status-link" data-id="<?php echo $comments[$i]->getId(); ?>" href="#" data-status="<?php echo $comments[$i]->isApproved() ? "approved" : "unapproved"; ?>"><img src="images/icons/accept.png" alt="approve" width="16" height="16" border="0" align="absmiddle" /> <span class="link-text"><?php echo $comments[$i]->isApproved() ? "Unapprove" : "Approve"; ?></span></a>
                                            <a class="edit-link" data-id="<?php echo $comments[$i]->getId(); ?>" href="#"><img src="images/icons/comment_edit.png" alt="edit" width="16" height="16" border="0" align="absmiddle" /> <span class="edit-text">Edit</span></a>
                                            <a class="delete-link" data-id="<?php echo $comments[$i]->getId(); ?>" href="#"><img src="images/icons/comment_delete.png" alt="delete" width="16" height="16" border="0" align="absmiddle" /> Delete</a>
                                        </div>
                                    </td>
                                    <td valign="top">
                                        <img src="images/icons/comment.png" alt="article" width="16" height="16" border="0" align="absmiddle" />
										<a href="<?php echo $_SERVER['PHP_SELF'] . '?article_id=' . $comments[$i]->getArticleID(); ?>">
											<?php echo $comments[$i]->getArticleTitle() . " (" . $comments[$i]->getArticle()->getCommentCount() . ")"; ?>
                                        </a>
                                        
                                        <a href="<?php echo $comments[$i]->getArticle()->getURL(); ?>" target="_blank" style="margin-left: 10px;">
                                            <img src="images/icons/magnifier.png" alt="article" width="16" height="16" border="0" align="absmiddle" /> View
                                        </a>
                                    </td>
                                </tr>
                                <?php
							 	} ?>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td height="38" class="nav_footer newsContent ">
                            <strong>Pages</strong>
                            <?php 
							$total = 0;
							if( isset($_GET['article_id']) ) {
								$total = $article->getCommentCount(false);
							} else if( isset($_GET['filter']) ) {
								$total = $manager->getTotalCommentCount($_GET['filter']);
							} else {
								$total = $manager->getTotalCommentCount();
							}
							
							for($i = 1; $i <= ceil($total/$limit); $i++) {
								$selected = (isset($_GET['page']) && $_GET['page'] == $i) || $i == 1 ? "selected" : "";
								$url = $_SERVER['PHP_SELF'] . "?page=$i";
								$url .= isset($_GET['article_id']) ? '&article_id=' . $_GET['article_id'] : '';
								$url .= isset($_GET['filter']) ? '&filter=' . $_GET['filter'] : '';
								
								echo "<a class='page-link $selected' href='$url'>" . ($i). "</a>";
							}
							?>
                            <span style="font-style: italic;">Displaying <?php echo (($start * $limit) + 1); ?> to <?php echo min((($start + 1) * $limit),$total); ?> of <?php echo $total; ?></span>
                        </td>
                    </tr>
                </table>
            </div>
            <!-- InstanceEndEditable -->
        </div>
    </div>
    
    <div id="footer">
    	<?php include('footer.php'); ?>
    </div>
</div>

</body>
<!-- InstanceEnd --></html>