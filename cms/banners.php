<?php include("accesscheck.php"); 

$editFormAction = $_SERVER['PHP_SELF'];
if (isset($_SERVER['QUERY_STRING'])) {
  $editFormAction .= "?" . htmlentities($_SERVER['QUERY_STRING']);
}

if ((isset($_POST["MM_update"])) && ($_POST["MM_update"] == "form2")) {
  $updateSQL = sprintf("UPDATE banner_images SET BannerName=%s, BannerDesc=%s, BannerPath=%s, BannerType=%s, Width=%s, Height=%s, BannerURL = %s, Published = %s WHERE id=%s",
					   GetSQLValueString($_POST['BannerName'], "text"),
                       GetSQLValueString($_POST['BannerDesc'], "text"),
                       GetSQLValueString($_POST['BannerPath'], "text"),
                       GetSQLValueString($_POST['BannerType'], "text"),
                       GetSQLValueString($_POST['Width'], "int"),
                       GetSQLValueString($_POST['Height'], "int"),
					   GetSQLValueString($_POST['BannerURL'], "text"),
					   GetSQLValueString( isset($_POST['Published']) ? 1 : 0, "int"),
                       GetSQLValueString($_POST['id'], "int"));

  mysql_select_db($database_config, $config);
  $Result1 = mysql_query($updateSQL, $config) or die(mysql_error());

  $updateGoTo = "banners.php";
  header(sprintf("Location: %s", $updateGoTo));
}

if ((isset($_POST["MM_insert"])) && ($_POST["MM_insert"] == "form1")) {
  include('utils/fileUpload.php');

  $insertSQL = sprintf("INSERT INTO banner_images (SiteID, BannerName, BannerDesc, BannerPath, BannerType, Width, Height, BannerURL, Published) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s)",
                       GetSQLValueString( $siteid, "int"),
					   GetSQLValueString($_POST['BannerName'], "text"),
                       GetSQLValueString($_POST['BannerDesc'], "text"),
                       GetSQLValueString($_POST['BannerPath'], "text"),
                       GetSQLValueString($_POST['BannerType'], "text"),
                       GetSQLValueString($_POST['Width'], "int"),
                       GetSQLValueString($_POST['Height'], "int"),
					   GetSQLValueString($_POST['BannerURL'], "text"),
					   GetSQLValueString( isset($_POST['Published']) ? 1 : 0, "int"));

  mysql_select_db($database_config, $config);
  $Result1 = mysql_query($insertSQL, $config) or die(mysql_error());

  $insertGoTo = "banners.php";
  header(sprintf("Location: %s", $insertGoTo));
}

mysql_select_db($database_config, $config);
$query_rsBanners = "SELECT banner_images.id, banner_images.BannerName, banner_images.BannerDesc, banner_images.BannerPath, banner_images.Width, banner_images.Height, banner_images.BannerType, banner_types.TypeName FROM banner_images , banner_types WHERE banner_types.TypeExtension =  banner_images.BannerType AND banner_images.SiteID = $siteid ";
$rsBanners = mysql_query($query_rsBanners, $config) or die(mysql_error());
$row_rsBanners = mysql_fetch_assoc($rsBanners);
$totalRows_rsBanners = mysql_num_rows($rsBanners);

mysql_select_db($database_config, $config);
$query_rsBannerTypes = "SELECT * FROM banner_types ORDER BY banner_types.TypeName";
$rsBannerTypes = mysql_query($query_rsBannerTypes, $config) or die(mysql_error());
$row_rsBannerTypes = mysql_fetch_assoc($rsBannerTypes);
$totalRows_rsBannerTypes = mysql_num_rows($rsBannerTypes);

$colname_rsBannerEdit = "-1";
if (isset($_GET['bid'])) {
  $colname_rsBannerEdit = $_GET['bid'];
}
mysql_select_db($database_config, $config);
$query_rsBannerEdit = sprintf("SELECT * FROM banner_images WHERE id = %s", GetSQLValueString($colname_rsBannerEdit, "int"));
$rsBannerEdit = mysql_query($query_rsBannerEdit, $config) or die(mysql_error());
$row_rsBannerEdit = mysql_fetch_assoc($rsBannerEdit);
$totalRows_rsBannerEdit = mysql_num_rows($rsBannerEdit);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/cms.dwt.php" codeOutsideHTMLIsLocked="false" -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<!-- InstanceBeginEditable name="doctitle" -->
<title><?php echo getSetting('CMSTitle','KAN Content Management System'); ?></title>
<!-- InstanceEndEditable -->
<link rel="shortcut icon" type="image/png" href="../favicon.png" />
<link rel="stylesheet" type="text/css" href="css/widestyles.css"/>
<link rel="stylesheet" type="text/css" href="css/cms.css"/>
<link rel="stylesheet" type="text/css" href="css/cms-ui.css"/>
<link rel="stylesheet" type="text/css" href="css/util.css"/>

<script type="text/javascript" src="../assets/scripts/jquery/js/jquery.js"></script>
<script type="text/javascript" src="../assets/scripts/jquery/js/jquery-ui.js"></script>
<script type="text/javascript" src="../assets/scripts/common/pageManager.js"></script>
<script type="text/javascript" src="scripts/system.js"></script>
<script type="text/javascript" src="scripts/ui.js"></script>

<!-- InstanceBeginEditable name="head" -->
<script type="text/javascript">
function MM_validateForm() { //v4.0
  if (document.getElementById){
    var i,p,q,nm,test,num,min,max,errors='',args=MM_validateForm.arguments;
    for (i=0; i<(args.length-2); i+=3) { test=args[i+2]; val=document.getElementById(args[i]);
      if (val) { nm=val.name; if ((val=val.value)!="") {
        if (test.indexOf('isEmail')!=-1) { p=val.indexOf('@');
          if (p<1 || p==(val.length-1)) errors+='- '+nm+' must contain an e-mail address.\n';
        } else if (test!='R') { num = parseFloat(val);
          if (isNaN(val)) errors+='- '+nm+' must contain a number.\n';
          if (test.indexOf('inRange') != -1) { p=test.indexOf(':');
            min=test.substring(8,p); max=test.substring(p+1);
            if (num<min || max<num) errors+='- '+nm+' must contain a number between '+min+' and '+max+'.\n';
      } } } else if (test.charAt(0) == 'R') errors += '- '+nm+' is required.\n'; }
    } if (errors) alert('The following error(s) occurred:\n'+errors);
    document.MM_returnValue = (errors == '');
} }
</script>
<!-- InstanceEndEditable -->
</head>

<body>

<div id="page">
	<div id="header">
    	<?php include('logo_header.php'); ?>
    </div>
    
    <?php if( isset($_SESSION['CMS_Username']) ) { ?>
    <div id="site-info-bar">
		<?php include('system_menu.php'); ?>
    </div>
    <?php } ?>
    
    
    <div id="content-wrapper">
    	<div id="content-header">
        	<span>
			<!-- InstanceBeginEditable name="section title" -->Site Banners<!-- InstanceEndEditable -->
            </span>
        </div>
    	<div id="content">
			<!-- InstanceBeginEditable name="content" -->
            <?php if( isset($_GET['edit']) ) { ?>
            <div style="clear:both;">
                <table width="100%" border="0" cellpadding="3" cellspacing="0" class="newsTbl" align="center" style="width:100%;">
                    <tr>
                        <td class="sqrtab">Banner Image Preview</td>
                    </tr>
                    <tr>
                        <td class="newsSummary">
                            <?php include('banner.php'); ?>
                        </td>
                    </tr>
                </table>
            </div>
            <?php } ?>
            
            <div style="clear:both;">
                <div id="nav">
                            <?php include('nav_section.php'); ?>
                </div>
                <div id="main">
                    <table width="98%" border="0" align="center" cellpadding="3" cellspacing="0" class="newsTbl">
                        <?php if( isset($_GET['edit']) ) { ?>
                        <tr>
                            <td class="sqrtab">Edit Banner Information</td>
                        </tr>
                        <tr>
                            <td class="newsSummary">
                                <div class="cms-form-message">
                                    Use this form to edit the information about the selected banner
                                </div>
                                <form action="<?php echo $editFormAction; ?>" method="post" name="form2" id="form2">
                                    <table width="450" align="center" cellpadding="5" cellspacing="0">
                                        <tr >
                                            <td width="128" align="right" nowrap="nowrap" class="sectionTitle1">Banner Name:</td>
                                            <td colspan="3">
                                                <input type="text" name="BannerName" value="<?php echo htmlentities($row_rsBannerEdit['BannerName'], ENT_COMPAT, 'iso-8859-1'); ?>" size="32" />
                                            </td>
                                        </tr>
                                        <tr >
                                            <td align="right" nowrap="nowrap" class="sectionTitle1">Banner Desc:</td>
                                            <td colspan="3">
                                                <input type="text" name="BannerDesc" value="<?php echo htmlentities($row_rsBannerEdit['BannerDesc'], ENT_COMPAT, 'iso-8859-1'); ?>" size="50" />
                                            </td>
                                        </tr>
                                        <tr  style="display:none;">
                                            <td align="right" nowrap="nowrap" class="sectionTitle1">Banner Type:</td>
                                            <td colspan="3">
                                                <select name="BannerType" class="small">
                                                    <?php 
do {  
?>
                                                    <option value="<?php echo $row_rsBannerTypes['TypeExtension']?>" <?php if (!(strcmp($row_rsBannerTypes['TypeExtension'], htmlentities($row_rsBannerEdit['BannerType'], ENT_COMPAT, 'iso-8859-1')))) {echo "SELECTED";} ?>><?php echo $row_rsBannerTypes['TypeName']?></option>
                                                    <?php
} while ($row_rsBannerTypes = mysql_fetch_assoc($rsBannerTypes));
?>
                                                </select>
                                            </td>
                                        </tr>
                                        <tr >
                                            <td align="right" nowrap="nowrap" class="sectionTitle1">URL (Target Page):</td>
                                            <td colspan="3">
                                                <input name="BannerURL" type="text" id="BannerURL" value="<?php echo $row_rsBannerEdit['BannerURL']; ?>" size="50" />
                                            </td>
                                        </tr>
                                        <tr >
                                            <td align="right" nowrap="nowrap" class="sectionTitle1">Width:</td>
                                            <td width="88">
                                                <input type="text" name="Width" value="<?php echo htmlentities($row_rsBannerEdit['Width'], ENT_COMPAT, 'iso-8859-1'); ?>" size="6" />
                                            </td>
                                            <td width="53"><span class="sectionTitle1">Height:</span></td>
                                            <td width="139">
                                                <input name="Height" type="text" id="Height" value="<?php echo htmlentities($row_rsBannerEdit['Height'], ENT_COMPAT, 'iso-8859-1'); ?>" size="6" />
                                            </td>
                                        </tr>
                                        <tr >
                                            <td align="right" nowrap="nowrap" class="sectionTitle1">
                                            Published:
                                            </td>
                                            <td colspan="3">
                                                <input <?php if ($row_rsBannerEdit['Published'] == '1') {echo "checked=\"checked\"";} ?> name="Published" type="checkbox" id="Published" value="1" />
                                            </td>
                                        </tr>
                                        <tr >
                                            <td align="right" nowrap="nowrap" class="sectionTitle1">&nbsp;</td>
                                            <td colspan="3">&nbsp;</td>
                                        </tr>
                                        <tr >
                                            <td colspan="4" align="center" nowrap="nowrap">
                                                <input type="submit" class="sectionTitle1" value="Save Changes" />
                                                <input name="button2" type="button" class="sectionTitle1" id="button2" onclick="cancelFormSubmit('Cancel Update Of Banner Information?', 'banners.php') " value="Cancel Update" />
                                            </td>
                                        </tr>
                                    </table>
                                    <input type="hidden" name="BannerPath" value="<?php echo htmlentities($row_rsBannerEdit['BannerPath'], ENT_COMPAT, 'iso-8859-1'); ?>" />
                                    <input type="hidden" name="MM_update" value="form2" />
                                    <input type="hidden" name="id" value="<?php echo $row_rsBannerEdit['id']; ?>" />
                                </form>
                            </td>
                        </tr>
                        <?php } ?>
                        <?php if( isset($_GET['upload']) ) { ?>
                        <tr>
                            <td class="sqrtab">Upload Banner IMage</td>
                        </tr>
                        <tr>
                            <td class="newsSummary">
                                <div class="cms-form-message">
                                    Fill the fields below and hit Upload Banner to add a new banner to the site
                                    <ul>
                                        <li>
                                            <strong>Note:</strong> if the <strong>Banner Dimensions</strong> dont match the default Width and Height, please specify the new values to prevent image distortion&nbsp;
                                        </li>
                                    </ul>
                                </div>
                                <form action="<?php echo $editFormAction; ?>" method="post" enctype="multipart/form-data" name="form1" id="form1" onsubmit="MM_validateForm('Height','','RisNum','BannerName','','R','Width','','RisNum');return document.MM_returnValue">
                                    <table align="center" cellpadding="5" cellspacing="0">
                                        <tr >
                                            <td align="right" nowrap="nowrap" class="sectionTitle1">Banner Name:</td>
                                            <td colspan="3">
                                                <input name="BannerName" type="text" class="sectionText1" id="BannerName" value="" size="32" />
                                            </td>
                                        </tr>
                                        <tr >
                                            <td align="right" nowrap="nowrap" class="sectionTitle1">Description:</td>
                                            <td colspan="3">
                                                <input name="BannerDesc" type="text" class="sectionText1" value="" size="50" />
                                            </td>
                                        </tr>
                                        <tr >
                                            <td align="right" nowrap="nowrap" class="sectionTitle1">Banner Type:</td>
                                            <td colspan="3">
                                                <select name="BannerType" id="BannerType" class="sectionText1">
                                                    <?php do {  ?>
                                                    <option value="<?php echo $row_rsBannerTypes['TypeExtension']?>" ><?php echo $row_rsBannerTypes['TypeName']?></option>
                                                    <?php } while ($row_rsBannerTypes = mysql_fetch_assoc($rsBannerTypes)); ?>
                                                </select>
                                            </td>
                                        </tr>
                                        <tr >
                                            <td align="right" nowrap="nowrap" class="sectionTitle1">URL (Target Page): </td>
                                            <td colspan="3">
                                                <input name="BannerURL" type="text" id="BannerURL" size="50" />
                                            </td>
                                        </tr>
                                        <tr >
                                            <td align="right" nowrap="nowrap" class="sectionTitle1">&nbsp;</td>
                                            <td colspan="3">&nbsp;</td>
                                        </tr>
                                        <tr >
                                            <td align="right" nowrap="nowrap" class="sectionTitle1">&nbsp;</td>
                                            <td colspan="3" class="small sectionTitle1">(Max File Upload Size: 1MB)</td>
                                        </tr>
                                        <tr >
                                            <td align="right" nowrap="nowrap" class="sectionTitle1">File To Upload:</td>
                                            <td colspan="3">
                                                <input name="uploadFile" type="file" class="course_insidetext" id="uploadFile" size="45" onchange="setDestinationFileName(this)" />
                                            </td>
                                        </tr>
                                        <tr >
                                            <td align="right" nowrap="nowrap" class="sectionTitle1">&nbsp;</td>
                                            <td colspan="3" >
                                                <?php $randNum = rand(1,100); ?>
                                                <input name="destFileName" type="hidden" id="destFileName" value="<?php echo "banner" . $totalRows_rsBanners . $randNum; ?>" />
                                                <input name="destFolder" type="hidden" id="destFolder" value="../assets/images/banners/" />
                                                <input name="allowOverwrite" type="hidden" value="yes" />
                                                <input name="BannerPath" type="hidden" id="BannerPath" value="../assets/images/banners/<?php echo "banner" . $totalRows_rsBanners . $randNum . ".jpg"; ?>" />
                                                <input name="redirect" type="hidden" id="redirect" value="<?php echo $editFormAction; ?>" />
                                                <input name="ImageFile" type="hidden" id="ImageFile" value="<?php echo "banner" . $totalRows_rsBanners . $randNum . ".jpg"; ?>" />
                                                <input name="maxlimit" type="hidden" id="maxlimit" value="300000" />
                                                <input name="allowed_ext" type="hidden" id="allowed_ext" value="jpg,png,swf,jar" />
                                                <script language="JavaScript" type="text/javascript">
															function setDestinationFileName(field) {
																var url = document.getElementById("BannerPath");
																var folder = document.getElementById("destFolder");
																
																if(url != null) {
																	var fileName = field.value;
																	var ext = fileName.substring( fileName.lastIndexOf("."), fileName.length );
																	fileName = folder.value + "banner<?php echo $totalRows_rsBanners . $randNum; ?>" + ext;
																	
																	url.value = fileName;
																	
																	selectBannerType(ext);
																}
															}
															
															function selectBannerType(ext) {
																var typeMenu = document.getElementById("BannerType");
																var menuOptions = typeMenu.options;
																for(var i = 0; i < menuOptions.length; i++) {
																	if( menuOptions[i].value == ext.substring(1) ) {
																		typeMenu.selectedIndex = i;
																		return;
																	}
																}
																
																alert("The Selected File Is Not A Supported Format"); 
															}
															</script>
                                            </td>
                                        </tr>
                                        <tr >
                                            <td align="right" nowrap="nowrap" class="sectionTitle1">Width:</td>
                                            <td width="98">
                                                <input name="Width" type="text" class="small" id="Width" value="950" size="6" />
                                            </td>
                                            <td width="44" class="sectionTitle1">Height:</td>
                                            <td width="189">
                                                <input name="Height" type="text" class="small" id="Height" value="200" size="6" />
                                            </td>
                                        </tr>
                                        <tr >
                                            <td align="right" nowrap="nowrap" class="sectionTitle1">
                                            Published:
                                            </td>
                                            <td colspan="3">
                                                <input name="Published" type="checkbox" id="Published" value="1" />
                                            </td>
                                        </tr>
                                        <tr >
                                            <td nowrap="nowrap" align="right">&nbsp;</td>
                                            <td colspan="3">&nbsp;</td>
                                        </tr>
                                        <tr >
                                            <td nowrap="nowrap" align="right">&nbsp;</td>
                                            <td colspan="3">
                                                <input type="submit" value="Upload Banner" />
                                                <input type="button" name="button" id="button" value="Cancel Upload" onclick="cancelFormSubmit('Cancel Banner Upload Process?', 'banners.php')" />
                                            </td>
                                        </tr>
                                    </table>
                                    <input type="hidden" name="MM_insert" value="form1" />
                                </form>
                            </td>
                        </tr>
                        <?php } ?>
                        <?php if( !isset($_GET['upload']) && !isset($_GET['edit']) ) { ?>
                        <tr>
                            <td class="sqrtab">Site Banners</td>
                        </tr>
                        <tr>
                            <td class="newsHeader"><a href="banners.php?upload"><img src="images/icons/image_add.png" width="16" height="16" border="0" align="absmiddle" /> Upload New</a> | <a href="#" onclick="deletePagesIn('banform','banners.php','banner_images', 'deleteFiles'); return false;"><img src="images/icons/image_delete.png" alt="del" width="16" height="16" border="0" align="absmiddle" /> Delete Selected</a></td>
                        </tr>
                        <tr>
                            <td class="newsSummary">
                                <form id="banform" name="banform" method="post" action="" style="margin:0px;">
                                    <table width="100%" border="0" cellpadding="3" cellspacing="0" class="cms-data-table">
                                        <tr>
                                            <th colspan="2" align="left">Images / Media</th>
                                            <th width="25%">Options</th>
                                        </tr>
                                        <?php if( $totalRows_rsBanners > 0 ) { ?>
                                        <?php $lastExt = ""; $count = 0; ?>
                                        <?php do { ?>
                                        <?php if( $lastExt != $row_rsBanners['TypeName'] ) { ?>
                                        <tr>
                                            <td colspan="3" align="left" class="row_header"><?php echo $row_rsBanners['TypeName']; ?></td>
                                        </tr>
                                        <?php $lastExt = $row_rsBanners['TypeName']; ?>
                                        <?php } ?>
                                        <tr>
                                            <td width="5%" align="center">
                                                <input name="<?php echo 'item' . $count; ?>" type="checkbox" id="<?php echo 'item' . $count; ?>" value="<?php echo $row_rsBanners['id']; ?>" />
                                                <input name="<?php echo 'ftd' . $count; ?>" type="hidden" id="<?php echo 'ftd' . $count; ?>" value="<?php echo $row_rsBanners['BannerPath']; ?>" />
                                            </td>
                                            <td width="70%"><a href="?edit&amp;bid=<?php echo $row_rsBanners['id']; ?>"><?php echo $row_rsBanners['BannerName']; ?></a></td>
                                            <td align="center"><a href="?edit&amp;bid=<?php echo $row_rsBanners['id']; ?>"><img src="images/icons/image_edit.png" alt="edit" width="16" height="16" border="0" align="absmiddle" /> Edit</a>&nbsp;&nbsp; <a href="utils/del.php?del=banner_images&amp;id=<?php echo $row_rsBanners['id']; ?>&amp;ref=<?php echo $_SERVER['PHP_SELF']; ?>&amp;ea=deleteFile&amp;fpath=<?php echo $row_rsBanners['BannerPath']; ?>" onclick="return confirm('Delete Selected Banner From Site?') "><img src="images/icons/image_delete.png" alt="delete" width="16" height="16" border="0" align="absmiddle" /> Delete</a></td>
                                        </tr>
                                        <?php $count++; ?>
                                        <?php } while ($row_rsBanners = mysql_fetch_assoc($rsBanners)); ?>
                                        <input type="hidden" name="items" id="items" value="<?php echo $count; ?>" />
                                        <?php } ?>
                                        <?php if( $totalRows_rsBanners == 0 ) { ?>
                                        <tr>
                                            <td colspan="2">No Banner Images Available</td>
                                            <td>&nbsp;</td>
                                        </tr>
                                        <?php } // end if( $totalRows_rsBanners ... ) ?>
                                    </table>
                                </form>
                            </td>
                        </tr>
                        <?php } // end if( !isset($_GET['upload'] ...) ) ?>
                    </table>
                </div>
            </div>
            <!-- InstanceEndEditable -->
        </div>
    </div>
    
    <div id="footer">
    	<?php include('footer.php'); ?>
    </div>
</div>

</body>
<!-- InstanceEnd --></html>
<?php
mysql_free_result($rsBanners);

mysql_free_result($rsBannerTypes);

mysql_free_result($rsBannerEdit);
?>