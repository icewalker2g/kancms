<?php 

include("accesscheck.php");
$currentPage = $_SERVER["PHP_SELF"];

$sm = new SpotlightManager();

$editFormAction = $_SERVER['PHP_SELF'];
if (isset($_SERVER['QUERY_STRING'])) {
  $editFormAction .= "?" . htmlentities($_SERVER['QUERY_STRING']);
}

if( isset($_GET['delete']) && !isset($_GET['id']) ) {
	$checkCount = $_POST['items'];
	
	for($i = 0; $i < $checkCount; $i++) {
		if( isset($_POST['item'.$i]) && ($_POST['item'.$i] != '-1') ) {
			
			// find the item to be deleted and select it from the database
			$id = $_POST['item'.$i];
			$spotlight = new Spotlight($id);
			$spotlight->delete();
		}
	}
	
	$deleteGoTo = $_SERVER['PHP_SELF'];
	header(sprintf("Location: %s", $deleteGoTo));
}

if ((isset($_POST["MM_insert"])) && ($_POST["MM_insert"] == "form2")) {
  $sm->createSpotlight();

  $insertGoTo = $_SERVER['PHP_SELF'];
  header(sprintf("Location: %s", $insertGoTo));
}

if ((isset($_POST["MM_update"])) && ($_POST["MM_update"] == "form1")) {
  $sm->updateSpotlight();
  
  $updateGoTo = $_SERVER['PHP_SELF'];
  header(sprintf("Location: %s", $updateGoTo));
}

$maxRows_rsSpotlight = 20;
$pageNum_rsSpotlight = 0;
if (isset($_GET['pageNum_rsSpotlight'])) {
  $pageNum_rsSpotlight = $_GET['pageNum_rsSpotlight'];
}
$startRow_rsSpotlight = $pageNum_rsSpotlight * $maxRows_rsSpotlight;

$query_rsSpotlight = "SELECT * FROM {$sm->Spotlight->getTableName()} WHERE SiteID = '$siteid' ORDER BY id DESC";
$query_limit_rsSpotlight = sprintf("%s LIMIT %d, %d", $query_rsSpotlight, $startRow_rsSpotlight, $maxRows_rsSpotlight);
$rsSpotlight = mysqli_query($config,$query_limit_rsSpotlight) or die(mysqli_error());
$row_rsSpotlight = mysqli_fetch_assoc($rsSpotlight);

if (isset($_GET['totalRows_rsSpotlight'])) {
  $totalRows_rsSpotlight = $_GET['totalRows_rsSpotlight'];
} else {
  $all_rsSpotlight = mysqli_query($config,$query_rsSpotlight);
  $totalRows_rsSpotlight = mysqli_num_rows($all_rsSpotlight);
}
$totalPages_rsSpotlight = ceil($totalRows_rsSpotlight/$maxRows_rsSpotlight)-1;

$colname_rsEditSpotlight = "-1";
if (isset($_GET['id'])) {
  $colname_rsEditSpotlight = $_GET['id'];
}

$query_rsEditSpotlight = sprintf("SELECT * FROM {$sm->Spotlight->getTableName()} WHERE id = %s", GetSQLValueString($colname_rsEditSpotlight, "int"));
$rsEditSpotlight = mysqli_query($config,$query_rsEditSpotlight) or die(mysqli_error());
$row_rsEditSpotlight = mysqli_fetch_assoc($rsEditSpotlight);
$totalRows_rsEditSpotlight = mysqli_num_rows($rsEditSpotlight);

$queryString_rsSpotlight = "";
if (!empty($_SERVER['QUERY_STRING'])) {
  $params = explode("&", $_SERVER['QUERY_STRING']);
  $newParams = array();
  foreach ($params as $param) {
    if (stristr($param, "pageNum_rsSpotlight") == false && 
        stristr($param, "totalRows_rsSpotlight") == false) {
      array_push($newParams, $param);
    }
  }
  if (count($newParams) != 0) {
    $queryString_rsSpotlight = "&" . htmlentities(implode("&", $newParams));
  }
}
$queryString_rsSpotlight = sprintf("&totalRows_rsSpotlight=%d%s", $totalRows_rsSpotlight, $queryString_rsSpotlight);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/cms.dwt.php" codeOutsideHTMLIsLocked="false" -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<!-- InstanceBeginEditable name="doctitle" -->
<title><?php echo getSetting('CMSTitle','KAN Content Management System'); ?></title>
<!-- InstanceEndEditable -->
<link rel="shortcut icon" type="image/png" href="../favicon.png" />
<link rel="stylesheet" type="text/css" href="css/widestyles.css"/>
<link rel="stylesheet" type="text/css" href="css/cms.css"/>
<link rel="stylesheet" type="text/css" href="css/cms-ui.css"/>
<link rel="stylesheet" type="text/css" href="css/util.css"/>

<script type="text/javascript" src="../assets/scripts/jquery/js/jquery.js"></script>
<script type="text/javascript" src="../assets/scripts/jquery/js/jquery-ui.js"></script>
<script type="text/javascript" src="../assets/scripts/common/pageManager.js"></script>
<script type="text/javascript" src="scripts/system.js"></script>
<script type="text/javascript" src="scripts/ui.js"></script>

<!-- InstanceBeginEditable name="head" -->
<script type="text/javascript" src="scripts/spotlight.js"></script>
<script type="text/javascript" src="scripts/tiny_mce/tiny_mce.js"></script>

<script type="text/javascript">
	// we register the required form validation
	pageManager.addLoadEvent(function() {
		spotlight.init();
	});
</script>

<style>
.parafix p {
	margin: 0px;	
}
</style>

<!-- InstanceEndEditable -->
</head>

<body>

<div id="page">
	<div id="header">
    	<?php include('logo_header.php'); ?>
    </div>
    
    <?php if( isset($_SESSION['CMS_Username']) ) { ?>
    <div id="site-info-bar">
		<?php include('system_menu.php'); ?>
    </div>
    <?php } ?>
    
    
    <div id="content-wrapper">
    	<div id="content-header">
        	<span>
			<!-- InstanceBeginEditable name="section title" -->Manage Spotlights<!-- InstanceEndEditable -->
            </span>
        </div>
    	<div id="content">
			<!-- InstanceBeginEditable name="content" -->
          <div id="nav"><?php include('nav_section.php'); ?></div>
          <div id="main">
              <table width="98%" border="0" align="center" cellpadding="3" cellspacing="0" class="newsTbl">
                  <?php if( $_SERVER['QUERY_STRING'] == "" ) { ?>
                  <tr>
                      <td class="sqrtab">Spotlights </td>
                  </tr>
                  <tr>
                      <td class="newsHeader">
                          <a href="?add"><img src="images/icons/image_add.png" alt="add" width="16" height="16" border="0" align="absmiddle" /> Add New Spotlight</a> 
                          <a href="#" onclick="if( confirm('Delete Selected Spotlight Information And Associated Content?')) { document.spot_form.submit(); } return false;"><img src="images/icons/image_delete.png" alt="delete" width="16" height="16" border="0" align="absmiddle" /> Delete Selected</a></td>
                  </tr>
                  <tr>
                      <td class="newsSummary">
                          <form action="<?php echo $_SERVER['PHP_SELF'] . '?delete'; ?>" enctype="application/x-www-form-urlencoded" name="spot_form" id="spot_form" method="post">
                              <table width="100%" border="0" cellpadding="4" cellspacing="0" id="spot-list-table" class="cms-data-table">
                                  <tr>
                                      <th align="left">
                                          <input type="checkbox" name="checkAll" id="checkAll" />
                                      </th>
                                      <th align="left">Spotlight Information</th>
                                      <th width="32%">Options</th>
                                  </tr>
                                  <?php
                                  $count = 1;
								  		if( $totalRows_rsSpotlight > 0 ) {
								  		   ?>
                                  <?php do { ?>
                                  <tr id="spot-row-<?php echo $row_rsSpotlight['id']; ?>">
                                      <td width="7%" align="left" valign="top">
                                          <input type="checkbox" name="item<?php echo $count; ?>" id="item<?php echo $count; ?>" value="<?php echo $row_rsSpotlight['id']; ?>" />
                                          <?php echo $count . ". "; ?>
                                      </td>
                                      <td width="61%" class="parafix">
                                          <strong><a href="?edit&amp;id=<?php echo $row_rsSpotlight['id']; ?>" title="Edit Spotlight Information"><?php echo $row_rsSpotlight['menu_header']; ?></a></strong><br />
                                          <?php echo $row_rsSpotlight['menu_subheader']; ?>
                                      </td>
                                      <td align="center"> 
                                          <a href="?edit&amp;id=<?php echo $row_rsSpotlight['id']; ?>"><img src="images/icons/image_edit.png" alt="edit" width="16" height="16" border="0" align="absmiddle" /> Edit&nbsp; </a>
                                          <a href="#" class="delete-link" data-id="<?php echo $row_rsSpotlight['id']; ?>" ><img src="images/icons/image_delete.png" alt="delete" width="16" height="16" border="0" align="absmiddle" /> Delete</a>
                                          <a href="#" class="preview-link" data-id="<?php echo $row_rsSpotlight['id']; ?>"><img src="images/icons/magnifier.png" alt="preview" width="16" height="16" border="0" align="absmiddle" /></a>
                                          <?php 
                                            $page_id = $row_rsSpotlight['id'];
                                            $published = $row_rsSpotlight['approved'];
                                            $publish_class = $published == "true" ? "cms-publish-link" : "cms-unpublish-link";
                                            $publish_title = $published == "true" ? "Published (Click To Unpublish)" : "Unpublished (Click To Publish Now)";

                                            echo "<a id='$page_id' href='#' class='$publish_class' title='$publish_title'></a>";
										
                                          ?>
                                      </td>
                                  </tr>
                                  <?php $count++; } while ($row_rsSpotlight = mysqli_fetch_assoc($rsSpotlight)); ?>
                                  <?php } else { ?>
                                  <tr>
                                      <td colspan="3">No Spotlight Have Been Created. <a href="?add">Click Here To Add a New Spotlight</a></td>
                                  </tr>
                                  <?php } ?>
                              </table>
                              <input type="hidden" name="items" id="items" value="<?php echo $count; ?>" />
                          </form>
                      </td>
                  </tr>
                  <tr>
                      <td class="newsHeader">
                          <div style="overflow:hidden; width:100%;">
                              <div style="float:right;">
                                  <table width="240" border="0" align="right">
                                      <tr>
                                          <td align="center">
                                              <?php if ($pageNum_rsSpotlight > 0) { // Show if not first page ?>
                                              <a href="<?php printf("%s?pageNum_rsSpotlight=%d%s", $currentPage, 0, $queryString_rsSpotlight); ?>">First</a>
                                              <?php } // Show if not first page ?>
                                          </td>
                                          <td align="center">
                                              <?php if ($pageNum_rsSpotlight > 0) { // Show if not first page ?>
                                              <a href="<?php printf("%s?pageNum_rsSpotlight=%d%s", $currentPage, max(0, $pageNum_rsSpotlight - 1), $queryString_rsSpotlight); ?>">Previous</a>
                                              <?php } // Show if not first page ?>
                                          </td>
                                          <td align="center">
                                              <?php if ($pageNum_rsSpotlight < $totalPages_rsSpotlight) { // Show if not last page ?>
                                              <a href="<?php printf("%s?pageNum_rsSpotlight=%d%s", $currentPage, min($totalPages_rsSpotlight, $pageNum_rsSpotlight + 1), $queryString_rsSpotlight); ?>">Next</a>
                                              <?php } // Show if not last page ?>
                                          </td>
                                          <td align="center">
                                              <?php if ($pageNum_rsSpotlight < $totalPages_rsSpotlight) { // Show if not last page ?>
                                              <a href="<?php printf("%s?pageNum_rsSpotlight=%d%s", $currentPage, $totalPages_rsSpotlight, $queryString_rsSpotlight); ?>">Last</a>
                                              <?php } // Show if not last page ?>
                                          </td>
                                      </tr>
                                  </table>
                              </div>
                          </div>
                      </td>
                  </tr>
                  <?php } ?>
                  <?php if( isset($_GET['edit']) ) { ?>
                  <tr>
                      <td class="sqrtab">Edit Spotlight</td>
                  </tr>
                  <tr>
                      <td class="newsSummary">
                          <div class="cms-form-message">
                              Edit the information for the currently selected spotlight
                          </div>
                          <form action="<?php echo $editFormAction; ?>" method="post" name="form1" id="form1">
                              <table width="98%" align="center" cellpadding="5" cellspacing="0">
                                  <tr valign="baseline">
                                      <td colspan="2" align="left" valign="middle" nowrap="nowrap" class="cms-warning" style="text-align: left;">
                                          <input type="checkbox" name="approved" value="true"  <?php if (!(strcmp(htmlentities($row_rsEditSpotlight['approved'], ENT_COMPAT, 'iso-8859-1'),"true"))) {echo "checked=\"checked\"";} ?> />
Approved For Display</td>
                                    </tr>
                                  
                                  <tr valign="baseline">
                                      <td width="168" align="right" nowrap="nowrap" class="sectionTitle1">Title:</td>
                                      <td width="551">
                                          <input type="text" name="menu_header" value="<?php echo htmlentities($row_rsEditSpotlight['menu_header'], ENT_COMPAT, 'iso-8859-1'); ?>" size="40" />
                                      </td>
                                  </tr>
                                  <tr valign="baseline">
                                      <td align="right" nowrap="nowrap" class="sectionTitle1">Subtitle:</td>
                                      <td>
                                          <input type="text" name="menu_subheader" value="<?php echo htmlentities($row_rsEditSpotlight['menu_subheader'], ENT_COMPAT, 'iso-8859-1'); ?>" size="40" />
                                      </td>
                                  </tr>
                                  <tr valign="baseline">
                                      <td align="right" valign="top" nowrap="nowrap" class="sectionTitle1">Description:</td>
                                      <td>
                                          <textarea name="description" id="description" cols="60" rows="5"><?php echo htmlentities($row_rsEditSpotlight['description'], ENT_COMPAT, 'iso-8859-1'); ?></textarea>
                                      </td>
                                  </tr>
                                  <tr valign="baseline">
                                      <td align="right" valign="middle" nowrap="nowrap" class="sectionTitle1">Spotlight URL</td>
                                      <td valign="top">
                                          <p>
                                              <input class="page-linker" name="spotlight_url" type="text" id="spotlight_url" value="<?php echo $row_rsEditSpotlight['spotlight_url']; ?>" size="50" />
                                          </p>
                                      </td>
                                  </tr>
                                  <tr valign="baseline">
                                      <td colspan="2" align="center" nowrap="nowrap">&nbsp;</td>
                                  </tr>
                                  <tr valign="baseline">
                                      <td colspan="2" align="center" nowrap="nowrap">
                                          <input type="submit" value="Save Changes" />
                                          <input type="button" name="Cancel" id="save-cancel" value="Cancel Update" />
                                      </td>
                                  </tr>
                              </table>
                              <input type="hidden" name="spotlight" value="<?php echo htmlentities($row_rsEditSpotlight['spotlight'], ENT_COMPAT, 'iso-8859-1'); ?>" />
                              <input type="hidden" name="date" value="<?php echo htmlentities($row_rsEditSpotlight['date'], ENT_COMPAT, 'iso-8859-1'); ?>" />
                              <input type="hidden" name="MM_update" value="form1" />
                              <input type="hidden" name="id" value="<?php echo $row_rsEditSpotlight['id']; ?>" />
                          </form>
                      </td>
                  </tr>
                  <?php } ?>
                  <?php if( isset($_GET['add']) ) { ?>
                  <tr>
                      <td class="sqrtab">Add Spotlight </td>
                  </tr>
                  <tr>
                      <td class="newsSummary">
                          <div class="cms-form-message">
                              Complete the fields below to add a new Spotlight / Banner to the site&nbsp;
                          </div>
                          <form action="<?php echo $editFormAction; ?>" method="post" enctype="multipart/form-data" name="form2" id="form2">
                              <table width="98%" border="0" align="center" cellpadding="6" cellspacing="0">
                                  <tr valign="baseline">
                                      <td colspan="2" align="left" valign="middle" nowrap="nowrap" class="cms-warning" style="text-align: left;">
                                          <input name="approved" type="checkbox" id="approved" value="" />
                                          Approved For Display
                                          <input name="date2" type="hidden" id="date" value="<?php echo date('Y-m-d'); ?>" />
                                          <input type="hidden" name="ext" id="ext" value=""  />
                                          <input type="hidden" name="allowedExt" id="allowedExt" value="jpg,jpeg,png,swf,jar"  />
                                          <script language="JavaScript" type="text/javascript">
											function setFileExtInfo(field) {
												//var url = document.getElementById("Picture");
												//var folder = document.getElementById("destFolder");
												var extField = document.getElementById("ext");
												var fileName = field.value;
												var ext = fileName.substring( fileName.lastIndexOf(".") + 1, fileName.length );
												var extFound = false;
												var extList = document.getElementById("allowedExt").value;
												var exts = extList.split(",");
	
												for(var i = 0; i < exts.length; i++) {
													if(exts[i].toLowerCase() == ext.toLowerCase()) {
														extFound = true;
														break;
													}
												}
												
												if( !extFound ) {
													alert("Chosen File Is Not In The Right Format. \n\nFile Format Must Be In Any Of These Formats: " + extList.toUpperCase()
														  + "\n\nPlease Cross-Check Your File Selection");
													
													field.value = "";
												} else {
													extField.value = "." + ext;
												}
												
											}
										  </script>                                      </td>
                                    </tr>
                                  <tr valign="baseline">
                                      <td width="20%" align="right" nowrap="nowrap" class="sectionTitle1">Title:</td>
                                      <td width="80%">
                                          <input class="required" type="text" name="menu_header" id="menu_header" value="" size="40" />
                                      </td>
                                  </tr>
                                  <tr valign="baseline">
                                      <td align="right" nowrap="nowrap" class="sectionTitle1">Subtitle:</td>
                                      <td>
                                          <input type="text" name="menu_subheader" id="menu_subheader" class="required" value="" size="40" />
                                      </td>
                                  </tr>
                                  <tr valign="baseline">
                                      <td align="right" valign="top" nowrap="nowrap" class="sectionTitle1">Description:</td>
                                      <td>
                                          <textarea name="description" id="description" cols="60" rows="5" ></textarea>
                                      </td>
                                  </tr>
                                  <tr valign="baseline">
                                      <td align="right" nowrap="nowrap" class="sectionTitle1">Spotlight URL:</td>
                                      <td>
                                          <input class="page-linker" type="text" name="spotlight_url" value="" size="50" /></td>
                                  </tr>
                                  <tr valign="baseline">
                                      <td align="right" valign="middle" nowrap="nowrap" class="sectionTitle1">&nbsp;</td>
                                      <td>
                                          <?php 
                                          $info = $site->theme->option("spotlight_info");
                                          
                                          if( $info ) { ?>
                                            <span class="small">Image to Upload Should Have Dimensions of <strong><?php echo $info['width'] . ' x ' . $info['height']; ?></strong> To Work With The Selected Theme</span><?php
                                          } ?>
                                      </td>
                                  </tr>
                                  <tr valign="baseline">
                                      <td align="right" valign="middle" nowrap="nowrap" class="sectionTitle1">Spotlight Image:</td>
                                      <td>
                                          <input class="required" name="uploadFile" type="file" id="uploadFile" size="40" onchange="setFileExtInfo(this);" />
                                          <span class="small">(Required)</span></td>
                                  </tr>
                                  <tr valign="baseline">
                                      <td nowrap="nowrap" align="right">&nbsp;</td>
                                      <td>&nbsp;</td>
                                  </tr>
                                  <tr valign="baseline">
                                      <td colspan="2" align="center" nowrap="nowrap">
                                          <input type="submit" value="Create Spotlight" />
                                          <input type="button" name="Cancel2" id="add-cancel" value="Cancel"  />
                                      </td>
                                  </tr>
                              </table>
                              <input type="hidden" name="spotlight" value="none" />
                              <input type="hidden" name="MM_insert" value="form2" />
                          </form>
                          <p>&nbsp;</p>
                      </td>
                  </tr>
                  <?php } ?>
              </table>
          </div>
        <!-- InstanceEndEditable -->
        </div>
    </div>
    
    <div id="footer">
    	<?php include('footer.php'); ?>
    </div>
</div>

</body>
<!-- InstanceEnd --></html>
<?php
mysqli_free_result($rsSpotlight);

mysqli_free_result($rsEditSpotlight);
?>
