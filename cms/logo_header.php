<style type="text/css">
    <!--
    .white_light {
        font-family:Verdana, Arial, Helvetica, sans-serif;
        font-size: 10px;
        font-weight: bold;
        color:#FFF;
    }

    .white_light a {
        color: #FFC;
    }

    .white_light a:hover {
        color:#FF6;
    }

    .white_light li {
        color: black;
        text-align: left;
        font-weight: normal;
    }

    .white_light li:hover {
        color: #FFF;
    }
    -->
</style>

<table width="1000" border="0" align="center" cellpadding="0" cellspacing="0">
    <tr>
        <td width="70%" height="60"><a href="sites.php"><img src="<?php echo getSetting('CMSLogo', 'images/kan.png'); ?>" alt="logo" border="0" /></a></td>
        <td width="30%" valign="top">

            <?php if (isset($_SESSION['User'])) { ?>
                <table width="98%" border="0" cellspacing="0" cellpadding="8">
                    <tr>
                        <td height="40" align="right" class="white_light">Welcome <a href="#" data-id="<?php echo $_SESSION['UserID']; ?>" id="user_link"><?php echo $_SESSION['User']; ?></a> | <a href="login.php?logout">Log Out</a></td>
                    </tr>
                    <tr style="display:none;">
                        <td class="white_light">Last Login Date: <?php echo $_SESSION['LastAccess']; ?></td>
                    </tr>
                </table>

                <script type="text/javascript">
                    pageManager.addLoadEvent(function() {
                        ui.menu( pageManager.$("#user_link"), {
                            items: [{
                                id: "menu-1",
                                name: "<img src='images/icons/user.png' alt='' align='absmiddle' /> Edit Your Profile",
                                title: "Edit Your Profile",
                                action: function(mi) {
                                    var user_id = mi.getTrigger().getAttribute("data-id");
                                    var parts = window.location.href.split("/");
                                    var page = parts[ parts.length - 1 ];
                                    window.location.href = "index.php?page=user_profile&id=" + user_id + "&ref=" + page;
                                }
                            },{
                                id: "menu-2",
                                name: "<img src='images/icons/house_go.png' alt='' align='absmiddle' /> Log Out",
                                title: "Log Out Of The System",
                                action: function() {
                                    window.location.href = "login.php?logout";
                                }
                            }]
                        });
                    });
                </script>
            <?php } ?>

        </td>
    </tr>
</table>
