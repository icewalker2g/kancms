<?php 

include("accesscheck.php"); 

$links = new LinksManager();

$editFormAction = $_SERVER['PHP_SELF'];
if (isset($_SERVER['QUERY_STRING'])) {
  $editFormAction .= "?" . htmlentities($_SERVER['QUERY_STRING']);
}

if ((isset($_POST["MM_update"])) && ($_POST["MM_update"] == "form3")) {
  $links->updateLink();
  
  $updateGoTo = $_SERVER['PHP_SELF'];
  header(sprintf("Location: %s", $updateGoTo));
}

if ((isset($_POST["MM_insert"])) && ($_POST["MM_insert"] == "form2")) {
  $links->createLink();
  
  $insertGoTo = $_SERVER['PHP_SELF'];
  header(sprintf("Location: %s", $insertGoTo));
}

if ((isset($_POST["MM_insert"])) && ($_POST["MM_insert"] == "form1")) {
  $links->createCategory();

  $insertGoTo = $_SERVER['PHP_SELF'];
  header(sprintf("Location: %s", $insertGoTo));
}

if ((isset($_POST["MM_update"])) && ($_POST["MM_update"] == "catEditForm")) {
  $links->updateCategory();
  $insertGoTo = $_SERVER['PHP_SELF'];
  header(sprintf("Location: %s", $insertGoTo));
}

if (isset($_GET['id'])) {
    $linkObj = $links->getLink($_GET['id']);
    $row_rsEditLink = $linkObj->getDataArray();
}


?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/cms.dwt.php" codeOutsideHTMLIsLocked="false" -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<!-- InstanceBeginEditable name="doctitle" -->
<title><?php echo getSetting('CMSTitle','KAN Content Management System'); ?></title>
<!-- InstanceEndEditable -->
<link rel="shortcut icon" type="image/png" href="../favicon.png" />
<link rel="stylesheet" type="text/css" href="css/widestyles.css"/>
<link rel="stylesheet" type="text/css" href="css/cms.css"/>
<link rel="stylesheet" type="text/css" href="css/cms-ui.css"/>
<link rel="stylesheet" type="text/css" href="css/util.css"/>

<script type="text/javascript" src="../assets/scripts/jquery/js/jquery.js"></script>
<script type="text/javascript" src="../assets/scripts/jquery/js/jquery-ui.js"></script>
<script type="text/javascript" src="../assets/scripts/common/pageManager.js"></script>
<script type="text/javascript" src="scripts/system.js"></script>
<script type="text/javascript" src="scripts/ui.js"></script>

<!-- InstanceBeginEditable name="head" -->
<link rel="stylesheet" type="text/css" href="css/cms-ui.css"/>
<script type="text/javascript" src="scripts/ui.js"></script>
<script type="text/javascript" src='scripts/quick_links.js'></script>
<script type="text/javascript">
function showCategoryEditWindow(id, catName, catDesc) {
	document.getElementById("EditCategoryName").value = catName;
	document.getElementById("EditCatDesc").value = catDesc;
	document.getElementById("EditCatID").value = id;

	pageManager.show("catEditDiv");
}

pageManager.addLoadEvent( function() {
	quick_links.init();
});
</script>
<!-- InstanceEndEditable -->
</head>

<body>

<div id="page">
	<div id="header">
    	<?php include('logo_header.php'); ?>
    </div>
    
    <?php if( isset($_SESSION['CMS_Username']) ) { ?>
    <div id="site-info-bar">
		<?php include('system_menu.php'); ?>
    </div>
    <?php } ?>
    
    
    <div id="content-wrapper">
    	<div id="content-header">
        	<span>
			<!-- InstanceBeginEditable name="section title" -->Manage Quick Links<!-- InstanceEndEditable -->
            </span>
        </div>
    	<div id="content">
			<!-- InstanceBeginEditable name="content" -->
            <div id="nav">
                <?php include('nav_section.php'); ?>
            </div>
            <div id="main">
                <table border="0" align="center" cellpadding="3" cellspacing="0" class="newsTbl">
                    <?php if( $_SERVER['QUERY_STRING'] == "" ) { ?>
                    <tr>
                        <td class="sqrtab">All Available quick links</td>
                    </tr>
                    <tr>
                        <td class="newsContent">
                            <div class="cms-content-pane">
                                <div class="category-pane">
                                    <div class="pane-header">
                                        Link Categories
                                        <div style="width: 50px; position: absolute; right: 10px; top: 2px; text-align:right; ">
                                            <a id="add-cat-link" href="#" title="Add Category" ><img src="images/icons/add.png" width="16" height="16" alt="add" align="absmiddle" border="0" /></a> <a id="del-cat-link" href="#" title="Delete Selected Category"><img src="images/icons/delete.png" width="16" height="16" alt="add" align="absmiddle" border="0" /></a>
                                        </div>
                                    </div>
                                    <div class="pane-content">
                                        <ul class="category-list">
                                            <?php
                                            $links = new LinksManager();
                                            $cats = $links->getLinkCategories($siteid);
    
                                            for($i = 0; $i < count($cats); $i++) {
                                                $cat_id = $cats[$i]->getCategoryId();
                                                $cat = $cats[$i]->getCategoryName();
                                                $desc = $cats[$i]->getCategoryDescription();
												
                                                echo "<li><a id='$cat_id' href='#' title='$desc' >$cat</a></li>";
                                            }
                                            ?>
                                        </ul>
                                    </div>
                                </div>
                                <div class="articles-pane">
                                    <div class="pane-header">
                                        Links In
                                    </div>
                                    <div class="pane-sub-header">
                                        <a id='add-link' href="?add"><img src="images/icons/newspaper_add.png" alt="add" width="16" height="16" border="0" align="absmiddle" /> New Quick Link</a> <a id="move-link" href="#" ><img src="images/icons/newspaper_go.png" alt="del" width="16" height="16" border="0" align="absmiddle" /> Move Selected</a> <a id="delete-link" href="#" ><img src="images/icons/newspaper_delete.png" alt="del" width="16" height="16" border="0" align="absmiddle" /> Delete Selected</a>
                                    </div>
                                    <div id="news-content" class="pane-content">
                                        Loading Page. Please Wait...
                                    </div>
                                    <div class="pane-footer">
                                        Pages:
                                    </div>
                                </div>
                                <div id="catAddDiv" style="display: none; overflow: hidden; position:absolute; z-index: 30px; width: 334px; left: 140px; top: 20px; background:#ececec; height: 122px; border: solid #aaa 1px; background-color: #FFFFFF;">
                                    <div style="padding:10px;">
                                        <form id="catForm" name="catForm" method="POST" action="<?php echo $editFormAction; ?>" style="margin:0px;">
                                            <table width="98%" border="0" align="right" cellpadding="3" cellspacing="0">
                                                <tr>
                                                    <td nowrap="nowrap"><strong>Category Name</strong></td>
                                                    <td>
                                                        <input name="CategoryName" type="text" id="CategoryName" size="25" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td nowrap="nowrap"><strong>Description:</strong></td>
                                                    <td>
                                                        <input name="CategoryDesc" type="text" id="CategoryDesc" size="25" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>&nbsp;</td>
                                                    <td>
                                                        <input name="add-cat-btn" type="button" class="small" id="add-cat-btn" value="Add Category" />
                                                        <input name="cancel-btn" type="button" class="small" id="cancel-btn" value="Cancel" />
                                                    </td>
                                                </tr>
                                            </table>
                                            <input type="hidden" name="MM_insert2" value="catForm" />
                                            <input type="hidden" name="MM_insert" value="catForm" />
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </td>
                    </tr>
                    <?php } ?>
                    <?php if( isset($_GET['add']) ) { ?>
                    <tr>
                        <td class="sqrtab">Add Quick Link</td>
                    </tr>
                    <tr>
                        <td class="newsSummary">
                            <div class="cms-form-message">
                                Complete the form below to create a quick link
                            </div>
                            <form action="<?php echo $editFormAction; ?>" method="post" name="form2" id="form2">
                                <table width="90%" align="center" cellpadding="6" cellspacing="0">
                                    <tr valign="baseline">
                                        <td width="18%" align="right" nowrap="nowrap"><strong>Link Category:</strong></td>
                                        <td colspan="3">
                                            <select id="LinkCategory" name="LinkCategory"><?php 
                                            $cats = $links->getLinkCategories($siteid);
                                            
                                            for($i = 0; $i < count($cats); $i++) {
                                                $id = $cats[$i]->getCategoryId();
                                                $catName = $cats[$i]->getCategoryName();
                                                
                                                echo "<option value='$id'>$catName</option>";
                                            }
                                            ?>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr valign="baseline">
                                        <td nowrap="nowrap" align="right"><strong>Quick Link Name:</strong></td>
                                        <td colspan="3">
                                            <input type="text" name="LinkName" value="" size="40" />
                                        </td>
                                    </tr>
                                    <tr valign="baseline">
                                        <td nowrap="nowrap" align="right" valign="top"><strong>Description:</strong></td>
                                        <td colspan="3">
                                            <textarea name="LinkDesc" cols="40" rows="3"></textarea>
                                        </td>
                                    </tr>
                                    <tr valign="baseline">
                                        <td nowrap="nowrap" align="right"><strong>Link URL:</strong></td>
                                        <td colspan="3">
                                            <input class="page-linker" type="text" id="LinkURL" name="LinkURL" value="http://" size="45" />
                                        </td>
                                    </tr>
                                    <tr >
                                        <td nowrap="nowrap" align="right"><strong>Target:</strong></td>
                                        <td width="32%">
                                            <select name="LinkTarget" id="LinkTarget">
                                                <option value="_self" >Open In Same Window/Tab</option>
                                                <option value="_blank" >Open In New Window/Tab</option>
                                            </select>
                                        </td>
                                        <td width="10%" align="right" nowrap="nowrap"><strong>Position:</strong></td>
                                        <td width="40%">
                                            <input name="LinkPosition" type="text" id="LinkPosition" value="1" size="4" style="text-align:center;" />
                                        </td>
                                    </tr>
                                    <tr valign="baseline">
                                        <td nowrap="nowrap" align="right"><strong>Published:</strong></td>
                                        <td colspan="3">
                                            <input name="Published" type="checkbox" id="Published" value="true" />
                                        </td>
                                    </tr>
                                    <tr valign="baseline">
                                        <td nowrap="nowrap" align="right">&nbsp;</td>
                                        <td colspan="3">&nbsp;</td>
                                    </tr>
                                    <tr valign="baseline">
                                        <td nowrap="nowrap" align="right">&nbsp;</td>
                                        <td colspan="3">
                                            <input type="submit" value="Create Quick Link" />
                                            <input type="button" name="Cancel2" id="Cancel2" value="Cancel" onclick="window.location.href = 'quick_links.php'" />
                                        </td>
                                    </tr>
                                </table>
                                <input type="hidden" name="MM_insert" value="form2" />
                            </form>
                        </td>
                    </tr>
                    <?php } ?>
                    <?php if( isset($_GET['edit']) ) { ?>
                    <tr>
                        <td class="sqrtab">Edit QUick Link</td>
                    </tr>
                    <tr>
                        <td class="newsSummary">
                            <div class="cms-form-message">
                                Complete the form below to update the quick link information&nbsp;
                            </div>
                            <form action="<?php echo $editFormAction; ?>" method="post" name="form3" id="form3">
                                <table width="90%" align="center" cellpadding="6" cellspacing="0">
                                    <tr valign="baseline">
                                        <td width="21%" align="right" nowrap="nowrap"><strong>Link Category:</strong></td>
                                        <td colspan="3">
                                            <select id="LinkCategory" name="LinkCategory"><?php 
												$cats = $links->getLinkCategories($siteid);
												
												for($i = 0; $i < count($cats); $i++) {
													$id = $cats[$i]->getCategoryId();
													$catName = $cats[$i]->getCategoryName();
													$selected = $row_rsEditLink['LinkCategory'] == $id ? "selected='selected'" : "";
													
													echo "<option value='$id' $selected>$catName</option>";
												}
                                            ?>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr> </tr>
                                    <tr valign="baseline">
                                        <td nowrap="nowrap" align="right"><strong>Quick Link Name:</strong></td>
                                        <td colspan="3">
                                            <input type="text" name="LinkName" value="<?php echo htmlentities($row_rsEditLink['LinkName'], ENT_COMPAT, 'iso-8859-1'); ?>" size="40" />
                                        </td>
                                    </tr>
                                    <tr valign="baseline">
                                        <td nowrap="nowrap" align="right" valign="top"><strong>Description:</strong></td>
                                        <td colspan="3">
                                            <textarea name="LinkDesc" cols="40" rows="3"><?php echo htmlentities($row_rsEditLink['LinkDesc'], ENT_COMPAT, 'iso-8859-1'); ?></textarea>
                                        </td>
                                    </tr>
                                    <tr valign="baseline">
                                        <td nowrap="nowrap" align="right"><strong>Link URL:</strong></td>
                                        <td colspan="3">
                                            <input class="page-linker" type="text" id="LinkURL" name="LinkURL" value="<?php echo htmlentities($row_rsEditLink['LinkURL'], ENT_COMPAT, 'iso-8859-1'); ?>" size="45" />
                                        </td>
                                    </tr>
                                    <tr >
                                        <td nowrap="nowrap" align="right"><strong>Target:</strong></td>
                                        <td width="31%">
                                            <select name="LinkTarget" id="LinkTarget">
                                                <option value="_self" <?php if (!(strcmp('_self', htmlentities($row_rsEditLink['LinkTarget'], ENT_COMPAT, 'iso-8859-1')))) {echo "SELECTED";} ?>>Open In Same Window/Tab</option>
                                                <option value="_blank" <?php if (!(strcmp('_blank', htmlentities($row_rsEditLink['LinkTarget'], ENT_COMPAT, 'iso-8859-1')))) {echo "SELECTED";} ?> >Open In New Window/Tab</option>
                                            </select>
                                        </td>
                                        <td width="11%" align="right" nowrap="nowrap"><strong>Position:</strong></td>
                                        <td width="37%">
                                            <input name="LinkPosition" type="text" id="LinkPosition" value="<?php echo $row_rsEditLink['LinkPosition']; ?>" size="4" style="text-align:center" />
                                        </td>
                                    </tr>
                                    <tr valign="baseline">
                                        <td nowrap="nowrap" align="right"><strong>Published:</strong></td>
                                        <td colspan="3">
                                            <input <?php if (!(strcmp($row_rsEditLink['Published'],'true'))) {echo "checked=\"checked\"";} ?> name="Published" type="checkbox" id="Published" value="true" />
                                        </td>
                                    </tr>
                                    <tr valign="baseline">
                                        <td nowrap="nowrap" align="right">&nbsp;</td>
                                        <td colspan="3">&nbsp;</td>
                                    </tr>
                                    <tr valign="baseline">
                                        <td nowrap="nowrap" align="right">&nbsp;</td>
                                        <td colspan="3">
                                            <input type="submit" value="Save Changes" />
                                            <input type="button" name="Cancel3" id="Cancel3" value="Cancel" onclick="window.location.href = 'quick_links.php'" />
                                        </td>
                                    </tr>
                                </table>
                                <input type="hidden" name="MM_update" value="form3" />
                                <input type="hidden" name="id" value="<?php echo $row_rsEditLink['id']; ?>" />
                            </form>
                        </td>
                    </tr>
                    <?php } ?>
                </table>
            </div>
            <!-- InstanceEndEditable -->
        </div>
    </div>
    
    <div id="footer">
    	<?php include('footer.php'); ?>
    </div>
</div>

</body>
<!-- InstanceEnd --></html>