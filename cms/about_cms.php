<?php include("accesscheck.php"); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/cms.dwt.php" codeOutsideHTMLIsLocked="false" -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<!-- InstanceBeginEditable name="doctitle" -->
<title>About KAN CMS</title>
<!-- InstanceEndEditable -->
<link rel="shortcut icon" type="image/png" href="../favicon.png" />
<link rel="stylesheet" type="text/css" href="css/widestyles.css"/>
<link rel="stylesheet" type="text/css" href="css/cms.css"/>
<link rel="stylesheet" type="text/css" href="css/cms-ui.css"/>
<link rel="stylesheet" type="text/css" href="css/util.css"/>

<script type="text/javascript" src="../assets/scripts/jquery/js/jquery.js"></script>
<script type="text/javascript" src="../assets/scripts/jquery/js/jquery-ui.js"></script>
<script type="text/javascript" src="../assets/scripts/common/pageManager.js"></script>
<script type="text/javascript" src="scripts/system.js"></script>
<script type="text/javascript" src="scripts/ui.js"></script>

<!-- InstanceBeginEditable name="head" --><!-- InstanceEndEditable -->
</head>

<body>

<div id="page">
	<div id="header">
    	<?php include('logo_header.php'); ?>
    </div>
    
    <?php if( isset($_SESSION['CMS_Username']) ) { ?>
    <div id="site-info-bar">
		<?php include('system_menu.php'); ?>
    </div>
    <?php } ?>
    
    
    <div id="content-wrapper">
    	<div id="content-header">
        	<span>
			<!-- InstanceBeginEditable name="section title" -->About KAN CMS<!-- InstanceEndEditable -->
            </span>
        </div>
    	<div id="content">
			<!-- InstanceBeginEditable name="content" -->
          <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td width="25%" valign="top" class="cms_nav"><?php include('nav_section.php'); ?></td>
              <td valign="top">
              	<table width="98%" border="0" align="center" cellpadding="4" cellspacing="0" class="newsTbl">
                 <tr>
                    <td class="sqrtab">about KAN Content management system</td>
                 </tr>
                 <tr>
                    <td class="newsSummary"><p>The KAN Content Management System (KAN CMS) is a product of 
                       <a href="http://www.edusoftghana.com">EduSOFT Ghana Ltd</a>
                       , developed in whole for the purpose of the website content management. This product is currently free and open source but EduSOFT Ghana reserves the right to all the original source code and the right to change the structure and implementation and the license under which this software is offered.</p></td>
                 </tr>
                 <tr>
                    <td class="newsSummary">&nbsp;</td>
                 </tr>
                 <tr>
                    <td class="sqrtab">Libraries used</td>
                 </tr>
                 <tr>
                    <td class="newsSummary"><p>The KAN CMS makes use of other open source libraries, including</p>
                       <ol>
                          <li>
                             <a href="http://tinymce.moxicode.com/">TinyMCE</a>
WYSIWYG HTML Editor, developed by Moxiecode Systems AB. Current version is 3.3.9.2</li>
                          <li>Several other free javascript libraries are also used under the respective free to use licenses offered. These libraries include motionpack.js, table_mod.js, fadeAnything.js. All these libraries can be found under the scripts folder for this CMS.</li>
                       </ol></td>
                 </tr>
                 <tr>
                    <td class="newsSummary">&nbsp;</td>
                 </tr>
                 <tr>
                    <td class="sqrtab">Credits and thanks</td>
                 </tr>
                 <tr>
                    <td class="newsSummary">
                       <p>This CMS was developed by 
                       <a href="http://icewalker2g.wordpress.com">Francis Adu-Gyamfi</a>, lead developer, 
                       <a href="http://www.edusoftghana.com">EduSOFT Ghana</a> 
                       with help from several other team members and software testers, including</p>
                       <ol>
                          <li>
                             <a href="http://www.knust.edu.gh/">Kwame Nkrumah University of Science and Technology</a>
(KNUST) Webteam</li>
                          <li>
                             <a href="http://www.nubsknust.com">National Union of Baptist Students, KNUST</a> Webteam</li>
                          <li>Emmanuel Adinkrah, Software Architect, Footprint Solutions GH</li>
                          </ol>
                       <p>For more information about this CMS and for latest releases, visit 
                          <a href="http://www.kancms.org">http://www.kancms.org/</a>
                          .
                       </p></td>
                 </tr>
              </table></td>
            </tr>
          </table>
        <!-- InstanceEndEditable -->
        </div>
    </div>
    
    <div id="footer">
    	<?php include('footer.php'); ?>
    </div>
</div>

</body>
<!-- InstanceEnd --></html>