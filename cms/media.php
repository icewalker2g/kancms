<?php
require_once("accesscheck.php");

$manager = new MediaManager();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/cms.dwt.php" codeOutsideHTMLIsLocked="false" -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<!-- InstanceBeginEditable name="doctitle" -->
        <title><?php echo getSetting('CMSTitle','KAN Content Management System'); ?></title>
        <!-- InstanceEndEditable -->
<link rel="shortcut icon" type="image/png" href="../favicon.png" />
<link rel="stylesheet" type="text/css" href="css/widestyles.css"/>
<link rel="stylesheet" type="text/css" href="css/cms.css"/>
<link rel="stylesheet" type="text/css" href="css/cms-ui.css"/>
<link rel="stylesheet" type="text/css" href="css/util.css"/>

<script type="text/javascript" src="../assets/scripts/jquery/js/jquery.js"></script>
<script type="text/javascript" src="../assets/scripts/jquery/js/jquery-ui.js"></script>
<script type="text/javascript" src="../assets/scripts/common/pageManager.js"></script>
<script type="text/javascript" src="scripts/system.js"></script>
<script type="text/javascript" src="scripts/ui.js"></script>

<!-- InstanceBeginEditable name="head" -->
        <link href="scripts/jqueryFileTree/jqueryFileTree.css" rel="stylesheet" type="text/css" />
        <link href="css/media.css" rel="stylesheet" type="text/css" />
        <!-- InstanceEndEditable -->
</head>

<body>

<div id="page">
	<div id="header">
    	<?php include('logo_header.php'); ?>
    </div>
    
    <?php if( isset($_SESSION['CMS_Username']) ) { ?>
    <div id="site-info-bar">
		<?php include('system_menu.php'); ?>
    </div>
    <?php } ?>
    
    
    <div id="content-wrapper">
    	<div id="content-header">
        	<span>
			<!-- InstanceBeginEditable name="section title" -->Media Management<!-- InstanceEndEditable -->
            </span>
        </div>
    	<div id="content">
			<!-- InstanceBeginEditable name="content" -->
                    <div id="nav">
                        <?php include('nav_section.php'); ?>
                    </div>
                    <div id="main">
                        <?php
                        include('utils/pui_content_pane_renderer.php');
                        $categories = $manager->getCategories();
                        
                        $categoryData = array();

                        for ($i = 0; $i < count($categories); $i++) {
                            array_push($categoryData, array(
                                "id" => $categories[$i]->getId(),
                                "name" => $categories[$i]->getData('Name'),
                                "description" => $categories[$i]->getData('Description')
                            ));
                        }

                        $renderer = new PUIContentPaneRenderer("Media");
                        $renderer->setItemTitle("Media");
                        $renderer->setCategoryArray($categoryData);
                        $renderer->render();
                        ?>
                    </div>
                    <script type="text/javascript" src="scripts/jqueryFileTree/jqueryFileTree.js"></script>
                    <script type="text/javascript" src="scripts/imgupload/picup.js"></script>
                    <script type="text/javascript" src="scripts/media.js"></script>
                    <script type="text/javascript">
                        pageManager.addLoadEvent( function() {
                            media.init();
                        } );
                    </script>
                    <!-- InstanceEndEditable -->
        </div>
    </div>
    
    <div id="footer">
    	<?php include('footer.php'); ?>
    </div>
</div>

</body>
<!-- InstanceEnd --></html>