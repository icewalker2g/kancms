// JavaScript Document

function checkAll(checkVal) {
	var checks = document.getElementById("items").value;
	
	for(var i = 0; i < checks; i++) {
		if( document.getElementById("item" + i) != null ) {
			document.getElementById("item" + i).checked = checkVal;
		}
	}
}

function deletePagesIn(formId,page,table,ea) {
	if( confirm('Delete Selected Pages From The List?') ) {
		var action = "utils/del.php?delete&tbl=" + table + "&ref=" + page; 
		
		if( ea != null ) {
			action += "&ea=" + ea;
		}
		
		document.getElementById(formId).action = action;
		document.getElementById(formId).submit();
	}
}

function deleteContent(page,table,id,ea) {
	if( confirm('Delete Selected Content From The Database?') ) {
		var url = "utils/del.php?delete&del=" + table + "&ref=" + page + "&id=" + id; 
		
		if( ea != null ) {
			url = url + "&ea=" + ea;
		}
		
		//alert(url);
		location.href = url;
		return true;
	}
	
	return false;
}


function cancelFormSubmit(msg,rdpage) {
	if( msg == null || msg == '' ) {
		location.href = rdpage;	
	}
	
	if( confirm(msg) ) {
		location.href = rdpage;	
	}
}

function movePageTo(formId,page,table,col,value,ea) {
	if( confirm('Move Selected Pages To The Specified Section?') ) {
		var action = "utils/move.php?tbl=" + table + "&col=" + col 
			+ "&value=" + value + "&ref=" + page; 
		
		if( ea != null ) {
			action += "&ea=" + ea;
		}
		
		document.getElementById(formId).action = action;
		document.getElementById(formId).submit();
	}
}