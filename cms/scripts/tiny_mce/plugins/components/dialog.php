<?php 
  $currentPage = $_SERVER['PHP_SELF'];
  $endPos = strpos($currentPage, "/cms/");
  $siteRoot = substr($currentPage, 0, $endPos);
  $basePath = $_SERVER['DOCUMENT_ROOT'] . $siteRoot;
  
  require_once($basePath . "/cms/accesscheck.php");
  
  kan_import('ComponentsManager');
  $cm = new ComponentsManager();
  $components = $cm->getComponents();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>{#components_dlg.title}</title>
	<script type="text/javascript" src="../../tiny_mce_popup.js"></script>
	<script type="text/javascript" src="js/dialog.js"></script>
</head>
<body>

<form onsubmit="ComponentsDialog.insert();return false;" action="#">
	<p>Use this dialog to insert a <strong>Component</strong> to into your HTML Documents</p>
	<table width="100%" border="0" cellspacing="0" cellpadding="4">
	    <tr>
	        <td width="18%" align="right">
	            Component:
            </td>
	        <td width="82%">
	            <select name="components" id="components">
	                <?php
					foreach( $components as $cm ) {
							
						$compName = $cm->getData('ComponentName');
						$compPath = $cm->getData('Component');
						
						echo "<option value='$compPath'>$compName</option>";
					} ?>
                </select>
            </td>
        </tr>
	    <tr>
	        <td align="right">
	            ID:
            </td>
	        <td>
	            <label for="id"></label>
	            <input name="id" type="text" id="id" size="15" />
            </td>
        </tr>
	    <tr>
	        <td align="right">
	            <label for="class_list">{#class_name}:</label>
            </td>
	        <td><select id="class_list" name="class_list" class="mceEditableSelect"><option value=""></option></select>
            </td>
        </tr>
	    <tr>
            <td align="right" class="column1"><label id="stylelabel" for="style">{#components_dlg.style}: </label></td> 
            <td colspan="2"><input name="style" type="text" id="style" value="" size="30" onchange="ImageDialog.changeAppearance();" /></td> 
        </tr>
	    <tr>
            <td class="column1"><label id="widthlabel" for="width">{#components_dlg.dimensions}:</label></td>
            <td class="nowrap">
                <input name="width" type="text" id="width" value="" size="5" maxlength="5" class="size" /> x 
                <input name="height" type="text" id="height" value="" size="5" maxlength="5" class="size" /> px
            </td>
        </tr>
	    <tr>
	        <td class="column1">&nbsp;
            </td>
	        <td class="nowrap">&nbsp;
            </td>
        </tr>
    </table>
	<div>
    </div>
	<div class="mceActionPanel" style="overflow: hidden;">
		<div style="float: left">
			<input type="button" id="insert" name="insert" value="{#insert}" onclick="ComponentsDialog.insert();" />
		</div>

		<div style="float: right">
			<input type="button" id="cancel" name="cancel" value="{#cancel}" onclick="tinyMCEPopup.close();" />
		</div>
	</div>
</form>

</body>
</html>
