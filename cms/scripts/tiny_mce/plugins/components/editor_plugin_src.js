/**
 * editor_plugin_src.js
 *
 * Copyright 2009, Moxiecode Systems AB
 * Released under LGPL License.
 *
 * License: http://tinymce.moxiecode.com/license
 * Contributing: http://tinymce.moxiecode.com/contributing
 */

(function() {
	// Load plugin specific language pack
	tinymce.PluginManager.requireLangPack('components');

	tinymce.create('tinymce.plugins.ComponentsPlugin', {
		 
		oldNode: null,
		
		/**
		 * Initializes the plugin, this will be executed after the plugin has been created.
		 * This call is done before the editor instance has finished it's initialization so use the onInit event
		 * of the editor instance to intercept that event.
		 *
		 * @param {tinymce.Editor} ed Editor instance that the plugin is initialized in.
		 * @param {string} url Absolute URL to where the plugin is located.
		 */
		init : function(ed, url) {
			// Register the command so that it can be invoked by using tinyMCE.activeEditor.execCommand('mceComponents');
			ed.addCommand('mceComponents', function() {
				ed.windowManager.open({
					file : url + '/dialog.php',
					width : 380 + parseInt(ed.getLang('components.delta_width', 0)),
					height : 225 + parseInt(ed.getLang('components.delta_height', 0)),
					inline : 1
				}, {
					plugin_url : url, // Plugin absolute URL
					some_custom_arg : 'custom arg' // Custom argument
				});
			});

			// Register components button
			ed.addButton('components', {
				title : 'components.desc',
				cmd : 'mceComponents',
				image : url + '/img/components.png'
			});

			// Add a node change handler, selects the button in the UI when a component is selected
			ed.onNodeChange.add(function(ed, cm, n) {
				var nodeSelected = n.nodeName == 'DIV' && n.hasAttribute("type");
				cm.setActive('components', nodeSelected );
				
				if( nodeSelected ) {
					tinymce.plugins.ComponentsPlugin.oldNode = n;
					ed.dom.addClass(n,"selected");
					
				} else if( tinymce.plugins.ComponentsPlugin.oldNode != null ) {
					var on = tinymce.plugins.ComponentsPlugin.oldNode; 
					ed.dom.removeClass(on,'selected');	
				}
			});
			
			// Add click handler, which selects the DIV representing the component
			/*ed.onClick.add(function(ed, e) {
				e = e.target;

				if (e.nodeName === 'DIV' && e.hasAttribute("type")  ) {
					ed.selection.select(e);
				}
			});*/
		},

		/**
		 * Creates control instances based in the incomming name. This method is normally not
		 * needed since the addButton method of the tinymce.Editor class is a more easy way of adding buttons
		 * but you sometimes need to create more complex controls like listboxes, split buttons etc then this
		 * method can be used to create those.
		 *
		 * @param {String} n Name of the control to create.
		 * @param {tinymce.ControlManager} cm Control manager to use inorder to create new control.
		 * @return {tinymce.ui.Control} New control instance or null if no control was created.
		 */
		createControl : function(n, cm) {
			return null;
		},

		/**
		 * Returns information about the plugin as a name/value array.
		 * The current keys are longname, author, authorurl, infourl and version.
		 *
		 * @return {Object} Name/value array containing information about the plugin.
		 */
		getInfo : function() {
			return {
				longname : 'Components plugin',
				author : 'Francis Adu-Gyamfi',
				authorurl : 'http://tinymce.moxiecode.com',
				infourl : 'http://wiki.moxiecode.com/index.php/TinyMCE:Plugins/components',
				version : "1.0"
			};
		}
	});

	// Register plugin
	tinymce.PluginManager.add('components', tinymce.plugins.ComponentsPlugin);
})();