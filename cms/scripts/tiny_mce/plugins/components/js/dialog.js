tinyMCEPopup.requireLangPack("components_dlg");

var ComponentsDialog = {
	init : function() {
		
		this.fillClassList('class_list');
		
		var f = document.forms[0], ed = tinyMCEPopup.editor, dom = ed.dom, n = ed.selection.getNode();
		
		if( n.nodeName == "DIV" && n.hasAttribute("type") ) {
			f.components.value = dom.getAttrib(n, 'data-component');
			
			// update the id attribute
			f.id.value = dom.getAttrib(n, 'id');
			
			// remove the selected state class if present
			dom.removeClass(n, 'selected');
			
			// update the class list
			f.class_list.value = dom.getAttrib(n, 'class');

			// get all styles
			var styles = dom.getAttrib(n, 'style');
			// extra dimensions
			var d = styles.substring( styles.indexOf('width') );
			// update the style field
			f.style.value = styles.substring(0, styles.indexOf('width') );
			
			var dvals = d.split(";");
			
			if( dvals[0].indexOf("width") != -1 ) {
				f.width.value = parseInt(dvals[0].split(":")[1]);
				
			} else if( dvals[0].indexOf("height") != -1 ) {
				f.height.value = parseInt(dvals[0].split(":")[1]);
			}
			
			if( dvals[1].indexOf("height") != -1 ) {
				f.height.value = parseInt(dvals[1].split(":")[1]);
				
			}
		}

		// Get the selected contents as text and place it in the input
		//f.someval.value = tinyMCEPopup.editor.selection.getContent({format : 'text'});
		//f.somearg.value = tinyMCEPopup.getWindowArg('some_custom_arg');
	},

	insert : function() {
		var ed = tinyMCEPopup.editor;
		
		// create the component tag
		var form = document.forms[0];
		var components = form.components;
		
		var id = form.id.value;
		var class_name = form.class_list.value;
		var comp_id = components.value;
		var style = form.style.value;
		var name = components.options[components.selectedIndex].text;
		
		if( style.length > 0 && style.substring( style.length - 1 ) != ";" ) {
		 	style += ";";
	    }
		
		if( form.width.value != "" ) {
			style += "width:" + form.width.value + "px;";	
		}
		
		if( form.height.value != '' ) {
			style += "height:" + form.height.value + "px;";	
		}
		
		// attributes for the elements
		var args = {
			'id' : id,
			'class' : class_name,
			'type': 'widget',
			'style': style,
			'title': name,
			'data-component': comp_id
		};
		
		var el = ed.selection.getNode();
		
		if( el && el.nodeName == "DIV" && el.hasAttribute("type") ) {
			ed.dom.setAttribs(el, args);
			ed.dom.setHTML(el, "{{!" + comp_id + "!}}" );
			
		} else {
			var tag = "<div type='widget' data-component='" + comp_id + "' id='" + id + "' title='" + name + "' "
				+ " class='" + class_name + "' style='" + style + "' >{{!" + comp_id + "!}}</div>";
				
			// Insert the contents from the input into the document
			ed.execCommand('mceInsertContent', false, tag);
			ed.undoManager.add();
		}
		
		tinyMCEPopup.close();
	},

	fillClassList : function(id) {
		var dom = tinyMCEPopup.dom, lst = dom.get(id), v, cl;

		if (v = tinyMCEPopup.getParam('theme_advanced_styles')) {
			cl = [];

			tinymce.each(v.split(';'), function(v) {
				var p = v.split('=');

				cl.push({'title' : p[0], 'class' : p[1]});
			});
		} else
			cl = tinyMCEPopup.editor.dom.getClasses();

		if (cl.length > 0) {
			lst.options.length = 0;
			lst.options[lst.options.length] = new Option(tinyMCEPopup.getLang('not_set'), '');

			tinymce.each(cl, function(o) {
				lst.options[lst.options.length] = new Option(o.title || o['class'], o['class']);
			});
		} else
			dom.remove(dom.getParent(id, 'tr'));
	}
};

tinyMCEPopup.onInit.add(ComponentsDialog.init, ComponentsDialog);
