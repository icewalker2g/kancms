// JavaScript Document

var system_ui = {
	
	ajax_page: 'ajax_pages/system.php',
	
	init: function() {
		var $tabs = $("#system-tabs").tabs();
		
		$("#setting-cancel-btn").click(function() {
			window.location.href = "sites.php";	
		});
	}
};

$(document).ready(function() {
	system_ui.init();	
});