// JavaScript Document

function setDestinationFileName(field) {
	var url = document.getElementById("AdvertPath");
	var folder = document.getElementById("destFolder");
	
	if(url != null) {
		var fileName = field.value;
		var ext = fileName.substring( fileName.lastIndexOf("."), fileName.length );
		fileName = folder.value + "advert<?php echo $totalRows_rsAdverts; ?>" + ext;
		
		url.value = fileName;
		
		selectAdvertType(ext);
	}
}

function selectAdvertType(ext) {
	var typeMenu = document.getElementById("AdvertType");
	var menuOptions = typeMenu.options;
	for(var i = 0; i < menuOptions.length; i++) {
		if( menuOptions[i].value == ext.substring(1) ) {
			typeMenu.selectedIndex = i;
			return;
		}
	}
	
	alert("The Selected File Is Not A Supported Format"); 
}

function setTypeCatsEnabled(value) {
	if( value != "swf" ) {
		document.getElementById("AdvertTypeCat").disabled = true;
	} else {
		document.getElementById("AdvertTypeCat").disabled = false;	
	}
}

function populateWidthAndHeight(value) {								
	if( value != null ) {
		var parts = value.split("x");
		var w = parts[0];
		var h = parts[1];
		
		document.getElementById("Width").value = w;
		document.getElementById("Height").value = h;
	} else {
		document.getElementById("Width").value = "950";
		document.getElementById("Height").value = "200";
	}
}