// JavaScript Document

var comments = {
	
	// page to send all ajax calls
	ajax_page: 'ajax_pages/articles.php',
	
	// the last comment text block
	lastCommentText: null,
	
	/**
	 * Initialises and configures the comments object
	 */
	init: function() {
		
		// create the popup menu for the show link
		ui.menu( pageManager.$("#show-link"), {
			items : [{
				id: 'show-1',
				name: 'Show All Comments',
				title: 'Show All Comments',
				action: function() {
					window.location.href = "comments.php";
				} 
			},{
				id: 'show-2',
				name: 'Show Pending Only',
				title: 'Show Pending Comments',
				action: function() {
					window.location.href = "comments.php?filter=pending";
				}	
			},{
				id: 'show-3',
				name: 'Show Approved Only',
				title: 'Show Only Approved Comments',
				action: function() {
					window.location.href = "comments.php?filter=approved";
				}	
			}]
		});	
		
		// create the popup menu for the selected link
		ui.menu( pageManager.$("#selected-link"), {
			items : [{
				id: 'sel-approve',
				name: 'Approve',
				title: 'Approve Selected Comment(s)',
				action: function() {
					comments.approveComments(true);
				} 
			},{
				id: 'sel-unapprove',
				name: 'Unapprove',
				title: 'Unapprove Selected Comment(s)',
				action: function() {
					comments.approveComments(false);
				}	
			}/*,{
				id: 'sel-delete',
				name: 'Delete Comment(s)',
				title: 'Delete Selected Comment(s)',
				action: function() {
					comments.deleteComments();
				}	
			}*/]
		});	
		
		pageManager.addEvent("#checkall", 'click', function() {
			var selected = this.checked;
			
			pageManager.all(".cms-data-table .checkitem", function(el) {
				el.checked = selected;
			});	
		});
		
		pageManager.addEvent('#delete-selected-link', 'click', function() {
			comments.deleteComments();
		});
		
		$p(".status-link").bind('click',function() {
			var status = $p(this).attr('data-status');
			var id = $p(this).attr('data-id');
			comments.approveComments( status == "unapproved", id);
		});
		
		pageManager.all(".edit-link", function(el) {
			pageManager.addEvent(el,'click',function() {
				
				var doClear = function() {
					var id = comments.lastCommentText.getAttribute("data-id");
					comments.lastCommentText.className = "comment_text";
					comments.lastCommentText.contentEditable = false;
					
					$p("#comment-row-" + id  + " .edit-link").attr("data-action","edit");
					$p("#comment-row-" + id  + " .edit-link .edit-text").html("Edit");
				};
				
				var doEdit = function(a) {
					var id = a.getAttribute('data-id');
					var comment_text = document.getElementById("comment-text-" + id);
						comment_text.contentEditable = true;
						comment_text.className = "comment-text editable";
						comment_text.focus();
					
					$p("#comment-row-" + id  + " .edit-link").attr("data-action","save");
					$p("#comment-row-" + id  + " .edit-link .edit-text").html("Save");
						
					comments.lastCommentText = comment_text;
				};
				
				var doSave = function(a) {
					var id = a.getAttribute('data-id');
					var comment_text = document.getElementById("comment-text-" + id);
						comment_text.contentEditable = false;
						
					var query = "action=update_comment_text&comment_id=" + id + "&comment_text=" + encodeURIComponent(comment_text.innerHTML);
					
					pageManager.post( comments.ajax_page, query, function(data) {
						comment_text.className = "comment-text";
					
						$p("#comment-row-" + id  + " .edit-link").attr("data-action","edit");
						$p("#comment-row-" + id  + " .edit-link .edit-text").html("Edit");
						
					});
				}
				
				if( this.getAttribute("data-action") == "save" ) {
					doSave(this);
				} else {
					if( comments.lastCommentText != null ) {
						doClear();
					}
										
					doEdit(this);	
				}
				
			});
		});
		
		$p(".delete-link").bind('click',function() {
			comments.deleteComments( this.getAttribute('data-id') );
		});
	},
	
	getSelectedCommentIDs: function() {
		var selected = [];
		pageManager.all(".cms-data-table .checkitem", function(el) {
			if( el.checked ) {
				selected.push(el.value);
			}
		});
		
		return selected;
	},
	
	/**
	 * Approves or disapproves the user comments
	 */
	approveComments: function(approve, ids) {
		// gets the list of selected comments
		if(ids == null) ids = comments.getSelectedCommentIDs();
		
		// check if any comments are actually selected
		if( ids.length == 0 ) {
			alert("Please Select Messages To Approve/Unapprove");	
			return;
		}
		
		// post the processing command
		var query = "action=approve_comments&approve=" + (approve ? "1" : "0") + "&ids=" + ids;
		pageManager.post( comments.ajax_page, query, function(data) {
			
			var process_row = function(row_id) {
				var row = document.getElementById("comment-row-" + row_id);
				row.className = approve ? "approved" : "pending";
				
				$p("#comment-row-" + row_id + " .status-link").attr("data-status", approve ? "approved" : "unapproved");
				$p("#comment-row-" + row_id + " .status-link .link-text").html(approve ? "Unapprove" : "Approve");
			};	
			
			// update the visual display for the comments
			if( typeof(ids) == "object" ) {
				for(var i = 0; i < ids.length; i++ ) {
					process_row(ids[i]);
				}
				
			} else if( parseInt(ids) > -1 ) {
				process_row(ids);	
			}
			
		});
	},
	
	/**
	 * Approves or disapproves the user comments
	 */
	deleteComments: function(ids) {
		// gets the list of selected comments
		if( ids == null ) ids = comments.getSelectedCommentIDs();
		
		// check if any comments are actually selected
		if( ids.length == 0 ) {
			alert("Please Select Comments To Delete");	
			return;
			
		} else if( !confirm("Delete Selected Article Comments From The System?") ) {
			return;	
		}
		
		// post the processing command
		var query = "action=delete_comments&ids=" + ids;
		pageManager.post( comments.ajax_page, query, function() {

            var row = null;

			// update the visual display for the comments
			if( typeof(ids) == "object" ) {
				for(var i = 0; i < ids.length; i++ ) {
					$p("#comment-row-" + ids[i]).remove();
				}
				
			// assume ids is an integer and try to convert it
			} else if( parseInt(ids) > -1 ) {
				$p("#comment-row-" + ids).remove();
			}
		});
	}
	
};