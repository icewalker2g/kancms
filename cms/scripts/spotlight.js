// JavaScript Document

var spotlight = {
	
    ajax_page: 'ajax_pages/spotlight.php',

    init: function() {
		
        // add check all event listener
        $p('#checkAll').bind('click', function() {
            var items = $p.all("#spot-list-table td input[type=checkbox]");
            
            for(var  i = 0; i < items.length; i++) {
                items[i].checked = this.checked;
            }
        });
		
        // add event list for the action buttons
		$p("#add-cancel, #save-cancel").bind('click',function() {
			window.location.href = 'spotlight.php';	
		});
        
        // add event listener for preview links
        $p(".preview-link").bind('click', function(){
            
            var query = "action=preview&id=" + $p(this).attr("data-id");
            
            $p.post( spotlight.ajax_page, query, function(data) {
                
                $p.showDialog({
                    title: 'Spotlight Image Preview',
                    content: data
                });
            });
        });
        
        // add event listener for delete links
        $p(".delete-link").bind('click', function() {
            spotlight.deleteSpotlight( $p(this).attr('data-id') );
        });
		
		// add the event listener for all the publishing links available
		$p.all(".cms-publish-link, .cms-unpublish-link", function(el) {
			el.onclick = function() {
				if( el.className == 'cms-publish-link' ) {
					spotlight.setApproved('unpublish', this );
				} else {
					spotlight.setApproved('publish', this );
				}
				
				return false;
			};
		});
		
        // if we are editting (i.e. description field is present) the initialise
        // the TinyMCE control and register the form handlers
        if( $p.$("#description") ) {
            // make the description field an html editor
			ui.htmlMiniEditor("description");
            
            $p.all("form", function(el) {
                el.onsubmit = function() {
                    tinyMCE.triggerSave();

                    return $p(this).validate({
                        menu_header: "Title",
                        menu_subheader: "Subtitle",
                        description: "Description",
                        uploadFile: "Spotlight File To Upload"
                    });
                }
            });
        }
    },
	
    
    /**
     * Deletes a spotlight from the database
     * 
     * @param id spotlight ID
     */
    deleteSpotlight: function(id) {
        if( !confirm('Delete Selected Spotlight From The Database') ) {
            return false;
        }
		
        var query = "action=delete_spotlight&id=" + id;
        
        $p.post( spotlight.ajax_page, query, function(data) {            
            $p('#spot-row-' + id).remove();
        });
		
        return false;
    },
	
    /**
     * Approves or disapproves a spotlight image from being displayed
     * 
     * @param action
     * @param linkEl
     */
	setApproved: function(action, linkEl) {
		var query = "action=" + action + "&id="	+ linkEl.id;
		
		$p.post( spotlight.ajax_page, query, function() {
																		   
			if( action == "publish" ) {
				linkEl.className = 'cms-publish-link';	
				linkEl.title = 'Published (Click To Unpublish)';
				linkEl.onclick = function() {
					spotlight.setApproved('unpublish', this);
				}
			} else {
				linkEl.className = 'cms-unpublish-link';
				linkEl.title = 'Unpublished (Click To Publish Now)';
				linkEl.onclick = function() {
					spotlight.setApproved('publish', this);
				}
			}
		});	
	}
};