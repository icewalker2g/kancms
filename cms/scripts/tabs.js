// JavaScript Document

var tabs = {
    lastTab: null,
	
    targetPage: null,
	
    setTargetPage: function(page) {
        tabs.targetPage = page;
    },
	
    selectTab: function(tabIndex) {
        var tab_row = document.getElementById("tab_row");
		
        if( tab_row != null ) {
            if( tab_row.childNodes ) {
                for( var i = 0, j = 0; i < tab_row.childNodes.length; i++) {
                    // we compare nodeType with document.ELEMENT_NODE value of 1
                    if( tab_row.childNodes[i].nodeType == 1 ) {
                        if( tabIndex == j ) {
                            tab_row.childNodes[i].className = "tab selected";
                            tabs.lastTab = tab_row.childNodes[i];
                        } else {
                            tab_row.childNodes[i].className = "tab";
                        }
						
                        j++;
                    }
                }
            } else if( tab_row.children ) {
                for( var i = 0; i < tab_row.children.length; i++) {
                    if( tabIndex == i ) {
                        tab_row.children[i].className = "tab selected";
                        tabs.lastTab = tab_row.children[i];
                    } else {
                        tab_row.children[i].className = "tab";
                    }
                }
            }
        }
    },
	
    loadTabContent: function(tabIndex, contentArea) {
        tabs.selectTab(tabIndex);
		
        tabs.ajax(contentArea, tabs.lastTab.getAttribute("category") );
    },

    ajax: function(contentArea, category) {
        var poststr = "cat=" + category;
        poststr = encodeURI(poststr);
		
        pageManager.ajax( tabs.targetPage, poststr, contentArea);
    },
	
    showContent: function(tabIndex, contentArea) {
        tabs.selectTab(tabIndex);
		var content = document.getElementById("tab_content" + tabIndex);
		
		if( content != null ) {
        	document.getElementById(contentArea).innerHTML = content.innerHTML;
		}
    }
};