// JavaScript Document

var components = {

	init: function() {
		tinyMCE.init({
			mode : "exact",
			theme : "advanced",
			elements : "Description",
			
			theme_advanced_toolbar_location : "top",
			theme_advanced_toolbar_align : "left",
			
			external_link_list_url : 'lists/link_list.js',
			
			theme_advanced_buttons1 : "bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,|,code,link,unlink,anchor",
			theme_advanced_buttons2 : ""
				//					+ "justifyleft,justifycenter,justifyright,justifyfull,styleselect,"
					//				+ "formatselect,fontselect,fontsizeselect"
		});
		
		var createForm = pageManager.$("#comp-create-form");
		if( createForm != null ) {
			createForm.onsubmit = function() {
				var component = pageManager.$("#Component");
				if( component.value.indexOf(" ") != -1 || component.value == "" ) {
					alert("Component ID Cannot Contain Spaces");
					component.focus();
					return false;
				}
				
				var tmpPath = "../components/<comp-dir>/<comp-file>.php";
				if( pageManager.$("#ComponentPath").value == tmpPath ) {
					alert("Please Complete The Component Path Field");
					pageManager.$("#ComponentPath").focus();
					return false;
				}
				
				// finally if the form checks out then we submit it
				if( $p(this).validate() ) {
					// ensure we store whatever code was written before submit
					file_utils.storeEditorContent();
				
					return true;	
				}
				
				return false;
			}
		}
		
		// attach a handler to the uploadFile field if it exists
        pageManager.addEvent('uploadFile', 'change', function() {
            
            var file_path = document.getElementById("uploadFile").value;
			var ext = file_path.substring( file_path.lastIndexOf(".") );
			
			if( ext.toLowerCase() != ".zip" ) {
				alert("Selected File Is Not In The Required Format");
				document.getElementById("uploadFile").value = "";
				return;
			}

			// calculate the data time to be used as the destination file name
			document.getElementById('destFolder').value = "../assets/components/";
			document.getElementById('destFileName').value = "component-package";
			document.getElementById("ArchivePath").value = "../assets/components/component-package.zip";
			
			// generate the destination file name to used for the picture field
            //file_utils.generateDestinationFileName('ArchivePath', dstFolder, file_path, 'comp-');
			
			//alert( document.getElementById("ArchivePath").value );
        });
		
		
		$p(".delete-link").bind("click", function() {
			var id = $p(this).attr("data-id");
			$p.showConfirmMessage({
				title: "Confirm Delete",
				content: "You Are About To Delete The Selected Component. Do you want to continue?",
				okButtonText: "Continue Delete",
				okButtonAction: function() {
					var query = "action=delete_component&id=" + id;
			
					$p.post( "ajax_pages/components.php", query, function() {
						$p.remove("#comp-row-" + id);	
					});
				}
			});
			
			
			
		});
	}
};
				