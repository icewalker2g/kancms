// JavaScript Document

var menus = {
	
	init: function() {
		// add the event listener for all the publishing links available
		pageManager.all(".cms-publish-link, .cms-unpublish-link", function(el) {
			el.onclick = function() {
				if( el.className == 'cms-publish-link' ) {
					menus.publishMenu('unpublish', this );
				} else {
					menus.publishMenu('publish', this );
				}
				
				return false;
			};
		});
		
		pageManager.addEvent('#UseMenuContent','click',function() {
			if( this.checked ) {
				pageManager.show('menu_content');
			} else {
				pageManager.hide('menu_content');
			}
		});
		
		pageManager.addEvent('#target-page-linker','click', function(e) {
			system.selectSiteLink(e, pageManager.$('#TargetPage') );
		});
		
		if( document.getElementById("MenuContent") != null ) {
			if( document.getElementById("MenuContent").value != "" ) {
				document.getElementById("menu_content").style.display = "block";
				document.getElementById("UseMenuContent").checked = true;
			}
		}
		
		// make the MenuContent field an html editor
		ui.htmlContentEditor("MenuContent");
		
		if( document.getElementById("MenuAddForm") ) {
			$("#MenuName").friendurl({id:'MenuAlias'});
		}
	},
	
	publishMenu: function(action, linkEl) {
		var query = "action=" + action + "&id="	+ linkEl.id;
		
		pageManager.post('ajax_pages/menus.php', query, null, function() {
			if( action == "publish" ) {
				linkEl.className = 'cms-publish-link';	
				linkEl.title = 'Published (Click To Unpublish)';
				linkEl.onclick = function() {
					menus.publishMenu('unpublish', this);
				}
			} else {
				linkEl.className = 'cms-unpublish-link';
				linkEl.title = 'Unpublished (Click To Publish Now)';
				linkEl.onclick = function() {
					menus.publishMenu('publish', this);
				}
			}
		});	
	}
};