/* 
 * This simple framework is meant to provide a simple way to create
 * some UI components. It should be noted that other frameworks are useful
 * but we going with this for now in this release.
 */

var ui = {

    /**
     * Creates a menu element based on the menu item information specified.
     * The menu items parameter should be an object that contains an array
     * menu item objects. Example
     * {
     *   items: [
     *      {
     *          id: "menu-1",
     *          name: "Menu Name 1",
     *          title: "Menu Title 1",
     *          action: function() {
     *              alert("Menu 1 clicked");
     *          }
     *      },
     *      {
     *          id: "menu-2",
     *          name: "Menu Name 2",
     *          title: "Menu Title 2",
     *          action: function() {
     *              alert("Menu Two clicked");
     *          }
     *      }
     *   ]
     * }
     */

    menu: function(el, menu_data) {
        if( el == null ) {
           return;
        }

        // make sure the element has the necessary styles to help out
        el.style.position = "relative";

        if( menu_data.items == null ) {
            return;
        }

        var items = menu_data.items;
        var menu = document.createElement("ul");
            menu.className = "cms-menu";
			menu.tabIndex = 1000; // add to enable element have focus
			menu.onblur = function() {
				this.style.display = "none";
			};
        
        for( var i = 0; i < items.length; i++ ) {
            var item = items[i];
            var li = document.createElement("li");
            li.id = item.id;
            li.innerHTML = item.name;
            li.title = item.title;
			li.action = item.action; // this is a simple work around since the item.action call is not working correctly
            li.onclick = function() {
                // call the cached action and pass this current menu item to the function as a parameter
                this.action(this);

                // hide the menu
                this.parentNode.style.display = "none";
            };
            li.getTrigger = function() {
                return el;
            };

            menu.appendChild(li);
        }

        el.parentNode.appendChild(menu);

        el.onclick = function() {
			menu.style.position = "absolute";
            menu.style.top = (el.offsetTop + 20) + "px";
            menu.style.left = (el.offsetLeft) + "px";
            menu.style.display = "block";
			if( menu.hideFocus ) menu.hideFocus = true;
			menu.focus(); // make the menu focused, as it can be hidden once focus is lost

			return false;
        };
    },
	
	htmlContentEditor: function(textarea_id) {
		if( !tinyMCE ) {
			return;	
		}
		
		tinyMCE.init({
			mode : "exact",
			theme : "advanced",
			elements : textarea_id,

			plugins : "advhr, advimage, table, media, print, save, spellchecker, paste, searchreplace, fullscreen, inlinepopups, components, template",

			theme_advanced_toolbar_location : "top",
			theme_advanced_toolbar_align : "left",
			theme_advanced_statusbar_location : "bottom",
			theme_advanced_resizing : true,
			content_css: "css/content.css, css/theme_css.php",
			extended_valid_elements: "div[id|class|type|data-component|style|title|align]",

			external_link_list_url : "lists/link_list.php",
			external_image_list_url : "lists/image_list.php",
			media_external_list_url : "lists/media_list.php",
			template_external_list_url : "lists/template_list.js",

			// Theme options
			theme_advanced_buttons1 : "save,newdocument,|,bold,italic,underline,strikethrough,|,"
			+ "justifyleft,justifycenter,justifyright,justifyfull,styleselect,"
			+ "formatselect,fontselect,fontsizeselect",
			theme_advanced_buttons2 : "cut,copy,paste,pastetext,pasteword,|,forecolor,backcolor,|, search,replace,spellchecker,|,bullist,numlist,|," +
			"outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,media,cleanup",
			theme_advanced_buttons3 : "tablecontrols,|,advhr,hr,removeformat,visualaid,|,sub,sup,|,charmap,emotions,|,fullscreen,|,code,template,components,help"
		});
	},
	
	htmlSummaryEditor: function(textarea_id) {
		if( !tinyMCE ) {
			return;	
		}
		
		tinyMCE.init({
			mode : "exact",
			theme : "advanced",
			elements : textarea_id,
			plugins : "advimage, spellchecker, paste, save, searchreplace, inlinepopups",
			
			theme_advanced_toolbar_location : "top",
			theme_advanced_toolbar_align : "left",
			theme_advanced_resizing : true,
			theme_advanced_statusbar_location : "bottom",
			content_css: "css/content.css, css/theme_css.php",
			
			external_link_list_url : "lists/link_list.php",
			external_image_list_url : "lists/image_list.php",
			
			// Theme options
			theme_advanced_buttons1 : "bold,italic,underline,strikethrough,removeformat,|,"
									+ "justifyleft,justifycenter,justifyright,justifyfull,styleselect,"
									+ "formatselect,fontselect,fontsizeselect",
			theme_advanced_buttons2 : "cut,copy,paste,pastetext,pasteword,|,forecolor,backcolor,|, bullist,numlist,|," +
						"outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,media,cleanup,code,help",
			theme_advanced_buttons3: ""	
		});
	},
	
	htmlMiniEditor: function( textarea_id ) {
		if( !tinyMCE ) {
			return;	
		}
		
		tinyMCE.init({
			mode : "exact",
			theme : "advanced",
			elements : textarea_id,
			plugins : "advimage",
			theme_advanced_toolbar_location : "top",
			theme_advanced_toolbar_align : "left",
			
			external_link_list_url : "lists/link_list.php",
			external_image_list_url : "lists/image_list.php",
			
			theme_advanced_resizing : true,
			
			// Theme options
			theme_advanced_buttons1 : "bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,|,link,unlink,image,code",
			theme_advanced_buttons2 : ""
		});
	},
	
	unregisterEditor: function(textarea_id) {
		if( !tinyMCE ) {
			return;	
		}
			
		try {
			tinyMCE.remove( tinyMCE.getInstanceById(textarea_id) );
		} catch(e) {}	
	}
};
