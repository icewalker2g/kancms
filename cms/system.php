<?php 
include("accesscheck.php"); 
include('forms/site_settings_form.php');

kan_import('SystemManager');

$system = new SystemManager();

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/cms.dwt.php" codeOutsideHTMLIsLocked="false" -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<!-- InstanceBeginEditable name="doctitle" -->
<title><?php echo getSetting('CMSTitle','KAN Content Management System'); ?></title>
<!-- InstanceEndEditable -->
<link rel="shortcut icon" type="image/png" href="../favicon.png" />
<link rel="stylesheet" type="text/css" href="css/widestyles.css"/>
<link rel="stylesheet" type="text/css" href="css/cms.css"/>
<link rel="stylesheet" type="text/css" href="css/cms-ui.css"/>
<link rel="stylesheet" type="text/css" href="css/util.css"/>

<script type="text/javascript" src="../assets/scripts/jquery/js/jquery.js"></script>
<script type="text/javascript" src="../assets/scripts/jquery/js/jquery-ui.js"></script>
<script type="text/javascript" src="../assets/scripts/common/pageManager.js"></script>
<script type="text/javascript" src="scripts/system.js"></script>
<script type="text/javascript" src="scripts/ui.js"></script>

<!-- InstanceBeginEditable name="head" -->
<link rel="stylesheet" type="text/css" href="css/system-ui.css"/>
<link rel="stylesheet" type="text/css" href="../assets/scripts/jquery/css/ui-lightness/jquery-ui.css"/>
<script type="text/javascript" src="../assets/scripts/jquery/js/jquery.js"></script>
<script type="text/javascript" src="../assets/scripts/jquery/js/jquery-ui.js"></script>
<script type="text/javascript" src="scripts/system-ui.js"></script>
<!-- InstanceEndEditable -->
</head>

<body>

<div id="page">
	<div id="header">
    	<?php include('logo_header.php'); ?>
    </div>
    
    <?php if( isset($_SESSION['CMS_Username']) ) { ?>
    <div id="site-info-bar">
		<?php include('system_menu.php'); ?>
    </div>
    <?php } ?>
    
    
    <div id="content-wrapper">
    	<div id="content-header">
        	<span>
			<!-- InstanceBeginEditable name="section title" -->CONTENT MANAGEMENT SECTION<!-- InstanceEndEditable -->
            </span>
        </div>
    	<div id="content">
			<!-- InstanceBeginEditable name="content" -->
            <div id="nav">
                <?php include('nav_section.php'); ?>
            </div>
            
            <div id="main">
            	<?php if( isset($_GET['saved']) ) { ?>
            	<div class="valid" style="padding: 10px;">
                	Settings Saved Sucessfully
                </div>
                <?php } ?>
                
            	<form action="ajax_pages/system.php" name="settings-form" id="settings-form" method="post">
                    <div class="cms-content-pane" style="border: solid #ddd 1px; border-spacing: 0px;">
                        <div class="pane-header">System Configuration</div>
                        <div class="pane-content">
                            <?php renderSettingsTabs(); ?>
                        </div>
                        
                        <div class="pane-content" style="text-align:center;">
                            <input type="submit" value="Save Changes" name="save" id="setting-save-btn" />
                            <input type="button" value="Cancel" name="cancel" id="setting-cancel-btn" />
                        </div>
                    </div>
                    
                    <input type="hidden" name="action" id="action" value="save_settings"  />
                </form>
            </div>
            <!-- InstanceEndEditable -->
        </div>
    </div>
    
    <div id="footer">
    	<?php include('footer.php'); ?>
    </div>
</div>

</body>
<!-- InstanceEnd --></html>