<?php 

include("accesscheck.php"); 
include('../core/controllers/EventsManager.php');

// create an instance of the EventsManager
$events = new EventsManager();

$currentPage = $_SERVER["PHP_SELF"];

$editFormAction = $_SERVER['PHP_SELF'];
if (isset($_SERVER['QUERY_STRING'])) {
  $editFormAction .= "?" . htmlentities($_SERVER['QUERY_STRING']);
}


if ((isset($_POST["MM_update"])) && ($_POST["MM_update"] == "form2")) {
  $events->updateEvent();

  $updateGoTo = $_SERVER['PHP_SELF'];
  header(sprintf("Location: %s", $updateGoTo));
}

if ((isset($_POST["MM_insert"])) && ($_POST["MM_insert"] == "form1")) {
  $events->addEvent();

  $insertGoTo = $_SERVER['PHP_SELF'];
  header(sprintf("Location: %s", $insertGoTo));
}


$colname_rsEvent = "-1";
if (isset($_GET['id'])) {
  $colname_rsEvent = $_GET['id'];
}

$eventObj = $events->getEvent($colname_rsEvent);
$row_rsEvent = $eventObj->getDataArray();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/cms.dwt.php" codeOutsideHTMLIsLocked="false" -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<!-- InstanceBeginEditable name="doctitle" -->
<title><?php echo getSetting('CMSTitle','KAN Content Management System'); ?></title>
<!-- InstanceEndEditable -->
<link rel="shortcut icon" type="image/png" href="../favicon.png" />
<link rel="stylesheet" type="text/css" href="css/widestyles.css"/>
<link rel="stylesheet" type="text/css" href="css/cms.css"/>
<link rel="stylesheet" type="text/css" href="css/cms-ui.css"/>
<link rel="stylesheet" type="text/css" href="css/util.css"/>

<script type="text/javascript" src="../assets/scripts/jquery/js/jquery.js"></script>
<script type="text/javascript" src="../assets/scripts/jquery/js/jquery-ui.js"></script>
<script type="text/javascript" src="../assets/scripts/common/pageManager.js"></script>
<script type="text/javascript" src="scripts/system.js"></script>
<script type="text/javascript" src="scripts/ui.js"></script>

<!-- InstanceBeginEditable name="head" -->
<?php if( isset($_GET['edit']) || isset($_GET['add']) ) { ?>

<!--Javascript Calendar Control-->
<link rel="stylesheet" type="text/css" href="../assets/scripts/jquery/css/ui-lightness/jquery-ui.css"/>
<script type="text/javascript" src="../assets/scripts/jquery/js/jquery.js"></script>
<script type="text/javascript" src="../assets/scripts/jquery/js/jquery-ui.js"></script>
<script type="text/javascript" src="scripts/friendurl/jquery.friendurl.min.js"></script>
<script type="text/javascript" src="scripts/tiny_mce/tiny_mce.js"></script>


<?php } ?>

<link rel="stylesheet" type="text/css" href="css/events.css"/>
<script type="text/javascript" src="scripts/events.js"></script>
<script language="javascript" type="text/javascript">
	pageManager.addLoadEvent( function() {
		events.init();
	});
</script>
<!-- InstanceEndEditable -->
</head>

<body>

<div id="page">
	<div id="header">
    	<?php include('logo_header.php'); ?>
    </div>
    
    <?php if( isset($_SESSION['CMS_Username']) ) { ?>
    <div id="site-info-bar">
		<?php include('system_menu.php'); ?>
    </div>
    <?php } ?>
    
    
    <div id="content-wrapper">
    	<div id="content-header">
        	<span>
			<!-- InstanceBeginEditable name="section title" -->Event And Notices<!-- InstanceEndEditable -->
            </span>
        </div>
    	<div id="content">
			<!-- InstanceBeginEditable name="content" -->
            <div id="nav"><?php include('nav_section.php'); ?></div>
            <div id="main">
                <table border="0" align="center" cellpadding="3" cellspacing="0" class="newsTbl">
                    <?php if( isset($_GET['id']) ) { ?>
                    <tr>
                        <td colspan="2" class="sqrtab">Edit selected Event Information</td>
                    </tr>
                    <tr>
                        <td colspan="2" valign="top" class="newsSummary">
                            <div class="cms-form-message">
                                Fill in the fields below to edit the contents of the selected article
                            </div>
                            <form action="<?php echo $editFormAction; ?>" method="post" name="form2" id="EventEditForm">
                                <table align="center" cellpadding="6" cellspacing="0">
                                    <tr >
                                        <td width="74" align="right" nowrap="nowrap" class="sectionTitle1">Category:</td>
                                        <td width="530">
                                            <select name="EventCategory">
                                                <option value="">Select Event Category</option>
                                                <?php
                                                $categories = $events->getCategories();
                                                foreach($categories as $category) {
                                                    $selected = $category->getId() == $row_rsEvent['id'] ? "selected=selected" : "";
                                                ?>
                                                <option value="<?php echo $category->getId(); ?>" <?php echo $selected; ?>><?php echo $category->getCategoryName(); ?></option>
                                                <?php }  ?>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr >
                                        <td align="right" nowrap="nowrap" class="sectionTitle1">
                                        Event Title:
                                        </td>
                                        <td>
                                            <input type="text" id="EventName" name="EventName" value="<?php echo htmlentities($row_rsEvent['EventName'], ENT_COMPAT, 'iso-8859-1'); ?>" size="60" />
                                        </td>
                                    </tr>
                                    <tr >
                                        <td align="right" class="sectionTitle1">
                                        Title Alias:
                                        </td>
                                        <td colspan="2">
                                            <label for="FriendlyURL"></label>
                                            <input name="FriendlyURL" type="text" class="alias" id="FriendlyURL" style="width: 92%;" value="<?php echo $row_rsEvent['FriendlyURL']; ?>" size="65" />
                                        </td>
                                    </tr>
                                    <tr >
                                        <td align="right" valign="top" nowrap="nowrap" class="sectionTitle1">Description:</td>
                                        <td>
                                            <textarea id="EventDesc" name="EventDesc" cols="70" rows="25"><?php echo htmlentities($row_rsEvent['EventDesc'], ENT_COMPAT, 'iso-8859-1'); ?></textarea>
                                        </td>
                                    </tr>
                                    <tr >
                                        <td align="right" nowrap="nowrap" class="sectionTitle1">Time:</td>
                                        <td>
                                            <input type="text" name="Time" value="<?php echo htmlentities($row_rsEvent['Time'], ENT_COMPAT, 'iso-8859-1'); ?>" size="18" />
                                        </td>
                                    </tr>
                                    <tr >
                                        <td align="right" valign="top" nowrap="nowrap" class="sectionTitle1">Date:</td>
                                        <td>
                                            <div class="checkDiv" id="singleDaySel">
                                                <input <?php if (!(strcmp($row_rsEvent['EventDuration'],"single"))) {echo "checked=\"checked\"";} ?> type="radio" name="EventDuration" id="singleCheck" value="single" onclick="showDateArea('#single')" />
                                                <label for="EventDuration">Single Day Event</label>
                                            </div>
                                            <div class="dateDiv" id="single" style="display:block; height: 30px;">
                                                <input type="text" name="Date" id="Date" value="<?php echo htmlentities($row_rsEvent['Date'], ENT_COMPAT, 'iso-8859-1'); ?>" size="12" />
                                            </div>
                                            <div class="checkDiv" id="periodSel">
                                                <input <?php if (!(strcmp($row_rsEvent['EventDuration'],"period"))) {echo "checked=\"checked\"";} ?> type="radio" name="EventDuration" id="periodCheck" value="period" onclick="showDateArea('#period')" />
                                                <label for="EventDuration">Period Event</label>
                                            </div>
                                            <div class="dateDiv" id="period" style="display:none; height: 30px;">
                                                <table width="361" border="0" cellpadding="2" cellspacing="0">
                                                    <tr>
                                                        <td>From:</td>
                                                        <td>
                                                            <input name="StartDate" id="StartDate" value="<?php echo $row_rsEvent['StartDate']; ?>" size="12" />
                                                        </td>
                                                        <td>To: </td>
                                                        <td>
                                                            <input name="EndDate" id="EndDate" value="<?php echo $row_rsEvent['EndDate']; ?>" size="12" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr >
                                        <td align="right" nowrap="nowrap" class="sectionTitle1">Venue:</td>
                                        <td>
                                            <input type="text" name="Venue" value="<?php echo htmlentities($row_rsEvent['Venue'], ENT_COMPAT, 'iso-8859-1'); ?>" size="50" />
                                        </td>
                                    </tr>
                                    <tr >
                                        <td nowrap="nowrap" align="right">&nbsp;</td>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" align="center" nowrap="nowrap">
                                            <input type="submit" class="sectionTitle1" value="Save Changes" />
                                            <input name="button2" type="button" class="sectionTitle1" id="button2" onclick="if( confirm('Cancel Update of Selected Event Information?') ) location.href = '<?php echo $_SERVER['PHP_SELF']; ?>'; " value="Cancel Update" />
                                        </td>
                                    </tr>
                                </table>
                                <input type="hidden" name="AssocImage" value="<?php echo htmlentities($row_rsEvent['AssocImage'], ENT_COMPAT, 'iso-8859-1'); ?>" />
                                <input type="hidden" name="MM_update" value="form2" />
                                <input type="hidden" name="id" value="<?php echo $row_rsEvent['id']; ?>" />
                            </form>
                        </td>
                    </tr>
                    <?php } // end if ?>
                    <?php if( isset($_GET['add']) ) { ?>
                    <tr>
                        <td colspan="2" class="sqrtab">Add new event</td>
                    </tr>
                    <tr>
                        <td colspan="2" valign="top" class="newsSummary">
                            <div class="cms-form-message">
                                Fill in the fields below to add a events article to the site
                            </div>
                            <form action="<?php echo $editFormAction; ?>" method="post" name="EventAddForm" id="EventAddForm">
                                <table align="center" cellpadding="6" cellspacing="0">
                                    <tr >
                                        <td align="right" nowrap="nowrap" class="sectionTitle1">Event Category:</td>
                                        <td>
                                            <select name="EventCategory">
                                                <option value="">Select Event Category</option>
                                                <?php
                                                $categories = $events->getCategories();
                                                foreach($categories as $category) { ?>
                                                <option value="<?php echo $category->getId(); ?>"><?php echo $category->getCategoryName(); ?></option>
                                                <?php }  ?>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr >
                                        <td align="right" nowrap="nowrap" class="sectionTitle1">
                                        Event Title:
                                        </td>
                                        <td>
                                            <input type="text" id="EventName" name="EventName" value="" size="60" />
                                        </td>
                                    </tr>
                                    <tr >
                                        <td align="right" class="sectionTitle1">
                                        Title Alias:
                                        </td>
                                        <td colspan="2">
                                            <label for="FriendlyURL"></label>
                                            <input name="FriendlyURL" type="text" class="alias" id="FriendlyURL" style="width: 92%;" size="65" readonly="readonly" />
                                        </td>
                                    </tr>
                                    <tr >
                                        <td align="right" valign="top" nowrap="nowrap" class="sectionTitle1">Description:</td>
                                        <td>
                                            <textarea id="EventDesc" name="EventDesc" cols="70" rows="25"></textarea>
                                        </td>
                                    </tr>
                                    <tr >
                                        <td align="right" nowrap="nowrap" class="sectionTitle1">Time:</td>
                                        <td>
                                            <input type="text" name="Time" value="" size="18" />
                                        </td>
                                    </tr>
                                    <tr >
                                        <td align="right" valign="top" nowrap="nowrap" class="sectionTitle1">Date:</td>
                                        <td>
                                            <div class="checkDiv" id="singleDaySel">
                                                <input name="EventDuration" type="radio" id="singleCheck" onclick="showDateArea('#single')" value="single" checked="checked" />
                                                <label for="EventDuration">Single Day Event</label>
                                            </div>
                                            <div class="dateDiv" id="single" style="display:block; height: 30px;">
                                                <input name="Date" type="text" id="Date" value="0000-00-00" size="12" />
                                            </div>
                                            <div class="checkDiv" id="periodSel">
                                                <input type="radio" name="EventDuration" id="periodCheck" value="period" onclick="showDateArea('#period')" />
                                                <label for="EventDuration">Period Event</label>
                                            </div>
                                            <div class="dateDiv" id="period" style="display:none; height: 30px;">
                                                <table width="361" border="0" cellpadding="2" cellspacing="0">
                                                    <tr>
                                                        <td width="39">From:</td>
                                                        <td width="139">
                                                            <input name="StartDate" id="StartDate" size="12" value="0000-00-00" />
                                                        </td>
                                                        <td width="23">To: </td>
                                                        <td width="144">
                                                            <input name="EndDate" id="EndDate" size="12" value="0000-00-00" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr >
                                        <td align="right" nowrap="nowrap" class="sectionTitle1">Venue:</td>
                                        <td>
                                            <input type="text" name="Venue" value="" size="50" />
                                        </td>
                                    </tr>
                                    <tr >
                                        <td nowrap="nowrap" align="right">&nbsp;</td>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr >
                                        <td colspan="2" align="center" nowrap="nowrap">
                                            <input type="submit" class="sectionTitle1" value="Add Event" />
                                            <input name="button" type="button" class="sectionTitle1" id="button" onclick="if( confirm('Cancel Addition Event Information?') ) location.href = '<?php echo $_SERVER['PHP_SELF']; ?>'; " value="Cancel Update" />
                                        </td>
                                    </tr>
                                </table>
                                <input type="hidden" name="AssocImage" value="" />
                                <input type="hidden" name="MM_insert" value="form1" />
                            </form>
                        </td>
                    </tr>
                    <?php } // end if ?>
                    <?php if( !isset($_GET['add']) && !isset($_GET['edit']) ) { ?>
                    <tr>
                        <td colspan="2" class="sqrtab">Events and notices</td>
                    </tr>
                    <tr>
                        <td colspan="2" class="newsContent">
                            <div class="cms-content-pane">
                                <div class="category-pane">
                                    <div class="pane-header">
                                        Event Categories
                                        <div style="width: 50px; position: absolute; right: 10px; top: 2px; text-align:right; ">
                                            <a id="add-cat-link" href="#" title="Add Category" ><img src="images/icons/add.png" width="16" height="16" alt="add" align="absmiddle" border="0" /></a> <a id="del-cat-link" href="#" title="Delete Selected Category"><img src="images/icons/delete.png" width="16" height="16" alt="add" align="absmiddle" border="0" /></a>
                                        </div>
                                    </div>
                                    <div class="pane-content">
                                        <ul class="category-list">
                                            <?php
													
													$categories = $events->getEventCategories($siteid);
			
													for($i = 0; $i < count($categories); $i++) {
														$cat_id = $categories[$i]->getId();
														$cat = $categories[$i]->getCategoryName();
														$desc = $categories[$i]->getCategoryDescription(); 
														echo "<li><a id='$cat_id' href='#' title='$desc' >$cat</a></li>";
													}
												?>
                                        </ul>
                                    </div>
                                </div>
                                <div class="articles-pane">
                                    <div class="pane-header">
                                        Upcoming Events In
                                    </div>
                                    <div class="pane-sub-header">
                                        <a href="?add"><img src="images/icons/newspaper_add.png" alt="add" width="16" height="16" border="0" align="absmiddle" /> New Event </a> <a id="delete-all-link" href="#" ><img src="images/icons/newspaper_delete.png" alt="del" width="16" height="16" border="0" align="absmiddle" /> Delete Selected</a>
                                    </div>
                                    <div id="event-content" class="pane-content">
                                        Loading Page. Please Wait...
                                    </div>
                                    <div class="pane-footer">
                                        Pages:
                                    </div>
                                </div>
                                <div id="catAddDiv" style="display: none; overflow: hidden; position:absolute; z-index: 30px; width: 334px; left: 140px; top: 20px; background:#ececec; height: 122px; border: solid #aaa 1px; background-color: #FFFFFF;">
                                    <div style="padding:10px;">
                                        <form id="catForm" name="catForm" method="post" action="<?php echo $editFormAction; ?>" style="margin:0px;">
                                            <table width="98%" border="0" align="right" cellpadding="3" cellspacing="0">
                                                <tr>
                                                    <td nowrap="nowrap"><strong>Category Name</strong></td>
                                                    <td>
                                                        <input name="CategoryName" type="text" id="CategoryName" size="25" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td nowrap="nowrap"><strong>Description:</strong></td>
                                                    <td>
                                                        <input name="CategoryDesc" type="text" id="CategoryDesc" size="25" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>&nbsp;</td>
                                                    <td>
                                                        <input name="add-cat-btn" type="button" class="small" id="add-cat-btn" value="Add Category" />
                                                        <input name="cancel-cat-btn" type="button" class="small" id="cancel-cat-btn" value="Cancel" />
                                                    </td>
                                                </tr>
                                            </table>
                                            <input type="hidden" name="MM_insert2" value="catForm" />
                                            <input type="hidden" name="MM_insert" value="catForm" />
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </td>
                    </tr>
                    <?php if( false ) { ?>
                    <tr>
                        <td width="56%" class="newsHeader"><a href="?add"><img src="images/icons/calendar_add.png" alt="add" width="16" height="16" border="0" align="absmiddle" /> New Event </a> | <a href="#" onclick="deletePagesIn('newsForm','events.php','events'); return false;"><img src="images/icons/calendar_delete.png" alt="delete" width="16" height="16" border="0" align="absmiddle" /> Delete Selected</a></td>
                        <td width="44%" class="newsHeader">
                            <table width="100%" border="0" cellpadding="3" cellspacing="0">
                                <tr>
                                    <td align="center">
                                        <?php if ($pageNum_rsEvents > 0) { // Show if not first page ?>
                                        <a href="<?php printf("%s?pageNum_rsEvents=%d%s", $currentPage, 0, $queryString_rsEvents); ?>">First 50</a>
                                        <?php } // Show if not first page ?>
                                    </td>
                                    <td align="center">
                                        <?php if ($pageNum_rsEvents > 0) { // Show if not first page ?>
                                        <a href="<?php printf("%s?pageNum_rsEvents=%d%s", $currentPage, max(0, $pageNum_rsEvents - 1), $queryString_rsEvents); ?>">Previous 50</a>
                                        <?php } // Show if not first page ?>
                                    </td>
                                    <td align="center">
                                        <?php if ($pageNum_rsEvents < $totalPages_rsEvents) { // Show if not last page ?>
                                        <a href="<?php printf("%s?pageNum_rsEvents=%d%s", $currentPage, min($totalPages_rsEvents, $pageNum_rsEvents + 1), $queryString_rsEvents); ?>">Next 50</a>
                                        <?php } // Show if not last page ?>
                                    </td>
                                    <td align="center">
                                        <?php if ($pageNum_rsEvents < $totalPages_rsEvents) { // Show if not last page ?>
                                        <a href="<?php printf("%s?pageNum_rsEvents=%d%s", $currentPage, $totalPages_rsEvents, $queryString_rsEvents); ?>">Last 50</a>
                                        <?php } // Show if not last page ?>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" class="newsSummary">
                            <form id="newsForm" name="newsForm" method="post" action="">
                                <table width="100%" border="0" align="center" cellpadding="3" cellspacing="0" class="cms-data-table">
                                    <tr>
                                        <th width="35" align="center">
                                            <input type="checkbox" name="checker" id="checker" onclick="checkAll(this.checked)" />
                                        </th>
                                        <th width="552" align="left"><span class="style1">Articles (Recent First)</span></th>
                                        <th align="center"><span class="style1">Options</span></th>
                                    </tr>
                                    <?php if( $totalRows_rsEvents > 0 ) { ?>
                                    <?php $icount = 1; $count = 1; $cat = ""; do { ?>
                                    <?php if( $cat != $row_rsEvents['Category'] ) { 
															$cat = $row_rsEvents['Category']; 
															$count = 1;
												  ?>
                                    <tr>
                                        <td colspan="3" align="left" class="row_header"><?php echo $row_rsEvents['Category']; ?></td>
                                    </tr>
                                    <?php } ?>
                                    <tr>
                                        <td align="center">
                                            <input type="checkbox" name="item<?php echo $icount; ?>" id="item<?php echo $icount; ?>" value="<?php echo $row_rsEvents['id']; ?>" />
                                        </td>
                                        <td><a href="?edit&amp;id=<?php echo $row_rsEvents['id']; ?>"><?php echo $count . '. '; ?><?php echo $row_rsEvents['EventName']; ?></a></td>
                                        <td width="143" align="center"><a href="?edit&amp;id=<?php echo $row_rsEvents['id']; ?>"><img src="images/icons/newspaper.png" width="16" height="16" border="0" align="absmiddle" /> Edit</a>&nbsp;&nbsp;<img src="images/icons/newspaper_delete.png" alt="del" width="16" height="16" border="0" align="absmiddle" /><a href="utils/del.php?del=events&amp;id=<?php echo $row_rsEvents['id']; ?>&amp;ref=events.php" 
                          onclick="if( !confirm('Delete Section: <?php echo $row_rsEvents['EventName']; ?>?') ) return false; "> Delete</a></td>
                                    </tr>
                                    <?php $icount++; $count++; } while ($row_rsEvents = mysql_fetch_assoc($rsEvents)); ?>
                                    <?php } else { ?>
                                    <tr>
                                        <td colspan="3">No Events Have Posted. <a href="?add">Click here to post a new event</a></td>
                                    </tr>
                                    <?php } ?>
                                </table>
                                <input name="items" type="hidden" id="items" value="<?php echo $icount; ?>" />
                            </form>
                        </td>
                    </tr>
                    <?php } ?>
                    <?php } ?>
                </table>
            </div>
            <!-- InstanceEndEditable -->
        </div>
    </div>
    
    <div id="footer">
    	<?php include('footer.php'); ?>
    </div>
</div>

</body>
<!-- InstanceEnd --></html>
