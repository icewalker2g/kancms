<form action="ajax_pages/themes.php" method="post" enctype="multipart/form-data" name="theme_upload_form" id="theme_upload_form">
    <table align="center" cellpadding="5" cellspacing="0">
        
        <tr valign="baseline">
            <td align="right" nowrap="nowrap" class="sectionTitle1">&nbsp;</td>
            <td>&nbsp;</td>
            <td align="center" valign="top">&nbsp;</td>
        </tr>
        <tr valign="baseline">
            <td align="right" nowrap="nowrap" class="sectionTitle1">&nbsp;</td>
            <td colspan="2"> <span class="small">ZIP file should be the same name as the THEME FOLDER. eg <strong>&quot;snow.zip&quot;</strong> if theme folder is <strong>&quot;snow&quot;</strong></span> </td>
        </tr>
        <tr valign="baseline">
            <td align="right" nowrap="nowrap" class="sectionTitle1"> Theme File: </td>
            <td>
                <input name="ThemeFile" type="file" id="ThemeFile" size="40" />
                <input type="hidden" name="maxlimit" id="maxlimit" value="50000000" />
            </td>
            <td align="center" valign="top">&nbsp;</td>
        </tr>
        <tr valign="baseline">
            <td nowrap="nowrap" align="right">&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
    </table>
    <input type="hidden" name="ThemeFolder" value="" />
    <input type="hidden" name="ThemePreview" id="ThemePreview" value="" />
    <input type="hidden" name="DateAdded" value="<?php echo date('Y-m-d'); ?>" />
    <input type="hidden" name="MM_insert" value="form2" />
    <input type="hidden" name="action" value="import_theme" />
</form>
