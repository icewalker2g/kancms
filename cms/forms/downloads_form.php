<?php
// prevent direct access
if (!defined("SITE_ID")) {
    die("Access Denied");
}

// get an instance of the Download Manager
$downloads = new DownloadsManager();

// use kan_get_parameter, so POST or GET method can be determine automatically
if( isset($_POST['item_id']) && intval($_POST['item_id']) > 0 ) {
    $id = $_POST['item_id'];

    // obtain the article data
    $download = $downloads->getDownload($id);
}

// get all current download categories
$categories = $downloads->getDownloadCategories();

?>
<div class="cms-form-message">
    Complete the form below to add a new file to the downloads list
</div>
<form method="post" enctype="multipart/form-data" name="downloads_form" id="downloads_form"  action="" >
    <table width="600" align="center" cellpadding="4" cellspacing="0">
        <tr >
            <td align="right" nowrap="nowrap" class="sectionTitle1">
                Category:
            </td>
            <td>
                <select name="Category" id="Category" class="required">
                    <option value="">Select Category</option>
                    <option value="">-------------------------------</option>
                    <?php
                    for($i = 0; $i < count($categories); $i++) {
                        $selected = isset($download) && $categories[$i]->getId() == $download->getCategoryID() ? "selected='selected'" : ""; ?>
                    
                        <option value="<?php echo $categories[$i]->getId(); ?>" <?php echo $selected; ?>>
                            <?php echo $categories[$i]->getCategoryName(); ?>
                        </option> <?php
                    }
                    ?>
                </select>
            </td>
        </tr>
        <tr >
            <td align="right" nowrap="nowrap">
                Name:
            </td>
            <td>
                <input name="Name" id="Name" type="text"  size="60" class="required" value="<?php echo isset($download) ? $download->getName() : ""; ?>" />
            </td>
        </tr>
        <tr >
            <td align="right" valign="top" nowrap="nowrap" >
            Alias:
            </td>
            <td>
                <input name="Alias" type="text" id="Alias" size="60" class="alias required" value="<?php echo isset($download) ? $download->getAlias() : ""; ?>" />
            </td>
        </tr>
        <tr >
            <td align="right" valign="top" nowrap="nowrap" >
                File Information:
            </td>
            <td>
                <textarea id="Description" name="Description" class="required" cols="55" rows="10" style="width:100%;"><?php echo isset($download) ? $download->getDescription() : ""; ?></textarea>
            </td>
        </tr>
        <tr >
            <td nowrap="nowrap" align="right">&nbsp;
            </td>
            <td>&nbsp;
            </td>
        </tr>

        <?php if( !isset($download) ) { ?>
        <tr>
            <td colspan="2" nowrap="nowrap" style="text-transform:uppercase; color: #000; font-weight:bold">
                Select File To Upload For Download (<span class="small" style="color:#900;">10 MB File Upload Limit - IF Server permits</span>)
            </td>
        </tr>
        
        <tr>
            <td align="right" nowrap="nowrap" class="sectionTitle1">
                File To Upload:
            </td>
            <td>
                <input name="uploadFile" type="file" id="uploadFile" class="required" size="45" disabled="disabled" />
            </td>
        </tr>
        <tr>
            <td nowrap="nowrap" class="error">&nbsp;</td>
            <td nowrap="nowrap" class="error">
                <input name="destFileName" type="hidden" id="destFileName" value="" />
                <input name="destFolder" type="hidden" id="destFolder" value="../downloads/" />
                <input name="allowOverwrite" type="hidden" id="allowOverwrite" value="yes" />
                <input name="url" type="hidden" id="url" value="../downloads/" />
                <input name="maxlimit" type="hidden" id="maxlimit" value="12000000" />
            </td>
        </tr>
        <tr >
            <td nowrap="nowrap" align="right">&nbsp;
            </td>
            <td class="expand">
                <div id="status" class="sectionTitle1" style="display:none;">
                    Please Wait. Uploading File...&nbsp;
                </div>
            </td>
        </tr>
        <?php } else { ?>
        <tr >
            <td colspan="2" align="center" nowrap="nowrap">
                <input name="url" type="hidden" value="<?php echo isset($download) ? $download->getURL() : ""; ?>" id="url"  />
            </td>
        </tr>
        <?php } ?>
        
        <tr >
            <td colspan="2" align="center" nowrap="nowrap">
                <input name="submit-btn" type="button" value="Add Download" id="dwn-save-btn"  />
                <input name="cancel" type="button" value="Cancel" id="dwn-cancel-btn" />
            </td>
        </tr>
    </table>
    <input type="hidden" name="Thumbnail" value="none" />
    <input type="hidden" name="id" value="<?php echo isset($download) ? $download->getId() : ""; ?>" />
    <input type="hidden" name="action" value="<?php echo !isset($download) ? 'add_download' : 'update_download'; ?>" />
</form>