<?php 
// prevent direct access
if( !defined("SITE_ID") ) {
	die("Access Denied");	
}

$edit_page = NULL;

if( isset($_POST['id']) ) {
	$edit_page = $pm->getPage($_POST['id']);
}


?>

<form action="ajax_pages/pages.php" method="post" name="PageAddForm" id="PageAddForm" class="PagesForm">
    <table width="100%" align="center" cellpadding="6" cellspacing="0">
        <tr >
            <td colspan="2" nowrap="nowrap" class="sectionTitle3">
                Page Definition
            </td>
        </tr>
        <tr >
            <td width="156" align="right" nowrap="nowrap">
                Parent Page:
            </td>
            <td width="543" >
                <select name="ParentID" id="ParentID">
                    <option value="">No Parent [Menu Level Page]</option>
                    <option value="">---------------------------</option>
                    <?php
                        function printPageOptions($page_data, $level = 0) {
                            global $edit_page;
                            
                            foreach($page_data as $data) {
                                $dashes = str_repeat("--",$level);
                                
                                $selected = isset($edit_page) && $edit_page->getParentID() == $data['id'] ? 'selected="selected"' : "";
                                echo "<option value='" . $data['id'] . "' $selected>" . $dashes . " " . $data['PageName'] . "</option>";
                                
                                if( isset($data['children']) ) {
                                    printPageOptions($data['children'], $l = $level + 1);	
                                }
                            }
                        }
                        
                        $parent_pages = $pm->getAdjacencyTree()->getFullNodes( array("id","PageName","PageDescription") ); 
                        printPageOptions($parent_pages);
                    ?>
                </select>
            </td>
        </tr>
        <tr >
            <td align="right" nowrap="nowrap">
                Page Title:
            </td>
            <td >
                <input type="text" id="PageName" name="PageName" size="50" value="<?php echo isset($edit_page) ? $edit_page->getName() : "Page Title"; ?>" />
            </td>
        </tr>
        <tr >
            <td align="right" nowrap="nowrap">
                Page Title Alias:
            </td>
            <td align="left" nowrap="nowrap">
                <input name="PageNameAlias" type="text" class="alias" id="PageNameAlias" value="<?php echo isset($edit_page) ? $edit_page->getAlias() : "page-title"; ?>" size="50" />
            </td>
        </tr>
        <tr >
            <td nowrap="nowrap" align="right">
                Position:
            </td>
            <td>
                <input name="PagePosition" type="text" id="PagePosition" value="<?php echo isset($edit_page) ? $edit_page->getPosition() : "1"; ?>" size="4" style="text-align: center;" />
            </td>
        </tr>
        <tr >
            <td align="right">
                Description:<br />
                <span class="small">(Used As A Meta Data or Tooltip)</span>
            </td>
            <td align="left" nowrap="nowrap">
                <textarea name="PageDescription" cols="50" rows="2" id="PageDescription"><?php echo isset($edit_page) ? htmlentities($edit_page->getDescription()) : ""; ?></textarea>
            </td>
        </tr>
        
        <tr >
            <td align="right"  nowrap="nowrap">
                Content Layout:
            </td>
            <td align="left" nowrap="nowrap">
                <select id="PageLayout" name="PageLayout">
                    <?php
                    
                    $layouts = $site->getTheme()->getOption('layouts');
                    
                    if( count($layouts) > 0 ) {
                        foreach($layouts as $layout => $desc) {
							$selected = isset($edit_page) && $edit_page->getPageLayout() == $layout ? "selected=selected" : "";
                            echo "<option value='{$layout}' {$selected}>{$desc}</option>";
                        }
                    } else {
                       echo "<option value='sections.php'>Default Pages Layout</option>";
                    } ?>
                </select>
            </td>
        </tr>
        <tr <?php echo !isAdminUser() ? 'style="display:none;"' : ""; ?>>
            <td align="right" nowrap="nowrap">
                Redirect To URL:
            </td>
            <td>
                <input class="page-linker" name="RedirectURL" type="text" id="RedirectURL" size="60" value="<?php echo isset($edit_page) ? $edit_page->getRedirectURL() : ""; ?>" />
            </td>
        </tr>
        
        <tr >
            <td align="right" colspan="2">&nbsp;
            </td>
        </tr>
        <tr >
            <td colspan="2" align="left" nowrap="nowrap" class="sectionTitle3">
                Page Summary <!--<span class="small">(Introductory text to page)</span>-->
            </td>
        </tr>
        <!--<tr >
            <td align="right" valign="top">
            Page Summary:<br />
            <span class="small">(Introductory text to page)</span>
            </td>
            <td>
                &nbsp;
            </td>
        </tr>-->
        <tr>
            <td colspan="2">
                <textarea name="PageSummary" id="PageSummary" cols="60" rows="7" style="width: 100%;"><?php echo isset($edit_page) ? kan_make_plain($edit_page->getSummary()) : ""; ?></textarea>
            </td>
        </tr>
        
        
        <tr >
            <td align="right" colspan="2">&nbsp;
            </td>
        </tr>
        <tr >
            <td colspan="2" align="left" nowrap="nowrap" class="sectionTitle3">
                Page Content
            </td>
        </tr>
        <!--<tr >
            <td colspan="2" align="left" valign="top">
                <div class="cms-form-message"><ul>
                    <li>
                        Use either the <strong>Formatted HTML</strong> or <strong>PHP Code</strong> to the main content for the page.
                    </li>
                    <li>
                    Use the <strong>Page Blocks</strong> to provide custom code snippets
                    or static HTML that can be displayed on the sidebar or as part of the content when the page is displayed by the theme.
                    </li>
                </ul></div>
            </td>
        </tr>-->
        
        
        
        <tr >
            <td colspan="2" align="left" valign="top" nowrap="nowrap">
                <div id="page-tabs" class="ui-tabs" style="background: transparent;" >
                    <ul class="ui-tabs-nav">
                        <li id="html-tab" class="ui-tabs-selected">
                            <a id="html" href="#html-pane">Formatted HTML</a>
                        </li>
                        <li id="php-tab">
                            <a id="php" href="#php-pane">PHP Code</a>
                        </li>
                        <!--<li id="blocks-tab" style="margin-left: 80px;">
                            <a id="blocks" href="#blocks-pane">Page Blocks</a>
                        </li>-->
                    </ul>
                    <div id="html-pane" class="ui-tabs-panel">
                        <textarea name="HTMLContent" cols="60" rows="20" id="HTMLContent" style="height:500px; width: 100%;"><?php echo isset($edit_page) && $edit_page->getContentType() == "html" ? kan_make_plain($edit_page->getContent()) : ""; ?></textarea>
                    </div>
                    <div id="php-pane" class="ui-tabs-panel">
                        <div style="border: solid #eee 1px;">
                            <?php 
                                $content = isset($edit_page) && $edit_page->getContentType() == "php" ? kan_make_plain($edit_page->getContent()) : "";
                                $fileUtils->getEditorForContent($content); 
                            ?>
                        </div>
                    </div>
                    <!--<div id="blocks-pane" class="ui-tabs-panel">
                        Page Blocks Here
                    </div>-->
                </div>
                <input type="hidden" name="PageContentType" id="ContentType" value="<?php echo isset($edit_page) ? $edit_page->getContentType() : "html"; ?>"  />
                <textarea name="PageContent" cols="1" rows="1" id="Content" style="position:absolute; z-index: -1;"><?php echo isset($edit_page) ? kan_make_plain($edit_page->getContent()) : ""; ?></textarea>
                <input type="hidden" name="id" id="id" value="<?php echo isset($edit_page) ? $edit_page->getId() : ""; ?>"  />
            </td>
        </tr>
        <tr style="display:none;" >
            <td colspan="2" align="left" nowrap="nowrap" class="sectionTitle3">
                Page Options
            </td>
        </tr>
        <tr style="display:none;">
            <td nowrap="nowrap" align="right">
                Use Accordion Mode:
            </td>
            <td>
                <select name="UseAccordionMode" class="sectionText1" id="UseAccordionMode">
                    <option value="true">Yes</option>
                    <option value="false">No</option>
                </select>
                <span class="small">(Enable / Disable Collapsing of Page Sections)</span>
            </td>
        </tr>
        <tr >
            <td nowrap="nowrap" align="right">&nbsp;
            </td>
            <td>&nbsp;
            </td>
        </tr>
        <tr >
            <td colspan="2" align="center" nowrap="nowrap">
            	<div style="position: fixed; bottom: 45px; left: 50%; margin-left: -100px; width: 400px; text-align: center; background-color: rgba(240,240,240,0.8); padding: 5px; border: solid #ccc 1px; border-radius: 5px;">
                    <input type="submit" class="form_button" id="submit-btn" value="<?php echo isset($edit_page) ? "Update Page" : "Save Page"; ?>" />
                    <input name="button" type="button" class="form_button" id="cancel-btn" value="Cancel"/>
                </div>
            </td>
        </tr>
    </table>
    <input type="hidden" name="MM_insert" value="form2" />
    <input type="hidden" id="action" name="action" value="<?php echo isset($_GET['edit']) ? "update_page" : "add_page"; ?>" />
</form>