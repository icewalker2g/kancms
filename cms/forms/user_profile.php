<?php 
$manager = new UserManager();

if( !isset($_GET['id']) ) {
	trigger_error("ID of User to Edit Not Specified");
	return;
}

$edit_user = $manager->getUser(intval($_GET['id']));

include('users_form.php'); 

?>
<script type="text/javascript" src="../assets/scripts/jquery/js/jquery.js"></script>
<script type="text/javascript" src="scripts/pane_ui.js"></script>
<script type="text/javascript" src="scripts/users.js"></script>
<script type="text/javascript">
$p.addLoadEvent(function() {
	users.bindProfileListeners();	
});
</script>