<?php 
// prevent direct access
if( !defined("SITE_ID") ) {
	die("Access Denied");	
}
?>

<form id="cat-add-form" name="cat-add-form" method="post" action="" >
    <table width="100%" border="0" cellpadding="5" cellspacing="0">
        <!--<tr>
            <td nowrap="nowrap">
                <strong>Parent Category:</strong>
            </td>
            <td>
                <select name="ParentID" id="ParentID">
                    <option value="">No Parent [Top Level Category]</option>
                    <option value="">---------------------------</option>
                    <?php
                        function printPageOptions($page_data, $level = 0) {
                            global $edit_page;
                            
                            foreach($page_data as $data) {
                                $dashes = str_repeat("--",$level);
                                
                                $selected = isset($edit_page) && $edit_page->getParentID() == $data['id'] ? 'selected="selected"' : "";
                                echo "<option id='cat-id-" . $data['id'] . "' value='" . $data['id'] . "' $selected>" . $dashes . " " . $data['CategoryName'] . "</option>";
                                
                                if( isset($data['children']) ) {
                                    printPageOptions($data['children'], $l = $level + 1);	
                                }
                            }
                        }
                        
						// for now a hack to accomodate the article page
						if( $articles ) {
							$parent_articles = $articles->getAdjacencyTree()->getFullNodes();
							printPageOptions($parent_articles);
						}
                    ?>
                </select>
            </td>
        </tr>-->
        <tr>
            <td nowrap="nowrap">
                <strong>Position: </strong>
            </td>
            <td>
                <input name="Position" type="text" id="Position" size="5" style="text-align:center;">
            </td>
        </tr>
        <tr>
            <td nowrap="nowrap">
                <strong>Category Name:</strong>
            </td>
            <td>
                <input name="CategoryName" type="text" id="CategoryName" class="required" size="25" />
            </td>
        </tr>
        <tr>
            <td nowrap="nowrap">
                <strong>Category Alias:</strong>
            </td>
            <td>
                <input class="alias required" name="CategoryAlias" type="text" id="CategoryAlias" size="25">
            </td>
        </tr>
        <tr>
            <td valign="top" nowrap="nowrap"><strong>Description:</strong></td>
            <td>
                <textarea name="CategoryDescription" cols="35" rows="2" id="CategoryDescription"></textarea>
            </td>
        </tr>
        <tr>
            <td>&nbsp;
            </td>
            <td>&nbsp;
            </td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>
                <input name="cat-add-btn" type="button" id="cat-add-btn" value="Add Category" />
                <input name="cat-cancel-btn" type="button" id="cat-cancel-btn" value="Cancel" />
            </td>
        </tr>
    </table>
    <input type="hidden" name="MM_insert2" value="catForm" />
    <input type="hidden" name="id" id="id" value="catForm" />
</form>