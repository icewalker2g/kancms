<?php 
include_once("../utils/file_utils.php");
?>



<form action="" method="post" name="form1" id="form1">
    <table width="100%" align="center" cellpadding="4" cellspacing="0">
        <tr valign="baseline">
            <td style="border-bottom: dashed #ccc 1px;">
                <div id="theme-name" style="font-weight:bold; color: #900; font-size: 16px; "><?php echo $theme->getData('ThemeName'); ?></div>
            </td>
        </tr>
        <tr valign="baseline">
            <td>
                <div id="theme-description" style="border: solid #f5f5f5 1px; padding: 10px;">
					<?php 
					if( $theme->getData("ThemePreview") != "" ) {
						echo $theme->HTML->renderImage( $theme->getData("ThemePreview"), array("align" => "left", "style" => "margin-right: 10px; border: solid #ccc 1px;") );	
					} ?>
					
					<?php echo $theme->getData('ThemeDesc'); ?>
                </div>
            </td>
        </tr>
        
        <!--
        <tr valign="baseline">
            <td nowrap="nowrap" style="border-bottom: dashed #ccc 1px;"><strong>Theme Style Sheets</strong></td>
        </tr>
        <tr valign="baseline">
            <td nowrap="nowrap">
                <table width="100%" border="0" cellspacing="0" cellpadding="6" class="css_table">
                    
                    <?php 
					$stylesheets = $theme->getThemeStylesheets();
					$count = 1;
					foreach($stylesheets as $sheet) {
					?>
                    <tr>
                        <td width="4%"><?php echo $count++ . ". "; ?></td>
                        <td width="63%"><?php echo $sheet->getData('CSSName'); ?></td>
                        <td align="center">
                        	<a href="#"><img src="images/icons/css_go.png" width="16" height="16" border="0" align="absmiddle"> Edit</a>
                            <a href="#"><img src="images/icons/page_white_compressed.png" width="16" height="16" border="0" align="absmiddle"> Export</a>  
                            <a href="#"><img src="images/icons/css_delete.png" width="16" height="16" border="0" align="absmiddle"> Delete</a>
                        </td>
                    </tr><?php 
					} ?>
                </table>
            </td>
        </tr>
        -->
        <tr valign="baseline">
            <td nowrap="nowrap" style="border-bottom: dashed #ccc 1px;"><strong>Theme Files</strong></td>
        </tr>
        <tr valign="baseline">
            <td>
                <?php 
					$folder = kan_fix_path($theme->getData('ThemeFolder'));
					$fileUtils->generateFolderTree($folder);
				 ?>
                <input type="hidden" name="ThemeFolder" value="<?php echo $theme->getData('ThemeFolder'); ?>" size="32" />
            </td>
        </tr>
    </table>
    <input type="hidden" name="ThemePreview" id="ThemePreview" value="<?php echo $theme->getData('ThemePreview'); ?>" />
    <input type="hidden" name="DateAdded" value="<?php echo $theme->getData('DateAdded'); ?>" />
    <input type="hidden" name="MM_update" value="form1" />
    <input type="hidden" name="id" value="<?php echo $theme->getData('id'); ?>" />
</form>
