<?php 


function renderOption($key, $value, $type = "bool" ) { 
	
	if( $type == "bool" ) { ?>
		<label>
			<input type="radio" <?php echo $value == "true" ? "checked=checked" : ""; ?> name="<?php echo $key; ?>" value="true" id="<?php echo $key . "_1"; ?>">Yes
		</label>
		<br>
		<label>
			<input type="radio" <?php echo $value == "false" ? "checked=checked" : ""; ?> name="<?php echo $key; ?>" value="false" id="<?php echo $key . "_2"; ?>"> No
		</label><?php 
	}
	else if( $type == "text" ) { ?>
		<label for="<?php echo $key; ?>">
        	<input type="text" name="<?php echo $key; ?>" value="<?php echo $value; ?>" size="50" />
        </label><?php
	}
}

function renderSettingsTabs() { 
	global $system;
	$categories = $system->getSettingCategories(); ?>
    
	<div id="system-tabs" class="ui-tabs" style="background: transparent;">
        <ul class="ui-tabs-nav"><?php
			for($i = 0; $i < count($categories); $i++) { 
				$category = $categories[$i]; ?>
                <li class="ui-tabs-selected" >
                    <a href="<?php echo "#setting-$i"; ?>"><?php echo $category->getCategoryName(); ?></a>
                </li><?php
			} ?>
        </ul><?php
		
		for($i = 0; $i < count($categories); $i++) {
			$category = $categories[$i]; ?>
            <div id="<?php echo "setting-$i"; ?>" class="ui-tabs-panel">
                <?php renderSettingsUI( $category ); ?>
            </div><?php
		} ?>                    
    </div><?php
}

function renderSettingsUI( $category ) { 
	
	$settings = $category->getSettings() ?> 
    
    <table width="100%" border="0" cellspacing="0" cellpadding="4" class="settings-table"><?php 
        for( $x = 0; $x < count($settings); $x++ ) { 
            $setting = $settings[$x]; ?>
            
            <tr>
                <td width="32%" valign="top">
                    <span class="set-name"><?php echo $setting->getName(); ?></span>
                </td>
                <td width="68%" valign="top">
                    <?php renderOption($setting->getKey(), $setting->getValue(), $setting->getType() ); ?>
                    <span class="set-desc"><?php echo $setting->getDescription(); ?></span>
                </td>
            </tr><?php
        } ?>
    </table><?php
}
?>