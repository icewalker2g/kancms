<?php 
// prevent direct access
if (!defined("SITE_ID")) {
    die("Access Denied");
}

// use kan_get_parameter, so POST or GET method can be determine automatically
if (kan_get_parameter('action') == 'edit_article') {
    $id = kan_get_parameter('id');
    // obtain the article data
    $article = $articles->getArticle($id);
}

$categories = $articles->getArticleCategories();

kan_import('GalleryManager');
$gm = new GalleryManager();
$galleries = $gm->getGalleries();
  
?>
<form id="article-form" name="article-form" method="post" action="">
  <div><table width="96%" border="0" cellspacing="0" cellpadding="4" align="center">
        <tr>
           <td width="148" align="right"><strong>Article Category:</strong></td>
           <td width="436" colspan="2">
                <select name="CategoryID" id="CategoryID" class="required">
                    <option value="">Select Article Category</option>
                    <option value="">------------------------------</option>
					<?php
                        function printPageOptions($page_data, $level = 0) {
                            global $article;
                            
                            foreach($page_data as $data) {
                                $dashes = str_repeat("--",$level);
                                
                                $selected = isset($article) && $data['id'] == $article->getCategoryID() ? "selected='selected'" : "";
                                echo "<option value='" . $data['id'] . "' $selected>" . $dashes . " " . $data['CategoryName'] . "</option>";
                                
                                if( isset($data['children']) ) {
                                    printPageOptions($data['children'], $l = $level + 1);	
                                }
                            }
                        }
                        
                        $parent_articles = $articles->getAdjacencyTree()->getFullNodes();
                        printPageOptions($parent_articles);
                    ?>
                </select>
           </td>
        </tr>
        
        <tr>
           <td width="148" align="right"><strong>Article Title</strong></td>
           <td width="436"><input name="ArticleTitle" type="text" id="ArticleTitle" size="50" class="required" style="width:100%" value="<?php echo isset($article) ? $article->getTitle() : "" ?>" /></td>
           <td width="172" rowspan="3" align="center" valign="top"><div id="picupArea"></div></td>
        </tr>
        <tr>
            <td align="right" valign="top">
                <strong>Title Alias:
                </strong>
            </td>
            <td valign="top">
                <input class="alias" name="ArticleTitleAlias" type="text" id="ArticleTitleAlias" size="50" style="width:100%;" value="<?php echo isset($article) ? $article->getTitleAlias() : "" ?>" />
            </td>
        </tr>
        <tr>
           <td align="right" valign="top"><strong>Article Summary:</strong></td>
           <td valign="top"><textarea name="ArticleSummary" id="ArticleSummary" cols="45" rows="5" style="width:100%;"><?php echo isset($article) ? htmlentities($article->getSummary()) : "" ?></textarea></td>
        </tr>
        <tr>
           <td align="right" valign="top">&nbsp;</td>
           <td colspan="2">&nbsp;</td>
        </tr>
        <tr>
            <td align="right" valign="top">&nbsp;
            </td>
            <td colspan="2" class="small">
            Redirect the article to a specific internal/external URL, i.e. content for the article will not be shown
            </td>
        </tr>
        <tr>
            <td align="right" valign="top">
                <strong>Redirect To URL:</strong>
            </td>
            <td colspan="2">
                <input class="page-linker" name="ArticleURL" type="text" id="ArticleURL" size="65" value="<?php echo isset($article) ? $article->getExternalURL() : "" ?>" />
            </td>
        </tr>
        <tr>
            <td align="right" valign="top">&nbsp;
            </td>
            <td colspan="2">&nbsp;
            </td>
        </tr>
        <tr>
           <td align="right" valign="top"><strong>Article Content:</strong><br /><span class="small">(Use the <strong>&quot;Paste From Word&quot; </strong>option when copying and pasting MS Word Data)</span></td>
           <td colspan="2"><textarea name="ArticleContent" id="ArticleContent" cols="45" rows="20" style="width:100%; height: 420px;"><?php echo isset($article) ? htmlentities($article->getContent()) : "" ?></textarea></td>
        </tr>
        <tr>
            <td align="right" valign="top">&nbsp;</td>
            <td colspan="2">&nbsp;</td>
        </tr>
        <tr>
            <td align="right" valign="top" nowrap="nowrap"><strong>Associated Gallery:</strong></td>
            <td colspan="2">
                <select name="ArticleGallery" id="ArticleGallery">
                    <option>Select Photo Gallery For Article</option>
                    <option>------------------------------</option><?php 
                    
                    for( $i = 0; $i < count($galleries); $i++ ) {
                        $id = $galleries[$i]->getId();
                        $name = $galleries[$i]->getName();
                        $selected = isset($article) && $id == $article->getGalleryID() ? "selected='selected'" : "";
                        echo "<option value='$id' $selected >$name</option>";
                    } ?>
                </select>
            </td>
        </tr>
        <tr>
           <td align="right" valign="top">
               <strong>Allow Comments:</strong>
           </td>
           <td colspan="2">
           	   <?php 
			   	  $yes_selected = isset($article) && $article->commentsAllowed() ? 'selected="selected"' : "";
				  $no_selected = isset($article) && !$article->commentsAllowed() ? 'selected="selected"' : "";
			   ?>
               <select name="AllowComments" id="AllowComments">
                   <option value="1" <?php echo $yes_selected; ?>>Yes</option>
                   <option value="0" <?php echo $no_selected; ?>>No</option>
               </select>
           </td>
        </tr>
        <tr>
           <td align="right" valign="top"><strong>Source / Author:</strong></td>
           <td colspan="2"><input name="ArticleSource" type="text" id="ArticleSource" size="30" value="<?php echo isset($article) ? $article->getAuthor() : "" ?>" /></td>
        </tr>
        <tr>
            <td align="right" valign="top">&nbsp;
            </td>
            <td colspan="2">&nbsp;
            </td>
        </tr>
        <tr>
            <td colspan="3" align="center" valign="top">
                <input type="button" name="sav-art-btn" id="sav-art-btn" value="Save Article" />
                <input type="button" name="can-art-btn" id="can-art-btn" value="Cancel" />
            </td>
            </tr>
     </table>
  </div>
  <input type="hidden" id="ArticleImage" name="ArticleImage" value="<?php echo isset($article) ? $article->getImage() : "" ?>"  />
  <input type="hidden" id="ArticleThumbnail" name="ArticleThumbnail" value="<?php echo isset($article) ? $article->getImageThumbnail() : "" ?>"  />
  <input type="hidden" id="id" name="id" value="<?php echo isset($article) ? $article->getId() : "" ?>"  />
</form>