<?php 

	include(CMS_PATH . 'utils/file.php');
	$folders = array(
		new File('Configuration File', '../core/config.php', '0644'),
		new File('Components Folder', '../assets/components/'),
		new File('Downloads Folder', '../assets/downloads/'),
		new File('Images Folder', '../assets/images/'),
		new File(' -- Banner Images Folder', '../assets/images/banners/'),
		new File(' -- Content Images Folder', '../assets/images/content/'),
		new File(' -- Spotlight Images Folder', '../assets/images/spotlight/'),
		new File('Media Folder', '../assets/media/'),
		new File('Themes Folder', '../assets/themes/')
	);
?>

<table width="100%" border="0" cellpadding="4" cellspacing="0" class="newsTbl" style="width:100%;">
    <tr>
        <td class="sqrtab">Check File / Folder Permissions</td>
    </tr>
    <tr>
        <td class="newsSummary">
        	<p>This page lists the various system folders and the permissions that they should have. If the file or folder write permissions are not like the recommended settings, then you may have challenges editting or uploading files. Ensure the recommend permissions apply to the folders and all subdirectories where present.</p>
            <p>All other folder not listed are expected to have permissions of 0644 to ensure security.</p>
            <table width="100%" border="0" cellpadding="4" cellspacing="0" class="dataTable">
                <tr>
                    <th colspan="3" align="left">Folder</th>
                    <th width="98" align="center">Permission</th>
                    <th width="98" align="center">Recommended</th>
                </tr>
                <?php 
					for($i = 0; $i < count($folders); $i++ ) { 
						$folder = $folders[$i]; ?>
                <tr>
                    <td width="28" align="right"><?php echo ($i + 1) . '.'; ?></td>
                    <td width="231"><?php echo $folder->getFileName(); ?></td>
                    <td width="232"><?php echo $folder->getFilePath(); ?></td>
                    <td align="center"><?php echo $folder->getFilePermissions(); ?></td>
                    <td align="center"><?php echo $folder->getRecommendedPerms(); ?></td>
                </tr>
                <?php } ?>
            </table>
         </td>
    </tr>
</table>
