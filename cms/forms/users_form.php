<?php 
$roles = $manager->getRoles();

$sm = new SitesManager();
$sites = $sm->getSites();

if( isset($_POST['item_id']) && intval($_POST['item_id']) > 0 ) {
    $edit_user = $manager->getUser( intval($_POST['item_id']) );
}

?>
<form action="" method="post" name="form2" id="user_data_form">
    <table width="600" align="center" cellpadding="5" cellspacing="0">
        <tr >
            <td colspan="4" align="left" nowrap="nowrap" class="navSec">User Profile</td>
        </tr>
        <tr >
            <td width="101" align="right" nowrap="nowrap" class="sectionTitle1">First Name:</td>
            <td width="199">
                <input type="text" class="required" name="FirstName" id="FirstName" value="<?php echo kan_make_plain( isset($edit_user) ? $edit_user->getData('FirstName') : "" ); ?>" size="25" />
            </td>
            <td width="77" align="right" nowrap="nowrap" class="sectionTitle1">Last Name:</td>
            <td width="181">
                <input name="LastName" class="required" type="text" id="LastName" value="<?php echo kan_make_plain( isset($edit_user) ? $edit_user->getData('LastName') : "" ); ?>" size="25" />
            </td>
        </tr>
        <tr >
            <td align="right" nowrap="nowrap" class="sectionTitle1">Email:</td>
            <td colspan="3">
                <input name="Email" class="email" type="text" id="Email" value="<?php echo kan_make_plain( isset($edit_user) ? $edit_user->getData('Email') : "" ); ?>" size="40" />
            </td>
        </tr>
        <tr >
            <td colspan="4" align="right" nowrap="nowrap">&nbsp;</td>
        </tr>
        <tr >
            <td colspan="4" nowrap="nowrap" class="navSec">Administrative Profile</td>
        </tr>
        <tr >
            <td align="right" valign="middle" nowrap="nowrap" class="sectionTitle1">Username:</td>
            <td valign="middle">
                <input name="Username" class="required" type="text" id="Username" value="<?php echo kan_make_plain( isset($edit_user) ? $edit_user->getData('Username') : "" ); ?>" size="25" />
            </td>
            
            
            <td colspan="2" align="center" valign="middle">
                <?php if( isset($edit_user) && !is_null($edit_user) ) { ?>
                <div>
                    <a href="#" onclick="users.showPassChanger();">Change Password</a>
                </div>
                <div id="pass-changer" style="height: 150px; width: 280px; display:none; text-align:left; overflow:hidden; border: solid #ccc 1px; padding: 4px; position:absolute; left: auto; top: 140px; background-color: #FFF;">
                    <div id="pass-process" class="small sectionTitle1" style="text-align:center;">
                    </div>
                    <table width="100%" border="0" cellspacing="0" cellpadding="3">
                        <tr>
                            <td class="small"><strong>Old Password:</strong></td>
                            <td>
                                <input type="password" name="OldPassword" id="OldPassword" />
                            </td>
                        </tr>
                        <tr>
                            <td class="small"><strong>New Password:</strong></td>
                            <td>
                                <input type="password" name="NewPassword" id="NewPassword" />
                            </td>
                        </tr>
                        <tr>
                            <td nowrap="nowrap" class="small"><strong>Confirm:</strong></td>
                            <td>
                                <input type="password" name="ConfirmPassword" id="ConfirmPassword" />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" align="center">
                                <input type="button" name="Change" id="Change" value="Change Password" onclick="users.changePassword();" />
                                <input type="button" name="cancelchng" id="cancelchng" value="Cancel" onclick="users.showPassChanger();" />
                            </td>
                        </tr>
                    </table>
                </div>
				<?php } ?>
            </td>
            
        </tr>
        <?php if( !isset($edit_user) ) { ?>
        <tr >
            <td align="right" class="sectionTitle1">Password:</td>
            <td>
                <input type="password" name="Password" id="Password" class="required" value="" size="25" />
            </td>
            <td colspan="2" class="small">(Must Be Atleast Six(6) Characters Long)</td>
        </tr>
        <?php  } ?>
        
        <?php if( isAdminUser() ) { ?>
        <tr >
            <td align="right" valign="top" nowrap="nowrap" class="sectionTitle1">Level:</td>
            <td colspan="3" valign="top">
                <select name="Role" class="small" id="Role" >
                    <?php 
					foreach($roles as $role) {
						if ($_SESSION['User_Role'] != 1 && $role->getId() == 1 ) {
							continue;
						}
						
						$selected = isset($edit_user) && $role->getId() == $edit_user->getRole() ? 'selected="selected"' : "";
						echo "<option value='{$role->getId()}' {$selected}>" . $role->getData('role_name') . "</option>";
					}
					?>
                </select>
            </td>
        </tr>
        <?php } ?>
        <tr >
            <td colspan="4" align="left" nowrap="nowrap">
                <div id="sites_div" style="display:none; height: 150px; overflow: hidden;">
                    <table width="100%" border="0" cellspacing="0" cellpadding="4">
                        <tr>
                            <td width="16%" align="right" valign="top" class="sectionTitle1">Sites:</td>
                            <td width="84%" valign="top">
                                <div style="padding-bottom: 3px;">
                                    <a href="#" onclick="users.showSiteSelector(); return false;">Add Site</a> | <a href="#" onclick="users.deleteSelectedUserSites(<?php echo kan_make_plain( isset($edit_user) ? $edit_user->getData('id') : "" ); ?>); return false;">Delete Selected</a> <span id="ajax_progress" style="display:none">&nbsp;</span>
                                </div>
                                <div id="site_add" style="padding: 5px; display:none; height: 25px; background-color: #f1f1f1; border: solid #ccc 1px; margin-bottom: 10px;">
                                    <select name="cmbsites" class="small" id="cmbsites" onchange="users.addSelectedSite()">
                                        <option value="">Select Site To Add To List</option>
                                        <option value="">-----------------------------</option><?php 
                                        if( isset($sites) ) {
                                            foreach($sites as $s) { ?>
                                            <option value="<?php echo $s->getId(); ?>"><?php echo $s->getData('SiteName'); ?></option><?php
                                            }     
                                        } ?>
                                    </select>
                                    <a href="#" style="margin-left: 15px;" onclick="users.saveSelectedSite(<?php echo kan_make_plain( isset($edit_user) ? $edit_user->getData('id') : "" ); ?>); return false;">Done</a>
                                </div>
                                <div>
                                    <select name="sites" size="6" multiple="multiple" class="small" id="sites" style="width: 300px;">
                                        <?php
                                        
                                        if ( isset($edit_user) ) {
                                            $sites = $edit_user->getUserSites();
                                            
                                            foreach($sites as $site) { ?>
                                                <option value="<?php echo $site->getId() ?>"><?php echo $site->getData('SiteName') ?></option><?php
                                            } 
                                        }
                                        ?>
                                    </select>
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
            </td>
        </tr>
        <tr >
            <td colspan="4" align="center" nowrap="nowrap">
                <input type="button" name="Submit" id="submit-btn"  value="Save Changes" />
                <input type="button" name="button2" id="cancel-btn" value="Cancel Update" />
            </td>
        </tr>
    </table>
    <input type="hidden" name="FullName" value="<?php echo kan_make_plain( isset($edit_user) ? $edit_user->getData('FullName') : "" ); ?>" />
    <input type="hidden" name="MM_update" value="form2" />
    <input type="hidden" name="id" id="userid" value="<?php echo kan_make_plain( isset($edit_user) ? $edit_user->getData('id') : "" ); ?>" />
</form>