<?php 
if( !defined("SITE_ID") ) {
	trigger_error("Access Denied");	
}

kan_import("SitesManager");

$sm = new SitesManager();
$site = $sm->getSite( SITE_ID );
?>


<form action="ajax_pages/pages.php" method="post" enctype="application/x-www-form-urlencoded" name="page_footer_form" id="page_footer_form">
    <table width="100%" border="0" align="center" cellpadding="5" cellspacing="0">
        <tr valign="baseline">
            <td nowrap="nowrap" class="sectionTitle1">
                <div id="home-page-tabs" class="ui-tabs" style="background: transparent;" >
                    <ul class="ui-tabs-nav">
                        <li id="html-tab" class="ui-tabs-selected">
                            <a id="html" href="#html-pane">Use Formatted HTML</a>
                        </li>
                        <!--<li id="php-tab">
                            <a id="php" href="#php-pane">Use PHP Code</a>
                        </li>
                        <li id="blocks-tab" style="margin-left: 80px;">
                            <a id="blocks" href="#blocks-pane">Page Blocks</a>
                        </li>-->
                    </ul>
                    <div id="html-pane" class="ui-tabs-panel">
                        <textarea name="HTMLContent" id="BlockContent" style="height:240px; width: 100%;"><?php echo htmlentities($site->getFooterText()); ?></textarea>
                    </div>
                    <!--<div id="php-pane" class="ui-tabs-panel">
                        <div style="border: solid #eee 1px;">
                            <?php 
                                /*$content = $site->getHomePageText();
                                $fileUtils->getEditorForContent($content); */
                            ?>
                        </div>
                    </div>
                    <div id="blocks-pane" class="ui-tabs-panel">
                        Page Blocks Here
                    </div>-->
                </div>
            
                <textarea name="SiteFooterText" id="SiteFooterText" cols="1" rows="1" style="position:absolute; visibility: hidden;"><?php echo $site->getFooterText(); ?></textarea>
                <input type="hidden" id="ContentType" name="ContentType" value="" />
                <input type="hidden" id="id" name="id" value="<?php echo $site->getSiteID(); ?>"  />
            </td>
        </tr>
    </table>
</form>