<?php 

// get the statistics manager object
//include_once(CORE_PATH . 'StatsManager.php');
//include_once(CORE_PATH . 'SitesManager.php');

$stats = new StatsManager();
$siteManager = new SitesManager();
?>

<table width="100%" border="0" cellpadding="4" cellspacing="0" class="newsTbl" style="width:100%;">
    <tr>
        <td class="sqrtab">Check Site Statistics</td>
    </tr>
    <?php if( !isset($_GET['site']) ) { ?>
    <tr>
        <td class="newsSummary">
            <p>This page shows the current visitor statistics to the various sites in this KAN installation. Clicking a specific site will provide a more detailed visit summary to that particular site.</p>
            <p class="cms-warning">This feature is currently not very comprehensive is does not replace your Host Provider's system statistics features. <br />
                Please note that your own visits to the site are also recorded currently.</p>
            <table width="100%" border="0" cellpadding="4" cellspacing="0" class="cms-data-table">
                <tr>
                    <th colspan="3" align="left">Sites</th>
                    <th width="126" align="center"> Visits Today</th>
                    <th width="128" align="center">Visits Yesterday</th>
                    <th width="118" align="center">% Change</th>
                </tr><?php

                    $allSites = $siteManager->getAllSites();

                    $today = date('Y-m-d');
                    $yesterday = date('Y-m-') . (date('d') - 1);

                    for($i = 0; $i < count($allSites); $i++ ) {
                        $site = $allSites[$i];

                        $visitsToday = $stats->getTotalVisitsFor( $site->getSiteID(), $today );
                        $visitsYest = $stats->getTotalVisitsFor( $site->getSiteID(), $yesterday );

                        if( $visitsYest > 0 ) {
                            $percentageChange = round( (($visitsToday - $visitsYest) * 100) / $visitsYest, 2);
                        } else {
                            $percentageChange = 0;
                        }

                        $siteDetailURL = $_SERVER['REQUEST_URI'] . '&site=' . $site->getSiteID(); ?>

                <tr>
                    <td width="33" align="right"><?php echo ($i + 1) . '.'; ?></td>
                    <td width="270"><a href="<?php echo $siteDetailURL; ?>"><?php echo $site->getSiteName(); ?></a></td>
                    <td width="247"><?php echo $site->getSiteDescription(); ?></td>
                    <td align="center"><?php echo $visitsToday; ?></td>
                    <td align="center"><?php echo $visitsYest; ?></td>
                    <td align="center"><?php echo $percentageChange; ?></td>
                </tr><?php

                    } ?>
            </table>
        </td>
    </tr>

        <?php
    } else if( isset($_GET['site']) ) {
        $site = $siteManager->getSite( $_GET['site'] );
        $statsData = $stats->getSiteStatsData( $site->getSiteID() ); ?>

    <tr>
        <td class="newsHeader">
            <a href="?check=stats">
                <img src="images/icons/house_go.png" alt="home" width="16" height="16" border="0" align="absmiddle" /> Back To Site Statistics Summary
            </a>  |
            <a id="clear-stats" href="#">
                <img src="images/icons/delete.png" alt="clear" width="16" height="16" border="0" align="absmiddle" /> Clear Stats
            </a> |
            <a href="sites.php?launch&amp;id=<?php echo $_GET['site']; ?>">
                <img src="images/icons/page_go.png" alt="manage" width="16" height="16" border="0" align="absmiddle" /> Manage Site
            </a>
        </td>
    </tr>
    <tr>
        <td class="newsSummary"><p>This page shows the current visitor statistics for the <b><?php echo $site->getSiteName(); ?></b>.</p>
            <div class="cms-accordion">
                <div class="cms-accordion-group">
                    <div class="cms-accordion-group-header">Page Hits (20 Highest Hits)</div>
                    <div class="cms-accordion-group-content">
                        <table border="0" cellpadding="4" cellspacing="0" width="100%" class="cms-data-table">
                            <tr>
                                <th align="left" colspan="2">Pages</th>
                                <th width="28%">Hits Today</th>
                            </tr><?php

						for( $i = 0; $i < $stats->getResultCount($statsData); $i++ ) { ?>
							<tr>
								<td width="5%" align="right"><?php echo ($i + 1) . "."; ?></td>
								<td width="67%"><a href="<?php echo $stats->getValue($statsData,'PageURL',$i); ?>" target="_blank"><?php echo $stats->getValue($statsData,'PageTitle',$i); ?></a></td>
								<td align="center"><?php echo $stats->getValue($statsData,'Total',$i); ?></td>
							</tr><?php
						} ?>
                            <tr>
                                <td colspan="2" align="left">&nbsp;</td>
                                <td align="center">&nbsp;</td>
                            </tr>
                            <tr>
                                <td colspan="2" align="left"><strong>Total Hits Today</strong></td>
                                <td align="center"><?php echo $stats->getTotalVisitsFor($site->getSiteID()); ?></td>
                            </tr>

                        </table>
                    </div>
                </div>
            </div>
        </td>
    </tr><?php

    } ?>
</table>

<script type="text/javascript">
var stats = {

	init: function() {
		
		// configure the global ajax experience
		pageManager.ajaxConfig({
			onStart: function() {
				if( pageManager.$('#ajax-progress') )
					$p('#ajax-progress').css('visibility','visible');
			},
			
			onComplete: function() {
				if( pageManager.$('#ajax-progress') )
					$p('#ajax-progress').css('visibility','hidden');
			}
		});
		
		/*pageManager.addEvent('#clear-stats', 'click', function() {
			stats.clearStats();
		});*/
		
		// create a UI Menu using the UI Script framework for the Actions Menu
		var actions_array = [{
			id: "day",
			name: "Clear Stats For Today",
			title: "Clear Statistics For Today Only",
			action: function(mi) {
				stats.clearStats();
			}
		},{
			id: "all",
			name: "Clear All Statistics",
			title: "Clear All Statistics Recorded For This Site",
			action: function(mi) {
				stats.clearAllStats();
			}
		}];

		ui.menu( pageManager.$("#clear-stats"), {items: actions_array});
	},

	clearStats: function() {

		pageManager.showConfirmMessage({
			title: 'Confirm Clearance of Site Statistics',
			content: 'Delete All Statistics For Today',
			okButtonText: 'Delete Stats',
			showOverlay: false,
			okButtonAction: function() {
				var query = "action=clear_site_stats&site_id=<?php echo isset($_GET['site']) ? $_GET['site'] : -1; ?>&range=day&date=<?php echo date('Y-m-d'); ?>";
				pageManager.post('ajax_pages/system.php', query, null, function() {
					history.go(0);
				});
			}
		});
	},
	
	clearAllStats: function() {

		pageManager.showConfirmMessage({
			title: 'Confirm Clearance of Site Statistics',
			content: 'Delete All Recorded Statistics For This Site? (Cannot Be Undone)',
			okButtonText: 'Delete Stats',
			showOverlay: false,
			okButtonAction: function() {
				var query = "action=clear_site_stats&site_id=<?php echo isset($_GET['site']) ? $_GET['site'] : -1; ?>&range=all";
				pageManager.post('ajax_pages/system.php', query, null, function(data) {
					history.go(0);
				});
			}
		});
	}
};

pageManager.addLoadEvent( function() {
	stats.init();
});
</script>