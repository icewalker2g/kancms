<?php 
if( !defined("SITE_ID") ) {
	trigger_error("Access Denied");	
}

kan_import("SitesManager");

$sm = new SitesManager();
$site = $sm->getSite( SITE_ID );
?>

<div class="cms-form-message" style="display: none;">
    <p>Use this form to </p>
    <ul>
        <li>
            enter the text or content that will be shown on the
            site's home page. 
        </li>
        <li>
        enter the text or content to show on the footer of all pages on the site
        </li>
    </ul>
</div>



<form action="ajax_pages/pages.php" method="post" enctype="application/x-www-form-urlencoded" name="home_page_form" id="home_page_form">
    <table width="100%" border="0" align="center" cellpadding="5" cellspacing="0">
        <tr valign="baseline">
            <td nowrap="nowrap" class="sectionTitle1">
                <div id="home-page-tabs" class="ui-tabs" style="background: transparent;" >
                    <ul class="ui-tabs-nav">
                        <li id="html-tab" class="ui-tabs-selected">
                            <a id="html" href="#html-pane">Use Formatted HTML</a>
                        </li>
                        <!--<li id="php-tab">
                            <a id="php" href="#php-pane">Use PHP Code</a>
                        </li>
                        <li id="blocks-tab" style="margin-left: 80px;">
                            <a id="blocks" href="#blocks-pane">Page Blocks</a>
                        </li>-->
                    </ul>
                    <div id="html-pane" class="ui-tabs-panel">
                        <textarea name="HTMLContent" cols="60" rows="15" id="BlockContent" style="height:400px; width: 100%;"><?php echo htmlentities($site->getHomePageText()); ?></textarea>
                    </div>
                    <!--<div id="php-pane" class="ui-tabs-panel">
                        <div style="border: solid #eee 1px;">
                            <?php 
                                /*$content = $site->getHomePageText();
                                $fileUtils->getEditorForContent($content); */
                            ?>
                        </div>
                    </div>
                    <div id="blocks-pane" class="ui-tabs-panel">
                        Page Blocks Here
                    </div>-->
                </div>
            
                <textarea name="SiteHomePageText" id="SiteHomePageText" cols="1" rows="1" style="position:absolute; visibility: hidden;"><?php echo $site->getHomePageText(); ?></textarea>
                <input type="hidden" id="ContentType" name="ContentType" value="" />
                <input type="hidden" id="id" name="id" value="<?php echo $site->getSiteID(); ?>"  />
            </td>
        </tr>
        <tr valign="baseline" style="display: none;">
            <td align="right" valign="top" nowrap="nowrap" >&nbsp;
            </td>
        </tr>
        <tr valign="baseline" style="display: none;">
            <td align="center" nowrap="nowrap">
                <input type="submit" name="submit" id="submit" value="Save Changes">
                <input type="button" name="cancel" id="cancel" value="Cancel" onClick="location.href = '<?php echo $_SERVER['PHP_SELF']; ?>';">
                <input type="hidden" name="action" value="update_homepage" >
            </td>
        </tr>
    </table>
</form>