<?php 
include_once("../utils/file_utils.php");
?>
<form action="" method="post" name="form4" id="create-theme-form">
    <table width="650" align="center" cellpadding="6" cellspacing="0">
        <tr valign="baseline">
            <td nowrap="nowrap" align="right"> <strong>Theme Name:</strong> </td>
            <td>
                <input type="text" id="ThemeName" name="ThemeName" value="" size="50" class="required" />
            </td>
        </tr>
        <tr valign="baseline">
            <td nowrap="nowrap" align="right" valign="top"> <strong>Theme Description:</strong> </td>
            <td>
                <textarea id="ThemeDesc" name="ThemeDesc" cols="60" rows="7"></textarea>
            </td>
        </tr>
        <tr valign="baseline">
            <td nowrap="nowrap" align="right">&nbsp;</td>
            <td class="small">Replace <strong>&lt;folder-name&gt;</strong> with the name of the folder to use for your theme</td>
        </tr>
        <tr valign="baseline">
            <td nowrap="nowrap" align="right"> <strong>Theme Folder:</strong> </td>
            <td>
                <input type="text" id="ThemeFolder" name="ThemeFolder" value="../assets/themes/&lt;folder-name&gt;/" size="40" />
                <?php echo $fileUtils->getFileEditInfo( kan_fix_path('../assets/themes/') ); ?>
            </td>
        </tr>
    </table>
    <input type="hidden" name="ThemePreview" value="" />
    <input type="hidden" name="DateAdded" value="<?php echo date('Y-m-d'); ?>" />
    <input type="hidden" name="MM_insert" value="form4" />
</form>
