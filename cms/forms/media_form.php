<?php 

$mm = new MediaManager(); 
$categories = $mm->getCategories();

if( isset($_POST['item_id']) && intval($_POST['item_id']) > 0 ) {
	$media = $mm->getItem( intval($_POST['item_id']) );
}

?>

<div class="cms-form-message"> Fill the fields below and hit Upload Media to add a new advert to the site
    <ul>
        <li> <strong>Note:</strong> if the <strong>Media Dimensions</strong> do not match the default Width and Height, please specify the new values to prevent image distortion when being displayed on the site to users.</li>
    </ul>
</div>
<form action="" method="post" enctype="multipart/form-data" name="media-form" id="media-form" >
    <table align="center" cellpadding="5" cellspacing="0">
        <tr >
            <td align="right" nowrap="nowrap" class="sectionTitle1">Category:</td>
            <td>
                <select name="MediaCategory" id="MediaCategory" class="required">
                    <option value="">Select Media Category</option>
                    <?php
					foreach($categories as $category) {
						$selected = isset($media) && $media->getData('CategoryID') == $category->getId() ? "selected=selected" : "";
						echo "<option value='" . $category->getId() . "' {$selected}>" . $category->getData('Name'). "</option>";
					} ?>
                </select>
            </td>
            <td width="191" rowspan="6" align="center" valign="top">
                <div id="picupArea"></div>
                <input name="MediaThumbnail" id="MediaThumbnail" type="hidden" value="<?php echo isset($media) ? $media->getData('MediaThumbnail') : ""; ?>" />
            </td>
        </tr>
        <tr >
            <td width="137" align="right" nowrap="nowrap" class="sectionTitle1">Title:</td>
            <td>
                <input name="MediaName" type="text" class="required" id="MediaName" value="<?php echo isset($media) ? $media->getData('MediaName') : ""; ?>" size="50" />
            </td>
        </tr>
        <tr >
            <td align="right" valign="top" nowrap="nowrap" class="sectionTitle1">Description:</td>
            <td>
                <textarea name="MediaDesc" cols="50" rows="5"><?php echo isset($media) ? $media->getData('MediaDesc') : ""; ?></textarea>
            </td>
        </tr>
        <tr >
            <td align="right" nowrap="nowrap" class="sectionTitle1">Artiste:</td>
            <td>
                <input name="MediaArtiste" type="text" id="MediaArtiste" value="<?php echo isset($media) ? $media->getData('MediaArtiste') : ""; ?>" size="40">
            </td>
        </tr>
        <tr >
            <td align="right" nowrap="nowrap" class="sectionTitle1">Genre:</td>
            <td>
                <input name="MediaGenre" type="text" id="MediaGenre" value="<?php echo isset($media) ? $media->getData('MediaGenre') : ""; ?>" size="20">
            </td>
        </tr>
        <tr >
            <td align="right" nowrap="nowrap" class="sectionTitle1">File Type:</td>
            <td width="317">
                <select name="MediaType" id="MediaType"  >
                    <option value="" selected>Select Media Type</option>
                    <?php 
						$options = array(
							'jpg' => 'JPEG Image',
							'gif' => 'GIF Image',
							'png' => 'PNG Image',
							'swf' => 'Flash Animation',
							'mp3' => 'MP3 Audio',
							'mp4' => 'MP4 Video',
							'wmv' => 'Windows Media Video',
							'amr' => 'AMR Audio'
						); 
						
						foreach($options as $key => $value) {
							$selected = isset($media) && $media->getData('MediaType') == $key ? "selected=selected" : "";
							echo "<option value='$key' {$selected}>$value</option>";
						}
					?>
                </select>
                <span class="sectionTitle1">Media Duration
                <input name="MediaDuration" type="text" id="MediaDuration" value="<?php echo isset($media) ? $media->getData('MediaDuration') : "0:00"; ?>" size="8" style="text-align:center" />
                </span></td>
        </tr>
        <tr >
            <td align="right" valign="top" nowrap="nowrap" class="sectionTitle1">&nbsp;</td>
            <td colspan="2" class="small">&nbsp;</td>
        </tr>
        <tr >
            <td align="right" valign="top" nowrap="nowrap" class="sectionTitle1">&nbsp;</td>
            <td colspan="2" class="small">(To Use External or Complicated Media, you may copy and paste Object / Embed code here)</td>
        </tr>
        <tr >
            <td align="right" valign="top" nowrap="nowrap" class="sectionTitle1"> Embed Code</td>
            <td colspan="2" valign="top">
                <textarea name="MediaEmbedCode" cols="75" rows="7" id="MediaEmbedCode"><?php echo isset($media) ? $media->getData('MediaEmbedCode') : ""; ?></textarea>
            </td>
        </tr>
        <tr >
            <td align="right" nowrap="nowrap" class="sectionTitle1">&nbsp;</td>
            <td colspan="2" class="small">&nbsp;</td>
        </tr>
        <?php 
		// hide if editing
		if( !isset($media) ) { ?>
        <tr >
            <td align="right" nowrap="nowrap" class="sectionTitle1">&nbsp;</td>
            <td colspan="2" class="small">(Simple or small media may be alternatively  uploaded here)<br />
                <strong>Maximum Upload File Size: 10MB (If PHP settings permit)</strong></td>
        </tr>
        <tr >
            <td align="right" nowrap="nowrap" class="sectionTitle1">File To Upload:</td>
            <td colspan="2">
                <input name="uploadFile" type="file" class="course_insidetext" id="uploadFile" size="45" />
            </td>
        </tr>
        <tr >
            <td align="right" nowrap="nowrap" class="sectionTitle1">&nbsp;</td>
            <td colspan="2" >
                <input name="destFileName" type="hidden" id="destFileName" value="<?php echo "media"; ?>" />
                <input name="destFolder" type="hidden" id="mediaDestFolder" value="" />
                <input name="allowOverwrite" type="hidden" value="yes" />
                <!--<input name="MediaPath" type="hidden" id="MediaPath" value="" />-->
                <input name="ImageFile" type="hidden" id="ImageFile" value="<?php echo "media.jpg"; ?>" />
                <input name="maxlimit" type="hidden" id="maxlimit" value="100000000" />
                <!--<input name="allowed_ext" type="hidden" id="allowed_ext" value="jpg,png,swf,jar,mp3" />--> 
            </td>
        </tr>
        <?php 
		} // end if condition ?>
        <tr >
            <td align="right" nowrap="nowrap" class="sectionTitle1">Or Select File:</td>
            <td colspan="2">
                <input name="MediaPath" type="text" id="MediaPath" size="50" value="<?php echo isset($media) ? $media->getData('MediaPath') : ""; ?>" />
                <a href="#" class="file_finder">
                	<img src="images/icons/folder_explore.png" alt="folder" width="16" height="16" border="0" align="absmiddle" />
                </a> 
                
                <div id="file_browser_container">
                	<div class="closer"><a href='#' class="closer">Close</a></div>
                	<div id="file_browser">&nbsp;</div>
                </div>
            </td>
        </tr>
        <tr >
            <td align="right" valign="top" nowrap="nowrap" class="sectionTitle1">&nbsp;</td>
            <td colspan="2">&nbsp;</td>
        </tr>
        <tr >
            <td align="right" valign="top" nowrap="nowrap" class="sectionTitle1">&nbsp;</td>
            <td colspan="2"><span class="small">(Select a pre-defined media dimension or manually specify size below)</span></td>
        </tr>
        <tr >
            <td align="right" nowrap="nowrap" class="sectionTitle1">Media Dimensions</td>
            <td colspan="2">
                <select name="MediaSize" id="MediaSize" >
                    <?php 
					$options = array('','320x240','480x320','640x480');
					
					foreach($options as $option) {
						$info = isset($media) ? $media->getData('MediaWidth') . "x" . $media->getData('MediaHeight') : "";
						$selected = $info == $option ? "selected=selected" : "";
						echo "<option value='$option' {$selected}>$option</a>";	
					}
					?>
                </select>
                <span class="sectionTitle1"> Width:
                <input name="MediaWidth" type="text" class="small" id="MediaWidth" value="<?php echo isset($media) ? $media->getData('MediaWidth') : "320"; ?>" size="6" />
                Height:
                <input name="MediaHeight" type="text" class="small" id="MediaHeight" value="<?php echo isset($media) ? $media->getData('MediaHeight') : "240"; ?>" size="6" />
                </span> </td>
        </tr>
        <tr >
            <td nowrap="nowrap" align="right">&nbsp;</td>
            <td colspan="2">&nbsp;</td>
        </tr>
        <tr >
            <td colspan="3" align="center" nowrap="nowrap">
                <input name="media-add-btn" type="submit" id="media-add-btn" value="Upload Media" />
                <input type="button" name="media-cancel-btn" id="media-cancel-btn" value="Cancel Upload"  />
            </td>
        </tr>
    </table>
    <input type="hidden" name="MM_insert" value="form1" />
    <input type="hidden" name="action" value="<?php echo !isset($media) ? 'add_media' : 'update_media'; ?>" />
    <input type="hidden" name="id" value="<?php echo isset($media) ? $media->getId() : ""; ?>" />
</form>
