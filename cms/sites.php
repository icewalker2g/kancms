<?php 
// block access to this script if not a valid user and import the base KAN settings
include_once("accesscheck.php"); 

// import the SiteManager library
//kan_import('SitesManager');
//kan_import('ThemeManager');

// create an instance of the SiteManager
$siteManager = new SitesManager();
$themesManager = new ThemeManager();

// if the launch option has been chosen, redirect early so we do not
// do anymore queries
if( isset($_GET['launch']) ) {
	if( !isset($_SESSION) ) {
		session_start();
	}
	
	$site = $siteManager->getSite($_GET['id']);
	
	$_SESSION['siteid'] = $site->getSiteID();
	$_SESSION['SiteName'] = $site->getSiteName(); 
	$_SESSION['SiteIdentifier'] = $site->getSiteIdentifier();
	
	$pageToGo = "pages.php";
	header("Location: " . $pageToGo );
	exit;
}

// get the current page
$currentPage = $_SERVER["PHP_SELF"];

$editFormAction = $currentPage;
if (isset($_SERVER['QUERY_STRING'])) {
  $editFormAction .= "?" . htmlentities($_SERVER['QUERY_STRING']);
}

// if the user has chosen to update the site, then update the page and move on
if ((isset($_POST["MM_update"])) && ($_POST["MM_update"] == "form2")) {
  
  $siteManager->updateSite();
  
  $updateGoTo = $_SERVER['PHP_SELF'];

  if( isset($_GET['ref'])) {
      $updateGoTo = $_GET['ref'];
  }
  header(sprintf("Location: %s", $updateGoTo));
}

// if the user has chosen to create a new site, create site and then redirect back to the main page
if ((isset($_POST["MM_insert"])) && ($_POST["MM_insert"] == "form1")) {

  $created = $siteManager->createNewSite();
  
  if( !$created ) {
	 echo "<br />Failed To Create Site. Please Check If Any Errors Are Present";  
	 return;
  }
  
  $insertGoTo = $_SERVER['PHP_SELF'];
  header(sprintf("Location: %s", $insertGoTo));
}

/**
 * If we are attempting an edit
 */
$colname_rsEditSite = "-1";
if (isset($_GET['id'])) {
  $colname_rsEditSite = $_GET['id'];
  $site = $siteManager->getSite($colname_rsEditSite);
}

// prepare to load the list of available sites
$siteManager_list = null;

if( $_SESSION['CMS_UserGroup'] == "specific" ) {
	$user = $_SESSION['UserID'];
	$query_rsSites = "SELECT * FROM sites WHERE sites.id IN (SELECT site FROM users_sites WHERE user = $user) ORDER BY id ASC";

    $siteManager_list = $siteManager->getSites(0, NULL, array(
        'fields' => array('id','SiteName','SiteActive','SiteIdentifier','SiteType'),
        "filter" => array("id IN" => "SELECT site FROM users_sites WHERE user = $user"),
        "order" => array("SiteType" => "ASC", "SiteName" => "ASC")
    ));
} else {
    $siteManager_list = $siteManager->getSites(0, NULL, array(
        'fields' => array('id','SiteName','SiteActive','SiteIdentifier','SiteType'),
        "order" => array("SiteType" => "ASC", "SiteName" => "ASC")
    ));
}

// get all the available
$themes = $themesManager->getThemes();

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/cms.dwt.php" codeOutsideHTMLIsLocked="false" -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<!-- InstanceBeginEditable name="doctitle" -->
<title><?php echo getSetting('CMSTitle','KAN Content Management System'); ?></title>
<!-- InstanceEndEditable -->
<link rel="shortcut icon" type="image/png" href="../favicon.png" />
<link rel="stylesheet" type="text/css" href="css/widestyles.css"/>
<link rel="stylesheet" type="text/css" href="css/cms.css"/>
<link rel="stylesheet" type="text/css" href="css/cms-ui.css"/>
<link rel="stylesheet" type="text/css" href="css/util.css"/>

<script type="text/javascript" src="../assets/scripts/jquery/js/jquery.js"></script>
<script type="text/javascript" src="../assets/scripts/jquery/js/jquery-ui.js"></script>
<script type="text/javascript" src="../assets/scripts/common/pageManager.js"></script>
<script type="text/javascript" src="scripts/system.js"></script>
<script type="text/javascript" src="scripts/ui.js"></script>

<!-- InstanceBeginEditable name="head" -->
<link rel="stylesheet" type="text/css" href="css/sites.css"/>
<!-- InstanceEndEditable -->
</head>

<body>

<div id="page">
	<div id="header">
    	<?php include('logo_header.php'); ?>
    </div>
    
    <?php if( isset($_SESSION['CMS_Username']) ) { ?>
    <div id="site-info-bar">
		<?php include('system_menu.php'); ?>
    </div>
    <?php } ?>
    
    
    <div id="content-wrapper">
    	<div id="content-header">
        	<span>
			<!-- InstanceBeginEditable name="section title" -->SITE MANAGEMENT HOMEPAGE<!-- InstanceEndEditable -->
            </span>
        </div>
    	<div id="content">
			<!-- InstanceBeginEditable name="content" -->
            <div id="nav">
                <?php include('nav_section.php'); ?>
            </div>
            <div id="main">
                <table border="0" cellspacing="0" cellpadding="5" class="newsTbl">
                    <?php if( isset($_GET['edit']) ) { ?>
                    <tr>
                        <td valign="top" class="bigheader">
                            Edit Selected Site
                        </td>
                    </tr>
                    <tr>
                        <td valign="top" class="newsSummary">
                            <p class="cms-warning">Edit the fields below to update the information for the selected site. Fields marked as <span style="color: red;">*</span> are required </p>
                            <form action="<?php echo $editFormAction; ?>" method="post" name="form2" id="form2">
                                <table width="96%" align="center" cellpadding="5" cellspacing="0" >
                                    <tr valign="baseline">
                                        <td colspan="3" align="left" valign="middle" nowrap="nowrap" class="sectionTitle3" style="color: #000">
                                            Site Definition
                                        </td>
                                    </tr>
                                    <tr valign="baseline">
                                        <td width="114" align="right" valign="middle" nowrap="nowrap" class="sectionTitle1">
                                            * Site Identifier:
                                        </td>
                                        <td width="374" valign="baseline">
                                            <input name="SiteIdentifier" type="text" id="SiteIdentifier" class="required" required="" value="<?php echo $site->getData('SiteIdentifier'); ?>" size="20" />
                                        </td>
                                        <td width="167" align="center" valign="top" rowspan="8">
                                            <div style="text-align: center; margin-bottom: 5px;"><b>Site Logo</b></div>
                                            <div id="picupArea">&nbsp; </div>
                                            <input type="hidden" name="SiteLogo" id="SiteLogo" value="<?php echo $site->getData('SiteLogo'); ?>"  />
                                        </td>
                                    </tr>
                                    <tr valign="baseline">
                                        <td align="right" valign="middle" nowrap="nowrap" class="sectionTitle1">
                                            * Site Name:
                                        </td>
                                        <td valign="baseline">
                                            <input type="text" name="SiteName" class="required" required value="<?php echo htmlentities($site->getData('SiteName'), ENT_COMPAT, 'iso-8859-1'); ?>" size="40" />
                                        </td>
                                    </tr>
                                    
                                    <tr valign="baseline">
                                        <td align="right" valign="middle" nowrap="nowrap" class="sectionTitle1">
                                            Site Slogan:
                                        </td>
                                        <td>
                                            <input name="SiteSlogan" type="text" id="SiteSlogan" placeholder="Organization Slogan" value="<?php echo $site->getData('SiteSlogan'); ?>" size="40" maxlength="255" />
                                        </td>
                                    </tr>
                                    <tr valign="baseline">
                                        <td align="right" valign="middle" nowrap="nowrap" class="sectionTitle1">
                                            Site Domain:
                                        </td>
                                        <td>
                                            <input name="SiteDomain" type="text" id="SiteDomain" placeholder="e.g. www.mysite.com" value="<?php echo $site->getData('SiteDomain'); ?>" size="40" />
                                        </td>
                                    </tr>
                                    <tr valign="baseline">
                                        <td align="right" valign="middle" nowrap="nowrap" class="sectionTitle1">
                                            Site Email:
                                        </td>
                                        <td>
                                            <input name="SiteEmail" type="email" id="SiteEmail" placeholder="e.g. info@mysite.com" value="<?php echo $site->getData('SiteEmail'); ?>" size="40" />
                                        </td>
                                    </tr>
                                    <tr valign="baseline">
                                        <td align="right" valign="top" nowrap="nowrap" class="sectionTitle1">
                                            Address:
                                        </td>
                                        <td>
                                            <textarea name="SiteAddress" id="SiteAddress" cols="40" rows="2" placeholder="Organisation Postal Address"><?php echo $site->getData('SiteAddress'); ?></textarea>
                                        </td>
                                    </tr>
                                    <tr valign="baseline">
                                        <td align="right" valign="middle" nowrap="nowrap" class="sectionTitle1">
                                         Contact Number(s):
                                        </td>
                                        <td>
                                            <input name="SitePhone" type="text" id="SitePhone" placeholder="Organisation Phone Numbers" value="<?php echo $site->getData('SitePhone'); ?>" size="40" />
                                        </td>
                                    </tr>
                                    <tr valign="baseline">
                                        <td align="right" valign="top" nowrap="nowrap" class="sectionTitle1">&nbsp;
                                        </td>
                                        <td>&nbsp;
                                        </td>
                                    </tr>
                                </table>
                                <table width="96%" border="0" align="center" cellpadding="5" cellspacing="0">
                                    <tr valign="baseline">
                                        <td colspan="2" align="left" valign="top" nowrap="nowrap" class="sectionTitle3" style="color: #333">
                                            Site Customisation
                                        </td>
                                    </tr>
                                    <tr valign="baseline">
                                        <td align="right" valign="top" nowrap="nowrap" class="sectionTitle1">
                                            Site Favicon:
                                        </td>
                                        <td>
                                            <input name="SiteFavicon" type="text" id="SiteFavicon" placeholder="Path to icon to show when site loads" value="<?php echo $site->getData('SiteFavicon'); ?>" size="40" />
                                        </td>
                                    </tr>
                                    <tr valign="baseline">
                                        <td width="133" align="right" valign="top" nowrap="nowrap">
                                            <span class="sectionTitle1">Site Description:</span><br />
                                            <span class="small">(Used By Meta Data)</span>
                                        </td>
                                        <td width="549">
                                            <textarea name="SiteDescription" cols="60" rows="3"  placeholder="General Description of the Site which is used by Search Engines" style="width:100%;"><?php echo htmlentities($site->getData('SiteDescription'), ENT_COMPAT, 'iso-8859-1'); ?></textarea>
                                        </td>
                                    </tr>
                                    <tr valign="baseline">
                                        <td align="right" valign="top" nowrap="nowrap">
                                            <span class="sectionTitle1">Site Keywords:</span><br />
                                            <span class="small">(Used By Meta Data)</span>
                                        </td>
                                        <td>
                                            <textarea name="SiteKeywords" cols="60" rows="4"  id="SiteKeywords" placeholder="List of Keywords used by search engines to determine how to identify your site when people search" style="width:100%;"><?php echo $site->getData('SiteKeywords'); ?></textarea>
                                        </td>
                                    </tr>
                                    <tr valign="baseline" style="display:none;">
                                        <td align="right" valign="top" nowrap="nowrap" class="sectionTitle1">
                                            Home Page Text:
                                        </td>
                                        <td>
                                            <textarea name="SiteHomePageText" id="SiteHomePageText" cols="50" rows="20" style="width: 100%;"><?php echo $site->getData('SiteHomePageText'); ?></textarea>
                                        </td>
                                    </tr>
                                    <tr valign="baseline" style="display: none;">
                                        <td align="right" valign="top"  >
                                            <span class="sectionTitle1">Footer Text:</span><br />
                                            <span class="small">Text that can appear at the bottom of all pages on site, e.g. Copyright Notices etc</span>
                                        </td>
                                        <td>
                                            <textarea name="SiteFooterText" id="SiteFooterText" cols="50" rows="6" style="width:100%;"><?php echo $site->getData('SiteFooterText'); ?></textarea>
                                        </td>
                                    </tr>
                                    <tr valign="baseline">
                                        <td align="right" valign="top" nowrap="nowrap" class="sectionTitle1">&nbsp;
                                        </td>
                                        <td>&nbsp;
                                        </td>
                                    </tr>
                                    <tr valign="baseline">
                                        <td align="right" valign="middle" nowrap="nowrap" class="sectionTitle1">
                                           * Site Theme
                                        </td>
                                        <td>
                                            <select name="SiteThemeID" id="SiteThemeID" onchange="sites.loadCSSListForTheme(this.options[this.selectedIndex].value);">
                                                <?php foreach($themes as $theme) { ?>
                                                <option value="<?php echo $theme->getId(); ?>" <?php if (!(strcmp( $theme->getId(), $site->getData('SiteThemeID')))) {echo "selected=\"selected\"";} ?>><?php echo $theme->getData('ThemeName'); ?></option>
                                                <?php } ?>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr valign="baseline">
                                        <td align="right" valign="middle" nowrap="nowrap" class="sectionTitle1">
                                            * Theme Style:
                                            <input name="TempCSSID" type="hidden" id="TempCSSID" value="<?php echo $site->getData('SiteThemeCSSID'); ?>" />
                                        </td>
                                        <td align="left" nowrap="nowrap" class="small">
                                            <div id="theme_list">
                                                (Select Theme To Choose Style)
                                            </div>
                                        </td>
                                    </tr>
                                    <tr valign="baseline">
                                        <td colspan="2" align="center" valign="middle" nowrap="nowrap">&nbsp;
                                        </td>
                                    </tr>
                                    <tr valign="baseline">
                                        <td colspan="2" align="center" valign="middle" nowrap="nowrap">
                                            <input type="submit" value="Save Changes" />
                                            &nbsp;
                                            <input type="button" name="Cancel2" id="Cancel2" value="Cancel" onclick="window.location.href = '<?php echo $_SERVER['PHP_SELF']; ?>';" />
                                        </td>
                                    </tr>
                                </table>
                                <input type="hidden" id="SiteCMSLogo" name="SiteCMSLogo" value="<?php echo htmlentities($site->getData('SiteCMSLogo'), ENT_COMPAT, 'iso-8859-1'); ?>" />
                                <input type="hidden" name="MM_update" value="form2" />
                                <input type="hidden" id="SiteType" name="SiteType" value="<?php echo $site->getData('SiteType'); ?>" />
                                <input type="hidden" name="id" value="<?php echo $site->getId(); ?>" />
                            </form>
                        </td>
                    </tr>
                    <?php } ?>
                    <?php if( isset($_GET['add']) ) { ?>
                    <tr>
                        <td valign="top" class="bigheader">
                            Create New Site
                        </td>
                    </tr>
                    <tr>
                        <td valign="top" class="newsSummary">
                            <p class="cms-warning">Complete the fields below to create a new site. Fields marked with <span style="color: red;">*</span> are required
                            </p>
                            <form action="<?php echo $editFormAction; ?>" method="post" name="form1" id="form1">
                                <table width="96%" align="center" cellpadding="5" cellspacing="0" >
                                    <tr valign="baseline">
                                        <td colspan="3" align="left" valign="middle" nowrap="nowrap" class="sectionTitle3" style="color: #000">
                                            Site Definition
                                        </td>
                                    </tr>
                                    <tr valign="baseline">
                                        <td width="132" align="right" nowrap="nowrap" class="sectionTitle1">
                                            * Site Identifier:
                                        </td>
                                        <td width="364" >
                                            <input name="SiteIdentifier" type="text" id="SiteIdentifier" size="20" class="required" required placeholder="Site ID" value="<?php echo "site" . ($siteManager->count( $siteManager->getItemClassName() ) + 1); ?>" />
                                        </td>
                                        <td width="174" align="center" valign="top" rowspan="7">
                                            <div style="text-align: center; margin-bottom: 5px;"><b>Site Logo</b></div>
                                            <div id="picupArea">&nbsp;</div>
                                            <input type="hidden" name="SiteLogo" id="SiteLogo" value=""  />
                                        </td>
                                    </tr>
                                    <tr valign="baseline">
                                        <td align="right" valign="top" nowrap="nowrap" class="sectionTitle1">
                                           * Site Name:
                                        </td>
                                        <td valign="baseline">
                                            <input name="SiteName" type="text" id="SiteName" size="40" class="required" required placeholder="Site Name Here" />
                                        </td>
                                    </tr>
                                    
                                    <tr valign="baseline">
                                        <td align="right" valign="top" nowrap="nowrap" class="sectionTitle1">
                                            Site Slogan:
                                        </td>
                                        <td>
                                            <input name="SiteSlogan" type="text" id="SiteSlogan" size="40" maxlength="255" placeholder="Organization Slogan" />
                                        </td>
                                    </tr>
                                    <tr valign="baseline">
                                        <td align="right" valign="top" nowrap="nowrap" class="sectionTitle1">
                                            Site Domain:
                                        </td>
                                        <td>
                                            <input name="SiteDomain" type="text" id="SiteDomain" size="40" placeholder="e.g. www.mysite.com" />
                                        </td>
                                    </tr>
                                    <tr valign="baseline">
                                        <td align="right" valign="top" nowrap="nowrap" class="sectionTitle1">
                                            Site Email:
                                        </td>
                                        <td>
                                            <input name="SiteEmail" type="email" id="SiteEmail" size="40" placeholder="e.g. joe@kancms.com" />
                                        </td>
                                    </tr>
                                    <tr valign="baseline">
                                        <td align="right" valign="top" nowrap="nowrap" class="sectionTitle1">
                                            Address:
                                        </td>
                                        <td>
                                            <textarea name="SiteAddress" id="SiteAddress" cols="40" rows="2" placeholder="Organization Postal Address"></textarea>
                                        </td>
                                    </tr>
                                    <tr valign="baseline">
                                        <td align="right" valign="middle" nowrap="nowrap" class="sectionTitle1">
                                            Contact Number(s):
                                        </td>
                                        <td>
                                            <input name="SitePhone" type="text" id="SitePhone" size="40" placeholder="Phone Number Here" />
                                        </td>
                                    </tr>
                                    <tr valign="baseline">
                                        <td align="right" valign="middle" nowrap="nowrap" class="sectionTitle1">&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td align="center" valign="top">&nbsp;</td>
                                    </tr>
                                </table>
                                <table width="96%" border="0" align="center" cellpadding="5" cellspacing="0">
                                    <tr valign="baseline">
                                        <td colspan="2" align="left" valign="top" nowrap="nowrap" class="sectionTitle3" style="color: #000">
                                            Site Customisation
                                        </td>
                                    </tr>
                                    <tr valign="baseline">
                                        <td align="right" valign="top" nowrap="nowrap" class="sectionTitle1">
                                            Site Favicon:
                                        </td>
                                        <td>
                                            <input name="SiteFavicon" type="text" id="SiteFavicon" value="../favicon.png" size="40" />
                                        </td>
                                    </tr>
                                    <tr valign="baseline">
                                        <td width="138" align="right" valign="top" nowrap="nowrap">
                                            <span class="sectionTitle1">Site Description:</span><br />
                                            <span class="small">(Used By Meta Data)</span>
                                        </td>
                                        <td width="544">
                                            <textarea name="SiteDescription" cols="60" rows="3"  style="width:100%;" placeholder="General Description of this website, used by search engines"></textarea>
                                        </td>
                                    </tr>
                                    <tr valign="baseline">
                                        <td align="right" valign="top" nowrap="nowrap">
                                            <span class="sectionTitle1">Site Keywords:</span><br />
                                            <span class="small">(Used By Meta Data)</span>
                                        </td>
                                        <td>
                                            <textarea name="SiteKeywords" cols="60" rows="4"  id="SiteKeywords" style="width:100%;" placeholder="Keywords Used By Search Engines To Locate This Website"></textarea>
                                        </td>
                                    </tr>
                                    <tr valign="baseline" style="display: none;">
                                        <td align="right" valign="top" nowrap="nowrap" class="sectionTitle1">
                                            <p>Home Page Text
                                                :</p>
                                        </td>
                                        <td>
                                            <textarea name="SiteHomePageText" id="SiteHomePageText" rows="20" style="width: 100%;"></textarea>
                                        </td>
                                    </tr>
                                    <tr valign="baseline" style="display:none;">
                                        <td align="right" valign="top" nowrap="nowrap" class="sectionTitle1">
                                            Footer Text:
                                        </td>
                                        <td>
                                            <textarea name="SiteFooterText" id="SiteFooterText" cols="50" rows="6" style="width:100%;"></textarea>
                                        </td>
                                    </tr>
                                    <tr valign="baseline">
                                        <td align="right" valign="top" nowrap="nowrap" class="sectionTitle1">&nbsp;
                                        </td>
                                        <td>&nbsp;
                                        </td>
                                    </tr>
                                    <tr valign="baseline">
                                        <td align="right" valign="middle" nowrap="nowrap" class="sectionTitle1">
                                            * Site Theme:
                                        </td>
                                        <td>
                                            <select class="required" required name="SiteThemeID" id="SiteThemeID" onchange="sites.loadCSSListForTheme(this.options[this.selectedIndex].value);">
                                                <option value="">Select Theme</option>
                                                <?php foreach($themes as $theme) { ?>
                                                <option value="<?php echo $theme->getId(); ?>" ><?php echo $theme->getData('ThemeName'); ?></option>
                                                <?php } ?>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr valign="baseline">
                                        <td align="right" valign="middle" nowrap="nowrap" class="sectionTitle1">
                                            * Theme Style:
                                        </td>
                                        <td align="left" nowrap="nowrap" class="small">
                                            <div id="theme_list">
                                                (Select Theme To Choose Style)
                                            </div>
                                        </td>
                                    </tr>
                                    <tr valign="baseline">
                                        <td colspan="2" align="center" nowrap="nowrap">&nbsp;
                                        </td>
                                    </tr>
                                    <tr valign="baseline">
                                        <td colspan="2" align="center" nowrap="nowrap">
                                            <input type="submit" value="Create Site" />
                                            &nbsp;
                                            <input type="button" name="Cancel" id="Cancel" value="Cancel" onclick="window.location.href = '<?php echo $_SERVER['PHP_SELF']; ?>';" />
                                        </td>
                                    </tr>
                                </table>
                                <input type="hidden" id="SiteCMSLogo" name="SiteCMSLogo" value="" />
                                <input type="hidden" id="SiteType" name="SiteType" value="sub" />
                                <input type="hidden" name="MM_insert" value="form1" />
                            </form>
                        </td>
                    </tr>
                    <?php } ?>
                    <?php if( !isset($_GET['add']) && !isset($_GET['edit']) ) { ?>
                    <tr>
                        <td valign="top" class="bigheader">
                            Choose Which Site To Manage
                        </td>
                    </tr>
                    <?php 
                    // creating or deleting sites is reserved only for system administrators
                    if( isAdminUser() ) { ?>
                    <tr>
                        <td valign="top" class="newsHeader">
                            <a id="site_create_link" href="?add">
                                <img alt="" src="images/icons/page_add.png" width="16" height="16" border="0" align="absmiddle" /> Create New Site
                            </a>
                            
                            <!--<a id="site_delete_link" href="#" onclick="sites.deleteSelectedSites(); return false;">
                                <img alt="" src="images/icons/page_delete.png" width="16" height="16" border="0" align="absmiddle" /> Delete Selected Site
                            </a>-->

                            <a id="site_actions_link" href="#" >
                                <img alt="" src="images/icons/page.png" width="16" height="16" border="0" align="absmiddle" /> With Selected
                            </a>
                            
                            <div id="ajax-progress">
                                <img src="../assets/images/general/loadcircles.gif" width="16" height="16" alt="progress" align="absmiddle" />
                                <span id="progress-text">Processing...</span>
                            </div>
                        </td>
                    </tr>
                    <?php } ?>
                    <?php if( count($siteManager_list) > 0 ) { ?>
                    <tr>
                        <td valign="top" class="newsSummary">
                            <form action="" id="sites_form" name="sites_form" onsubmit="return false;">
                                <?php  
									$filterShow = 'style="display:none;"';
									
									// only show the filter option if we have more than 10 sites in the system
									if( count($siteManager_list) > 10 ) $filterShow = 'style="display:block;"';
								 ?>
                                <div id="filter_pane" <?php echo $filterShow; ?>>
                                    <span style="display:inline-block;">
                                        <strong>Filter / Find Site:</strong>
                                        <input type="text" size="15" onkeyup="sites.filterSiteList(this.value);"  />
                                    </span>

                                    <span id="ajax_progress" style="display:none; margin-left: 20px; width: 140px; ">
                                        <img src="images/loadSnake.gif" width="16" height="16" alt="loading" align="absmiddle" /> Processing...
                                    </span>
                                    
                                    <div id="ajax_content" style="display:none;">&nbsp;</div>
                                </div>
                                <div id="site_list"><?php 
							   	   $count = 1;
                                   
								   foreach($siteManager_list as $site){
									  
									  $classDefault = ($site->getData('SiteType') == "main") ? "default_site" : "";
                                      $classActive = ($site->getData('SiteActive') == "1" ? "active" : "inactive"); ?>

                                    <div id="sitebox<?php echo $site->getData('id'); ?>" class="site_box <?php echo "$classDefault $classActive"; ?>">
                 
                                        <div class="site_details">
                                            <div class="site_name">

                                                <?php echo $count . ". "; ?>
                                                
                                                <input type="checkbox" name="item<?php echo $count; ?>" id="item<?php echo $count; ?>" value="<?php echo $site->getId(); ?>" />
                                                <a href="?launch&amp;id=<?php echo $site->getId(); ?>">
                                                    <?php echo $site->getData('SiteName'); ?>
                                                </a>
                                            </div>
                                            <div class="site_options">
                                                <a href="?launch&amp;id=<?php echo $site->getId(); ?>">Manage</a>
                                                |
                                                <a href="?edit&amp;id=<?php echo $site->getId(); ?>">Edit Definition</a>
                                                |
                                                <?php 
                                                    $siteURL = $site->isActive() ? $site->getHomePageURL() : kan_preview_url( $site->getHomePageURL() );
                                                    $linkName = $site->isActive() ? "View" : "Preview";
												?>
                                                <a href="<?php echo $siteURL; ?>" target="_blank"><?php echo $linkName; ?></a>
                                                
                                                <?php if( isAdminUser() ) { ?>
                                                <!--|
                                                <a data-id="<?php echo $site->getId(); ?>" class="actions_menu" href="#" >Actions</a>-->
                                                |
                                                <a href="#" onclick="return sites.setDefault(<?php echo $site->getId(); ?>);">Set As Default</a>
                                                |
                                                <a href="#" class="activator" data-id="<?php echo $site->getId(); ?>" data-status="<?php echo $site->getData('SiteActive'); ?>"><?php echo $site->getData('SiteActive') == 1 ? 'De-Activate' : 'Activate'; ?></a>
                                                |
                                                <a href="#" onclick="return sites.deleteSite(<?php echo $site->getId(); ?>);">Delete</a>
                                                <?php } ?>
                                            </div>
                                        </div>
                                    </div>
                                    <?php $count++; } ?>
                                </div>
                            </form>
                        </td>
                    </tr>
                    <?php } else if( isAdminUser() ) { ?>
                    <tr>
                        <td class="newsSummary">
                            Welcome to the KAN CMS Sites Home Page. There are currently no Sites defined in this CMS
                            installation. Simply choose the <a href="?add">Create New Site</a> Option to begin 
                            defining your site and its contents
                        </td>
                    </tr>
                    <?php } else { ?>
                    <tr>
                        <td class="newsSummary">
                            Welcome to the KAN CMS Sites Home Page. There are currently no Sites defined in this CMS
                            installation or you have not been authorized to manage any site. 
                            Please contact a site administrator for more information.
                        </td>
                    </tr>
                    <?php } 
                    } ?>
                </table>
            </div>
            
            
            <script type="text/javascript" src="scripts/imgupload/picup.js"></script>
			<script type="text/javascript" src="scripts/sites.js"></script>
            <script type="text/javascript">
                pageManager.addLoadEvent( function() {
                    sites.init({ siteid: <?php echo isset($_GET['id']) ? $_GET['id'] : -1; ?> });
                    picup.init({
                        targetDiv : "picupArea",
                        imgPath : "<?php echo isset($site) ? $site->getSiteLogo() : ''; ?>",
                        pictureField : "SiteLogo",
                        destFolder : "../images/content/pics/",
                        thumbFolder : "../images/content/thumbs/",
                        useSection: "cms",
                        siteid: <?php echo isset($_GET['id']) ? $_GET['id'] : -1; ?>
                    });
                });
            </script>
            <!-- InstanceEndEditable -->
        </div>
    </div>
    
    <div id="footer">
    	<?php include('footer.php'); ?>
    </div>
</div>

</body>
<!-- InstanceEnd --></html>