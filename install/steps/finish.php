<link href="../css/install-ui.css" rel="stylesheet" type="text/css">
<style type="text/css">
#finish ul li {
	float: left;
    display: block;
    width: 370px;
    padding-left: 20px;
    padding-top: 20px;
    list-style: none;
    background-image: url(../../cms/images/icons/folder_explore.png);
    background-repeat:no-repeat;
    background-position: left 22px;
}
</style>
<div id="finish">
    <table width="100%" border="0" cellspacing="0" cellpadding="4" class="newsTbl" style="width: 100%;">
        <tr>
            <td class="newsHeader">FINISH</td>
        </tr>
        <tr>
            <td class="newsHeader">KAN Installation Now Complete</td>
        </tr>
        <tr>
            <td>
                <p> Your KAN installation is now complete. You may now  view your default KAN Web site or the KAN Content Management Section </p>
                <ul>
                    <li style="margin-bottom: 15px;">
                        <a href="../pages/index.php" target="_blank" class="redirect_link">View Your Default KAN Site</a>
                    </li>
                    <li>
                        <a href="../cms/index.php" target="_blank" class="redirect_link">View The KAN Content Management Section</a>
                    </li>
                </ul>
                <p>&nbsp;</p>
            </td>
        </tr>
        <tr>
            <td class="newsSummary">
                <div class="table_block" align="center">
                    <div class="sectionTitle1"  style="width: 90%; margin-left: auto; margin-right: auto;">
Please ensure to  delete (rename) the KAN installation folder (/install/) since it is no longer required and could pose a potential security risk. Also, the CMS section will not operate if this folder is still present
                    </div>
                </div>
            </td>
        </tr>
    </table>
</div>
