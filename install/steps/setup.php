<?php 


$setup = new Setup();

class Setup {

   var $yesValue = '<span style="color:green; font-weight: bold;">Yes</span>';
   var $noValue = '<span style="color:red; font-weight: bold;">No</span>';
   var $onValue = '<span style="color:green; font-weight: bold;">On</span>';
   var $offValue = '<span style="color:red; font-weight: bold;">Off</span>';
   var $settingsValid = true;

   function phpVersionValid() {
      if( phpversion() >= '5.2.0' ) {
         return $this->yesValue;
      }

      $this->settingsValid = false;
      return $this->noValue;
   }
   
   function apacheModRewriteEnabled() {
	   if( in_array("mod_rewrite", apache_get_modules()) ) {
		  return $this->yesValue;   
	   }
	   
	   $this->settingsValid = false;
	   return $this->noValue;
   }

   function extensionLoaded($extension) {
      if( extension_loaded($extension) ) {
         return $this->yesValue;
      }

      $this->settingsValid = false;
      return $this->noValue;
   }

   function getSettingState($setting) {
      if( (bool)ini_get($setting) ) {
         return $this->onValue;
      }

      return $this->offValue;
   }

   function mysql_loaded() {
      if( function_exists('mysql_connect') || function_exists('mysqli_connect') ) {
         return $this->yesValue;
      }

      $this->settingsValid = false;
      return $this->noValue;
   }

   function isSettingsValid() {
      return $this->settingsValid;
   }
}
?>