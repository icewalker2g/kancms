<?php 

if( !isset($_SESSION) ) {
    session_start();
}

if( isset($_POST['step']) ) {

	$stepPage = $_POST['step'];
	$dir = $_POST['dir'];
	
	if( $dir == 'next' && isset($_POST['prevStep']) ) {
		$processor = 'process_' . $_POST['prevStep'] . '.php';
		
		if( file_exists($processor) ) {
			include($processor);

            if( $_POST['prevStep'] == "database" ) {
                $kan_database = new KANDatabase();
                
                if( !$kan_database->doInstall() ) {
                    echo "Could Not Install The KAN Database<br />";
                }
            } else if( $_POST['prevStep'] == "site_config" ) {
                $site_config = new KANDefaultSiteConfig();

                if( !$site_config->doInstall() ) {
                    echo "Could Not Setup The Default KAN Site<br />";
                }
            }
		}
	}
	
	include('setup.php');
	include( $stepPage . '.php' );

    $_SESSION['LastStep'] = $_POST['stepPos'];
}
?>