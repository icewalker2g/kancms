<?php 
if( !isset($_SESSION) ) {
	session_start();
}
?>
<link href="../css/install-ui.css" rel="stylesheet" type="text/css">
<table width="100%" border="0" cellspacing="0" cellpadding="4" class="newsTbl" style="width:100%;">
  <tr>
    <td colspan="2" class="newsHeader">Database Connection Settings</td>
  </tr>
  <tr>
    <td width="50%" valign="top" class="newsSummary"><div class="small">
      <p><strong>Database Instructions<br />
      </strong>Setting up KAN to run on your server involves four easy steps.The first step is database configuration. Choose your preferred settings:      </p>
      <ul>
          <li>Select the type of database from the drop down list. This will generally be <STRONG>MySQL</STRONG>.</li>
        <li>Enter the hostname of the database server KAN will be installed on. This   may not necessarily be the same as your Web server so check with your hosting   provider if you are not sure.</li>
        <li>Enter the MySQL username, password and database name you wish to use with   KAN. These must already exist for the database you are going to use.      </li>
      </ul>
      <div style="display:none;">
          <H2 class="sectionTitle3">Advanced Settings</H2>
      <p>Select how to handle existing tables from a previous installation.</p>
    <p>Enter a table prefix to be used for this Joomla! installation.</p>
</div>
    </div></td>
    <td valign="top" class="newsSummary"><table width="100%" border="0" cellpadding="4" cellspacing="0" class="table_block">
      <tr>
        <td><strong>Basic Settings</strong></td>
      </tr>
      <tr>
        <td><table cellpadding="3" cellspacing="0" class="db_table">
          <TR>
            <TD width="130" class="small"><label for="vars_dbtype3"><strong>Database Type </strong></label></TD>
            <TD width="253" class="small"><select name="dbtype" size="1" class="small required" id="dbtype">
              <option value="">Select   Type</option>
              <option selected value="mysql">mysql</option>
              <!--<option value="mysqli">mysqli</option>-->
            </select></TD>
          </TR>
          <TR>
            <TD colSpan="2" class="info_row">Only <STRONG>MySQL</STRONG> is supported currently</TD>
            </TR>
          <TR>
            <TD valign="middle"><p>
              <span class="small">
              <LABEL for="dbhostname"><SPAN id="dbhostnamemsg"><strong>Host Name</strong></SPAN></LABEL>
              </span></p></TD>
            <TD><span class="small">
              <input name="dbhostname" type="text" class="required" id="dbhostname" value="<?php echo isset($_SESSION['hostname']) ? $_SESSION['hostname'] : "localhost"; ?>" />
            </span></TD>
            </TR>
          <TR>
            <TD colSpan="2" valign="top" class="info_row">This is usually <STRONG>localhost</STRONG> or a host name provided by   the hosting provider. </TD>
            </TR>
          <TR>
            <TD valign="middle"><strong><span class="small">
              <LABEL for="dbusername"><SPAN id="dbusernamemsg">Username</SPAN></LABEL>
              </span></strong></TD>
            <TD valign="top"><span class="small">
              <input name="dbusername" type="text" class="required" id="dbusername" value="<?php echo isset($_SESSION['dbusername']) ? $_SESSION['dbusername'] : "root"; ?>">
            </span></TD>
            </TR>
          <TR valign="middle">
            <TD colSpan="2" class="info_row"><EM>This can be the default MySQL username <STRONG>root</STRONG>, a username   provided by your hosting provider, or one that you created in setting up your   database server. </EM></TD>
            </TR>
          <TR>
            <TD valign="middle"><strong><span class="small">
              <LABEL for="dbpassword">Password </LABEL>
              </span></strong></TD>
            <TD valign="top"><span class="small">
              <input id="dbpassword" value="" type="password" name="dbpassword">
            </span></TD>
            </TR>
          <TR>
            <TD colSpan="2" valign="top" class="info_row">Using a password for the MySQL account is mandatory for site security.   This is the same password used to access your database. This may be predefined   by your hosting provider. </TD>
            </TR>
          <TR>
            <TD valign="middle"><strong><span class="small">
              <LABEL for="dbname"><SPAN id="dbnamemsg">Database Name</SPAN></LABEL>
              </span></strong></TD>
            <TD valign="top"><span class="small">
              <input id="dbname" type="text" name="dbname" class="required" value="<?php echo isset($_SESSION['databaseName']) ? $_SESSION['databaseName'] : ""; ?>">
            </span></TD>
            </TR>
          <TR>
            <TD colSpan="2" valign="top" class="info_row">The name of the database to use for this KAN installation.</TD>
            </TR>
          <tr>
              <td valign="middle"><strong><span class="small">
                  <label for="dbTablePrefix"><span id="dbnamemsg2">Table Name Prefix</span></label>
              </span></strong></td>
              <td valign="top"><span class="small">
                  <input id="dbTablePrefix" type="text" name="dbTablePrefix" class="required" value="<?php echo isset($_SESSION['dbTablePrefix']) ? $_SESSION['dbTablePrefix'] : "kan_"; ?>" />
              </span></td>
          </tr>
          <tr>
              <td colspan="2" valign="top" class="info_row">A prefix to use in creating the database table names.</td>
          </tr>
          </table>
        </td>
      </tr>
    </table>
    </td>
  </tr>
</table>
