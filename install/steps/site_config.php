<?php 
if( !isset($_SESSION) ) {
	session_start();
}
?>
<link href="../css/install-ui.css" rel="stylesheet" type="text/css">
<table width="100%" border="0" cellspacing="0" cellpadding="4" style="width: 100%;">
  <tr>
    <td colspan="2" class="sectionTitle3">Default KAN Configuration</td>
  </tr>
  <tr>
    <td colspan="2" valign="bottom"><p>Use this section to setup your default site on KAN. </p></td>
  </tr>
  <tr>
    <td height="40" colspan="2" valign="bottom" class="sectionTitle3">Default Site Configuration</td>
  </tr>
  <tr class="newsSummary">
    <td width="40%" valign="top" class="small">      <p>Provide the name of the default site to display in KAN. Also provide the unique simple identifier for the site to distinguish it from the other sites that may be created later.</p></td>
    <td valign="top">
    <div class="table_block">
    <table width="100%" border="0" cellspacing="0" cellpadding="4">
      <tr>
        <td width="30%" class="small"><strong>Site Name:</strong></td>
        <td><input name="site_name" type="text" class="required" id="site_name" value="<?php if( isset($_SESSION['siteName']) ) echo $_SESSION['siteName']; ?>" size="40"></td>
      </tr>
      <tr>
        <td valign="top" class="small"><strong>Site Identifier:</strong></td>
        <td><span class="small">Must Be A Simple Unique Name - No Spaces</span><br>
          <input name="site_identifier" type="text" id="site_identifier" size="15" class="required" value="<?php if( isset($_SESSION['siteIdentifier']) ) echo $_SESSION['siteIdentifier']; ?>" /></td>
      </tr>
    </table>
    </div>
    </td>
  </tr>
  <tr>
    <td colspan="2" class="sectionTitle3">System Administrator Information</td>
  </tr>
  <tr>
    <td valign="top" class="small"><p>Enter your e-mail address. This will be the e-mail address of the Web site   Super Administrator.</p>
    <p>Enter a new password and then confirm it in the   appropriate fields. Along with the username, this will be   the password that you will use to login to the Content Management System at   the end of the installation.</p></td>
    <td valign="top"><div class="table_block"><table width="100%" border="0" cellspacing="0" cellpadding="4">
      <tr>
        <td width="30%" class="small"><strong>Admin Email:</strong></td>
        <td><input name="admin_email" type="text" id="admin_email" size="32" class="email" value="<?php if( isset($_SESSION['adminEmail']) ) echo $_SESSION['adminEmail']; ?>"></td>
      </tr>
      <tr>
        <td class="small"><strong>Admin Username:</strong></td>
        <td><input type="text" name="admin_username" id="admin_username" class="required" value="<?php if( isset($_SESSION['adminUsername']) ) echo $_SESSION['adminUsername']; ?>"></td>
      </tr>
      <tr>
        <td class="small"><strong>Admin Password:</strong></td>
        <td><input type="password" name="admin_password" id="admin_password" class="required" value="<?php if( isset($_SESSION['adminPassword']) ) echo $_SESSION['adminPassword']; ?>"></td>
      </tr>
      <tr>
        <td class="small"><strong>Confirm Password:</strong></td>
        <td><input type="password" name="admin_pass_check" id="admin_pass_check" class="required" value="<?php if( isset($_SESSION['adminPassword']) ) echo $_SESSION['adminPassword']; ?>"></td>
      </tr>
    </table></div></td>
  </tr>
  <tr>
      <td colspan="2" valign="top" class="sectionTitle3">Sample Data</td>
    </tr>
  <tr>
      <td valign="top" class="small">KAN comes with some sample data to included in your first (default) site which would give you a brief tutorial and examples of how KAN can be used. It is recommended to install this if this your first time, however it may be skipped if you are an advanced user.</td>
      <td valign="top">
          <input name="install-sample-data" type="checkbox" id="install-sample-data" checked="checked" />
          <label for="install-sample-data">Install Sample Data</label>
      </td>
  </tr>
</table>
