
<table width="100%" border="0" cellpadding="4" cellspacing="0" class="newsTbl" style="width:100%;">
  <tr>
    <td colspan="2" class="newsHeader">Pre-Installation Check For KAN 1.0</td>
  </tr>
  <tr>
    <td width="50%" class="newsHeader">Required System Settings</td>
    <td align="right" class="newsHeader"><input type="button" name="recheck" id="recheck" value="Check Again" class="stepBtn" onclick="history.go(0);" /></td>
  </tr>
  <tr>
    <td valign="top" class="newsSummary"><div class="small">If any of these items are not supported (marked as No), your system does not meet the minimum requirements for installation. Please take appropriate actions to correct the errors. Failure to do so could lead to your KAN CMS installation not functioning properly.</div></td>
    <td valign="top" class="newsSummary"><div class="table_block"><table width="100%" border="0" cellspacing="0" cellpadding="2">
      <tr>
        <td><strong>Required Feature</strong></td>
        <td align="center"><strong>Enabled</strong></td>
      </tr>
      <tr>
        <td>
        Apache MOD Rewrite
        </td>
        <td width="40%" align="center"><?php echo $setup->apacheModRewriteEnabled(); ?></td>
      </tr>
      <tr>
        <td>PHP Version &gt;= 5.2.0</td>
        <td width="40%" align="center"><?php echo $setup->phpVersionValid(); ?></td>
      </tr>
      <tr>
        <td>MySQL Support</td>
        <td width="20%" align="center"><?php echo $setup->mysql_loaded(); ?></td>
        </tr>
      <tr>
        <td>XML Support</td>
        <td align="center"><?php echo $setup->extensionLoaded("xml"); ?></td>
        </tr>
      <!--<tr>
        <td>ZLib Compression Support</td>
        <td align="center"><?php //echo $setup->extensionLoaded("zlib"); ?></td>
        </tr>-->
      <tr>
        <td>Zip Compression</td>
        <td align="center"><?php echo $setup->extensionLoaded("zip"); ?></td>
        </tr>
    </table></div></td>
  </tr>
  <tr>
    <td colspan="2" valign="top" class="newsHeader">Recommended Settings</td>
  </tr>
  <tr>
    <td valign="top" class="newsSummary"><div class="small">These are the recommended settings for PHP in order to ensure full compatibility with KAN CMS.                        KAN CMS will still operate even if your settings do not match.</div></td>
    <td valign="top" class="newsSummary"><div class="table_block"><table width="100%" border="0" cellspacing="0" cellpadding="2">
      <tr>
        <td><strong>Setting</strong></td>
        <td width="20%" align="center"><strong>Recomm.</strong></td>
        <td width="20%" align="center"><p><strong>Actual</strong></p></td>
      </tr>
      <tr>
        <td>Safe Mode</td>
        <td align="center">Off</td>
        <td align="center"><?php echo $setup->getSettingState("safe_mode"); ?></td>
      </tr>
      <tr>
        <td>Display Errors</td>
        <td align="center">Off</td>
        <td align="center"><?php echo $setup->getSettingState("display_errors"); ?></td>
      </tr>
      <tr>
        <td>File Uploads</td>
        <td align="center">On</td>
        <td align="center"><?php echo $setup->getSettingState("file_uploads"); ?></td>
      </tr>
      <tr>
        <td>Magic Qoutes Runtime</td>
        <td align="center">Off</td>
        <td align="center"><?php echo $setup->getSettingState("magic_qoutes_runtime"); ?></td>
      </tr>
      <tr>
        <td>Register Globals</td>
        <td align="center">Off</td>
        <td align="center"><?php echo $setup->getSettingState("register_globals"); ?></td>
      </tr>
      <tr>
        <td>Output Buffering</td>
        <td align="center">Off</td>
        <td align="center"><?php echo $setup->getSettingState("output_buffering"); ?></td>
      </tr>
      <tr>
        <td>Session Auto Start</td>
        <td align="center">Off</td>
        <td align="center"><?php echo $setup->getSettingState("session.auto_start"); ?></td>
      </tr>
    </table></div></td>
  </tr>
</table>
<input type="hidden" value="<?php echo $setup->isSettingsValid() ? "true" : ""; ?>" class="required" name="settings_valid" id="settings_valid" />
