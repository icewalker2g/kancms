<?php

// find the database configuration path
$currentPage = $_SERVER['PHP_SELF'];
$endPos = strpos($currentPage, "install/");
$siteRoot = substr($currentPage, 0, $endPos);
require_once($_SERVER['DOCUMENT_ROOT'] . $siteRoot . 'assets/config/config.php');

class KANDefaultSiteConfig {

   private $siteName;
   private $siteIdentifier;
   private $adminEmail;
   private $adminUsername;
   private $adminPassword;
   private $prefix;

   function doInstall() {
      global $database_config, $config;

      // if the siteName session variable exists, then the information might
      // already be present, so we just exit the install
      if( isset($_SESSION['siteName']) ) {
         return true;
      }

      $hasError = false;

      $this->siteName = isset($_POST['site_name']) ? $_POST['site_name'] : "";
      $this->siteIdentifier = isset($_POST['site_identifier']) ? $_POST['site_identifier'] : "";
      $this->adminEmail = isset($_POST['admin_email']) ? $_POST['admin_email'] : "";
      $this->adminUsername = isset($_POST['admin_username']) ? $_POST['admin_username'] : "";
      $this->adminPassword = isset($_POST['admin_password']) ? $_POST['admin_password'] : "";
      $this->prefix = isset($_SESSION['dbTablePrefix']) ? $_SESSION['dbTablePrefix'] : "";

      if( $this->siteName == "" || $this->siteIdentifier == "" ) {
         echo "Invalid Specification For Site Name or Site Identifier <br />";
         return false;
      }

      if( $this->adminEmail == "" || $this->adminUsername == "" || $this->adminPassword == "" ) {
         echo "Invalid Value For Administrator Username, Password or Email <br />";
         return false;
      }

      mysql_select_db($database_config, $config);

      // create the default site specified by the user
      $query = sprintf("INSERT INTO {$this->prefix}sites (SiteIdentifier, SiteName, SiteType, SiteDescription, SiteThemeID, SiteThemeCSSID) " 
						. "VALUES ('%s', '%s', 'main', 'This is the default KAN Site', 1, 1)",
          				mysql_real_escape_string($this->siteIdentifier), 
						mysql_real_escape_string($this->siteName));
	  
      $result = mysql_query($query, $config);

      if( mysql_error() ) {
         echo mysql_error() . "<br />";
      }

      // encrypt the password before saving to the database
      $today = date("Y-m-d");
      $passSalt = rand(1,100000);
      $passwordHash = md5( $this->adminPassword . $passSalt );

      // create the super administrator user
      $query = sprintf("INSERT INTO {$this->prefix}users (id, FirstName, LastName, FullName, Username, Password, PassSalt, Email, Level, Role, LastAccess, Activated) ".
          "VALUES(1, 'Super', 'Administrator', 'Super Administrator', '%s', '%s', '%s', '%s', 'super', 1, '%s', 1) ",
          $this->adminUsername, $passwordHash, $passSalt, $this->adminEmail, $today
      );

      $result = mysql_query($query, $config);

      // if an error occurs, indicate and continue
      if( mysql_error() ) {
         echo mysql_error() . "<br />";
      }
	  
	  // if the user has chosen to install the sample data, then go ahead
	  // and process the data
	  if( isset($_POST['install-sample-data']) ) {
		  
		  // load the default site data from the schema file if it exists
		  $data_schema_file = '../sql/sample_data.sql';
		  
		  if( file_exists($data_schema_file) ) {
			  // include the KANDatabase object for processing the data
			  include('process_database.php');
			  $kandb = new KANDatabase();
			  $kandb->populateDatabase($data_schema_file);
		  }
	  }
	  

      // once the installation has been completed, store the variables as Session variables
      // so they can be retrieved if the page is refreshed or re-visited
      $_SESSION['siteName'] = $this->siteName;
      $_SESSION['siteIdentifier'] = $this->siteIdentifier;
      $_SESSION['adminEmail'] = $this->adminEmail;
      $_SESSION['adminUsername'] = $this->adminUsername;
      $_SESSION['adminPassword'] = $this->adminPassword;


      // return true to indicate success
      return true;
   }
}
?>
