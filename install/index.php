<?php 
	if( !isset($_SESSION) ) {
		session_start();
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/install.dwt.php" codeOutsideHTMLIsLocked="false" -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<!-- InstanceBeginEditable name="doctitle" -->
<title>KAN CMS Installation</title>
<!-- InstanceEndEditable -->
<style type="text/css">
<!--
body {
	background-color: #E5E5E5;
	margin-left: 0px;
	margin-top: 0px;
	margin-right: 0px;
	margin-bottom: 0px;
}
-->
</style>
<link rel="stylesheet" type="text/css" href="css/install-ui.css"/>
<script language="javascript" src="../cms/scripts/form_control.js"></script>
<style>
<!--
.cms_style { background-color: #600; }
-->
</style>

<!-- InstanceBeginEditable name="head" -->
<script type="text/javascript" src="../assets/scripts/common/pageManager.js"></script>
<script type="text/javascript" src="js/install.js"></script>
<script type="text/javascript">
function loadCurrentStep() {
	install.currentStep = <?php echo isset($_SESSION['LastStep']) ? $_SESSION['LastStep'] - 1 : -1; ?>;
	install.showNextStep();	
}

pageManager.addLoadEvent( loadCurrentStep );
</script>
<!-- InstanceEndEditable -->
</head>

<body>

<div id="page">
	<div id="header">
    	<?php include('header.php'); ?>
    </div>
    
    <div id="content">
    	<div id="nav">
        	<?php include('setup_steps.php'); ?>
        </div>
        
        <div id="main">
        	<!-- InstanceBeginEditable name="content" -->
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td valign="top">
                        <table width="100%" border="0" cellpadding="4" cellspacing="0" class="newsTbl">
                            <tr>
                                <td width="50%" class="newsHeader">Welcome To The KAN Install Process</td>
                                <td align="right" class="newsHeader">
                                    <input name="btnPrevious" type="button" class="stepBtn" id="btnPrevious" onclick="install.showPrevStep();" value="Previous Step" />
                                    &nbsp;
                                    <input name="btnNext" type="button" class="stepBtn" id="btnNext" onclick="install.showNextStep();" value="Next Step" />
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" valign="top" class="newsSummary">
                                    <form id="step_form" name="step_form" action="">
                                        <div id="step_content">
                                            Loading Installation Steps. Please Wait..
                                        </div>
                                    </form>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            <!-- InstanceEndEditable -->
        </div>
    </div>
    
    <div id="footer">
    	<?php include('footer.php'); ?>
    </div>
</div>


</body>
<!-- InstanceEnd --></html>