// JavaScript Document

var install = {
	
    installSteps: null,
    currentStep: 0,
    fields: null,
	validation_messages: null,
	
    init: function() {
        var isteps = new Array();
        //isteps[0] = "language";
        isteps[0] = "pre-install";
        //isteps[2] = "license";
        isteps[1] = "database";
        //isteps[4] = "ftp";
        isteps[2] = "site_config";
        isteps[3] = "finish";
		
        install.installSteps = isteps;
		
        // form fields required for validation
        var ff = new Array();
        ff['settings_valid'] = "System Settings Or Required Libraries Are Invalid or Not Enabled";

        ff['dbtype'] = "Database Type";
        ff['dbhostname'] = "Database Host";
        ff['dbusername'] = "Database User";
        ff['dbpassword'] = "Database Password";
        ff['dbname'] = "Database Name";
		
        ff['site_name'] = "Default Site Name";
        ff['site_identifier'] = "Default Site Identifier";
        ff['admin_email'] = "System Administrator Email";
        ff['admin_username'] = "Administrator Username";
        ff['admin_password'] = "Administrator Password";
        ff['admin_pass_check'] = "Password Configuration";
		
        // method called from form_validator.js
        install.validation_messages = ff;
    },
	
    showNextStep: function() {
        if( (install.currentStep + 1) >= install.installSteps.length ) return;
		
        install.showStep('next', install.installSteps[++install.currentStep] );
    },
	
    showPrevStep: function() {
        if( (install.currentStep - 1) <= -1 ) return;
		
        install.showStep('prev', install.installSteps[--install.currentStep] );
    },
	
    showStep: function(dir, step) {
        var form = document.getElementById("step_form");
		
        if( dir == 'next' && !$p(form).validate( install.validation_messages ) ) {
            install.currentStep--; // ensure to return the current step to the previous
            return;
        }

        var query = "step=" + step + "&stepPos=" + install.currentStep + "&dir=" + dir;
		
        if( dir == 'next' ) {
            query = query + "&prevStep=" + install.installSteps[ install.currentStep - 1];
        }
		
        query = query + "&" + $p(form).serialize();
        //alert(query);
		
        $p.post('steps/step_load.php', query, '#step_content', install.indicateCurrentStep );
    },
	
    indicateCurrentStep: function() {
        var menuList = document.getElementById("steps-menu");
        var childNodes = menuList.childNodes;
		
        for(var i = 0; i < childNodes.length; i++) {
            var node = childNodes.item(i);
            var menuName = "menu-" + install.installSteps[ install.currentStep ];
			
            if( node.id == menuName ) {
                node.className = "current";
            } else {
                node.className = "";
            }
        }
		
		document.getElementById("btnPrevious").disabled = !(install.currentStep > 0);
		document.getElementById("btnNext").disabled = !(install.currentStep < install.installSteps.length - 1);
    }
};

$p.addLoadEvent( install.init );