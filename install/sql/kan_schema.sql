SET FOREIGN_KEY_CHECKS=0;
SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";

-- --------------------------------------------------------

--
-- Table structure for table `advert_images`
--

DROP TABLE IF EXISTS `<prefix>advert_images`;
CREATE TABLE IF NOT EXISTS `<prefix>advert_images` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `SiteID` int(5) DEFAULT NULL,
  `AdvertName` varchar(255) NOT NULL,
  `AdvertDesc` varchar(255) DEFAULT NULL,
  `AdvertPath` varchar(255) NOT NULL,
  `AdvertType` varchar(20) NOT NULL,
  `AdvertTypeCat` varchar(10) DEFAULT NULL,
  `Width` int(4) DEFAULT NULL,
  `Height` int(4) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `SiteID` (`SiteID`),
  KEY `SiteID_2` (`SiteID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `advert_types`
--

DROP TABLE IF EXISTS `<prefix>advert_types`;
CREATE TABLE IF NOT EXISTS `<prefix>advert_types` (
  `id` int(3) NOT NULL AUTO_INCREMENT,
  `TypeName` varchar(50) NOT NULL,
  `TypeExtension` varchar(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `advert_types_cats`
--

DROP TABLE IF EXISTS `<prefix>advert_types_cats`;
CREATE TABLE IF NOT EXISTS `<prefix>advert_types_cats` (
  `id` int(3) NOT NULL AUTO_INCREMENT,
  `TypeExtension` varchar(10) CHARACTER SET latin1 NOT NULL,
  `CategoryName` varchar(150) CHARACTER SET latin1 NOT NULL,
  `CategoryCode` varchar(50) CHARACTER SET latin1 DEFAULT NULL,
  `Width` varchar(10) CHARACTER SET latin1 DEFAULT NULL,
  `Height` varchar(10) CHARACTER SET latin1 DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `articles`
--

DROP TABLE IF EXISTS `<prefix>articles`;
CREATE TABLE IF NOT EXISTS `<prefix>articles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `CategoryID` int(5) NOT NULL COMMENT 'ID to the category to which this article belongs',
  `SiteID` int(5) NOT NULL COMMENT 'ID of the site to which this article belongs',
  `ArticleTitle` varchar(255) NOT NULL COMMENT 'Title of the article',
  `ArticleTitleAlias` varchar(255) DEFAULT NULL,
  `ArticleSummary` text COMMENT 'Summary of article content',
  `ArticleContent` text COMMENT 'Actual content for this article',
  `ArticleSource` varchar(255) DEFAULT NULL COMMENT 'Source or Credits for this article',
  `ArticleImage` varchar(255) DEFAULT NULL COMMENT 'path to Image File for this article',
  `ArticleThumbnail` varchar(255) DEFAULT NULL COMMENT 'path to thumbnail image file',
  `ArticleURL` varchar(255) DEFAULT NULL COMMENT 'URL to External Article. Use this to post referal articles',
  `ArticleGallery` varchar(30) DEFAULT NULL COMMENT 'ID of the Associated Photo Gallery of this article',
  `AllowComments` tinyint(1) NOT NULL DEFAULT '1' COMMENT 'Indicates whether comments can be posted for this article',
  `PostDate` datetime DEFAULT NULL,
  `Published` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Indicates whether this article is published or not. Valid values are either true or false',
  PRIMARY KEY (`id`),
  KEY `art_cat_fk` (`CategoryID`),
  KEY `art_site_fk` (`SiteID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `article_categories`
--

DROP TABLE IF EXISTS `<prefix>article_categories`;
CREATE TABLE IF NOT EXISTS `<prefix>article_categories` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `SiteID` int(5) DEFAULT NULL,
  `ParentID` int(5) DEFAULT NULL,
  `Position` int(3) DEFAULT NULL,
  `CategoryName` varchar(100) CHARACTER SET latin1 NOT NULL,
  `CategoryAlias` varchar(50) DEFAULT NULL,
  `CategoryDescription` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `art_site_fk1` (`SiteID`),
  KEY `art_par_fk1` (`ParentID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `article_comments`
--

DROP TABLE IF EXISTS `<prefix>article_comments`;
CREATE TABLE IF NOT EXISTS `<prefix>article_comments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ArticleID` int(11) NOT NULL,
  `InReplyTo` int(11) DEFAULT NULL,
  `ReaderName` varchar(255) CHARACTER SET latin1 NOT NULL,
  `ReaderEmail` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `ReaderWebsite` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `ReaderPhone` varchar(100) CHARACTER SET latin1 DEFAULT NULL,
  `ReaderIP` varchar(50) DEFAULT NULL,
  `Subject` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `Message` text CHARACTER SET latin1,
  `PostDate` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `Approved` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `com_art_fk` (`ArticleID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `banner_images`
--

DROP TABLE IF EXISTS `<prefix>banner_images`;
CREATE TABLE IF NOT EXISTS `<prefix>banner_images` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `SiteID` int(6) DEFAULT NULL,
  `BannerName` varchar(255) NOT NULL DEFAULT '',
  `BannerDesc` varchar(255) DEFAULT NULL,
  `BannerPath` varchar(255) NOT NULL DEFAULT '',
  `BannerType` varchar(20) NOT NULL DEFAULT '',
  `BannerURL` varchar(255) DEFAULT NULL,
  `Width` int(4) DEFAULT NULL,
  `Height` int(4) DEFAULT NULL,
  `Published` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `Banner_SiteID_FK` (`SiteID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `banner_types`
--

DROP TABLE IF EXISTS `<prefix>banner_types`;
CREATE TABLE IF NOT EXISTS `<prefix>banner_types` (
  `id` int(3) NOT NULL AUTO_INCREMENT,
  `TypeName` varchar(50) CHARACTER SET latin1 NOT NULL DEFAULT '',
  `TypeExtension` varchar(10) CHARACTER SET latin1 NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `components`
--

DROP TABLE IF EXISTS `<prefix>components`;
CREATE TABLE IF NOT EXISTS `<prefix>components` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `ComponentType` varchar(100) NOT NULL DEFAULT 'widget',
  `Component` varchar(100) NOT NULL,
  `ComponentName` varchar(200) NOT NULL,
  `Description` text,
  `ComponentPath` varchar(255) NOT NULL,
  `DateAdded` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `downloads`
--

DROP TABLE IF EXISTS `<prefix>downloads`;
CREATE TABLE IF NOT EXISTS `<prefix>downloads` (
  `id` int(8) NOT NULL AUTO_INCREMENT,
  `CategoryID` int(6) DEFAULT NULL,
  `Name` varchar(255) NOT NULL DEFAULT '',
  `Alias` varchar(255) DEFAULT NULL,
  `Description` text NOT NULL,
  `Thumbnail` varchar(255) NOT NULL DEFAULT '',
  `URL` varchar(255) NOT NULL DEFAULT '',
  `EncryptedURL` varchar(255) DEFAULT NULL COMMENT 'Contains an MD5 encrypted string representing the URL. The download controller can search the database using this string, which may be passed via GET variables in the URL',
  `TotalDownloads` int(10) NOT NULL DEFAULT '0',
  `PostDate` date DEFAULT '0000-00-00',
  `Published` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `down_cat_fk` (`CategoryID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `download_categories`
--

DROP TABLE IF EXISTS `<prefix>download_categories`;
CREATE TABLE IF NOT EXISTS `<prefix>download_categories` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `SiteID` int(5) NOT NULL,
  `ParentID` int(5) DEFAULT NULL,
  `Position` int(3) DEFAULT NULL,
  `CategoryName` varchar(200) NOT NULL DEFAULT '',
  `CategoryAlias` varchar(100) DEFAULT NULL,
  `CategoryImage` varchar(255) DEFAULT NULL,
  `CategoryDescription` text,
  PRIMARY KEY (`id`),
  KEY `DownCats_SiteID_FK` (`SiteID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `events`
--

DROP TABLE IF EXISTS `<prefix>events`;
CREATE TABLE IF NOT EXISTS `<prefix>events` (
  `id` int(8) NOT NULL AUTO_INCREMENT,
  `EventCategory` int(5) NOT NULL,
  `EventName` varchar(255) CHARACTER SET latin1 NOT NULL,
  `EventDesc` text CHARACTER SET latin1,
  `AssocImage` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `Time` varchar(30) CHARACTER SET latin1 DEFAULT NULL,
  `EventDuration` varchar(40) CHARACTER SET latin1 DEFAULT 'single',
  `Date` date NOT NULL,
  `StartDate` date DEFAULT NULL,
  `EndDate` date DEFAULT NULL,
  `Venue` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `RedirectURL` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `FriendlyURL` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `Published` varchar(5) CHARACTER SET latin1 DEFAULT 'false',
  PRIMARY KEY (`id`),
  UNIQUE KEY `Event_SEFURL_indx` (`FriendlyURL`),
  KEY `EventCat_indx` (`EventCategory`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `event_categories`
--

DROP TABLE IF EXISTS `<prefix>event_categories`;
CREATE TABLE IF NOT EXISTS `<prefix>event_categories` (
  `id` int(3) NOT NULL AUTO_INCREMENT,
  `SiteID` int(5) DEFAULT NULL,
  `Category` varchar(100) CHARACTER SET latin1 NOT NULL,
  `CategoryAlias` varchar(100) DEFAULT NULL,
  `CategoryDescription` varchar(200) CHARACTER SET latin1 DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `EventCat_SiteID_FK` (`SiteID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `feedback`
--

DROP TABLE IF EXISTS `<prefix>feedback`;
CREATE TABLE IF NOT EXISTS `<prefix>feedback` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `CategoryID` int(11) NOT NULL DEFAULT '1',
  `name` varchar(255) NOT NULL,
  `email` varchar(255) DEFAULT NULL,
  `phone_number` varchar(255) DEFAULT NULL,
  `website` varchar(255) DEFAULT NULL,
  `subject` varchar(255) DEFAULT NULL,
  `message` text,
  `date_posted` date DEFAULT NULL,
  `status` varchar(100) DEFAULT 'unread',
  PRIMARY KEY (`id`),
  KEY `FB_CatID_FK` (`CategoryID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `feedback_categories`
--

DROP TABLE IF EXISTS `<prefix>feedback_categories`;
CREATE TABLE IF NOT EXISTS `<prefix>feedback_categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `SiteID` int(5) DEFAULT NULL,
  `CategoryName` varchar(255) DEFAULT NULL,
  `CategoryTag` varchar(30) DEFAULT NULL COMMENT 'A Tag For Quick Identification of Categories',
  `CategoryDesc` text,
  `FwdToEmail` varchar(5) DEFAULT 'false',
  `FwdEmail` varchar(255) DEFAULT NULL,
  `DateCreated` date DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fb_site_fk` (`SiteID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `galleries`
--

DROP TABLE IF EXISTS `<prefix>galleries`;
CREATE TABLE IF NOT EXISTS `<prefix>galleries` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `SiteID` int(5) NOT NULL,
  `GalleryID` varchar(10) NOT NULL DEFAULT '',
  `GalleryName` varchar(250) NOT NULL DEFAULT '',
  `GalleryFolder` varchar(255) NOT NULL DEFAULT '',
  `GalleryDescription` text,
  `DateCreated` date DEFAULT NULL,
  `AllowComments` tinyint(1) NOT NULL DEFAULT '0',
  `ImageSortOrder` enum('RAND','DESC','ASC') DEFAULT 'DESC',
  `ImageWidth` int(5) NOT NULL DEFAULT '600',
  `ThumbnailWidth` int(5) NOT NULL DEFAULT '150',
  `Published` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `gallery_tag_idx` (`GalleryID`),
  KEY `siteid_idx` (`SiteID`) USING BTREE
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `gallery_images`
--

DROP TABLE IF EXISTS `<prefix>gallery_images`;
CREATE TABLE IF NOT EXISTS `<prefix>gallery_images` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `GalleryID` int(11) DEFAULT NULL,
  `ImageName` varchar(255) DEFAULT NULL,
  `ImageDescription` mediumtext,
  `ImageURL` varchar(255) DEFAULT NULL,
  `ImageThumbURL` varchar(255) DEFAULT NULL,
  `ImageWidth` int(4) DEFAULT NULL,
  `ImageHeight` int(4) DEFAULT NULL,
  `Published` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `gal_imgs_fk` (`GalleryID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `images`
--

DROP TABLE IF EXISTS `<prefix>images`;
CREATE TABLE IF NOT EXISTS `<prefix>images` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `SiteID` int(5) DEFAULT NULL,
  `GalleryID` varchar(10) DEFAULT '',
  `UseSection` varchar(255) DEFAULT NULL,
  `PageID` varchar(255) DEFAULT NULL,
  `ImageName` varchar(250) NOT NULL DEFAULT '',
  `ImageDescription` varchar(250) NOT NULL DEFAULT '',
  `ImageFile` varchar(255) NOT NULL DEFAULT 'none',
  `ThumbPath` varchar(255) DEFAULT 'none',
  `DateAdded` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `SiteID` (`SiteID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `image_categories`
--

DROP TABLE IF EXISTS `<prefix>image_categories`;
CREATE TABLE IF NOT EXISTS `<prefix>image_categories` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `CategoryName` varchar(255) CHARACTER SET latin1 NOT NULL,
  `CategoryCode` varchar(255) CHARACTER SET latin1 NOT NULL,
  `ImageFolder` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `ThumbnailFolder` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `MaxImageWidth` int(4) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `managers`
--

DROP TABLE IF EXISTS `<prefix>managers`;
CREATE TABLE IF NOT EXISTS `<prefix>managers` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `CategoryID` int(5) NOT NULL,
  `ManagerName` varchar(255) CHARACTER SET latin1 NOT NULL,
  `ManagerDescription` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `ManagerClass` varchar(255) CHARACTER SET latin1 NOT NULL,
  `ManagerCMSFile` varchar(255) DEFAULT NULL,
  `ManagerTable` varchar(255) CHARACTER SET latin1 NOT NULL,
  `ManagerOptions` int(5) DEFAULT NULL,
  `Activated` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `man_cat_fk` (`CategoryID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `manager_categories`
--

DROP TABLE IF EXISTS `<prefix>manager_categories`;
CREATE TABLE IF NOT EXISTS `<prefix>manager_categories` (
  `id` int(3) NOT NULL AUTO_INCREMENT,
  `CategoryName` varchar(255) CHARACTER SET latin1 NOT NULL,
  `CategoryDescription` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `CategoryType` enum('general','site_specific') DEFAULT 'site_specific',
  `Position` int(2) DEFAULT NULL,
  `Activated` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `media`
--

DROP TABLE IF EXISTS `<prefix>media`;
CREATE TABLE IF NOT EXISTS `<prefix>media` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `SiteID` int(5) NOT NULL,
  `CategoryID` int(5) DEFAULT NULL,
  `MediaName` varchar(255) NOT NULL,
  `MediaDesc` varchar(255) DEFAULT NULL,
  `MediaType` varchar(20) DEFAULT NULL,
  `MediaArtiste` varchar(200) DEFAULT NULL,
  `MediaGenre` varchar(100) DEFAULT NULL,
  `MediaDuration` varchar(20) DEFAULT NULL,
  `MediaPath` varchar(255) DEFAULT NULL,
  `MediaEmbedCode` text,
  `MediaWidth` varchar(10) DEFAULT NULL,
  `MediaHeight` varchar(10) DEFAULT NULL,
  `MediaThumbnail` varchar(255) DEFAULT NULL,
  `DatePosted` date DEFAULT NULL,
  `Published` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `media_site_FK` (`SiteID`),
  KEY `media_cat_fk` (`CategoryID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `media_categories`
--

DROP TABLE IF EXISTS `<prefix>media_categories`;
CREATE TABLE IF NOT EXISTS `<prefix>media_categories` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `SiteID` int(5) DEFAULT NULL,
  `Position` int(2) DEFAULT NULL,
  `Name` varchar(200) DEFAULT NULL,
  `Alias` varchar(200) DEFAULT NULL,
  `Description` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

DROP TABLE IF EXISTS `<prefix>pages`;
CREATE TABLE IF NOT EXISTS `<prefix>pages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `SiteID` int(5) NOT NULL,
  `ParentID` int(11) DEFAULT NULL,
  `PagePosition` int(5) NOT NULL,
  `PageLevel` int(3) DEFAULT NULL,
  `PageName` varchar(255) DEFAULT NULL,
  `PageNameAlias` varchar(255) DEFAULT NULL,
  `PageDescription` varchar(255) DEFAULT NULL,
  `PageSummary` text,
  `PagePath` varchar(255) DEFAULT NULL COMMENT 'The hierachical path of this page',
  `PageType` enum('homepage','sitepage') DEFAULT 'sitepage',
  `PageContentType` enum('php','html') NOT NULL DEFAULT 'html',
  `PageContent` text,
  `PageLayout` varchar(100) DEFAULT NULL,
  `RedirectURL` varchar(255) DEFAULT NULL,
  `Published` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `page_site_fk` (`SiteID`),
  KEY `page_parent_fk` (`ParentID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `page_blocks`
--

DROP TABLE IF EXISTS `<prefix>page_blocks`;
CREATE TABLE IF NOT EXISTS `<prefix>page_blocks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `PageID` int(11) NOT NULL,
  `BlockPosition` int(5) DEFAULT NULL,
  `BlockTitle` varchar(255) NOT NULL,
  `BlockAlias` varchar(255) DEFAULT NULL,
  `BlockDescription` varchar(255) DEFAULT NULL,
  `BlockContentType` enum('php','html') DEFAULT NULL,
  `BlockContent` text,
  PRIMARY KEY (`id`),
  KEY `block_page_fk` (`PageID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `quick_links`
--

DROP TABLE IF EXISTS `<prefix>quick_links`;
CREATE TABLE IF NOT EXISTS `<prefix>quick_links` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `LinkCategory` int(3) DEFAULT NULL,
  `LinkName` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `LinkDesc` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `LinkURL` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `LinkTarget` enum('','_blank','_self') DEFAULT '_self' COMMENT 'The target window in to show the page in. Conforms to the HTML Anchor Target attribute',
  `LinkPosition` int(3) DEFAULT NULL,
  `Published` varchar(10) NOT NULL DEFAULT 'false',
  PRIMARY KEY (`id`),
  KEY `QLCats_FK` (`LinkCategory`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `quick_links_categories`
--

DROP TABLE IF EXISTS `<prefix>quick_links_categories`;
CREATE TABLE IF NOT EXISTS `<prefix>quick_links_categories` (
  `id` int(3) NOT NULL AUTO_INCREMENT,
  `SiteID` int(6) DEFAULT NULL,
  `CategoryName` varchar(255) DEFAULT NULL,
  `CategoryDescription` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `QLCats_SiteID_FK` (`SiteID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sites`
--

DROP TABLE IF EXISTS `<prefix>sites`;
CREATE TABLE IF NOT EXISTS `<prefix>sites` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `SiteIdentifier` varchar(50) NOT NULL,
  `SiteType` varchar(50) DEFAULT NULL,
  `SiteName` varchar(255) NOT NULL,
  `SiteDomain` varchar(255) DEFAULT NULL,
  `SiteCMSLogo` varchar(255) DEFAULT NULL,
  `SiteSlogan` varchar(255) DEFAULT NULL,
  `SiteDescription` text,
  `SiteEmail` varchar(255) DEFAULT NULL,
  `SitePhone` varchar(255) DEFAULT NULL,
  `SiteAddress` varchar(255) DEFAULT NULL,
  `SiteKeywords` text,
  `SiteLogo` varchar(255) DEFAULT NULL,
  `SiteHomePageText` text,
  `SiteFooterText` text,
  `SiteThemeID` int(5) DEFAULT NULL,
  `SiteThemeCSSID` int(5) DEFAULT NULL,
  `SiteFavicon` varchar(255) DEFAULT '../favicon.ico' COMMENT 'Provides the path to the favicon.ico file for this site. This defaults to the PNG file provided with the KAN installation',
  `SiteActive` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `SiteIden_Idx` (`SiteIdentifier`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `site_statistics`
--

DROP TABLE IF EXISTS `<prefix>site_statistics`;
CREATE TABLE IF NOT EXISTS `<prefix>site_statistics` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `SiteID` int(4) NOT NULL,
  `PageTitle` varchar(255) DEFAULT NULL,
  `PageURL` varchar(255) DEFAULT NULL,
  `RefererURL` varchar(255) DEFAULT NULL,
  `ViewDate` date DEFAULT NULL,
  `ViewCount` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `site_stats_fk` (`SiteID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `site_themes`
--

DROP TABLE IF EXISTS `<prefix>site_themes`;
CREATE TABLE IF NOT EXISTS `<prefix>site_themes` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `ThemeName` varchar(255) DEFAULT NULL,
  `ThemeDesc` text,
  `ThemeFolder` varchar(255) DEFAULT NULL,
  `ThemePreview` varchar(255) DEFAULT NULL,
  `DateAdded` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `site_themes_css`
--

DROP TABLE IF EXISTS `<prefix>site_themes_css`;
CREATE TABLE IF NOT EXISTS `<prefix>site_themes_css` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `ThemeID` int(5) DEFAULT NULL,
  `CSSName` varchar(255) DEFAULT NULL,
  `CSSDescription` text,
  `CSSFolder` varchar(255) DEFAULT NULL,
  `CSSURL` varchar(255) DEFAULT NULL,
  `InUse` varchar(10) DEFAULT NULL,
  `UseArea` varchar(30) DEFAULT NULL COMMENT 'Homepage Or Pages',
  `DateAdded` date DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `Temp_CSS_FK` (`ThemeID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `spotlights`
--

DROP TABLE IF EXISTS `<prefix>spotlights`;
CREATE TABLE IF NOT EXISTS `<prefix>spotlights` (
  `id` int(4) NOT NULL AUTO_INCREMENT,
  `SiteID` int(5) DEFAULT NULL,
  `menu_header` varchar(255) DEFAULT NULL,
  `menu_subheader` varchar(255) DEFAULT NULL,
  `spotlight` varchar(255) NOT NULL,
  `spotlight_url` varchar(255) DEFAULT NULL,
  `description` text,
  `date` date NOT NULL,
  `position` int(3) DEFAULT NULL,
  `approved` enum('false','true') NOT NULL DEFAULT 'false',
  PRIMARY KEY (`id`),
  KEY `SL_SiteID_FK` (`SiteID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `system_help`
--

DROP TABLE IF EXISTS `<prefix>system_help`;
CREATE TABLE IF NOT EXISTS `<prefix>system_help` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `HelpCategory` int(5) DEFAULT NULL,
  `FieldID` varchar(50) DEFAULT NULL,
  `HelpTitle` varchar(255) DEFAULT NULL,
  `HelpText` text,
  `DateAdded` date DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `help_cat_fk` (`HelpCategory`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `system_help_categories`
--

DROP TABLE IF EXISTS `<prefix>system_help_categories`;
CREATE TABLE IF NOT EXISTS `<prefix>system_help_categories` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `CategoryName` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `system_settings`
--

DROP TABLE IF EXISTS `<prefix>system_settings`;
CREATE TABLE IF NOT EXISTS `<prefix>system_settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `SettingCategory` int(5) DEFAULT NULL,
  `SettingKey` varchar(100) DEFAULT NULL,
  `SettingValue` varchar(255) DEFAULT NULL,
  `SettingName` varchar(255) DEFAULT NULL,
  `SettingType` enum('number','array','text','bool') DEFAULT NULL,
  `SettingDescription` text,
  PRIMARY KEY (`id`),
  KEY `set_cat_fk` (`SettingCategory`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `system_settings_categories`
--

DROP TABLE IF EXISTS `<prefix>system_settings_categories`;
CREATE TABLE IF NOT EXISTS `<prefix>system_settings_categories` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `CategoryName` varchar(255) DEFAULT NULL,
  `CategoryDescription` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `<prefix>users`;
CREATE TABLE IF NOT EXISTS `<prefix>users` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `FirstName` varchar(50) NOT NULL DEFAULT '',
  `LastName` varchar(50) NOT NULL DEFAULT '',
  `FullName` varchar(120) NOT NULL,
  `Username` varchar(50) NOT NULL,
  `Password` varchar(100) NOT NULL,
  `PassSalt` varchar(200) DEFAULT NULL,
  `Email` varchar(255) DEFAULT NULL,
  `Level` varchar(20) NOT NULL DEFAULT 'editor',
  `Role` int(5) DEFAULT NULL,
  `SecureQuestion` varchar(200) DEFAULT NULL,
  `SecureAnswer` varchar(200) DEFAULT NULL,
  `LastAccess` date DEFAULT '0000-00-00',
  `Activated` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `usr_role_fk` (`Role`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `users_sites`
--

DROP TABLE IF EXISTS `<prefix>users_sites`;
CREATE TABLE IF NOT EXISTS `<prefix>users_sites` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `User` int(5) NOT NULL,
  `Site` int(5) NOT NULL,
  `DateModified` date NOT NULL,
  PRIMARY KEY (`id`),
  KEY `User` (`User`),
  KEY `Site` (`Site`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `user_roles`
--

DROP TABLE IF EXISTS `<prefix>user_roles`;
CREATE TABLE IF NOT EXISTS `<prefix>user_roles` (
  `id` int(3) NOT NULL AUTO_INCREMENT,
  `role_name` varchar(150) NOT NULL,
  `role_tag` varchar(20) NOT NULL,
  `role_description` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `user_role_menus`
--

DROP TABLE IF EXISTS `<prefix>user_role_menus`;
CREATE TABLE IF NOT EXISTS `<prefix>user_role_menus` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role_id` int(5) DEFAULT NULL,
  `menu_id` int(5) DEFAULT NULL COMMENT 'References the Manager Categories item',
  PRIMARY KEY (`id`),
  KEY `usr_role_id` (`role_id`),
  KEY `usr_menu_id` (`menu_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `advert_images`
--
ALTER TABLE `<prefix>advert_images`
  ADD CONSTRAINT `advert_images_ibfk_1` FOREIGN KEY (`SiteID`) REFERENCES `<prefix>sites` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `articles`
--
ALTER TABLE `<prefix>articles`
  ADD CONSTRAINT `art_cat_fk` FOREIGN KEY (`CategoryID`) REFERENCES `<prefix>article_categories` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `art_site_fk` FOREIGN KEY (`SiteID`) REFERENCES `<prefix>sites` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `article_categories`
--
ALTER TABLE `<prefix>article_categories`
  ADD CONSTRAINT `art_par_fk` FOREIGN KEY (`ParentID`) REFERENCES `<prefix>article_categories` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `art_site_fk1` FOREIGN KEY (`SiteID`) REFERENCES `<prefix>sites` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `article_comments`
--
ALTER TABLE `<prefix>article_comments`
  ADD CONSTRAINT `com_art_fk` FOREIGN KEY (`ArticleID`) REFERENCES `<prefix>articles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `banner_images`
--
ALTER TABLE `<prefix>banner_images`
  ADD CONSTRAINT `Banner_SiteID_FK` FOREIGN KEY (`SiteID`) REFERENCES `<prefix>sites` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `downloads`
--
ALTER TABLE `<prefix>downloads`
  ADD CONSTRAINT `down_cat_fk` FOREIGN KEY (`CategoryID`) REFERENCES `<prefix>download_categories` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `download_categories`
--
ALTER TABLE `<prefix>download_categories`
  ADD CONSTRAINT `DownCats_SiteID_FK` FOREIGN KEY (`SiteID`) REFERENCES `<prefix>sites` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `events`
--
ALTER TABLE `<prefix>events`
  ADD CONSTRAINT `event_cat_fk` FOREIGN KEY (`EventCategory`) REFERENCES `<prefix>event_categories` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `event_categories`
--
ALTER TABLE `<prefix>event_categories`
  ADD CONSTRAINT `EventCat_SiteID_FK` FOREIGN KEY (`SiteID`) REFERENCES `<prefix>sites` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `feedback`
--
ALTER TABLE `<prefix>feedback`
  ADD CONSTRAINT `FB_CatID_FK` FOREIGN KEY (`CategoryID`) REFERENCES `<prefix>feedback_categories` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `feedback_categories`
--
ALTER TABLE `<prefix>feedback_categories`
  ADD CONSTRAINT `fb_site_fk` FOREIGN KEY (`SiteID`) REFERENCES `<prefix>sites` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `galleries`
--
ALTER TABLE `<prefix>galleries`
  ADD CONSTRAINT `gallery_site_FK` FOREIGN KEY (`SiteID`) REFERENCES `<prefix>sites` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `gallery_images`
--
ALTER TABLE `<prefix>gallery_images`
  ADD CONSTRAINT `gal_imgs_fk` FOREIGN KEY (`GalleryID`) REFERENCES `<prefix>galleries` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `images`
--
ALTER TABLE `<prefix>images`
  ADD CONSTRAINT `Images_SiteID_FK` FOREIGN KEY (`SiteID`) REFERENCES `<prefix>sites` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `managers`
--
ALTER TABLE `<prefix>managers`
  ADD CONSTRAINT `man_cat_fk` FOREIGN KEY (`CategoryID`) REFERENCES `<prefix>manager_categories` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `media`
--
ALTER TABLE `<prefix>media`
  ADD CONSTRAINT `media_cat_fk` FOREIGN KEY (`CategoryID`) REFERENCES `<prefix>media_categories` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `media_site_FK` FOREIGN KEY (`SiteID`) REFERENCES `<prefix>sites` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `pages`
--
ALTER TABLE `<prefix>pages`
  ADD CONSTRAINT `page_parent_fk` FOREIGN KEY (`ParentID`) REFERENCES `<prefix>pages` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `page_site_fk` FOREIGN KEY (`SiteID`) REFERENCES `<prefix>sites` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `page_blocks`
--
ALTER TABLE `<prefix>page_blocks`
  ADD CONSTRAINT `block_page_fk` FOREIGN KEY (`PageID`) REFERENCES `<prefix>pages` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `quick_links`
--
ALTER TABLE `<prefix>quick_links`
  ADD CONSTRAINT `QLCats_FK` FOREIGN KEY (`LinkCategory`) REFERENCES `<prefix>quick_links_categories` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `quick_links_categories`
--
ALTER TABLE `<prefix>quick_links_categories`
  ADD CONSTRAINT `QLCats_SiteID_FK` FOREIGN KEY (`SiteID`) REFERENCES `<prefix>sites` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `site_statistics`
--
ALTER TABLE `<prefix>site_statistics`
  ADD CONSTRAINT `site_stats_fk` FOREIGN KEY (`SiteID`) REFERENCES `<prefix>sites` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `site_themes_css`
--
ALTER TABLE `<prefix>site_themes_css`
  ADD CONSTRAINT `site_themes_css_ibfk_1` FOREIGN KEY (`ThemeID`) REFERENCES `<prefix>site_themes` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `spotlights`
--
ALTER TABLE `<prefix>spotlights`
  ADD CONSTRAINT `SL_SiteID_FK` FOREIGN KEY (`SiteID`) REFERENCES `<prefix>sites` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `system_help`
--
ALTER TABLE `<prefix>system_help`
  ADD CONSTRAINT `help_cat_fk` FOREIGN KEY (`HelpCategory`) REFERENCES `<prefix>system_help_categories` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `system_settings`
--
ALTER TABLE `<prefix>system_settings`
  ADD CONSTRAINT `set_cat_fk` FOREIGN KEY (`SettingCategory`) REFERENCES `<prefix>system_settings_categories` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `users`
--
ALTER TABLE `<prefix>users`
  ADD CONSTRAINT `usr_role_fk` FOREIGN KEY (`Role`) REFERENCES `<prefix>user_roles` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE;

--
-- Constraints for table `users_sites`
--
ALTER TABLE `<prefix>users_sites`
  ADD CONSTRAINT `users_sites_ibfk_1` FOREIGN KEY (`User`) REFERENCES `<prefix>users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `users_sites_ibfk_2` FOREIGN KEY (`Site`) REFERENCES `<prefix>sites` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `user_role_menus`
--
ALTER TABLE `<prefix>user_role_menus`
  ADD CONSTRAINT `usr_menu_id` FOREIGN KEY (`menu_id`) REFERENCES `<prefix>manager_categories` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `usr_role_id` FOREIGN KEY (`role_id`) REFERENCES `<prefix>user_roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Dumping data for table `advert_types`
--

INSERT INTO `<prefix>advert_types` (`id`, `TypeName`, `TypeExtension`) VALUES
(1, 'JPEG Image', 'jpg'),
(2, 'PNG Image', 'png'),
(3, 'Flash Animation', 'swf'),
(4, 'Java Applet', 'jar');

--
-- Dumping data for table `advert_types_cats`
--

INSERT INTO `<prefix>advert_types_cats` (`id`, `TypeExtension`, `CategoryName`, `CategoryCode`, `Width`, `Height`) VALUES
(2, 'swf', 'Full Banner', '468x80', '468', '80'),
(3, 'swf', 'Vertical Banner', '120x240', '120', '240'),
(4, 'swf', 'Sky Scrapper', '120x600', '120', '600'),
(5, 'swf', 'Rectangle', '180x150', '180', '150'),
(6, 'swf', 'Square Pop-up Banner', '250x250', '250', '250'),
(7, 'swf', 'Medium Rectangle', '300x250', '300', '250'),
(8, 'swf', 'Vertical Rectangle', '240x400', '240', '400'),
(1, 'swf', 'Custom', '468x80', '468', '80');

--
-- Dumping data for table `banner_types`
--

INSERT INTO `<prefix>banner_types` (`id`, `TypeName`, `TypeExtension`) VALUES
(1, 'JPEG Image', 'jpg'),
(2, 'PNG Image', 'png'),
(3, 'Flash Animation', 'swf'),
(4, 'Java Applet', 'jar');

--
-- Dumping data for table `comments`
--


--
-- Dumping data for table `components`
--

INSERT INTO `<prefix>components` (`id`, `ComponentType`, `Component`, `ComponentName`, `Description`, `ComponentPath`, `DateAdded`) VALUES
(1, 'widget', 'banner', 'Alternating Banners', 'Displays on banner image or animation at a time one page refresh or load.', '../assets/components/banners/banner.php', '2010-07-17'),
(4, 'widget', 'spotlight', 'Spotlight', 'Displays a the images chosen in the Spotlight Extension', '../assets/components/spotlight/spotlight.php', '2009-12-17'),
(5, 'page', 'gallery', 'Photo Gallery', '<p>Displays the albums and images in the Photo Gallery of the site</p>', '../assets/components/gallery/gallery.php', '2009-12-17'),
(6, 'widget', 'quick_links_sidebar', 'Quick Links SIdebar', 'Displays the site quick links as a simple sidebar with heading for each of the link categories', '../assets/components/quicklinks/ql_sidebar.php', '2009-12-17'),
(7, 'widget', 'quick_links_jump_menus', 'Quick Links Jump Menus', 'Displays the quick links for the site as Jump Menu ComboBoxes grouped by category.<br><Br>\r\n\r\nTo use this component, the theme must ensure to implement the component specific CSS', '../assets/components/quicklinks/ql_jump_menus.php', '2009-12-17'),
(11, 'page', 'search_results', 'Google Search Results', 'Displays the search results of a google search on the site domain', '../assets/components/search/search_results.php', '2010-02-14'),
(12, 'widget', 'search_box', 'Google Search Box', 'Displays a search field which provides search functionality for the selected site', '../assets/components/search/search_box.php', '2010-02-14'),
(13, 'widget', 'feedback', 'Feeback Form', '<p>Provides an embeddable feedback form, which can be used to provide feedback for the site</p>', '../assets/components/feedback/feedback.php', '2010-02-16'),
(15, 'widget', 'slider-gallery', 'Slider Gallery', '<p>A JQuery powered slider Photo Gallery</p>', '../assets/components/sliderviewer/sliderviewer.php', '2010-06-25');

-- Dumping data for table `downloads`
--

--
-- Dumping data for table `image_categories`
--

INSERT INTO `<prefix>image_categories` (`id`, `CategoryName`, `CategoryCode`, `ImageFolder`, `ThumbnailFolder`) VALUES
(1, 'Page Content', 'cms', '../assets/images/content/pics/', '../assets/images/content/thumbs/'),
(2, 'Articles', 'articles', '../assets/images/content/pics/', '../assets/images/content/thumbs/');

--
-- Dumping data for table `managers`
--

INSERT INTO `<prefix>managers` (`id`, `CategoryID`, `ManagerName`, `ManagerDescription`, `ManagerClass`, `ManagerCMSFile`, `ManagerTable`, `ManagerOptions`, `Activated`) VALUES
(1, 2, 'Site Pages', 'Manage Your Site Pages', 'PageManager', 'pages.php', 'pages', NULL, 1),
(2, 2, 'Content Blocks', 'Block / Snippet Code Management', 'PageManager', 'blocks.php', 'page_blocks', NULL, 0),
(3, 2, 'Articles', 'Unified Article Management', 'ArticlesManager', 'articles.php', 'articles', NULL, 1),
(4, 2, 'Events & Notices', 'Event Management', 'EventsManager', 'events.php', 'events', NULL, 1),
(5, 2, 'Quick Links', 'Quick direct links management', 'LinksManager', 'quick_links.php', 'quick_links', NULL, 1),
(6, 2, 'Downloads', 'Download Management', 'DownloadManager', 'downloads.php', 'downloads', NULL, 1),
(14, 3, 'Banner Management', 'Banner Image Management', 'BannerManager', 'banners.php', 'banner_images', NULL, 0),
(15, 3, 'Spotlights (Banners)', 'Spotlight Management', 'SpotlightManager', 'spotlight.php', 'spotlights', NULL, 1),
(16, 4, 'Advertisements', 'Advertisement Management', 'AdManager', 'adverts.php', 'adverts', NULL, 1),
(17, 4, 'Manage Images', 'Image Management', 'ImageManager', 'image_manager.php', 'images', NULL, 1),
(18, 4, 'Other Media', 'Manage Other Media Files aside images', 'MediaManager', 'media.php', 'media', NULL, 1),
(19, 4, 'Photo Gallery', 'Create and Manage Photo Galleries', 'GalleryManager', 'galleries.php', 'galleries', NULL, 1),
(20, 5, 'Site Feedback', 'Manage User Feedback supplied via feedback forms', 'FeedbackManager', 'feedback.php', 'feedback', NULL, 1),
(21, 5, 'Article Comments', 'Manager User Comments on Articles', 'ArticlesManager', 'comments.php', 'article_comments', NULL, 1),
(22, 1, 'Sites Home', 'Manage All Sites Present in your installation', 'SitesManager', 'sites.php', 'sites', NULL, 0),
(23, 1, 'Manage Components', 'Manage All Components', 'ComponentsManager', 'components.php', 'components', NULL, 1),
(24, 1, 'Manage Themes', 'Manage Themes in your installation', 'ThemeManager', 'themes.php', 'site_themes', NULL, 1),
(25, 1, 'Manage Users', 'User Management', 'UserManager', 'users.php', 'users', NULL, 1),
(26, 1, 'System Configuration', 'Manage System Settings and Configuration', 'SystemManager', 'system.php', 'system_settings', NULL, 1),
(27, 1, 'Usage Statistics', 'View and Manage Usage Statistic', 'StatsManager', 'index.php?page=system_stats', 'site_statistics', NULL, 1);

--
--
-- Dumping data for table `manager_categories`
--

INSERT INTO `<prefix>manager_categories` (`id`, `CategoryName`, `CategoryDescription`, `CategoryType`, `Position`, `Activated`) VALUES
(1, 'CMS Management Options', 'CMS Management Interfaces', 'general', 1, 1),
(2, 'Site Layout and Content', 'Layout and Content Management', 'site_specific', 2, 1),
(3, 'Site UI Management', 'UI Element', 'site_specific', 3, 1),
(4, 'Media Management', 'Media and File Management', 'site_specific', 4, 1),
(5, 'Feedback Management', 'User Feedback Management', 'site_specific', 5, 1);



--
-- Dumping data for table `media_types`
--

--
-- Dumping data for table `quick_links`
--


--
-- Dumping data for table `site_themes`
--

INSERT INTO `<prefix>site_themes` (`id`, `ThemeName`, `ThemeDesc`, `ThemeFolder`, `ThemePreview`, `DateAdded`) VALUES
(1, 'KAN Default Theme', '<p>Default KAN CMS Theme. This theme is designed for use with the default KAN Site.</p>\r\n<ul>\r\n</ul>', '../assets/themes/default/', '../assets/images/content/thumbs/img2842010_173021.jpg', NULL),
(2, 'KAN Silver Theme', '<p>A simple alternative theme</p>', '../assets/themes/silver/', '../assets/images/content/thumbs/img2842010_172753.jpg', '2010-05-21'),
(26, 'KAN Blog Theme', '<p>This is a simple KAN Theme which will be used to power a KAN CMS based blog site.</p>', '../assets/themes/simple-blog/', NULL, '2010-08-16');

--
-- Dumping data for table `site_themes_css`
--

INSERT INTO `<prefix>site_themes_css` (`id`, `ThemeID`, `CSSName`, `CSSDescription`, `CSSFolder`, `CSSURL`, `InUse`, `UseArea`, `DateAdded`) VALUES
(1, 1, 'Default Theme CSS', '<p>This is the default themes CSS and should not be deleted. The ThemeFolder specification has been left blank in order to prevent the from being deleted accidentally if even clicked</p>', '../assets/themes/default/css/default/', '../assets/themes/default/css/default/default.css', NULL, NULL, '2009-08-02'),
(3, 1, 'Simple Theme', '<p>Alternate Style Sheet for Default KAN Theme</p>\r\n<p>Color: Gold</p>', '../assets/themes/default/css/simple/', '../assets/themes/default/css/simple/simple.css', NULL, NULL, '2009-08-04'),
(7, 2, 'Default Theme CSS', 'Default Theme Stylesheet Which is included by default', '../assets/themes/silver/css/default/', '../assets/themes/silver/css/default/default.css', NULL, NULL, '2010-05-21'),
(48, 26, 'Default Theme CSS', 'Default Theme Stylesheet Which is included by default', '../assets/themes/simple-blog/css/default/', '../assets/themes/simple-blog/css/default/default.css', NULL, NULL, '2010-08-16');

--
-- Dumping data for table `system_help`
--

INSERT INTO `<prefix>system_help` (`id`, `HelpCategory`, `FieldID`, `HelpTitle`, `HelpText`, `DateAdded`) VALUES
(1, 1, 'SiteIdentifier', 'Site Identifier', 'A simple <strong>unique</strong> identifier for this site, eg <strong>info</strong> for <em>Info Tech Company Inc.</em>', '2010-04-03'),
(2, 1, 'Level', 'User Access Level', '<ul>\r\n                                                            <li><strong>Administrators</strong> have all rights to the all sites and the CMS, except the ability to delete the Super User Account</li>\r\n                                                            <li><strong>Content Editors</strong> are semi-administrators and have access to all sites, however, they are not permitted\r\n                                	to create or delete sites</li>\r\n                                                            <li><strong>Site Specific Admin</strong>s are Content Editors for only specific sites in the CMS chosen by an administrator</li>\r\n                                                         </ul>', '2010-04-03'),
(3, 1, 'SiteDomain', 'Site Public Domain', 'This is used by the KAN Site loader to determine which site to load if your KAN setup uses subdomains to redirect the content for the each sub site created. e.g.:\r\n\r\n<ul>\r\n<li><b>www</b>.yoursite.com</li>\r\n<li><b>department</b>.yoursite.com</li>\r\n</ul>\r\n\r\nUsually the <b>SiteIdentifer</b> field would be the same as your subdomain name, but would be similar to the main site name when specifying your default site.', '2010-04-06'),
(4, 1, 'uploadFile', 'File To Upload', '<p>Several files are permitted for upload and the maximum file size limit is dependent on the area of the CMS where file uploads are being carried out.\r\n\r\n<p>If not specified by the manager utility, the following file types are permitted:\r\n<ul>\r\n	<li><b>Images</b>: jpg, gif, png, jpeg\r\n	<li><b>Media Files</b>: mp3, mp4, m4v, flv, amr, midi, mpg, mpeg, wmv, divx, wma, swf\r\n	<li><b>Compressed Files</b>: zip, jar, rar\r\n	<li><b>Documents</b>: pdf, doc, xml, ppt, xls, docx, xlsx, pptx\r\n</ul>', '2010-04-18'),
(5, 1, 'ComponentPath', 'Component File Path', 'The file path to the Primary Component file. When creating a new component, the template path <b>(../components/&lt;comp-dir&gt;/&lt;comp-file&gt;.php)</b> must be completed with the following information:\r\n\r\n<ul>\r\n    <li><b>&lt;comp-dir&gt;</b>: The Component Directory. This will be created if it does not exist. Sometimes it is ideal to group related components in the same directory even though they will be registered differently and can be included independently. e.g. Various types of menus can be grouped in a Menus folder</li>\r\n    <li><b>&lt;comp-file&gt;</b>: The Component File Name. This is will be created with the content entered when creating and registering the component</li>\r\n</ul>', '2010-04-24'),
(6, 1, 'Component', 'Component ID', 'The Identifier to use access this widget/page component via direct access through the api call. Component IDs cannot have spaces in them in order that they may be kept simple ', '2010-04-24'),
(7, 1, 'TargetPage', 'Menu Item Target Page', '<p>This field specifies the relative path to a page in this site. If this Menu Item is not to be directed to any page, then the field should have a <b>#</b>. \r\n\r\n<p>Example path that this field can take:\r\n<ul>\r\n    <li><b>index.php</b>: This will direct to the home page of the site and load the <b>default</b> KAN site for this installation\r\n    <li><b>../pages/index.php?siteid=mysite</b>: This will direct back to the home page of the site with the ID <b>mysite</b>\r\n</ul>', '2010-05-08'),
(8, 1, 'SiteFavicon', 'Site Favourites Icon', '<p>Specifies the file path to the Favourite Icon file. The default file path <b>"../favicon.ico"</b> points to KAN''s default favourites icon,\r\nin ICO format for compartibility with Internet Explorer\r\n\r\n<p>Favourites Icons can be uploaded via the Image Manager for the specified site, and the file path referenced', '2010-05-09'),
(9, 1, 'CategoryTag', 'Feedback Category Tag', '<p>Provides a simple tag that can be used instead of the Category ID to identify the category to which specific user feedback belong to. \r\nE.g. the tags <b>general</b> or <b>requests</b> can used to identify the <b><i>General Feedback</i></b> and <b><i>Consumer Product Requests</i></b>\r\n feedback categories respectively.', '2010-05-19'),
(10, 1, 'FwdToEmail', 'Forward Feedback To Email Address', '<p>Setting this option to <b>Yes</b> will ensure that all feedback sent by users will be forwarded to the specified email address below</p>', '2010-05-19'),
(11, 1, 'SiteEmail', 'Site Email Address', '<p>An email address to use for this site. This address is used by the feedback manager to forward all out going mails from this site. </p>\r\n<p>If this is not specified the user''s email address will be used, and the emails may end up as spam since the addresses keep varying</p>\r\n<p><b>Note:</b> To help prevent mails from going to SPAM, ensure the address used has the same domain as where KAN is being hosted. e.g. <b>webmaster@your-domain.org</b></p>', '2010-06-07'),
(12, 1, 'SectionAlias', 'Page Search Engine Friendly (SEF) URL Alias', '<p>This field indicates the Alias / Alternate form of the page title that would be used to when Search Engine Friendly URLs are enabled. \r\nThis may be used standalone or as part of a larger url system\r\n\r\n<p>e.g. "Contact Us" would be represented as <b>"contact-us"</b>', '2010-07-10'),
(13, 1, 'MenuAlias', 'Menu Search Engine Friendly (SEF) URL Alias', '<p>This field indicates the Alias / Alternate form of the menu title that would be used to when Search Engine Friendly URLs are enabled. \r\nThis may be used standalone or as part of a larger url system\r\n\r\n<p>e.g. "About Us" would be represented as <b>"about-us"</b>', '2010-07-10');

--
-- Dumping data for table `system_help_categories`
--

INSERT INTO `<prefix>system_help_categories` (`id`, `CategoryName`) VALUES
(1, 'Input Field Help');

--
-- Dumping data for table `system_settings`
--

INSERT INTO `<prefix>system_settings` (`id`, `SettingCategory`, `SettingKey`, `SettingValue`, `SettingName`, `SettingType`, `SettingDescription`) VALUES
(1, 1, 'RecordStats', 'false', 'Record Usage Statistics', 'bool', 'When set to true, KAN will record page visits for statistical reporting purposes'),
(2, 1, 'RecordLocalStats', 'false', 'Record Localhost Usage Statistics', 'bool', 'When set to true, KAN will record page visit information for local kan installations, i.e. when $_SERVER[''HTTP_HOST''] == ''localhost'''),
(3, 1, 'CacheEnabled', 'false', 'Cache Content', 'bool', 'When set to true, KAN will cache page requests to improve load performance of sites'),
(4, 1, 'UseFriendlyURLs', 'true', 'Use Friendly URLs', 'bool', 'When set to true, KAN internal classes will return SEF URL versions of the various pages and content instead of the "dirty" url version where record primary key IDs are shown'),
(5, 2, 'CMSTitle', 'KAN Content Management System', 'CMS Title', 'text', 'The title to be used by the Content Management Interface');

--
-- Dumping data for table `system_settings_categories`
--

INSERT INTO `<prefix>system_settings_categories` (`id`, `CategoryName`, `CategoryDescription`) VALUES
(1, 'Site Settings', NULL),
(2, 'CMS Settings', NULL);

--
-- Dumping data for table `user_roles`
--

INSERT INTO `<prefix>user_roles` (`id`, `role_name`, `role_tag`, `role_description`) VALUES
(1, 'Super Administrator', 'super', 'A user who has access to every single part of the system and cannot (should not) be deleted'),
(2, 'Administrator', 'admin', 'A user who has access to all parts of the system but can still be deleted'),
(3, 'Content Editor', 'editor', 'A user with all privelages but limited authority over system definition'),
(4, 'Site Specific Admin', 'specific', 'A user with access to only a specific site or set of sites in the system');

--
-- Dumping data for table `user_role_menus`
--

INSERT INTO `<prefix>user_role_menus` (`id`, `role_id`, `menu_id`) VALUES
(1, 1, 1),
(2, 1, 2),
(3, 1, 3),
(4, 1, 4),
(5, 1, 5),
(6, 2, 1),
(7, 2, 2),
(8, 2, 3),
(9, 2, 4),
(10, 2, 5),
(11, 3, 2),
(12, 3, 3),
(13, 3, 4),
(14, 3, 5),
(15, 4, 2),
(16, 4, 3),
(17, 4, 4),
(18, 4, 5);


SET FOREIGN_KEY_CHECKS=1;
