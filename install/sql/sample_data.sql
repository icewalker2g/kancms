
SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET FOREIGN_KEY_CHECKS=0;

--

--
-- Dumping data for table `articles`
--

INSERT INTO `articles` (`id`, `CategoryID`, `SiteID`, `ArticleTitle`, `ArticleTitleAlias`, `ArticleSummary`, `ArticleContent`, `ArticleSource`, `ArticleImage`, `ArticleThumbnail`, `ArticleURL`, `ArticleGallery`, `AllowComments`, `PostDate`, `Published`) VALUES
(1, 1, 1, 'KAN CMS 1.0 Beta Coming Soon', 'kan-cms-1-0-beta-coming-soon', '<p>The Premier Ghanaian CMS releases its sixth Beta Release Candidate, and this build sports some brave new features</p>', '<p>The Premier Ghanaian CMS releases its third Beta Release Candidate, and this build sports some brave new features</p>', 'KAN Team', '../assets/images/news/pics/img2010426_181416.png', '../assets/images/news/thumbs/img2010426_181416.png', NULL, NULL, 1, '2009-10-12', 1),
(2, 2, 1, 'My First KAN Website', 'my-first-kan-website', '<p>This article announces that my first website built with KAN has been made public.</p>', '<p>The KAN CMS 1.0 Release Candidate 2 has made available for download from the project site.</p>', 'KAN Team', 'none', 'none', NULL, NULL, 1, '2010-05-28', 0);

--
-- Dumping data for table `article_categories`
--

INSERT INTO `article_categories` (`id`, `SiteID`, `ParentID`, `Position`, `CategoryName`, `CategoryAlias`, `CategoryDescription`) VALUES
(1, 1, NULL, NULL, 'News Articles', 'news', NULL),
(2, 1, NULL, NULL, 'Archived', 'archived', 'Archived News Articles');

--
-- Dumping data for table `pages`
--

INSERT INTO `pages` (`id`, `SiteID`, `ParentID`, `PagePosition`, `PageLevel`,`PageName`, `PageNameAlias`, `PageDescription`, `PageSummary`, `PagePath`, `PageType`, `PageContentType`, `PageContent`, `RedirectURL`, `Published`) VALUES
(2, 1, NULL, 2, 1, 'About CMS', 'about-cms', NULL, NULL, '/about-cms', 'sitepage', 'html', NULL, NULL, 1),
(3, 1, 2, 1, 2, 'About KAN', 'about-kan', NULL, NULL, '/about-cms/about-kan', 'sitepage', 'html', '<p>KAN CMS'' most treasured functionality is the ability and ease in creating and managing multiple websites for various departments on the same database. Content can be reused in many places on the websites. Example of sub-sites created using KAN CMS can be accessed on the various departments'' websites at KNUST on this URL: <a href="http://www.knust.edu.gh/pages/sections.php?siteid=knust&amp;mid=15&amp;sid=1124&amp;id=441" target="_blank">List Of KNUST Departments</a></p>\r\n<p><a href="http://www.knust.edu.gh/pages/sections.php?siteid=knust&amp;mid=15&amp;sid=1124&amp;id=441" target="_blank"></a>KAN CMS is open source and can be adopted, modified and implemented or improved on by interested participants since it has over 70 templates, 100 CSS layouts and 20 modules which have been developed within its lifespan of less than one year. It enforces consistent branding and ensures that content is fresh and accurate. KAN CMS is new, it''s fresh, and its level of functionality is growing rapidly. Built on proven technologies such as PHP and MySQL. KAN CMS is fast, lightweight and incredibly powerful</p>', NULL, 1),
(4, 1, 2, 5, 2, 'Contact Us', 'contact-us', NULL, NULL, '/about-cms/contact-us', 'sitepage', '', NULL, NULL, 1),
(5, 1, 2, 4, 2, 'How To Contribute', 'how-to-contribute', NULL, NULL, '/about-cms/how-to-contribute', 'sitepage', 'html', '<p>How to contribute</p>', NULL, 1),
(6, 1, 2, 2, 2, 'System Requirements', 'system-requirements', NULL, NULL, '/about-cms/system-requirements', 'sitepage', 'html', '<p>This Content Management System was developed using the following technologies and will run on any web server that supports and runs them:</p>\r\n<ul>\r\n<li>PHP Version 5.x</li>\r\n<li>MySQL 5.1.X</li>\r\n</ul>\r\n<p>The disk space requirements for a basic CMS installation is averagely about 4MB.</p>', NULL, 1),
(7, 1, 2, 3, 2, 'Sites Powered By KAN CMS', 'sites-powered-by-kan-cms', NULL, NULL, '/about-cms/sites-powered-by-kan-cms', 'sitepage', '', NULL, '../pages/index.php?page=gallery', 1),
(8, 1, NULL, 3, 1, 'Themes / Templates', 'themes-templates', NULL, NULL, '/themes-templates', 'sitepage', 'html', NULL, NULL, 1),
(9, 1, 8, 1, 2, 'Theme Management', 'theme-management', 'How To Build A KAN Theme', NULL, '/themes-templates/theme-management', 'sitepage', 'html', '<p>With KAN it is simple to start off the task of developing a new Theme. Simply to go to the <a href="../pages/index.php?goto=../cms/themes.php" target="_blank">Theme Manager</a> and follow the following steps:</p>\r\n<ol>\r\n<li>Choose to <strong>Create A New Theme</strong></li>\r\n<li>Fill the form provided to create the theme by providing the Name for the theme and a general description of the theme. These may be edited later so you may not need to be overlay specific the first time round.</li>\r\n<li>You may leave out the Theme Image (or snapshot) until you have completed the theme and can take a snapshot</li>\r\n<li>Complete the Theme Folder template to specify the name to be used for the theme folder on your server. i.e. replace the <strong></strong> section in the Theme Folder field with the desired folder name</li>\r\n<li>Click the Create Theme button to complete the process.&nbsp;</li>\r\n</ol>', NULL, 1),
(10, 1, 8, 2, 2, 'Tips on Handling Content', 'tips-on-handling-content', NULL, NULL, '/themes-templates/tips-on-handling-content', 'sitepage', 'html', '<p>With KAN how you handle content is largely determined by the type of theme chosen. Each theme can be designed to handle content in the way that best suits the design. These include accordion interfaces, AJAX content loaders, etc. The articles in this section will give some simple examples or how to handle content in KAN</p>', NULL, 1),
(11, 1, NULL, 4, 1, 'News & Events', 'news-events', NULL, NULL, '/news-events', 'sitepage', 'html', NULL, NULL, 1),
(12, 1, 11, 0, 2, 'Latest News', 'latest-news', NULL, NULL, '/news-events/latest-news', 'sitepage', 'html', NULL, '../pages/articles.php', 1),
(13, 1, 11, 0, 2, 'Upcoming Events', 'upcoming-events', NULL, NULL, '/news-events/upcoming-events', 'sitepage', 'html', NULL, '../pages/events.php', 1);


INSERT INTO `banner_images` (`id`, `SiteID`, `BannerName`, `BannerDesc`, `BannerPath`, `BannerType`, `BannerURL`, `Width`, `Height`) VALUES
(1, 1, 'KAN CMS Banner', 'KAN CMS Banner', '../assets/images/banners/banner056.png', 'png', NULL, 940, 225);

--
-- Dumping data for table `download_categories`
--

INSERT INTO `download_categories` (`id`, `SiteID`, `CategoryName`, `CategoryAlias`,`CategoryImage`) VALUES
(1, 1, 'General', 'general', NULL);

--
-- Dumping data for table `event_categories`
--

INSERT INTO `event_categories` (`id`, `SiteID`, `Category`, `CategoryAlias`, `CategoryDescription`) VALUES
(1, 1, 'General', 'general', NULL);

--
-- Dumping data for table `events`
--

INSERT INTO `events` (`id`, `EventCategory`, `EventName`, `EventDesc`, `AssocImage`, `Time`, `EventDuration`, `Date`, `StartDate`, `EndDate`, `Venue`, `RedirectURL`, `Published`) VALUES
(1, 1, 'KAN CMS Beta 1 Coming Soon', '<p>KAN CMS Beta 1, will be released June 1st.</p>\r\n<p>KAN CMS Beta 2, will be released June 16th</p>\r\n<p>KAN CMS 1.0 Final will be released July 1st</p>', NULL, '7:00pm', 'single', '2010-07-01', '2010-07-01', '2010-05-30', 'KAN CMS Project Site', NULL, 'true');



--
-- Dumping data for table `feedback_categories`
--

INSERT INTO `feedback_categories` (`id`, `SiteID`, `CategoryName`, `CategoryTag`, `CategoryDesc`, `FwdToEmail`, `FwdEmail`, `DateCreated`) VALUES
(1, 1, 'General', 'general', 'General Feedback', 'false', NULL, '2010-03-04');
--
-- Dumping data for table `images`
--

INSERT INTO `images` (`id`, `SiteID`, `GalleryID`, `UseSection`, `PageID`, `ImageName`, `ImageDescription`, `ImageFile`, `ThumbPath`) VALUES
(1, 1, '', 'site', NULL, 'Uploaded Image 3', '', '../assets/images/pics/img5112009_1317.gif', '../assets/images/pics/img5112009_1317.gif'),
(2, 1, '', 'site', NULL, 'Uploaded Image 4', '', '../assets/images/pics/img5112009_1319.jpg', '../assets/images/pics/img5112009_1319.jpg'),
(3, 1, '', 'themes', NULL, 'Uploaded Image 5', '', '../assets/images/content/pics/img7112009_207.jpg', '../assets/images/content/thumbs/img7112009_207.jpg'),

(4, NULL, '', 'cms', NULL, 'KAN Logo', 'KAN Logo', '../assets/images/content/pics/img2010426_14818.png', '../assets/images/content/thumbs/img2010426_14818.png'),
(5, 1, '', 'cms', NULL, 'KAN Logo', 'KAN Logo', '../assets/images/content/pics/img2010426_14840.png', '../assets/images/content/thumbs/img2010426_14840.png'),
(6, 1, '', 'news', NULL, 'KAN Logo', 'KAN Logo', '../assets/images/news/pics/img2010426_181416.png', '../assets/images/news/thumbs/img2010426_181416.png'),
(7, NULL, 'kansites', NULL, NULL, 'NUPS-G KNUST', 'NUPS-G KNUST Powered By KAN', '../assets/images/gallery/kansites/photo2010426_19233.jpg', '../assets/images/gallery/kansites/thumbs/photo2010426_19233.jpg'),
(8, 1, '', 'themes', NULL, 'Uploaded Image 5', '', '../assets/images/content/pics/img2842010_172753.jpg', '../assets/images/content/thumbs/img2842010_172753.jpg'),
(9, 1, '', 'themes', NULL, 'Uploaded Image 6', '', '../assets/images/content/pics/img2842010_173021.jpg', '../assets/images/content/thumbs/img2842010_173021.jpg');


--
-- Dumping data for table `quick_links_categories`
--

INSERT INTO `quick_links_categories` (`id`, `SiteID`, `CategoryName`, `CategoryDescription`) VALUES
(1, 1, 'Online Links', NULL),
(2, 1, 'Who Uses KAN?', 'Sites That Use KAN'),
(3, 1, 'Docs and Samples', 'Documentation and Sample Sites');

--
-- Dumping data for table `quick_links`
--

INSERT INTO `quick_links` (`id`, `LinkCategory`, `LinkName`, `LinkDesc`, `LinkURL`, `Published`) VALUES
(1, 1, 'KAN CMS Home Page', NULL, 'http://www.kancms.org/', 'true'),
(2, 1, 'KAN Project Site', NULL, 'http://code.google.com/p/kancms/', 'true'),
(3, 1, 'KAN Developer Blog', NULL, 'http://icewalker2g.wordpress.com', 'true'),
(4, 2, 'Kwame Nkrumah University of Science and Technology', NULL, 'http://www.knust.edu.gh', 'true'),
(5, 2, 'KAN CMS Official Site', NULL, 'http://www.kancms.org/', 'true'),
(6, 2, 'EduSOFT Ghana', NULL, 'http://www.edusoftgh.com', 'true'),
(7, 3, 'KAN Blog', NULL, 'http://icewalker2g.wordpress.com', 'false');


--
-- Dumping data for table `spotlights`
--

INSERT INTO `spotlights` (`id`, `SiteID`, `menu_header`, `menu_subheader`, `spotlight`, `spotlight_url`, `description`, `date`, `approved`) VALUES
(1, 1, 'KAN CMS', 'KAN: An Open Source CMS', '../assets/images/spotlight/spotlight24.png', NULL, '', '2010-05-28', 'true');


--
-- Dumping data for table `sites`
--
UPDATE `sites` SET `SiteLogo` = '../assets/images/content/pics/img2010426_14840.png',`SiteHomePageText` = '<h2>Welcome To KAN CMS</h2>\r\n<p>This is the KAN CMS Introductory Site, designed to provide a sample of general best practices with KAN with respect to how to create and organise content in your own site using KAN.</p>\r\n<p>The content provided in this demo is basically a collection of articles found on the KANCMS.org website and a few other areas. By going through this sample site, you will become familiar with most the KAN Manage Interface and the default tools provided by the system.</p>\r\n<p>When you are ready to build your own site, simply <strong>Create A New Site</strong>&nbsp;in the <a title="Content Management Section" href="../pages/index.php?goto=../cms/index.php" target="_blank">CMS</a> and set is as the default site. Then go over to the themes tutorial to learn how to build you own KAN Theme from scratch.</p>',`SiteFooterText` = '<p><a href="http://www.kancms.org" target="_blank">KAN CMS</a> is free software released under the <a href="http://www.gnu.org/licenses/gpl-2.0.html" target="_blank">GNU General Public License</a>. <a href="../pages/index.php?goto=../cms/index.php" target="_blank">Click Here</a> To Go To Content Manager Interface</p>',`SiteFavicon` = '../favicon.ico',`SiteThemeID` = 1, `SiteThemeCSSID` = 1 WHERE  `sites`.`id` = 1;

SET FOREIGN_KEY_CHECKS=1;